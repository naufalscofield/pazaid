-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2020 at 02:50 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `s_2019_gab`
--

-- --------------------------------------------------------

--
-- Table structure for table `ak_daycare`
--

CREATE TABLE `ak_daycare` (
  `id` int(11) NOT NULL,
  `kode_transaksi` varchar(10) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `nama_orang_tua` varchar(50) NOT NULL,
  `tempat_lahir` varchar(15) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `umur` int(11) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `jenis_daycare` enum('fullday','halfday') NOT NULL,
  `total_tagihan` int(11) NOT NULL,
  `jenis_pembayaran` enum('cash','transfer') DEFAULT NULL,
  `no_rek` varchar(10) DEFAULT NULL,
  `status` enum('Sudah Bayar','Belum Bayar') NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ak_daycare`
--

INSERT INTO `ak_daycare` (`id`, `kode_transaksi`, `nama_lengkap`, `nama_orang_tua`, `tempat_lahir`, `tanggal_lahir`, `umur`, `no_telp`, `jenis_daycare`, `total_tagihan`, `jenis_pembayaran`, `no_rek`, `status`, `created_at`) VALUES
(5, 'N6F47TK1', 'Zaid', 'Bambang', 'Klaten', '2020-07-03', 0, '081388698975', 'fullday', 125000, 'cash', NULL, 'Sudah Bayar', '2020-07-03');

-- --------------------------------------------------------

--
-- Table structure for table `ak_kelas`
--

CREATE TABLE `ak_kelas` (
  `id_kelas` int(11) NOT NULL,
  `kode_kelas` varchar(255) NOT NULL,
  `nama_kelas` varchar(255) NOT NULL,
  `level_kelas` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL COMMENT 'fullday/halfday'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ak_kelas`
--

INSERT INTO `ak_kelas` (`id_kelas`, `kode_kelas`, `nama_kelas`, `level_kelas`, `status`) VALUES
(1, 'BB-0001', 'Matahari', 'Bayi', 'Fullday'),
(2, 'PG-0001', 'Bulan', 'Playgroup', 'Fullday'),
(3, 'TK-0001', 'Bumi', 'TK', 'Fullday'),
(5, 'BB-0002', 'Pohon', 'Bayi', 'Fullday');

-- --------------------------------------------------------

--
-- Table structure for table `ak_siswa`
--

CREATE TABLE `ak_siswa` (
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_tahun` int(11) NOT NULL,
  `kode_pendaftaran` varchar(255) NOT NULL,
  `nis` varchar(255) NOT NULL,
  `nama_siswa` varchar(255) NOT NULL,
  `panggilan_siswa` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `agama` varchar(255) NOT NULL,
  `jumlah_saudara` varchar(255) NOT NULL,
  `anak_ke` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `ot_bpk_nama_lengkap` varchar(255) DEFAULT NULL,
  `ot_bpk_tempat_lahir` varchar(255) DEFAULT NULL,
  `ot_bpk_tanggal_lahir` date DEFAULT NULL,
  `ot_bpk_no_telp` varchar(255) DEFAULT NULL,
  `ot_bpk_agama` varchar(255) DEFAULT NULL,
  `ot_bpk_pekerjaan` varchar(255) DEFAULT NULL,
  `ot_bpk_pendidikan` varchar(255) DEFAULT NULL,
  `ot_ib_nama_lengkap` varchar(255) DEFAULT NULL,
  `ot_ib_tempat_lahir` varchar(255) DEFAULT NULL,
  `ot_ib_tanggal_lahir` date DEFAULT NULL,
  `ot_ib_no_telp` varchar(255) DEFAULT NULL,
  `ot_ib_agama` varchar(255) DEFAULT NULL,
  `ot_ib_pekerjaan` varchar(255) DEFAULT NULL,
  `ot_ib_pendidikan` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL COMMENT 'anak guru/bukan',
  `status_bayar` varchar(255) NOT NULL COMMENT 'bayar/belum',
  `status_siswa` varchar(255) NOT NULL COMMENT 'undur/lanjut',
  `radio` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ak_siswa`
--

INSERT INTO `ak_siswa` (`id_siswa`, `id_kelas`, `id_tahun`, `kode_pendaftaran`, `nis`, `nama_siswa`, `panggilan_siswa`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `agama`, `jumlah_saudara`, `anak_ke`, `alamat`, `ot_bpk_nama_lengkap`, `ot_bpk_tempat_lahir`, `ot_bpk_tanggal_lahir`, `ot_bpk_no_telp`, `ot_bpk_agama`, `ot_bpk_pekerjaan`, `ot_bpk_pendidikan`, `ot_ib_nama_lengkap`, `ot_ib_tempat_lahir`, `ot_ib_tanggal_lahir`, `ot_ib_no_telp`, `ot_ib_agama`, `ot_ib_pekerjaan`, `ot_ib_pendidikan`, `status`, `status_bayar`, `status_siswa`, `radio`) VALUES
(19, 0, 15, '', '2021060001', 'Zaid Muhammad Rasid Safari', 'Zaid', 'Klaten', '2015-07-03', 'Laki - Laki', 'Islam', '5', '1', 'Curug', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Bukan Anak Guru', 'Belum Lunas', '', '3'),
(20, 1, 15, 'V13NT6BZ', '2021060002', 'Muthia Azka', 'Aka', 'Depok', '2015-07-03', 'Laki - Laki', 'Islam', '3', '1', 'PBB 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Anak Guru', 'Belum Lunas', '', '3');

-- --------------------------------------------------------

--
-- Table structure for table `ak_tahun_ajaran`
--

CREATE TABLE `ak_tahun_ajaran` (
  `id_tahun` int(11) NOT NULL,
  `kode_tahun` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `ended_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ak_tahun_ajaran`
--

INSERT INTO `ak_tahun_ajaran` (`id_tahun`, `kode_tahun`, `status`, `created_at`, `ended_at`) VALUES
(14, 'TAH-20-0001', '2', '2020-06-10', '2021-03-10'),
(15, 'TAH-20-0002', '1', '2021-06-10', '2022-03-10');

-- --------------------------------------------------------

--
-- Table structure for table `biaya_spp`
--

CREATE TABLE `biaya_spp` (
  `id` int(11) NOT NULL,
  `level_kelas` varchar(15) NOT NULL,
  `jenis_kelas` enum('Fullday','Halfday') NOT NULL,
  `biaya` int(11) NOT NULL,
  `keterangan` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `biaya_spp`
--

INSERT INTO `biaya_spp` (`id`, `level_kelas`, `jenis_kelas`, `biaya`, `keterangan`) VALUES
(1, 'Bayi', 'Fullday', 1250000, 'Bisa Dicicil'),
(2, 'Playgroup', 'Fullday', 1000000, 'Bisa Dicicil'),
(3, 'TK', 'Fullday', 1250000, 'Bisa Dicicil'),
(4, 'Bayi', 'Halfday', 850000, 'Bisa Dicicil'),
(5, 'Playgroup', 'Halfday', 600000, 'Bisa Dicicil'),
(6, 'TK', 'Halfday', 650000, 'Bisa Dicicil');

-- --------------------------------------------------------

--
-- Table structure for table `biaya_umum`
--

CREATE TABLE `biaya_umum` (
  `id` int(11) NOT NULL,
  `nama_biaya` varchar(25) NOT NULL,
  `biaya` int(11) NOT NULL,
  `keterangan` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `biaya_umum`
--

INSERT INTO `biaya_umum` (`id`, `nama_biaya`, `biaya`, `keterangan`) VALUES
(1, 'Biaya Pendaftaran', 250000, 'Tidak Bisa Dici'),
(2, 'Seragam Laki-Laki', 450000, 'Tidak Bisa Dici'),
(3, 'Seragam Perempuan', 500000, 'Tidak Bisa Dici'),
(4, 'DPP (Pembangunan)', 3600000, 'Bisa Dicicil'),
(5, 'DSK (Semester)', 4400000, 'Bisa Dicicil');

-- --------------------------------------------------------

--
-- Table structure for table `coa`
--

CREATE TABLE `coa` (
  `id` int(10) NOT NULL,
  `kode_coa` varchar(10) DEFAULT NULL,
  `nama_coa` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coa`
--

INSERT INTO `coa` (`id`, `kode_coa`, `nama_coa`) VALUES
(1, '111', 'Kas'),
(2, '112', 'Aktiva Tetap'),
(3, '113', 'Bank'),
(4, '211', 'Utang'),
(6, '511', 'Perbaikan'),
(7, '512', 'Pemeliharaan'),
(8, '411', 'Pendapatan Hibah'),
(9, '11321', 'Piutang Pegawai'),
(10, '513', 'Beban Gaji'),
(11, '212', 'Utang Pph'),
(12, '514', 'Beban Pajak'),
(13, '412', 'Pendapatan BOP'),
(14, '515', 'Beban BOP'),
(15, '516', 'Beban Lembur'),
(16, '517', 'Beban Overtime Untuk Pegawai'),
(17, '51108', 'Tunjangan Makan Pegawai'),
(18, '51105', 'Tunjangan Transport'),
(19, '51107', 'Tunjangan Jabatan'),
(20, '51106', 'Tunjangan Kesehatan'),
(21, '522', 'Beban Penyusutan'),
(22, '213', 'Akumulasi Penyusutasan'),
(23, '312', 'Prive'),
(24, '313', 'Modal'),
(25, '114', 'Kas Kecil'),
(26, '115', 'Kas Institusi'),
(27, '267', 'Bayar Dimuka'),
(28, '121', 'Aktiva Dalam Proses'),
(29, '11314', 'Piutang Seragam'),
(30, '41104', 'Pendapatan Seragam'),
(31, '11312', 'Piutang DPP'),
(32, '21152', 'Pendapatan Yang Ditangguhkan DPP'),
(33, '11313', 'Piutang DSK'),
(34, '21153', 'Pendapatan Yang Ditangguhkan DSK'),
(35, '1131', 'Piutang SPP'),
(36, '41101', 'Pendapatan SPP'),
(38, '11201', 'Bank Mandiri'),
(39, '11202', 'Bank Jabar'),
(40, '11203', 'Bank Rakyat Indonesia Syariah'),
(41, '11103', 'Panjar'),
(43, '11315', 'Piutang Siswa Overtime'),
(44, '11316', 'Piutang Siswa Catering'),
(45, '11317', 'Piutang Siswa Sarapan'),
(46, '11318', 'Piutang Daycare'),
(48, '11324', 'Piutang Mandi'),
(49, '11401', 'Perlengkapan Sekolah'),
(50, '11402', 'Perlengkapan Kantor'),
(51, '12101', 'Tanah'),
(52, '12201', 'Bangunan'),
(53, '12202', 'Akumulasi Penyesuaian Bangunan'),
(54, '12301', 'Peralatan Sekolah'),
(55, '12302', 'Akumulasi Penyesuaian Peralatan Sekolah'),
(56, '12401', 'Peralatan Kantor'),
(57, '12402', 'Akumulasi Penyesuaian Peralatan Kantor'),
(58, '12501', 'Kendaraan'),
(59, '12502', 'Akumulasi Penyesuaian Kendaraan'),
(60, '31101', 'Modal Suryatiningsih Ocha'),
(61, '31102', 'Prive Suryatiningsih Ocha'),
(62, '31103', 'Modal Fitri Susanti'),
(63, '31104', 'Prive Fitri Susanti'),
(64, '33101', 'Laba Ditahan Tahun Berjalan'),
(65, '33102', 'Laba Ditahan Tahun Sebelumnya'),
(66, '41102', 'Pendapatan DPP'),
(67, '41103', 'Pendapatan DSK'),
(68, '41105', 'Pendapatan Overtime'),
(69, '41106', 'Pendapatan Catering'),
(70, '41107', 'Pendapatan Sarapan'),
(71, '41108', 'Pendapatan Daycare'),
(72, '41111', 'Pendapatan Mandi'),
(73, '41201', 'Pendapatan Donasi Pemerintah'),
(74, '41202', 'Pendapatan Zonasi Non Pemerintah'),
(75, '41299', 'Pendapatan Donasi Lainnya'),
(76, '51101', 'Beban Gaji Pegawai'),
(77, '51102', 'Beban Lembur Pegawai'),
(78, '51103', 'Beban Overtime Pegawai'),
(79, '51199', 'Beban SDM Gaji Lainnya'),
(80, '51203', 'Beban Pelatihan Pegawai'),
(81, '51204', 'Beban Perjalanan Dinas'),
(82, '51299', 'Beban Refreshing Pegawai'),
(83, '51301', 'Beban Kegiatan Pengajaran'),
(84, '51302', 'Beban Pengajaran Habis Pakai'),
(85, '51401', 'Beban Pemeliharaan dan Perbaikan Bangunan'),
(86, '51402', 'Beban Pemeliharaan dan Perbaikan Peralatan Sekolah'),
(87, '51403', 'Beban Pemeliharaan dan Perbaikan Peralatan Kantor'),
(88, '51404', 'Beban Pemeliharaan dan Perbaikan Peralatan Kendaraan'),
(89, '51501', 'Beban Keperluan Operasional Rumah Tangga'),
(90, '51502', 'Beban Alat Tulis Kantor'),
(91, '51503', 'Beban Rapat'),
(92, '51599', 'Beban Umum(Operasional Harian) Lainnya'),
(93, '51601', 'Beban Penyusutan Bangunan'),
(94, '51602', 'Beban Penyusutan Peralatan Sekolah'),
(95, '51603', 'Beban Penyusutan Kendaraan'),
(96, '51605', 'Beban Catering'),
(97, '51701', 'Beban Acara Parenting'),
(98, '42204', 'Deposito Bank BRI Suryatiningsih Ocha'),
(99, '42205', 'Deposito Bank BRI Fitri Susanti'),
(100, '52401', 'Potongan PPH21'),
(104, '41110', 'Pendapatan Lainnya'),
(105, '41301', 'Pendapatan Dana Bantuan Operasional Pemerintah'),
(106, '42102', 'Pendapatan Pengelolaan Lain'),
(107, '42201', 'Pendapatan Bunga Tabungan'),
(108, '42202', 'Pendapatan Bunga Deposito'),
(109, '41109', 'Pendapatan Pendaftaran'),
(110, '11320', 'Piutang Pendaftaran'),
(111, '31105', 'Modal Almalia');

-- --------------------------------------------------------

--
-- Table structure for table `fdl_absensi`
--

CREATE TABLE `fdl_absensi` (
  `id` int(10) NOT NULL,
  `pegawai_id` int(10) DEFAULT NULL,
  `waktu_masuk` datetime DEFAULT NULL,
  `waktu_keluar` datetime DEFAULT NULL,
  `status` enum('hadir','izin','dinas','sakit','terlambat','cuti') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fdl_absensi`
--

INSERT INTO `fdl_absensi` (`id`, `pegawai_id`, `waktu_masuk`, `waktu_keluar`, `status`) VALUES
(21, 10, '2020-03-02 06:18:00', '2020-03-02 18:18:00', 'hadir'),
(22, 10, '2020-03-05 06:18:00', '2020-03-05 18:18:00', 'hadir'),
(23, 10, '2020-03-11 06:18:00', '2020-03-11 18:18:00', 'hadir'),
(24, 10, '2020-03-10 06:18:00', '2020-03-10 18:18:00', 'hadir'),
(25, 10, '2020-03-09 06:18:00', '2020-03-09 18:18:00', 'hadir'),
(26, 10, '2020-03-06 06:18:00', '2020-03-06 18:18:00', 'hadir'),
(27, 10, '2020-02-03 06:18:00', '2020-02-03 18:18:00', 'hadir'),
(28, 10, '2020-02-06 06:18:00', '2020-02-06 18:18:00', 'hadir'),
(29, 10, '2020-02-07 06:18:00', '2020-02-07 18:18:00', 'hadir'),
(30, 10, '2020-02-12 06:18:00', '2020-02-12 18:18:00', 'hadir'),
(31, 10, '2020-02-13 06:18:00', '2020-02-13 18:18:00', 'hadir'),
(32, 10, '2020-02-11 06:18:00', '2020-02-11 18:18:00', 'hadir'),
(33, 10, '2020-02-10 06:18:00', '2020-02-10 18:18:00', 'hadir'),
(34, 10, '2020-02-14 06:18:00', '2020-02-14 18:18:00', 'hadir'),
(35, 10, '2020-02-27 00:00:00', '2020-02-27 00:00:00', 'dinas'),
(36, 10, '2020-02-28 00:00:00', '2020-02-28 00:00:00', 'dinas'),
(37, 10, '2020-03-13 06:50:00', '2020-03-13 18:00:00', 'hadir'),
(38, 10, '2020-03-03 07:55:00', '2020-03-03 19:00:00', 'terlambat'),
(39, 10, '2020-03-12 06:50:00', '2020-03-12 18:00:00', 'hadir'),
(40, 10, '2020-03-04 06:00:00', '2020-03-04 18:00:00', 'hadir'),
(41, 10, '2020-03-16 06:00:00', '2020-03-16 18:00:00', 'hadir'),
(42, 10, '2020-03-18 06:00:00', '2020-03-18 18:00:00', 'hadir'),
(43, 10, '2020-03-17 06:00:00', '2020-03-17 18:00:00', 'hadir'),
(44, 10, '2020-03-20 06:00:00', '2020-03-20 18:00:00', 'hadir'),
(45, 10, '2020-02-05 06:00:00', '2020-02-05 18:00:00', 'hadir'),
(46, 10, '2020-02-04 06:00:00', '2020-02-04 18:00:00', 'hadir'),
(47, 10, '2020-02-17 06:00:00', '2020-02-17 18:00:00', 'hadir'),
(48, 10, '2020-02-19 06:00:00', '2020-02-19 18:00:00', 'hadir'),
(49, 10, '2020-02-18 06:00:00', '2020-02-18 18:00:00', 'hadir'),
(50, 10, '2020-02-26 06:00:00', '2020-02-26 18:00:00', 'hadir'),
(51, 10, '2020-02-25 06:00:00', '2020-02-25 18:00:00', 'hadir'),
(52, 10, '2020-02-24 06:00:00', '2020-02-24 18:00:00', 'hadir'),
(53, 10, '2020-04-01 07:00:00', '2020-04-01 18:00:00', 'hadir'),
(54, 10, '2020-04-07 07:30:00', '2020-04-07 18:30:00', 'hadir'),
(55, 10, '2020-04-09 06:30:00', '2020-04-09 18:30:00', 'hadir'),
(56, 10, '2020-06-05 05:50:00', '2020-06-05 18:30:00', 'hadir');

-- --------------------------------------------------------

--
-- Table structure for table `fdl_gaji`
--

CREATE TABLE `fdl_gaji` (
  `id` int(10) NOT NULL,
  `bulan` enum('1','2','3','4','5','6','7','8','9','10','11','12') DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL,
  `waktu_generate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fdl_gaji`
--

INSERT INTO `fdl_gaji` (`id`, `bulan`, `tahun`, `waktu_generate`) VALUES
(46, '2', 2020, '2020-06-19 07:13:46');

-- --------------------------------------------------------

--
-- Table structure for table `fdl_gaji_detail`
--

CREATE TABLE `fdl_gaji_detail` (
  `id` int(10) NOT NULL,
  `gaji_id` int(10) DEFAULT NULL,
  `pegawai_id` int(10) DEFAULT NULL,
  `total_masuk` int(10) DEFAULT 0,
  `total_hadir` int(10) DEFAULT 0,
  `total_terlambat` int(10) DEFAULT 0,
  `maksimal_kehadiran` int(10) DEFAULT 0,
  `gaji_pokok` int(50) DEFAULT 0,
  `gaji_kehadiran` int(50) DEFAULT 0,
  `gaji_keterlambatan` int(50) DEFAULT 0,
  `tunjangan_makan` int(50) DEFAULT 0,
  `tunjangan_lembur` int(50) DEFAULT 0,
  `tunjangan_lain` int(50) DEFAULT 0,
  `tunjangan_komponen` text DEFAULT NULL,
  `hutang` int(50) DEFAULT 0,
  `pph` int(50) DEFAULT 0,
  `total_gaji` int(50) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fdl_gaji_detail`
--

INSERT INTO `fdl_gaji_detail` (`id`, `gaji_id`, `pegawai_id`, `total_masuk`, `total_hadir`, `total_terlambat`, `maksimal_kehadiran`, `gaji_pokok`, `gaji_kehadiran`, `gaji_keterlambatan`, `tunjangan_makan`, `tunjangan_lembur`, `tunjangan_lain`, `tunjangan_komponen`, `hutang`, `pph`, `total_gaji`) VALUES
(116, 46, 10, 18, 18, 0, 20, 10800000, 10800000, 0, 0, 65000, 865000, '[{\"nominal\":450000,\"name\":\"Transportasi\"},{\"nominal\":270000,\"name\":\"Makan\"},{\"nominal\":125000,\"name\":\"Kesehatan\"},{\"nominal\":\"20000\",\"name\":\"Nyuapin Makan\"}]', NULL, 555333, 11174667),
(117, 46, 12, 0, 0, 0, 20, 0, 0, 0, 0, 0, 125000, '[{\"nominal\":0,\"name\":\"Transportasi\"},{\"nominal\":0,\"name\":\"Makan\"},{\"nominal\":125000,\"name\":\"Kesehatan\"}]', NULL, 0, 125000),
(118, 46, 13, 0, 0, 0, 20, 0, 0, 0, 0, 0, 125000, '[{\"nominal\":0,\"name\":\"Transportasi\"},{\"nominal\":0,\"name\":\"Makan\"},{\"nominal\":125000,\"name\":\"Kesehatan\"}]', NULL, 0, 125000);

-- --------------------------------------------------------

--
-- Table structure for table `fdl_gaji_tunjangan`
--

CREATE TABLE `fdl_gaji_tunjangan` (
  `id` int(10) NOT NULL,
  `pegawai_id` int(10) DEFAULT NULL,
  `tunjangan_id` int(10) DEFAULT NULL,
  `waktu_tunjangan` date DEFAULT NULL,
  `jumlah` int(50) DEFAULT NULL,
  `total_nominal` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fdl_gaji_tunjangan`
--

INSERT INTO `fdl_gaji_tunjangan` (`id`, `pegawai_id`, `tunjangan_id`, `waktu_tunjangan`, `jumlah`, `total_nominal`) VALUES
(1, 10, 6, '2020-03-11', 3, 30000),
(2, 10, 6, '2020-02-05', 2, 20000);

-- --------------------------------------------------------

--
-- Table structure for table `fdl_izin`
--

CREATE TABLE `fdl_izin` (
  `id` int(10) NOT NULL,
  `pegawai_id` int(10) DEFAULT NULL,
  `tipe` enum('Izin','Sakit','Dinas','Cuti') DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status` enum('pending','approve','deny') DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fdl_izin`
--

INSERT INTO `fdl_izin` (`id`, `pegawai_id`, `tipe`, `tanggal_mulai`, `tanggal_selesai`, `keterangan`, `status`) VALUES
(33, 10, 'Izin', '2019-11-26', '2019-11-28', NULL, 'approve'),
(34, 10, 'Dinas', '2020-02-27', '2020-02-28', 'Pasang Blower di Balikpapan', 'approve');

-- --------------------------------------------------------

--
-- Table structure for table `fdl_jabatan`
--

CREATE TABLE `fdl_jabatan` (
  `id` int(10) NOT NULL,
  `kode_jabatan` varchar(50) DEFAULT NULL,
  `nama_jabatan` varchar(200) DEFAULT NULL,
  `gaji` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fdl_jabatan`
--

INSERT INTO `fdl_jabatan` (`id`, `kode_jabatan`, `nama_jabatan`, `gaji`) VALUES
(2, 'JBT-0001', 'Guru', 7500000),
(3, 'JBT-0002', 'Kepala Sekolah', 12000000);

-- --------------------------------------------------------

--
-- Table structure for table `fdl_lembur`
--

CREATE TABLE `fdl_lembur` (
  `id` int(10) NOT NULL,
  `pegawai_id` int(10) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `total_jam` float DEFAULT NULL,
  `nominal_lembur` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fdl_lembur`
--

INSERT INTO `fdl_lembur` (`id`, `pegawai_id`, `tanggal`, `total_jam`, `nominal_lembur`) VALUES
(12, 10, '2019-11-17', 1, 25000),
(16, 10, '2020-02-05', 0.6, 10000),
(17, 10, '2020-02-21', 2, 55000);

-- --------------------------------------------------------

--
-- Table structure for table `fdl_pegawai`
--

CREATE TABLE `fdl_pegawai` (
  `id` int(10) NOT NULL,
  `jabatan_id` int(10) DEFAULT NULL,
  `rfid` varchar(200) DEFAULT NULL,
  `kode_pegawai` varchar(100) DEFAULT NULL,
  `nama_pegawai` varchar(100) DEFAULT NULL,
  `alamat` varchar(250) DEFAULT NULL,
  `jenis_kelamin` enum('Laki - Laki','Perempuan') DEFAULT NULL,
  `no_telp` varchar(13) DEFAULT NULL,
  `is_nikah` enum('0','1') DEFAULT NULL,
  `is_anak` enum('0','1') DEFAULT '0',
  `jml_anak` int(10) DEFAULT 0,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `piutang_pinjaman` int(50) DEFAULT 0,
  `jenis` enum('Tetap','Kontrak') DEFAULT NULL,
  `tanggal_mulai_kerja` date DEFAULT NULL,
  `pendidikan_terakhir` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fdl_pegawai`
--

INSERT INTO `fdl_pegawai` (`id`, `jabatan_id`, `rfid`, `kode_pegawai`, `nama_pegawai`, `alamat`, `jenis_kelamin`, `no_telp`, `is_nikah`, `is_anak`, `jml_anak`, `username`, `password`, `piutang_pinjaman`, `jenis`, `tanggal_mulai_kerja`, `pendidikan_terakhir`, `tempat_lahir`, `tanggal_lahir`) VALUES
(10, 3, '123456789', 'PGW-0002', 'Wader Jhonson', 'asd', 'Laki - Laki', '123123', '1', '0', 1, 'user01', 'user01', 0, 'Tetap', '2017-03-01', 'S1', 'Jakarta', '2020-03-02'),
(12, 2, '12345', 'PGW-0003', 'Bellatrix Lestrange', 'Gang PGA', 'Perempuan', '0895373483105', '0', '0', 0, 'user02', 'user02', 0, 'Kontrak', '2020-03-09', 'SMA', 'Padang', '2020-03-03'),
(13, 2, '98765', 'PGW-0004', 'Albus Dumbledore', 'Hogwart', 'Laki - Laki', '083181826488', '1', '0', 2, 'user03', 'user03', 0, 'Tetap', '2020-01-02', 'S2', 'Padang', '2020-03-12');

-- --------------------------------------------------------

--
-- Table structure for table `fdl_tunjangan`
--

CREATE TABLE `fdl_tunjangan` (
  `id` int(10) NOT NULL,
  `coa_id` int(10) DEFAULT NULL,
  `nama_tunjangan` varchar(200) DEFAULT NULL,
  `jml` double DEFAULT NULL,
  `nominal` int(50) DEFAULT NULL,
  `tipe` enum('lembur','nikah','anak','harian','bulanan','per_orang') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fdl_tunjangan`
--

INSERT INTO `fdl_tunjangan` (`id`, `coa_id`, `nama_tunjangan`, `jml`, `nominal`, `tipe`) VALUES
(1, NULL, NULL, 1, 25000, 'lembur'),
(2, NULL, NULL, 2, 55000, 'lembur'),
(3, NULL, NULL, 3, 125000, 'lembur'),
(6, 17, 'Nyuapin Makan', NULL, 10000, 'per_orang'),
(7, 18, 'Transportasi', NULL, 25000, 'harian'),
(9, NULL, NULL, 0.5, 10000, 'lembur'),
(10, 17, 'Makan', NULL, 15000, 'harian'),
(11, 20, 'Kesehatan', NULL, 125000, 'bulanan');

-- --------------------------------------------------------

--
-- Table structure for table `fdl_waktu`
--

CREATE TABLE `fdl_waktu` (
  `id` int(10) NOT NULL,
  `hari` enum('Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu') DEFAULT NULL,
  `jam_tiba_mulai` time DEFAULT NULL,
  `jam_tiba_terlambat` time DEFAULT NULL,
  `jam_tiba_selesai` time DEFAULT NULL,
  `jam_pulang_mulai` time DEFAULT NULL,
  `jam_pulang_selesai` time DEFAULT NULL,
  `is_aktif` enum('0','1') DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fdl_waktu`
--

INSERT INTO `fdl_waktu` (`id`, `hari`, `jam_tiba_mulai`, `jam_tiba_terlambat`, `jam_tiba_selesai`, `jam_pulang_mulai`, `jam_pulang_selesai`, `is_aktif`) VALUES
(1, 'Senin', '05:00:00', '07:30:00', '08:00:00', '17:00:00', '20:00:00', '1'),
(2, 'Selasa', '05:00:00', '07:30:00', '08:30:00', '17:00:00', '20:00:00', '1'),
(3, 'Rabu', '05:00:00', '07:30:00', '08:00:00', '17:00:00', '20:00:00', '1'),
(4, 'Kamis', '05:00:00', '07:30:00', '08:00:00', '17:00:00', '20:00:00', '1'),
(5, 'Jumat', '05:00:00', '07:30:00', '08:00:00', '17:00:00', '20:00:00', '1'),
(6, 'Sabtu', '05:00:00', '07:30:00', '08:00:00', '17:00:00', '20:00:00', '0'),
(7, 'Minggu', '05:00:00', '07:30:00', '08:00:00', '17:00:00', '20:00:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `hru_aset`
--

CREATE TABLE `hru_aset` (
  `id` int(10) NOT NULL,
  `kategori_id` int(10) DEFAULT NULL,
  `kode_aset` varchar(50) DEFAULT NULL,
  `nama_aset` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hru_aset`
--

INSERT INTO `hru_aset` (`id`, `kategori_id`, `kode_aset`, `nama_aset`) VALUES
(12, 9, 'AT-0001', 'Genset 2000'),
(13, 10, 'AT-0002', 'Meja Olympic'),
(16, 13, 'AT-0003', 'Sepeda');

-- --------------------------------------------------------

--
-- Table structure for table `hru_aset_detail`
--

CREATE TABLE `hru_aset_detail` (
  `id` int(11) NOT NULL,
  `transaksi_aset_id` int(11) DEFAULT NULL,
  `aset_id` int(11) DEFAULT NULL,
  `ruangan_id` int(11) DEFAULT NULL,
  `kode_detail_aset` varchar(50) DEFAULT NULL,
  `is_retur` enum('0','1') DEFAULT '0',
  `is_terima` enum('0','1') DEFAULT '0',
  `is_active` enum('0','1') DEFAULT '1',
  `tanggal_terima` date DEFAULT NULL,
  `tanggal_penempatan` date DEFAULT NULL,
  `is_fix` enum('0','1') DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hru_aset_detail`
--

INSERT INTO `hru_aset_detail` (`id`, `transaksi_aset_id`, `aset_id`, `ruangan_id`, `kode_detail_aset`, `is_retur`, `is_terima`, `is_active`, `tanggal_terima`, `tanggal_penempatan`, `is_fix`) VALUES
(118, 43, 12, 1, 'AT-0001-1', '0', '1', '1', '2020-06-19', '2020-06-19', '0'),
(119, 43, 12, 1, 'AT-0001-2', '0', '1', '1', '2020-06-19', '2020-06-19', '0'),
(120, 43, 12, 1, 'AT-0001-3', '0', '1', '1', '2020-06-19', '2020-06-19', '0'),
(121, 43, 12, NULL, 'AT-0001-4', '1', '0', '1', '2020-06-19', '2020-06-19', '0'),
(122, 43, 12, NULL, 'AT-0001-5', '1', '0', '1', '2020-06-19', '2020-06-19', '0'),
(123, 43, 12, NULL, 'AT-0001-6', '1', '0', '1', '2020-06-19', '2020-06-19', '0'),
(124, 43, 12, 1, 'AT-0001-7', '0', '1', '1', '2020-06-19', '2020-06-19', '0'),
(125, 43, 12, NULL, 'AT-0001-8', '1', '0', '1', '2020-06-19', '2020-06-19', '0'),
(126, 43, 12, 1, 'AT-0001-9', '0', '1', '1', '2020-06-19', '2020-06-19', '0'),
(127, 43, 12, 1, 'AT-0001-10', '0', '1', '1', '2020-06-19', '2020-06-19', '0'),
(128, 43, 12, NULL, 'AT-0001-11', '1', '0', '1', '2020-06-19', '2020-06-19', '0'),
(129, 43, 12, NULL, 'AT-0001-12', '0', '1', '1', '2020-06-19', '2020-06-19', '0'),
(130, 43, 12, NULL, 'AT-0001-13', '1', '0', '1', '2020-06-19', '2020-06-19', '0'),
(131, 43, 12, NULL, 'AT-0001-14', '0', '1', '1', '2020-06-19', '2020-06-19', '0'),
(132, 43, 12, NULL, 'AT-0001-15', '0', '1', '1', '2020-06-19', '2020-06-19', '0'),
(133, 43, 12, NULL, 'AT-0001-16', '0', '1', '1', '2020-06-19', '2020-06-19', '0'),
(134, 43, 12, NULL, 'AT-0001-17', '0', '1', '1', '2020-06-19', '2020-06-19', '0'),
(135, 43, 12, NULL, 'AT-0001-18', '1', '0', '1', '2020-06-19', '2020-06-19', '0'),
(136, 43, 12, NULL, 'AT-0001-19', '0', '1', '1', '2020-06-19', '2020-06-19', '0'),
(137, 43, 12, NULL, 'AT-0001-20', '1', '0', '1', '2020-06-19', '2020-06-19', '0'),
(138, 43, 12, NULL, 'AT-0001-21', '0', '1', '1', '2020-06-19', '2020-06-19', '0'),
(139, 43, 12, NULL, 'AT-0001-22', '1', '0', '1', '2020-06-19', '2020-06-19', '0'),
(140, 43, 12, NULL, 'AT-0001-23', '1', '0', '1', '2020-06-19', '2020-06-19', '0'),
(141, 43, 12, NULL, 'AT-0001-24', '0', '0', '1', '2020-06-19', '2020-06-19', '0'),
(143, 44, 13, 6, 'AT-0002-1', '0', '1', '1', '2020-06-20', '2020-06-20', '0'),
(144, 44, 13, 6, 'AT-0002-2', '0', '1', '1', '2020-06-20', '2020-06-20', '0'),
(145, 44, 13, 6, 'AT-0002-3', '0', '1', '1', '2020-06-20', '2020-06-20', '0'),
(146, 44, 13, 6, 'AT-0002-4', '0', '1', '1', '2020-06-20', '2020-06-20', '0');

-- --------------------------------------------------------

--
-- Table structure for table `hru_kategori`
--

CREATE TABLE `hru_kategori` (
  `id` int(10) NOT NULL,
  `kode_kategori` varchar(50) DEFAULT NULL,
  `nama_kategori` varchar(200) DEFAULT NULL,
  `masa_pakai` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hru_kategori`
--

INSERT INTO `hru_kategori` (`id`, `kode_kategori`, `nama_kategori`, `masa_pakai`) VALUES
(9, 'KTG-0001', 'Mesin', 5),
(10, 'KTG-0002', 'Tanah', 10),
(11, 'KTG-0003', 'Gedung', 10),
(12, 'KTG-0004', 'Peralatan', 5),
(13, 'KTG-0005', 'Kendaraan', 5);

-- --------------------------------------------------------

--
-- Table structure for table `hru_penyusutan_log`
--

CREATE TABLE `hru_penyusutan_log` (
  `id` int(10) NOT NULL,
  `aset_detail_id` int(10) DEFAULT NULL,
  `tanggal_penyusutan` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hru_penyusutan_log`
--

INSERT INTO `hru_penyusutan_log` (`id`, `aset_detail_id`, `tanggal_penyusutan`) VALUES
(149, 118, '2020-06-20'),
(150, 119, '2020-06-20'),
(151, 120, '2020-06-20'),
(152, 124, '2020-06-20'),
(153, 126, '2020-06-20'),
(154, 127, '2020-06-20'),
(155, 129, '2020-06-20'),
(156, 131, '2020-06-20'),
(157, 132, '2020-06-20'),
(158, 133, '2020-06-20'),
(159, 134, '2020-06-20'),
(160, 136, '2020-06-20'),
(161, 138, '2020-06-20'),
(162, 143, '2020-06-20'),
(163, 144, '2020-06-20'),
(164, 145, '2020-06-20'),
(165, 146, '2020-06-20');

-- --------------------------------------------------------

--
-- Table structure for table `hru_ruangan`
--

CREATE TABLE `hru_ruangan` (
  `id` int(10) NOT NULL,
  `kode_ruangan` varchar(50) DEFAULT NULL,
  `nama_ruangan` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hru_ruangan`
--

INSERT INTO `hru_ruangan` (`id`, `kode_ruangan`, `nama_ruangan`) VALUES
(1, 'RNG-0001', 'Tata Usaha'),
(2, 'RNG-0002', 'Gudang'),
(3, 'RNG-0003', 'Aula'),
(4, 'RNG-0004', 'Administrasi'),
(6, 'RNG-0005', 'Parkiran');

-- --------------------------------------------------------

--
-- Table structure for table `hru_vendor`
--

CREATE TABLE `hru_vendor` (
  `id` int(10) NOT NULL,
  `kode_vendor` varchar(50) DEFAULT NULL,
  `nama_vendor` varchar(200) DEFAULT NULL,
  `no_telp` varchar(13) DEFAULT NULL,
  `alamat` varchar(250) DEFAULT NULL,
  `keterangan` varchar(250) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hru_vendor`
--

INSERT INTO `hru_vendor` (`id`, `kode_vendor`, `nama_vendor`, `no_telp`, `alamat`, `keterangan`, `username`, `password`) VALUES
(6, 'VR-0001', 'Ucok Bengkel', '089992312', 'Jl. Radio 1', NULL, 'ucok01', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `jurnal`
--

CREATE TABLE `jurnal` (
  `id` int(10) NOT NULL,
  `jurnal_master_id` int(10) DEFAULT NULL,
  `coa_id` int(10) DEFAULT NULL,
  `transaksi_id` int(11) DEFAULT NULL,
  `transaksi_pendaftaran_id` int(11) DEFAULT NULL,
  `transaksi_pembayaran_id` int(11) DEFAULT NULL,
  `transaksi_spp_id` int(11) DEFAULT NULL,
  `transaksi_lainnya_id` int(11) DEFAULT NULL,
  `transaksi_daycare_id` int(11) DEFAULT NULL,
  `alt_name` varchar(200) DEFAULT NULL,
  `tgl_jurnal` datetime DEFAULT NULL,
  `posisi` enum('debit','kredit') DEFAULT NULL,
  `nominal` int(50) DEFAULT NULL,
  `status` enum('0','1') DEFAULT '0',
  `keterangan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurnal`
--

INSERT INTO `jurnal` (`id`, `jurnal_master_id`, `coa_id`, `transaksi_id`, `transaksi_pendaftaran_id`, `transaksi_pembayaran_id`, `transaksi_spp_id`, `transaksi_lainnya_id`, `transaksi_daycare_id`, `alt_name`, `tgl_jurnal`, `posisi`, `nominal`, `status`, `keterangan`) VALUES
(577, 165, 12, 104, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-03 13:15:21', 'debit', 10000, '0', NULL),
(578, 165, 3, 104, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-03 13:15:21', 'kredit', 10000, '0', NULL),
(579, 166, 6, 105, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-03 13:19:45', 'debit', 1000, '0', NULL),
(580, 166, 26, 105, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-03 13:19:45', 'kredit', 1000, '0', NULL),
(581, NULL, 46, NULL, NULL, NULL, NULL, 5, NULL, NULL, '2020-07-03 01:21:08', 'debit', 125000, '', NULL),
(582, NULL, 71, NULL, NULL, NULL, NULL, 5, NULL, NULL, '2020-07-03 01:21:08', 'kredit', 125000, '', NULL),
(583, 167, 16, 106, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-03 13:21:50', 'debit', 20000, '0', NULL),
(584, 167, 26, 106, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-03 13:21:50', 'kredit', 20000, '0', NULL),
(585, NULL, 26, NULL, NULL, NULL, NULL, NULL, 5, NULL, '2020-07-03 01:22:49', 'debit', 125000, '', NULL),
(586, NULL, 46, NULL, NULL, NULL, NULL, NULL, 5, NULL, '2020-07-03 01:22:49', 'kredit', 125000, '', NULL),
(587, 168, 6, 107, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-03 13:25:17', 'debit', 10000, '0', NULL),
(588, 168, 26, 107, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-03 13:25:17', 'kredit', 10000, '0', NULL),
(589, NULL, 32, NULL, 21, NULL, NULL, NULL, NULL, NULL, '2020-07-06 01:55:08', 'debit', 3600000, '', NULL),
(590, NULL, 34, NULL, 21, NULL, NULL, NULL, NULL, NULL, '2020-07-06 01:55:08', 'debit', 4400000, '', NULL),
(591, NULL, 31, NULL, 21, NULL, NULL, NULL, NULL, NULL, '2020-07-06 01:55:08', 'kredit', 0, '', NULL),
(592, NULL, 33, NULL, 21, NULL, NULL, NULL, NULL, NULL, '2020-07-06 01:55:08', 'kredit', 0, '', NULL),
(593, NULL, 3, NULL, 21, NULL, NULL, NULL, NULL, NULL, '2020-07-06 01:55:08', 'kredit', 8000000, '', NULL),
(594, NULL, 29, NULL, 23, NULL, NULL, NULL, NULL, NULL, '2020-07-06 02:39:57', 'debit', 450000, '', NULL),
(595, NULL, 31, NULL, 23, NULL, NULL, NULL, NULL, NULL, '2020-07-06 02:39:57', 'debit', 3600000, '', NULL),
(596, NULL, 33, NULL, 23, NULL, NULL, NULL, NULL, NULL, '2020-07-06 02:39:57', 'debit', 4400000, '', NULL),
(597, NULL, 30, NULL, 23, NULL, NULL, NULL, NULL, NULL, '2020-07-06 02:39:57', 'kredit', 450000, '', NULL),
(598, NULL, 32, NULL, 23, NULL, NULL, NULL, NULL, NULL, '2020-07-06 02:39:57', 'kredit', 3600000, '', NULL),
(599, NULL, 34, NULL, 23, NULL, NULL, NULL, NULL, NULL, '2020-07-06 02:39:57', 'kredit', 4400000, '', NULL),
(603, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-06 03:43:24', 'debit', 2450000, '', NULL),
(604, NULL, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-06 03:43:24', 'kredit', 450000, '', NULL),
(605, NULL, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-06 03:43:24', 'kredit', 1000000, '', NULL),
(606, NULL, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-06 03:43:24', 'kredit', 1000000, '', NULL),
(611, NULL, 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-06 04:40:45', 'debit', 600000, '', NULL),
(612, NULL, 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-06 04:40:45', 'debit', 733333, '', NULL),
(613, NULL, 66, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-06 04:40:45', 'kredit', 600000, '', NULL),
(614, NULL, 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-06 04:40:45', 'kredit', 733333, '', NULL),
(615, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-07 07:48:00', 'debit', 200000, '', NULL),
(616, NULL, 111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-07 07:48:00', 'kredit', 200000, '', NULL),
(617, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-07 07:48:23', 'debit', 300000, '', NULL),
(618, NULL, 62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-07 07:48:23', 'kredit', 300000, '', NULL),
(619, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-07 07:48:41', 'debit', 400000, '', NULL),
(620, NULL, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-07 07:48:41', 'kredit', 400000, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_master`
--

CREATE TABLE `jurnal_master` (
  `id` int(10) NOT NULL,
  `kode_jurnal` varchar(200) DEFAULT NULL,
  `tanggal_input` date DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `is_post` enum('0','1') DEFAULT '0',
  `waktu_post` datetime DEFAULT NULL,
  `source` enum('Manual','Otomatis') DEFAULT 'Otomatis'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jurnal_master`
--

INSERT INTO `jurnal_master` (`id`, `kode_jurnal`, `tanggal_input`, `keterangan`, `is_post`, `waktu_post`, `source`) VALUES
(165, 'JRNL-0001', '2020-07-03', '', '1', '2020-07-03 13:15:33', 'Manual'),
(166, 'JRNL-0002', '2020-07-03', 'Transaksi Beban TBBN-03072020-0001', '1', '2020-07-03 13:20:22', 'Otomatis'),
(167, 'JRNL-0003', '2020-07-03', '', '1', '2020-07-03 13:22:04', 'Manual'),
(168, 'JRNL-0004', '2020-07-03', 'Transaksi Beban TBBN-03072020-0002', '1', '2020-07-03 13:25:38', 'Otomatis');

-- --------------------------------------------------------

--
-- Table structure for table `k_beban`
--

CREATE TABLE `k_beban` (
  `id` int(10) NOT NULL,
  `kode_beban` varchar(200) DEFAULT NULL,
  `nama_beban` varchar(200) DEFAULT NULL,
  `coa_id` int(11) DEFAULT NULL,
  `is_bulanan` enum('0','1') DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `k_beban`
--

INSERT INTO `k_beban` (`id`, `kode_beban`, `nama_beban`, `coa_id`, `is_bulanan`) VALUES
(1, 'BBN-0001', 'Pensil', 6, '0'),
(2, 'BBN-0002', 'Listrik', 4, '1'),
(3, 'BBN-0003', 'Air', 3, '1'),
(4, 'BBN-0004', 'Buku', 6, '0');

-- --------------------------------------------------------

--
-- Table structure for table `k_bunga`
--

CREATE TABLE `k_bunga` (
  `id` int(11) NOT NULL,
  `id_rekening` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `saldo_akhir` int(11) NOT NULL,
  `potongan_bunga` int(11) NOT NULL,
  `perolehan_bunga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `k_bunga`
--

INSERT INTO `k_bunga` (`id`, `id_rekening`, `tanggal`, `saldo_akhir`, `potongan_bunga`, `perolehan_bunga`) VALUES
(7, 1, '2020-06-28', 30174000, 20, 5029000),
(8, 2, '2020-06-29', 1837500, 5, 87500),
(9, 5, '2020-07-02', 3692400, 2, 72400);

-- --------------------------------------------------------

--
-- Table structure for table `k_komponenbiaya`
--

CREATE TABLE `k_komponenbiaya` (
  `id_komponen` int(11) NOT NULL,
  `nama_komponen` varchar(50) NOT NULL,
  `tipe_komponen` varchar(100) NOT NULL,
  `harga_komponen` varchar(100) NOT NULL,
  `status_komponen` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `k_pemilik`
--

CREATE TABLE `k_pemilik` (
  `id` int(10) NOT NULL,
  `nama_pemilik` varchar(200) DEFAULT NULL,
  `no_hp` varchar(200) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `p_no_rek` varchar(50) DEFAULT NULL,
  `p_an` varchar(100) DEFAULT NULL,
  `p_bank` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `k_pemilik`
--

INSERT INTO `k_pemilik` (`id`, `nama_pemilik`, `no_hp`, `alamat`, `p_no_rek`, `p_an`, `p_bank`) VALUES
(5, 'Bellatrix', '083181827488', 'asdasd', '123', 'asdasd', 'BNI');

-- --------------------------------------------------------

--
-- Table structure for table `k_rek`
--

CREATE TABLE `k_rek` (
  `id` int(10) NOT NULL,
  `kode_rek` varchar(50) DEFAULT NULL,
  `nama_bank` varchar(200) DEFAULT NULL,
  `atas_nama` varchar(200) DEFAULT NULL,
  `no_rek` varchar(50) DEFAULT NULL,
  `saldo` int(50) DEFAULT 0,
  `is_default` enum('0','1') DEFAULT '0',
  `is_bop` enum('0','1') DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `k_rek`
--

INSERT INTO `k_rek` (`id`, `kode_rek`, `nama_bank`, `atas_nama`, `no_rek`, `saldo`, `is_default`, `is_bop`) VALUES
(1, 'REK-0001', 'BRI', 'Almalia', '1039759213', 30074000, '1', '0'),
(2, 'REK-0002', 'BJB', 'Almalia Daycare dan Preschool', '0069524801100', 1837500, '0', '1'),
(3, 'REK-0003', 'BRI', 'Bunda Fitri', '1045370883', 455000, '0', '0'),
(4, 'REK-0004', 'BRI', 'Bunda Ocha', '1003057808', 1075000, '0', '0'),
(5, 'REK-0005', 'MANDIRI', 'Almalia', '1310012683514', 11692400, '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `k_rek_history`
--

CREATE TABLE `k_rek_history` (
  `id` int(10) NOT NULL,
  `rek_id` int(10) DEFAULT NULL,
  `transaksi_pembayaran_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `k_rek_history`
--

INSERT INTO `k_rek_history` (`id`, `rek_id`, `transaksi_pembayaran_id`) VALUES
(1, 1, 43),
(3, 1, 43),
(14, 1, 43),
(30, 1, 97),
(31, 1, 98),
(40, 2, 107),
(42, 2, 109),
(43, 2, 110);

-- --------------------------------------------------------

--
-- Table structure for table `k_rek_koran`
--

CREATE TABLE `k_rek_koran` (
  `id` int(11) NOT NULL,
  `id_bunga` int(11) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `k_rek_koran`
--

INSERT INTO `k_rek_koran` (`id`, `id_bunga`, `deskripsi`) VALUES
(4, 7, 'aweu'),
(5, 8, 'Blablablablabla.....'),
(6, 9, 'gils');

-- --------------------------------------------------------

--
-- Table structure for table `k_tunai`
--

CREATE TABLE `k_tunai` (
  `id` int(11) NOT NULL,
  `nama_kas` varchar(255) NOT NULL,
  `saldo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `k_tunai`
--

INSERT INTO `k_tunai` (`id`, `nama_kas`, `saldo`) VALUES
(1, 'Kas Institusi', 1420000),
(2, 'Kas Kecil', 5000);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(10) NOT NULL,
  `rek_id` int(10) DEFAULT NULL,
  `pemilik_id` int(10) DEFAULT NULL,
  `pegawai_id` int(10) DEFAULT NULL,
  `vendor_id` int(10) DEFAULT NULL,
  `kode_transaksi` varchar(50) DEFAULT NULL,
  `nama_instuisi` varchar(200) DEFAULT NULL,
  `tanggal_transaksi` datetime DEFAULT NULL,
  `tipe` enum('perolehan','transaksi_beban','bop','pinjaman','pemeliharaan','perbaikan','penarikan','setoran','setoran_bank','jurnal') DEFAULT NULL,
  `jenis` enum('masuk','keluar','none') DEFAULT NULL,
  `pembayaran` enum('Tunai','Kredit','Hibah') DEFAULT NULL,
  `metode` enum('Cash','Transfer') DEFAULT NULL,
  `level` enum('0','1','2','3') DEFAULT '1',
  `level_vendor` enum('0','1','2','3','4') DEFAULT '1',
  `total_transaksi` int(50) DEFAULT 0,
  `total_bayar` int(50) DEFAULT 0,
  `sisa_bayar` int(50) DEFAULT 0,
  `status` enum('Lunas','Belum Lunas') DEFAULT NULL,
  `is_created` enum('0','1') DEFAULT '0',
  `is_deny` enum('0','1') DEFAULT '0',
  `is_received` enum('0','1') DEFAULT '0',
  `keterangan` text DEFAULT NULL,
  `keterangan_deny` text DEFAULT NULL,
  `keterangan_acc` text DEFAULT NULL,
  `tanggal_konfirmasi_vendor` datetime DEFAULT NULL,
  `tanggal_konfirmasi_keuangan` datetime DEFAULT NULL,
  `tanggal_selesai` datetime DEFAULT NULL,
  `bukti_terima` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `rek_id`, `pemilik_id`, `pegawai_id`, `vendor_id`, `kode_transaksi`, `nama_instuisi`, `tanggal_transaksi`, `tipe`, `jenis`, `pembayaran`, `metode`, `level`, `level_vendor`, `total_transaksi`, `total_bayar`, `sisa_bayar`, `status`, `is_created`, `is_deny`, `is_received`, `keterangan`, `keterangan_deny`, `keterangan_acc`, `tanggal_konfirmasi_vendor`, `tanggal_konfirmasi_keuangan`, `tanggal_selesai`, `bukti_terima`) VALUES
(86, 1, NULL, NULL, 6, 'PRL-19062020-0001', NULL, '2020-06-19 20:18:07', 'perolehan', 'keluar', 'Tunai', 'Transfer', '3', '1', 240000, 240000, 0, 'Lunas', '1', '0', '0', 'Tes', NULL, NULL, NULL, NULL, NULL, '86_1592572759.jpg'),
(87, 1, NULL, NULL, 6, 'PRB-19122020-0001', NULL, '2020-12-19 22:43:24', 'perbaikan', 'keluar', 'Tunai', 'Transfer', '3', '3', 165000, 165000, 165000, NULL, '1', '0', '0', 'asd', NULL, NULL, '2020-12-19 22:44:23', '2020-12-19 22:46:32', NULL, '87_1608392792.jpg'),
(96, 2, 5, NULL, NULL, 'PNRK-19062020-0001', NULL, '2020-06-19 23:43:52', 'penarikan', 'keluar', 'Tunai', NULL, '3', '1', 235000, 235000, 0, NULL, '1', '0', '0', 'asd', NULL, NULL, NULL, NULL, NULL, NULL),
(97, NULL, NULL, NULL, NULL, 'SBNK-19062020-0001', NULL, '2020-06-19 23:45:30', 'setoran_bank', 'keluar', 'Tunai', 'Cash', '3', '1', 500000, 500000, 0, 'Lunas', '1', '0', '0', 'asd', NULL, NULL, NULL, NULL, NULL, '97_1592585963.jpeg'),
(98, 2, 5, NULL, NULL, 'PNRK-20062020-0001', NULL, '2020-06-20 00:50:15', 'penarikan', 'keluar', 'Tunai', NULL, '3', '1', 2000000, 2000000, 0, NULL, '1', '0', '0', 'asd', NULL, NULL, NULL, NULL, NULL, NULL),
(99, NULL, NULL, NULL, 6, 'PRL-20062020-0001', NULL, '2020-06-20 01:17:01', 'perolehan', 'keluar', 'Kredit', 'Cash', '3', '1', 500000, 350000, 150000, 'Belum Lunas', '1', '0', '1', 'asd', NULL, NULL, NULL, NULL, NULL, '99_1592590716.jpg'),
(100, NULL, NULL, NULL, NULL, '-20062020-0001', NULL, '2020-06-20 01:47:46', 'jurnal', 'none', NULL, NULL, '3', '1', 0, 0, 0, NULL, '0', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, 1, NULL, NULL, NULL, 'SBNK-28062020-0001', NULL, '2020-06-28 15:14:14', 'setoran_bank', 'keluar', 'Tunai', 'Cash', '1', '1', 200000, 200000, 0, NULL, '1', '0', '0', 'aweu', NULL, NULL, NULL, NULL, NULL, NULL),
(102, NULL, NULL, NULL, NULL, '-28062020-0001', NULL, '2020-06-28 16:54:34', 'jurnal', 'none', NULL, NULL, '3', '1', 0, 0, 0, NULL, '0', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, NULL, NULL, NULL, NULL, '-03072020-0001', NULL, '2020-07-03 12:59:24', 'jurnal', 'none', NULL, NULL, '3', '1', 0, 0, 0, NULL, '0', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, NULL, NULL, NULL, NULL, '-03072020-0002', NULL, '2020-07-03 13:15:21', 'jurnal', 'none', NULL, NULL, '3', '1', 0, 0, 0, NULL, '0', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, NULL, NULL, NULL, NULL, 'TBBN-03072020-0001', NULL, '2020-07-03 13:16:55', 'transaksi_beban', 'keluar', 'Tunai', 'Cash', '3', '1', 1000, 1000, 0, 'Lunas', '1', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, '105_1593757185.png'),
(106, NULL, NULL, NULL, NULL, '-03072020-0003', NULL, '2020-07-03 13:21:50', 'jurnal', 'none', NULL, NULL, '3', '1', 0, 0, 0, NULL, '0', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(107, NULL, NULL, NULL, NULL, 'TBBN-03072020-0002', NULL, '2020-07-03 13:24:24', 'transaksi_beban', 'keluar', 'Tunai', 'Cash', '3', '1', 10000, 10000, 0, 'Lunas', '1', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, '107_1593757517.png');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_aset`
--

CREATE TABLE `transaksi_aset` (
  `id` int(10) NOT NULL,
  `transaksi_id` int(10) DEFAULT NULL,
  `aset_id` int(10) DEFAULT NULL,
  `jumlah` int(10) DEFAULT NULL,
  `harga` int(10) DEFAULT NULL,
  `subtotal` int(10) DEFAULT NULL,
  `nilai_residu` int(10) DEFAULT NULL,
  `umur` int(50) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_aset`
--

INSERT INTO `transaksi_aset` (`id`, `transaksi_id`, `aset_id`, `jumlah`, `harga`, `subtotal`, `nilai_residu`, `umur`) VALUES
(43, 86, 12, 24, 10000, 240000, 1000, 0),
(44, 99, 13, 4, 125000, 500000, 4000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_beban`
--

CREATE TABLE `transaksi_beban` (
  `id` int(10) NOT NULL,
  `transaksi_id` int(10) DEFAULT NULL,
  `beban_id` int(10) DEFAULT NULL,
  `qty` int(10) DEFAULT NULL,
  `harga` int(50) DEFAULT NULL,
  `subtotal` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_beban`
--

INSERT INTO `transaksi_beban` (`id`, `transaksi_id`, `beban_id`, `qty`, `harga`, `subtotal`) VALUES
(1, 105, 1, 1, 1000, 1000),
(2, 107, 4, 1, 10000, 10000);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_bop`
--

CREATE TABLE `transaksi_bop` (
  `id` int(10) NOT NULL,
  `transaksi_id` int(10) DEFAULT NULL,
  `rek_id` int(10) DEFAULT NULL,
  `nama_komponen` varchar(200) DEFAULT NULL,
  `subtotal` int(50) DEFAULT NULL,
  `subtotal_acc` int(5) DEFAULT NULL,
  `tanggal_pengeluaran` datetime DEFAULT NULL,
  `metode_pengeluaran` enum('Cash','Transfer') DEFAULT NULL,
  `is_acc` enum('0','1') DEFAULT NULL,
  `keterangan_pengeluaran` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi_bop`
--

INSERT INTO `transaksi_bop` (`id`, `transaksi_id`, `rek_id`, `nama_komponen`, `subtotal`, `subtotal_acc`, `tanggal_pengeluaran`, `metode_pengeluaran`, `is_acc`, `keterangan_pengeluaran`) VALUES
(3, 79, 1, 'Pembelian Meja Guru', 2500000, NULL, '2020-06-19 02:35:40', 'Transfer', '1', 'lorem ipsum sip dolor amet'),
(4, 79, NULL, 'Beli Papan Tulis', 1000000, NULL, '2020-06-19 02:35:44', 'Cash', '0', 'Papan Tulis untuk ruangan baru');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_lainnya`
--

CREATE TABLE `transaksi_lainnya` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `nis` int(11) NOT NULL,
  `jumlah_jam` int(11) NOT NULL,
  `total_tagihan` int(11) NOT NULL,
  `kode_pegawai` varchar(15) DEFAULT NULL,
  `jenis_tambahan` varchar(10) NOT NULL,
  `jenis_pembayaran` enum('cash','transfer') DEFAULT NULL,
  `kode_rek` int(11) DEFAULT NULL,
  `status_lainnya` enum('Belum Lunas','Lunas') NOT NULL,
  `tanggal_transaksi` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi_lainnya`
--

INSERT INTO `transaksi_lainnya` (`id`, `tanggal`, `nis`, `jumlah_jam`, `total_tagihan`, `kode_pegawai`, `jenis_tambahan`, `jenis_pembayaran`, `kode_rek`, `status_lainnya`, `tanggal_transaksi`) VALUES
(19, '2020-07-02', 2021060001, 1, 25000, 'PGW-0004', 'overtime', 'cash', NULL, 'Lunas', '2020-07-02'),
(20, '2020-07-02', 2021060001, 1, 18000, NULL, 'catering', 'cash', NULL, 'Lunas', '2020-07-02'),
(21, '2020-07-02', 2021060001, 1, 15000, 'PGW-0004', 'sarapan', 'cash', NULL, 'Lunas', '2020-07-02'),
(22, '2020-07-02', 2021060001, 1, 15000, 'PGW-0004', 'mandi', 'cash', NULL, 'Lunas', '2020-07-02');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_pembayaran`
--

CREATE TABLE `transaksi_pembayaran` (
  `id` int(10) NOT NULL,
  `transaksi_id` int(10) DEFAULT NULL,
  `jumlah_bayar` int(50) DEFAULT NULL,
  `tanggal_bayar` datetime DEFAULT NULL,
  `keterangan_pembayaran` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_pembayaran`
--

INSERT INTO `transaksi_pembayaran` (`id`, `transaksi_id`, `jumlah_bayar`, `tanggal_bayar`, `keterangan_pembayaran`) VALUES
(43, NULL, NULL, NULL, NULL),
(45, NULL, NULL, NULL, NULL),
(97, 86, 240000, '2020-06-19 20:19:19', NULL),
(98, 87, 165000, '2020-12-19 22:46:32', NULL),
(107, 96, 235000, '2020-06-19 23:43:52', NULL),
(109, 97, 500000, '2020-06-19 23:59:23', NULL),
(110, 98, 2000000, '2020-06-20 00:50:15', NULL),
(111, 99, 350000, '2020-06-20 01:18:36', NULL),
(112, 105, 1000, '2020-07-03 13:19:45', NULL),
(113, 107, 10000, '2020-07-03 13:25:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_penarikan_bank`
--

CREATE TABLE `transaksi_penarikan_bank` (
  `id` int(11) NOT NULL,
  `tanggal_penarikan` date NOT NULL,
  `id_user_penarik` int(11) NOT NULL,
  `id_rekening` int(11) NOT NULL,
  `nominal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi_penarikan_bank`
--

INSERT INTO `transaksi_penarikan_bank` (`id`, `tanggal_penarikan`, `id_user_penarik`, `id_rekening`, `nominal`) VALUES
(2, '2020-06-25', 1, 2, 145000),
(3, '2020-06-28', 1, 1, 200000),
(4, '2020-06-29', 1, 2, 250000),
(5, '2020-06-29', 1, 1, 500000),
(6, '2020-07-02', 1, 5, 5000),
(7, '2020-07-02', 1, 4, 25000),
(8, '2020-07-02', 1, 3, 8000),
(9, '2020-07-02', 1, 3, 37000);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_pendaftaran`
--

CREATE TABLE `transaksi_pendaftaran` (
  `id` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_tahun` int(11) NOT NULL,
  `biaya_pendaftaran` int(11) NOT NULL,
  `biaya_seragam` int(11) NOT NULL,
  `biaya_dpp` int(11) NOT NULL,
  `biaya_dsk` int(11) NOT NULL,
  `cicilan_dpp` int(11) DEFAULT NULL,
  `cicilan_dsk` int(11) DEFAULT NULL,
  `jenis_pembayaran` enum('cash','transfer') DEFAULT NULL,
  `kode_rek` varchar(11) DEFAULT NULL,
  `total_tagihan` int(11) NOT NULL,
  `total_pembayaran` int(11) DEFAULT NULL,
  `sisa_tagihan` int(11) DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `tanggal_transaksi` date DEFAULT NULL,
  `tanggal_pendaftaran` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi_pendaftaran`
--

INSERT INTO `transaksi_pendaftaran` (`id`, `id_siswa`, `id_tahun`, `biaya_pendaftaran`, `biaya_seragam`, `biaya_dpp`, `biaya_dsk`, `cicilan_dpp`, `cicilan_dsk`, `jenis_pembayaran`, `kode_rek`, `total_tagihan`, `total_pembayaran`, `sisa_tagihan`, `status`, `tanggal_transaksi`, `tanggal_pendaftaran`) VALUES
(18, 16, 15, 0, 450000, 0, 0, 0, 0, 'transfer', 'REK-0001', 450000, 450000, 0, '1', '2020-06-26', '2020-06-26'),
(19, 18, 15, 0, 450000, 0, 0, 0, 0, 'cash', NULL, 450000, 450000, 0, '1', '2020-06-29', '2020-06-29'),
(20, 17, 15, 250000, 450000, 3600000, 4400000, 3000000, 4000000, 'transfer', '0774422315', 8700000, 7700000, 1000000, '1', '2020-06-29', '2020-06-29'),
(21, 19, 15, 250000, 450000, 3600000, 4400000, 1000000, 1000000, 'transfer', '13100126835', 8700000, 2700000, 6000000, '1', '2020-07-03', '2020-07-03'),
(22, 20, 15, 0, 450000, 0, 0, 0, 0, 'cash', NULL, 450000, 449993, 0, '1', '2020-07-03', '2020-07-03'),
(23, 19, 15, 0, 450000, 3600000, 4400000, 1000000, 1000000, 'cash', NULL, 8450000, 2700000, 6000000, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_perawatan`
--

CREATE TABLE `transaksi_perawatan` (
  `id` int(10) NOT NULL,
  `transaksi_id` int(10) DEFAULT NULL,
  `aset_detail_id` int(10) DEFAULT NULL,
  `harga_perawatan` int(50) DEFAULT NULL,
  `keterangan_perawatan` text DEFAULT NULL,
  `keterangan_vendor` text DEFAULT NULL,
  `kerusakan` enum('Ringan','Sedang','Berat') DEFAULT NULL,
  `gambar` varchar(200) DEFAULT NULL,
  `is_done` enum('0','1') DEFAULT '0',
  `tgl_fix` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi_perawatan`
--

INSERT INTO `transaksi_perawatan` (`id`, `transaksi_id`, `aset_detail_id`, `harga_perawatan`, `keterangan_perawatan`, `keterangan_vendor`, `kerusakan`, `gambar`, `is_done`, `tgl_fix`) VALUES
(14, 87, 118, 165000, 'asd', 'Patah Kaki', NULL, '118_1608392576.jpg', '0', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_setor_modal`
--

CREATE TABLE `transaksi_setor_modal` (
  `id` int(11) NOT NULL,
  `kode_setoran` varchar(20) DEFAULT NULL,
  `tanggal_setoran` date NOT NULL,
  `id_pemilik` int(11) NOT NULL,
  `rek_id` int(11) DEFAULT NULL,
  `nominal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi_setor_modal`
--

INSERT INTO `transaksi_setor_modal` (`id`, `kode_setoran`, `tanggal_setoran`, `id_pemilik`, `rek_id`, `nominal`) VALUES
(1, NULL, '2020-06-25', 5, 2, 0),
(2, NULL, '2020-06-25', 5, 2, 0),
(3, NULL, '2020-06-25', 5, 2, 0),
(4, NULL, '2020-06-25', 5, 1, 100000),
(5, 'STRM-28062020-0001', '2020-06-28', 5, 1, 200000),
(6, 'STRM-29062020-0001', '2020-06-29', 5, 1, 200000),
(7, 'STRM-07072020-0001', '2020-07-07', 5, 1, 200000),
(8, 'STRM-07072020-0001', '2020-07-07', 5, 3, 300000),
(9, 'STRM-07072020-0001', '2020-07-07', 5, 4, 400000);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_spp`
--

CREATE TABLE `transaksi_spp` (
  `id` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_tahun` int(11) NOT NULL,
  `bulan_ke` int(11) NOT NULL,
  `biaya_spp` int(11) NOT NULL,
  `jenis_pembayaran` enum('transfer','cash') DEFAULT NULL,
  `kode_rek` int(11) DEFAULT NULL,
  `status_spp` enum('Belum Dibayar','Sudah Dibayar') NOT NULL DEFAULT 'Belum Dibayar',
  `tanggal_transaksi` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi_spp`
--

INSERT INTO `transaksi_spp` (`id`, `id_siswa`, `id_tahun`, `bulan_ke`, `biaya_spp`, `jenis_pembayaran`, `kode_rek`, `status_spp`, `tanggal_transaksi`) VALUES
(27, 19, 15, 1, 1250000, 'cash', NULL, 'Sudah Dibayar', '2020-07-03'),
(28, 20, 15, 1, 750000, 'transfer', NULL, 'Sudah Dibayar', '2020-07-05');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `posisi` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `posisi`) VALUES
(1, 'Fikri Reformasi Gunawan', 'admin', 'admin', 'Superadmin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ak_daycare`
--
ALTER TABLE `ak_daycare`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ak_kelas`
--
ALTER TABLE `ak_kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `ak_siswa`
--
ALTER TABLE `ak_siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `ak_tahun_ajaran`
--
ALTER TABLE `ak_tahun_ajaran`
  ADD PRIMARY KEY (`id_tahun`);

--
-- Indexes for table `biaya_spp`
--
ALTER TABLE `biaya_spp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `biaya_umum`
--
ALTER TABLE `biaya_umum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coa`
--
ALTER TABLE `coa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fdl_absensi`
--
ALTER TABLE `fdl_absensi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `karyawan_id` (`pegawai_id`);

--
-- Indexes for table `fdl_gaji`
--
ALTER TABLE `fdl_gaji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fdl_gaji_detail`
--
ALTER TABLE `fdl_gaji_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gaji_id` (`gaji_id`),
  ADD KEY `pegawai_id` (`pegawai_id`);

--
-- Indexes for table `fdl_gaji_tunjangan`
--
ALTER TABLE `fdl_gaji_tunjangan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tunjangan_id` (`tunjangan_id`),
  ADD KEY `pegawai_id` (`pegawai_id`);

--
-- Indexes for table `fdl_izin`
--
ALTER TABLE `fdl_izin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `karyawan_id` (`pegawai_id`);

--
-- Indexes for table `fdl_jabatan`
--
ALTER TABLE `fdl_jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fdl_lembur`
--
ALTER TABLE `fdl_lembur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `karyawan_id` (`pegawai_id`);

--
-- Indexes for table `fdl_pegawai`
--
ALTER TABLE `fdl_pegawai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jabatan_id` (`jabatan_id`);

--
-- Indexes for table `fdl_tunjangan`
--
ALTER TABLE `fdl_tunjangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fdl_waktu`
--
ALTER TABLE `fdl_waktu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hru_aset`
--
ALTER TABLE `hru_aset`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategori_id` (`kategori_id`);

--
-- Indexes for table `hru_aset_detail`
--
ALTER TABLE `hru_aset_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aset_id` (`aset_id`),
  ADD KEY `lokasi_id` (`ruangan_id`),
  ADD KEY `transaksi_aset_id` (`transaksi_aset_id`);

--
-- Indexes for table `hru_kategori`
--
ALTER TABLE `hru_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hru_penyusutan_log`
--
ALTER TABLE `hru_penyusutan_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aset_detail_id` (`aset_detail_id`);

--
-- Indexes for table `hru_ruangan`
--
ALTER TABLE `hru_ruangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hru_vendor`
--
ALTER TABLE `hru_vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnal`
--
ALTER TABLE `jurnal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coa_id` (`coa_id`),
  ADD KEY `transaksi_id` (`transaksi_id`),
  ADD KEY `transaksi_pembayaran_id` (`transaksi_pembayaran_id`),
  ADD KEY `jurnal_master_id` (`jurnal_master_id`);

--
-- Indexes for table `jurnal_master`
--
ALTER TABLE `jurnal_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `k_beban`
--
ALTER TABLE `k_beban`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coa_id` (`coa_id`);

--
-- Indexes for table `k_bunga`
--
ALTER TABLE `k_bunga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `k_komponenbiaya`
--
ALTER TABLE `k_komponenbiaya`
  ADD PRIMARY KEY (`id_komponen`);

--
-- Indexes for table `k_pemilik`
--
ALTER TABLE `k_pemilik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `k_rek`
--
ALTER TABLE `k_rek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `k_rek_history`
--
ALTER TABLE `k_rek_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rek_id` (`rek_id`),
  ADD KEY `transaksi_pembayaran_id` (`transaksi_pembayaran_id`);

--
-- Indexes for table `k_rek_koran`
--
ALTER TABLE `k_rek_koran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `k_tunai`
--
ALTER TABLE `k_tunai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pegawai_id` (`pegawai_id`),
  ADD KEY `vendor_id` (`vendor_id`),
  ADD KEY `pemilik_id` (`pemilik_id`),
  ADD KEY `rek_id` (`rek_id`);

--
-- Indexes for table `transaksi_aset`
--
ALTER TABLE `transaksi_aset`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aset_id` (`aset_id`),
  ADD KEY `transaksi_id` (`transaksi_id`);

--
-- Indexes for table `transaksi_beban`
--
ALTER TABLE `transaksi_beban`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksi_id` (`transaksi_id`),
  ADD KEY `beban_id` (`beban_id`);

--
-- Indexes for table `transaksi_bop`
--
ALTER TABLE `transaksi_bop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_lainnya`
--
ALTER TABLE `transaksi_lainnya`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_pembayaran`
--
ALTER TABLE `transaksi_pembayaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksi_id` (`transaksi_id`);

--
-- Indexes for table `transaksi_penarikan_bank`
--
ALTER TABLE `transaksi_penarikan_bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_pendaftaran`
--
ALTER TABLE `transaksi_pendaftaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_perawatan`
--
ALTER TABLE `transaksi_perawatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksi_id` (`transaksi_id`),
  ADD KEY `aset_detail_id` (`aset_detail_id`);

--
-- Indexes for table `transaksi_setor_modal`
--
ALTER TABLE `transaksi_setor_modal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi_spp`
--
ALTER TABLE `transaksi_spp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ak_daycare`
--
ALTER TABLE `ak_daycare`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ak_kelas`
--
ALTER TABLE `ak_kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ak_siswa`
--
ALTER TABLE `ak_siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `ak_tahun_ajaran`
--
ALTER TABLE `ak_tahun_ajaran`
  MODIFY `id_tahun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `biaya_spp`
--
ALTER TABLE `biaya_spp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `biaya_umum`
--
ALTER TABLE `biaya_umum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `coa`
--
ALTER TABLE `coa`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `fdl_absensi`
--
ALTER TABLE `fdl_absensi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `fdl_gaji`
--
ALTER TABLE `fdl_gaji`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `fdl_gaji_detail`
--
ALTER TABLE `fdl_gaji_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `fdl_gaji_tunjangan`
--
ALTER TABLE `fdl_gaji_tunjangan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fdl_izin`
--
ALTER TABLE `fdl_izin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `fdl_jabatan`
--
ALTER TABLE `fdl_jabatan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fdl_lembur`
--
ALTER TABLE `fdl_lembur`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `fdl_pegawai`
--
ALTER TABLE `fdl_pegawai`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `fdl_tunjangan`
--
ALTER TABLE `fdl_tunjangan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `fdl_waktu`
--
ALTER TABLE `fdl_waktu`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `hru_aset`
--
ALTER TABLE `hru_aset`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `hru_aset_detail`
--
ALTER TABLE `hru_aset_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `hru_kategori`
--
ALTER TABLE `hru_kategori`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `hru_penyusutan_log`
--
ALTER TABLE `hru_penyusutan_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;

--
-- AUTO_INCREMENT for table `hru_ruangan`
--
ALTER TABLE `hru_ruangan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `hru_vendor`
--
ALTER TABLE `hru_vendor`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `jurnal`
--
ALTER TABLE `jurnal`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=621;

--
-- AUTO_INCREMENT for table `jurnal_master`
--
ALTER TABLE `jurnal_master`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;

--
-- AUTO_INCREMENT for table `k_beban`
--
ALTER TABLE `k_beban`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `k_bunga`
--
ALTER TABLE `k_bunga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `k_komponenbiaya`
--
ALTER TABLE `k_komponenbiaya`
  MODIFY `id_komponen` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `k_pemilik`
--
ALTER TABLE `k_pemilik`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `k_rek`
--
ALTER TABLE `k_rek`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `k_rek_history`
--
ALTER TABLE `k_rek_history`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `k_rek_koran`
--
ALTER TABLE `k_rek_koran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `k_tunai`
--
ALTER TABLE `k_tunai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `transaksi_aset`
--
ALTER TABLE `transaksi_aset`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `transaksi_beban`
--
ALTER TABLE `transaksi_beban`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transaksi_bop`
--
ALTER TABLE `transaksi_bop`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transaksi_lainnya`
--
ALTER TABLE `transaksi_lainnya`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `transaksi_pembayaran`
--
ALTER TABLE `transaksi_pembayaran`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `transaksi_penarikan_bank`
--
ALTER TABLE `transaksi_penarikan_bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `transaksi_pendaftaran`
--
ALTER TABLE `transaksi_pendaftaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `transaksi_perawatan`
--
ALTER TABLE `transaksi_perawatan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `transaksi_setor_modal`
--
ALTER TABLE `transaksi_setor_modal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `transaksi_spp`
--
ALTER TABLE `transaksi_spp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fdl_absensi`
--
ALTER TABLE `fdl_absensi`
  ADD CONSTRAINT `fdl_absensi_ibfk_1` FOREIGN KEY (`pegawai_id`) REFERENCES `fdl_pegawai` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fdl_gaji_detail`
--
ALTER TABLE `fdl_gaji_detail`
  ADD CONSTRAINT `fdl_gaji_detail_ibfk_1` FOREIGN KEY (`gaji_id`) REFERENCES `fdl_gaji` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fdl_gaji_detail_ibfk_2` FOREIGN KEY (`pegawai_id`) REFERENCES `fdl_pegawai` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fdl_gaji_tunjangan`
--
ALTER TABLE `fdl_gaji_tunjangan`
  ADD CONSTRAINT `fdl_gaji_tunjangan_ibfk_1` FOREIGN KEY (`tunjangan_id`) REFERENCES `fdl_tunjangan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fdl_gaji_tunjangan_ibfk_2` FOREIGN KEY (`pegawai_id`) REFERENCES `fdl_pegawai` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fdl_izin`
--
ALTER TABLE `fdl_izin`
  ADD CONSTRAINT `fdl_izin_ibfk_1` FOREIGN KEY (`pegawai_id`) REFERENCES `fdl_pegawai` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fdl_lembur`
--
ALTER TABLE `fdl_lembur`
  ADD CONSTRAINT `fdl_lembur_ibfk_1` FOREIGN KEY (`pegawai_id`) REFERENCES `fdl_pegawai` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fdl_pegawai`
--
ALTER TABLE `fdl_pegawai`
  ADD CONSTRAINT `fdl_pegawai_ibfk_1` FOREIGN KEY (`jabatan_id`) REFERENCES `fdl_jabatan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hru_aset`
--
ALTER TABLE `hru_aset`
  ADD CONSTRAINT `hru_aset_ibfk_1` FOREIGN KEY (`kategori_id`) REFERENCES `hru_kategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hru_aset_detail`
--
ALTER TABLE `hru_aset_detail`
  ADD CONSTRAINT `hru_aset_detail_ibfk_1` FOREIGN KEY (`aset_id`) REFERENCES `hru_aset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hru_aset_detail_ibfk_2` FOREIGN KEY (`ruangan_id`) REFERENCES `hru_ruangan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hru_aset_detail_ibfk_3` FOREIGN KEY (`transaksi_aset_id`) REFERENCES `transaksi_aset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hru_penyusutan_log`
--
ALTER TABLE `hru_penyusutan_log`
  ADD CONSTRAINT `hru_penyusutan_log_ibfk_1` FOREIGN KEY (`aset_detail_id`) REFERENCES `hru_aset_detail` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jurnal`
--
ALTER TABLE `jurnal`
  ADD CONSTRAINT `jurnal_ibfk_1` FOREIGN KEY (`coa_id`) REFERENCES `coa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jurnal_ibfk_2` FOREIGN KEY (`transaksi_pembayaran_id`) REFERENCES `transaksi_pembayaran` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jurnal_ibfk_3` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jurnal_ibfk_4` FOREIGN KEY (`jurnal_master_id`) REFERENCES `jurnal_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `k_beban`
--
ALTER TABLE `k_beban`
  ADD CONSTRAINT `k_beban_ibfk_1` FOREIGN KEY (`coa_id`) REFERENCES `coa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `k_rek_history`
--
ALTER TABLE `k_rek_history`
  ADD CONSTRAINT `k_rek_history_ibfk_1` FOREIGN KEY (`rek_id`) REFERENCES `k_rek` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `k_rek_history_ibfk_2` FOREIGN KEY (`transaksi_pembayaran_id`) REFERENCES `transaksi_pembayaran` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`pegawai_id`) REFERENCES `fdl_pegawai` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`vendor_id`) REFERENCES `hru_vendor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`pemilik_id`) REFERENCES `k_pemilik` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_4` FOREIGN KEY (`rek_id`) REFERENCES `k_rek` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi_aset`
--
ALTER TABLE `transaksi_aset`
  ADD CONSTRAINT `transaksi_aset_ibfk_1` FOREIGN KEY (`aset_id`) REFERENCES `hru_aset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_aset_ibfk_2` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi_beban`
--
ALTER TABLE `transaksi_beban`
  ADD CONSTRAINT `transaksi_beban_ibfk_1` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_beban_ibfk_2` FOREIGN KEY (`beban_id`) REFERENCES `k_beban` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi_pembayaran`
--
ALTER TABLE `transaksi_pembayaran`
  ADD CONSTRAINT `transaksi_pembayaran_ibfk_1` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi_perawatan`
--
ALTER TABLE `transaksi_perawatan`
  ADD CONSTRAINT `transaksi_perawatan_ibfk_1` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_perawatan_ibfk_2` FOREIGN KEY (`aset_detail_id`) REFERENCES `hru_aset_detail` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
