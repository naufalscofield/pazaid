'use strict';
$(document).ready(function () {

    $('.clockpicker-demo').clockpicker({
        donetext: 'Done',
        twelvehour: false,
        autoclose:false
    });

    $('.clockpicker-autoclose-demo').clockpicker({
        autoclose: false
    });

    var input = $('.clockpicker-minutes-demo').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: false,
        'default': 'now'
    });

    $(document).on('click', '#check-minutes', function (e) {
        e.stopPropagation();
        input.clockpicker('show')
            .clockpicker('toggleView', 'minutes');
    });

    $('.create-event-demo').clockpicker({
        donetext: 'Done',
        autoclose: false
    });

});