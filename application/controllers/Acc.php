<?php 

class Acc extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->model('transaksi_model');
		$this->load->model('asset/Aset_model', 'm_aset');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu', 'acc');
	}

	public function kas(){

		$req = $this->transaksi_model->get_detail([
													'is_created' => '1'
												])->result_array();

		$bop = $this->db->select('*, transaksi_bop.id AS detail_id')
						->join('transaksi', 'transaksi.id = transaksi_bop.transaksi_id')
						->get('transaksi_bop')->result_array();
		$data['req'] = $this->_combine($req, $bop);

		$this->template->load('layout/template','acc/kas/index', $data);
	}

	public function detail_kas($transaksi_id){
		$perolehan = $this->transaksi_model->get_detail('id', $transaksi_id);
		
		if($perolehan->num_rows() > 0){
			$data['transaksi']  = $perolehan->row_array();
			$data['item']       = $this->transaksi_model->get_list_item($data['transaksi']['tipe'], $data['transaksi']['id']);
			$data['pembayaran'] = $this->transaksi_model->get_list_pembayaran($data['transaksi']['tipe'], $data['transaksi']['id']);
			$this->template->load('layout/template','acc/kas/detail', $data);
		
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> ID Transaksi tidak diketahui','danger'));
			redirect('acc/kas');
		}
	}

	public function detail_kas_bop($transaksi_id){
		$perolehan = $this->db->select('*, transaksi_bop.id AS detail_id')
						->where('transaksi_bop.id', $transaksi_id)
						->join('transaksi', 'transaksi.id = transaksi_bop.transaksi_id')
						->get('transaksi_bop');

		if($perolehan->num_rows() > 0){
			$trans = $perolehan->row_array();

			$item = $this->db->where('transaksi_id', $trans['transaksi_id'])
						     ->get('transaksi_bop')->result_array();

			$i = 1;
			foreach ($item as $row) {
				if($row['transaksi_id'] == $trans['transaksi_id']){
					break;
				}
				$i++;
			}

			if($trans['is_acc'] == '1'){
				$status = 'Lunas';
				$is_deny = '0';
				$level   = '3';

			}else if($trans['is_acc'] == '0'){
				$status = 'Belum Lunas';
				$is_deny = '1';
				$level   = '2';

			}else{
				$status = 'Belum Lunas';
				$is_deny = '0';
				$level   = '1';
			}

			$data['transaksi'] = [
				'id'				=> $trans['detail_id'],
				'kode_transaksi'    => $trans['kode_transaksi']."-".$i,
				'tanggal_transaksi' => $trans['tanggal_pengeluaran'],
				'metode' 			=> $trans['metode_pengeluaran'],
				'total_transaksi'   => $trans['subtotal'],
				'total_bayar'		=> $trans['subtotal'],
				'sisa_bayar'		=> 0,
				'pembayaran'		=> 'Tunai',
				'status'			=> $status,
				'tipe'				=> 'Pemakaian BOP',
				'keterangan'		=> $trans['keterangan_pengeluaran'],
				'level'				=> $level,
				'is_deny'			=> $is_deny
			];

			$get_item[] = [
				'nama_komponen' => 'Pemakaian BOP <br><br><small>'.$trans['keterangan_pengeluaran'].'</small>',
				'harga'			=> $trans['subtotal'],
				'jumlah'		=> '',
				'subtotal'		=> $trans['subtotal']
			];
			$data['item'] = $get_item;
			$this->template->load('layout/template','acc/kas/detail', $data);
		
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> ID Transaksi tidak diketahui','danger'));
			redirect('acc/kas');
		}
	}

	public function set_approval($set, $transaksi_id){
		if(in_array($set, ['approve', 'deny'])){

			if($set == 'approve'){
				$p['level']   = '2';
			}else{
				$p['is_deny'] = '1';
			}

			$this->transaksi_model->update($p, $transaksi_id);
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Status Berhasil diubah','success'));
			redirect('acc/kas/detail/'.$transaksi_id);

		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> ID Transaksi tidak diketahui','danger'));
			redirect('acc/kas');
		}
	}

	public function set_approval_bop($set, $transaksi_id){
		if(in_array($set, ['approve', 'deny'])){

			if($set == 'approve'){
				$p['is_acc']   = '1';
			}else{
				$p['is_acc'] = '0';
			}

			$get = $this->db->where('transaksi_bop.id', $transaksi_id)
							->join('transaksi', 'transaksi.id = transaksi_bop.transaksi_id')
							->get('transaksi_bop')->row_array();

			$this->db->where('id', $transaksi_id)->update('transaksi_bop', $p);

			if($set == 'approve'){
				$data = [
					'sisa_bayar'  => $get['sisa_bayar'] - $get['subtotal'],
					'total_bayar' => $get['total_bayar'] + $get['subtotal']
				];
				$this->db->where('id', $get['transaksi_id'])->update('transaksi', $data);

				$coa_id = '1'; //KAS
				if($get['metode_pengeluaran'] == 'Transfer'){
					$coa_id = '3'; //BANK
				}

				$waktu = date('Y-m-d');
				$jurnal[] = [
	      			'coa_id' 	 => '14', //BEBAN BOP
	      			'keterangan' => '',
	      			'tgl_jurnal' => $waktu,
	      			'posisi'	 => 'debit',
	      			'nominal'    => $get['subtotal']
	      		];

	      		$jurnal[] = [
	      			'coa_id' 	 => $coa_id, 
	      			'keterangan' => '',
	      			'tgl_jurnal' => $waktu,
	      			'posisi'	 => 'kredit',
	      			'nominal'    => $get['subtotal']
	      		];
	      		$this->db->insert_batch('jurnal', $jurnal);
			}

      		if($this->db->trans_status()){
				$this->db->trans_commit();
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Status Berhasil diubah','success'));
			}else{
				$this->db->trans_rollback();
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Terjadi Kesalahan','danger'));
			}

			
			redirect('acc/kas/detail_bop/'.$transaksi_id);

		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> ID Transaksi tidak diketahui','danger'));
			redirect('acc/kas');
		}
	}

			private function _combine($req, $bop){
				$result = [];
				$tmp    = [];
				$new_bop = [];

				foreach ($bop as $row){
					if(array_key_exists($row['transaksi_id'], $tmp)){
						$tmp[$row['transaksi_id']]['current']++;
						$curr = $tmp[$row['transaksi_id']]['current'];
						$kode_transaksi = $row['kode_transaksi']."-".$curr;

					}else{
						$tmp[$row['transaksi_id']]['current'] = 1;
						$kode_transaksi = $row['kode_transaksi']."-1";
					}

					if($row['is_acc'] == '1'){
						$level   = '3';
						$is_deny = '0';
					}else if($row['is_acc'] == '0'){
						$level   = '2';
						$is_deny = '1';
					}else{
						$level   = '1';
						$is_deny = '0';
					}

					$new_bop[] = [
						'id' => $row['detail_id'],
						'kode_transaksi' => $kode_transaksi,
						'tipe'			 => 'Pemakaian BOP',
						'tanggal_transaksi' => $row['tanggal_pengeluaran'],
						'total_transaksi'   => $row['subtotal'],
						'level'	=> $level,
						'is_deny' => $is_deny,
						'metode' => $row['metode_pengeluaran'],
						'pembayaran' => 'Tunai'
					];
				}

				$result = array_merge($req, $new_bop);
				usort($result, function($a, $b) {
				    return strtotime($b['tanggal_transaksi']) - strtotime($a['tanggal_transaksi']);
				});

				//echo "<pre>".print_r($result, true)."</pre>";

				return $result;
			}
}