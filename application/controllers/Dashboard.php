<?php 

class Dashboard extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->model('user_model');
		$this->load->model('penggajian/Absensi_model','m_absensi');
		$this->load->model('penggajian/Gaji_model','m_gaji');
		$this->load->model('transaksi_model');
		$this->load->model('asset/penyusutan_model', 'm_penyusutan');

		if(!$this->session->userdata('login')){
			redirect('');
		}
	}

	public function index(){
		$this->m_penyusutan->set_penyusutan_to_jurnal();
		$this->template->load('layout/template','dashboard');
	}

	public function karyawan(){
		if($this->input->get('bulan')){
			$req['bulan'] = $this->input->get('bulan');
		}else{
			$req['bulan'] = date('m');
		}

		if($this->input->get('tahun')){
			$req['tahun'] = $this->input->get('tahun');
		}else{
			$req['tahun'] = date('Y');
		}

		$req['condition'] = array(
			'pegawai.id' => $this->session->userdata('user_data')['id']
		);
		$absensi = $this->m_absensi->get_list($req)->result_array();

		$ab = [];
		foreach ($absensi as $row){
			$ab[$row['id']]['id_pegawai']   = $row['id'];
			$ab[$row['id']]['nama_pegawai'] = $row['nama_pegawai'];
			$ab[$row['id']]['absensi'][$row['hari']]['id_absensi'] 	 = $row['id_absensi'];
			$ab[$row['id']]['absensi'][$row['hari']]['hari'] 		 = $row['hari'];
			$ab[$row['id']]['absensi'][$row['hari']]['waktu_masuk']  = $row['waktu_masuk'];
			$ab[$row['id']]['absensi'][$row['hari']]['waktu_keluar'] = $row['waktu_keluar'];
			$ab[$row['id']]['absensi'][$row['hari']]['status'] 	     = $row['status'];
		}

		$absensi = $this->m_absensi->get_list($req)->result_array();

		$ab = [];
		foreach ($absensi as $row){
			$ab[$row['id']]['id_pegawai']   = $row['id'];
			$ab[$row['id']]['nama_pegawai'] = $row['nama_pegawai'];
			$ab[$row['id']]['absensi'][$row['hari']]['id_absensi'] 	 = $row['id_absensi'];
			$ab[$row['id']]['absensi'][$row['hari']]['hari'] 		 = $row['hari'];
			$ab[$row['id']]['absensi'][$row['hari']]['waktu_masuk']  = $row['waktu_masuk'];
			$ab[$row['id']]['absensi'][$row['hari']]['waktu_keluar'] = $row['waktu_keluar'];
			$ab[$row['id']]['absensi'][$row['hari']]['status'] 	     = $row['status'];
		}

		$data['absensi'] = $ab;
		$check = $req;
		unset($check['condition']);
		$data['gaji']    = $this->db->where($check)->get('fdl_gaji')->num_rows();

		$data['workday'] = HARI_KERJA;

		$cond = array(
					'pegawai.id' => $this->session->userdata('user_data')['id'],
					'MONTH(izin.tanggal_mulai)' => $req['bulan'],
					'YEAR(izin.tanggal_mulai)' => $req['tahun'],
					'status' => 'approve'
				);
		$izin = $this->m_absensi->get_izin_list($cond)->result_array();
		$d = 0;
		foreach ($izin as $row) {
			$date1 = new DateTime($row['tanggal_mulai']);
            $date2 = new DateTime($row['tanggal_selesai']);
            $interval = $date1->diff($date2);
            $d += $interval->days + 1;
		}

		$data['total_cuti'] = $d;

		$this->template->load('layout/karyawan', 'karyawan/index', $data);
	}

	public function izin(){

		if($this->session->userdata('login') && $this->session->userdata('role') == 'karyawan'){
			$data['active'] = 'izin';
			$data['izin'] = $this->m_absensi->get_izin_list(['pegawai.id' => $this->session->userdata('user_data')['id']])->result_array();
			$this->template->load('layout/karyawan','karyawan/izin', $data);
		
		}else{
			redirect();
		}
	}

	public function gaji(){

		if($this->session->userdata('login') && $this->session->userdata('role') == 'karyawan'){
			$data['active'] = 'gaji';

			if($this->input->get('tahun')){
				$data['tahun'] = $this->input->get('tahun');
			}else{
				$data['tahun'] = date('Y');
			}

			$stop = false;

			for ($i=1; $i <= 12; $i++){ 
				$list[$i] = $this->m_gaji->get_list($i, $data['tahun'], $this->session->userdata('user_data')['id']);
				$list[$i]['next'] = $this->m_gaji->calculateGaji($i, $data['tahun'], $this->session->userdata('user_data')['id']);
			}

			$data['gaji'] = $list;
			$this->template->load('layout/karyawan','karyawan/gaji', $data);
		
		}else{
			redirect();
		}
	}

	public function gaji_detail($id_gaji){
		if($this->session->userdata('login') && $this->session->userdata('role') == 'karyawan'){

			$id_karyawan = $this->session->userdata('user_data')['id'];
			$data['gaji'] = $this->m_gaji->get_detail($id_gaji, $id_karyawan)->row_array();

			if(count($data['gaji']) > 0){

				$data['active'] = 'gaji';
				$data['list'] = $this->m_gaji->get_list_detail($id_gaji, $id_karyawan)->result_array();
				//var_dump($data['list']);
				$this->template->load('layout/karyawan','karyawan/gaji_detail',$data);
			}else{
				redirect('karyawan/gaji');
			}

		}else{
			reidirect();
		}
	}

	public function gaji_detail_cetak($id_gaji){
		if($this->session->userdata('login')){

			$this->load->model('gaji_model');
			$id_karyawan = $this->session->userdata('data_user')['id'];
			$data['gaji'] = $this->m_gaji->get_detail($id_gaji, $id_karyawan)->row_array();

			if(count($data['gaji']) > 0){

				$data['active'] = 'gaji';
				$data['list'] = $this->m_gaji->get_list_detail($id_gaji, $id_karyawan)->row_array();

				$this->load->view('auth_karyawan/gaji_cetak',$data);
			}else{
				redirect('auth_karyawan/gaji');
			}

		}else{
			reidirect();
		}
	}

	public function pinjaman(){
		if($this->session->userdata('login') && $this->session->userdata('role') == 'karyawan'){
			$karyawan_id = $this->session->userdata('user_data')['id'];

			$data['pinjaman_pending'] = $this->transaksi_model->get_detail([
												'is_deny' => '0', 
												'level <' => '3',
												'level >=' => '1',
												'tipe' => 'pinjaman',
												'pegawai_id' => $this->session->userdata('user_data')['id']
											])->result_array();
			$data['pinjaman_acc'] = $this->transaksi_model->get_detail(
				['is_deny' => '0', 'level' => '3', 'tipe' => 'pinjaman', 'pegawai_id' => $this->session->userdata('user_data')['id']]
				)->result_array();
			$data['pinjaman_deny'] = $this->transaksi_model->get_detail(['level' => '0', 'tipe'=> 'pinjaman', 'pegawai_id' => $this->session->userdata('user_data')['id']])->result_array();
			
			$data['sisa_pinjaman'] = $this->transaksi_model->get_nominal_pinjaman($karyawan_id);
			$data['gaji']		   = $this->db->where('pegawai.id', $karyawan_id)
											  ->join('fdl_jabatan jabatan', 'jabatan.id = pegawai.jabatan_id')
											  ->get('fdl_pegawai pegawai')->row_array()['gaji'];

			$this->template->load('layout/karyawan','karyawan/pinjaman',$data);

		}else{
			reidirect();
		}
	}

	//==========================================

	public function vendor(){
		$this->template->load('layout/vendor','vendor/index');
	}

	public function perawatan($tipe){
		if(in_array($tipe, ['perbaikan', 'pemeliharaan'])){
			$data['tipe'] = $tipe;
			$find = [
				'is_created' => '1',
				'tipe'		 => $tipe,
				'level'		 => '3'
			];
			$data['list'] = $this->transaksi_model->get_detail($find)->result_array();
			$this->template->load('layout/vendor','vendor/perawatan',$data);

		}else{
			show_404();
		}
	}

	public function perawatan_detail($tipe, $transaksi_id){
		if(in_array($tipe, ['perbaikan', 'pemeliharaan'])){
			$data['tipe'] = $tipe;
			$perolehan = $this->transaksi_model->get_detail('id', $transaksi_id);
		
			if($perolehan->num_rows() > 0){
				$data['transaksi']  = $perolehan->row_array();
				$data['item']       = $this->transaksi_model->get_list_item($tipe, $transaksi_id);
				$data['pembayaran'] = $this->transaksi_model->get_list_pembayaran($transaksi_id);
				$this->template->load('layout/vendor','vendor/perawatan_detail', $data);

			}else{
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> ID '.$tipe.' tidak diketahui','danger'));
				redirect('vendor/'.$tipe);
			}

		}else{
			show_404();
		}
	}

	public function insert_price($tipe, $transaksi_id){
		$p = $this->input->post();
		$total = 0;
		$this->db->trans_begin();

		foreach ($p['harga'] as $key => $value){ $total += format_angka($value);
			$data['harga_perawatan']   = format_angka($value);
			$data['keterangan_vendor'] = $p['keterangan'][$key];
			$this->db->where('id', $key)
					 ->update('transaksi_perawatan', $data);
		}

		$data = [
			'tanggal_konfirmasi_vendor' => date('Y-m-d H:i:s'),
			'level_vendor'				=> '2',
			'total_transaksi'			=> $total,
			'sisa_bayar'				=> $total
		];
		$this->transaksi_model->update($data, $transaksi_id);

		if($this->db->trans_status()){
			$this->db->trans_commit();
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-check-circle"></i> Berhasil konfirmasi harga','success'));

		}else{
			$this->db->trans_rollback();
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Terjadi Kesalahan','danger'));
		}

		redirect('vendor/'.$tipe.'/detail/'.$transaksi_id);
	}

	public function set_done($tipe, $transaksi_id, $fix_id){
		$waktu = date('Y-m-d H:i:s');
		$data = [
			'tgl_fix' => $waktu,
			'is_done' => '1'
		];
		$this->db->where('id', $fix_id)
				 ->update('transaksi_perawatan', $data);

		$all = $this->db->where('transaksi_id', $transaksi_id)
				 		->get('transaksi_perawatan')->num_rows();
		$done = $this->db->where('transaksi_id', $transaksi_id)
						 ->where('is_done', '1')
				 		 ->get('transaksi_perawatan')->num_rows();

		if($all == $done){
			$data = [
				'level_vendor'    => '4',
				'tanggal_selesai' => $waktu
			];
			$this->db->where('id', $transaksi_id)
					 ->update('transaksi', $data);
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-check-circle"></i> Semua aset sudah berhasil diselesaikan','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-check-circle"></i> Berhasil diselesaikan','success'));
		}

		redirect('vendor/'.$tipe.'/detail/'.$transaksi_id);
	}
}