<?php 

class Modal extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('keuangan/pemilik_model', 'm_pemilik');
		$this->load->model('keuangan/rekening_model', 'm_rekening');
		$this->load->model('transaksi_model');
		$this->load->model('keuangan/transaksi_setor_modal', 'm_modal');
		$this->load->model('Keuangan/Jurnal_model', 'm_jurnal');


		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','keuangan');
	}

	public function index(){
		$data['list']      = $this->db->where('tipe', 'penarikan')
									  ->join('k_rek', 'k_rek.id = transaksi.rek_id')
									  ->join('k_pemilik', 'k_pemilik.id = transaksi.pemilik_id')
									  ->get('transaksi')->result_array();
		$data['last_code'] = $this->transaksi_model->generate_code('transaksi_modal');
		$data['pemilik']   = $this->m_pemilik->get_data();
		$data['rek']	   = $this->m_rekening->get_data();
		//var_dump($data['list']);
		$this->template->load('layout/template','transaksi/keuangan/modal/penarikan', $data);
	}

	public function add_penarikan(){
		$p = $this->input->post();


		$p['total_transaksi'] = format_angka($p['total_transaksi']);
		$p['total_bayar']	  = $p['total_transaksi'];
		$p['tanggal_transaksi'] = date('Y-m-d H:i:s');
		$p['is_created']		= '1';
		$p['tipe']				= 'penarikan';
		$p['pembayaran']		= 'Tunai';
		$p['jenis']	= 'keluar';
		$p['level'] = '3';

		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('total_transaksi', 'Nominal', 'required|numeric|greater_than[0]');
		$this->form_validation->set_rules('pemilik_id', 'Pemilik', 'required|numeric');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

		if($this->form_validation->run() == TRUE){

			$this->db->trans_begin();
			$this->transaksi_model->insert($p);

			$trans = $this->transaksi_model->last_data('penarikan');
			$this->transaksi_model->insert_pembayaran([
										'transaksi_id'  => $trans['id'],
										'jumlah_bayar'  => $trans['total_bayar'],
										'tanggal_bayar' => $trans['tanggal_transaksi']
									]);
			$last = $this->transaksi_model->get_last_pembayaran($trans['id']);
			$rek = [
				'rek_id' 				  => $p['rek_id'],
				'transaksi_pembayaran_id' => $last['id']
			];
			$this->m_rekening->insert_history($rek);

			$detail_rek = $this->m_rekening->get_detail('id', $p['rek_id'])->row_array();
			$detail_rek['saldo'] -= $trans['total_bayar'];
			
			$this->m_rekening->update(['saldo' => $detail_rek['saldo']], $p['rek_id']);

			$waktu = date('Y-m-d H:i:s');
			$kode_jurnal = $this->transaksi_model->generate_jurnal();
			$this->db->insert('jurnal_master', [
				'kode_jurnal'   => $kode_jurnal,
				'tanggal_input' => $waktu,
				'keterangan'    => "Penarikan Modal ".$trans['kode_transaksi'],
			]);
			$last_jurnal = $this->transaksi_model->last_jurnal();

			$jurnal[0]['coa_id'] 	   = '23'; //Prive
			$jurnal[0]['jurnal_master_id'] = $last_jurnal['id'];
			$jurnal[0]['transaksi_id'] = $trans['id'];
			$jurnal[0]['tgl_jurnal']   = $waktu;
			$jurnal[0]['posisi']	   = 'debit';
			$jurnal[0]['nominal']      = $p['total_transaksi'];

			$jurnal[1]['coa_id'] 	   = '3'; //Bank
			$jurnal[1]['jurnal_master_id'] = $last_jurnal['id'];
			$jurnal[1]['transaksi_id'] = $trans['id'];
			$jurnal[1]['tgl_jurnal']   = $waktu;
			$jurnal[1]['posisi']	   = 'kredit';
			$jurnal[1]['nominal']      = $p['total_transaksi'];

			$this->db->insert_batch('jurnal', $jurnal);

			if($this->db->trans_status()){
				$this->db->trans_commit();
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan','success'));
			}else{
				$this->db->trans_rollback();
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fa fa-minus-circle"></i></b> Data gagal dimasukkan'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-warning"><i class="fas fa-info-circle"></i></b> Form tidak valid<br>'.validation_errors(),'warning'));
		}

		redirect('keuangan/modal/penarikan');

	}

	public function setor()
	{
		// print_r($this->session->userdata('user_data')['id']); die;
		$data['last_code'] 	= $this->transaksi_model->generate_code('setoran_modal');
		$rek	   			= $this->m_rekening->get_data();
		$data['data'] 		= $this->m_modal->get_data();
		$data['pemilik'] 	= $this->m_pemilik->get_data();

		$data['rek'] = [];
		foreach ($rek as $el)
		{
			if ( ($el['kode_rek'] == 'REK-0001') || ($el['kode_rek'] == 'REK-0003') || ($el['kode_rek'] == 'REK-0004') )
			{
				array_push($data['rek'], $el);
			}
		}
		
		$this->template->load('layout/template','transaksi/keuangan/modal/setoran', $data);
	}

	public function add_setoran()
	{
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');
		$nominal = $this->input->post('nominal');

		$rek_id = $this->input->post('rek_id');
		$getRek =  $this->m_rekening->get_detail('id', $rek_id)->row();
		$saldoLama = $getRek->saldo;
		$saldoBaru = $saldoLama + $nominal;
		$dataUpdate = [
			'saldo' => $saldoBaru
		];
		$update = $this->m_rekening->update($dataUpdate, $rek_id);

		$data = [
			'kode_setoran' => $this->input->post('kode_transaksi'),
			'tanggal_setoran' => date('Y-m-d'),
			'id_pemilik' => $this->input->post('id_pemilik'),
			'rek_id' => $rek_id,
			'nominal' => $nominal
		];

		
		$insert = $this->m_modal->insert($data);

		if ($getRek->kode_rek == 'REK-0001')
		{
			$coa = 111;
		}
		if ($getRek->kode_rek == 'REK-0003')
		{
			$coa = 62;
		}
		if ($getRek->kode_rek == 'REK-0004')
		{
			$coa = 60;
		}

		$jurnal1 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 1,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'debit',
			'nominal'                   => $nominal,
			'status'                    => 0
		];

		$jurnal2 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => $coa,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'kredit',
			'nominal'                   => $nominal,
			'status'                    => 0
		];


		$dataJurnal = [$jurnal1, $jurnal2];

		$insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);

		if($insert && $update){
            $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
        }else{
            $this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diinput','danger'));
        }
        redirect('keuangan/modal/setor');
	}

    
}