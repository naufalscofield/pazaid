<?php 

class Bop extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->model('transaksi_model');
		$this->load->model('keuangan/rekening_model', 'm_rekening');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','keuangan');
	}

	public function get_page($page){
		if(in_array($page, ['masuk','keluar'])){
			$find = [
				'is_created' => '1',
				'tipe'		 => 'bop'
			];

			$data['last_code'] = $this->transaksi_model->generate_code('bop');
			$data['tipe'] = $page;
			$data['list'] = $this->transaksi_model->get_detail($find)->result_array();
			$data['rek']	   = $this->m_rekening->get_data();

			$this->template->load('layout/template','transaksi/keuangan/bop/index', $data);

		}else{
			show_404();
		}
	}

	public function add(){
		$p = $this->input->post();
		$p['total_transaksi'] = format_angka($p['total_transaksi']);

		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('nama_instuisi', 'Nama Instuisi', 'required');
		$this->form_validation->set_rules('tanggal_transaksi', 'Tanggal Transaksi', 'required');
		$this->form_validation->set_rules('tanggal_selesai', 'Jangka Waktu', 'required');
		$this->form_validation->set_rules('total_transaksi', 'Nominal', 'required|numeric|greater_than[0]');

		if($this->form_validation->run() == TRUE){
			$now    = strtotime(date('Y-m-d'));
			$terima = strtotime($p['tanggal_transaksi']);
			$jangka = strtotime($p['tanggal_selesai']);

			if($jangka > $now){
				if($terima <= $now){
					$this->db->trans_begin();

					$waktu = date('H:i:s');
					$p['tanggal_transaksi'] = $p['tanggal_transaksi']." ".$waktu;
					$p['tipe']   = 'bop';
					$p['level']  = '3';
					$p['status'] = 'Belum Lunas';
					$p['metode'] = 'Transfer';
					$p['pembayaran'] = 'Tunai';
					$p['jenis']		 = 'masuk';
					$p['is_created'] = '1';
					$p['sisa_bayar'] = $p['total_transaksi'];

					$this->transaksi_model->insert($p);
					$last_trans = $this->transaksi_model->last_data('bop');

					$this->transaksi_model->insert_pembayaran([
						'transaksi_id'  => $last_trans['id'],
						'jumlah_bayar'  => $p['total_transaksi'],
						'tanggal_bayar' => $p['tanggal_transaksi']
					]);

					$last_paid = $this->transaksi_model->get_last_pembayaran($last_trans['id']);
					$rek = [
						'rek_id' 				  => $p['rek_id'],
						'transaksi_pembayaran_id' => $last_paid['id']
					];
					$this->m_rekening->insert_history($rek);

					$current = $this->db->where('id', $p['rek_id'])
									 	->get('k_rek')->row_array()['saldo'];

					$this->db->where('id', $p['rek_id'])
							 ->update('k_rek', [
							 	'saldo' => $current + $p['total_transaksi']
							 ]);

					$waktu = date('Y-m-d', $terima);
					$jurnal[] = [
		      			'coa_id' 	 => '3', //BANK
		      			'keterangan' => '',
		      			'tgl_jurnal' => $waktu,
		      			'posisi'	 => 'debit',
		      			'nominal'    => $p['total_transaksi']
		      		];

		      		$jurnal[] = [
		      			'coa_id' 	 => '13', //PENDAPATAN BOP
		      			'keterangan' => '',
		      			'tgl_jurnal' => $waktu,
		      			'posisi'	 => 'kredit',
		      			'nominal'    => $p['total_transaksi']
		      		];
		      		$this->db->insert_batch('jurnal', $jurnal);

					if($this->db->trans_status()){
						$this->db->trans_commit();
						$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-check"></i> Data berhasil dimasukkan','success'));
					}else{
						$this->db->trans_rollback();
						$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dimasukkan','danger'));
					}

				}else{
					$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-warning"></i> Tanggal penerimaan harus dibawah atau sama dengan tanggal sekarang','warning'));

				}

			}else{
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-warning"></i> Tanggal jangka waktu harus diatas tanggal sekarang','warning'));

			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
		}

		redirect('keuangan/bop/masuk');
	}

	public function detail($transaksi_id){
		$trans = $this->transaksi_model->get_detail('transaksi.id', $transaksi_id, 'bop');
		
		if($trans->num_rows() > 0){
			$data['transaksi']  = $trans->row_array();
			$data['item']       = $this->transaksi_model->get_list_item('bop', $data['transaksi']['id']);
			$data['total_penggunaan'] = $this->transaksi_model->get_total_list_item($data['transaksi']['tipe'], $data['transaksi']['id']);
			$data['rek']	   = $this->m_rekening->get_data();

			$this->template->load('layout/template','transaksi/keuangan/bop/detail', $data);
		
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> ID Transaksi tidak diketahui','danger'));
			redirect('keuangan/bop/keluar');
		}	
	}

	public function add_bop_keluar($transaksi_id){
		$p = $this->input->post();
		unset($p['kode_transaksi']);
		if(isset($p['rek_id'])){
			$rek_id = $p['rek_id'];
		}

		$p['transaksi_id'] 		  = $transaksi_id;
		$p['subtotal'] 			  = format_angka($p['subtotal']);
		$p['tanggal_pengeluaran'] = date('Y-m-d');

		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('subtotal', 'Nominal', 'required|numeric|greater_than[0]');
		$this->form_validation->set_rules('keterangan_pengeluaran', 'Keterangan', 'required');

		if($this->form_validation->run() == TRUE){

			$this->db->trans_begin();

			$this->transaksi_model->insert_bop_keluar($p);

			if($this->db->trans_status()){
				$this->db->trans_commit();
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan','success'));
			}else{
				$this->db->trans_rollback();
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dimasukkan','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
		}

		redirect('keuangan/bop/keluar/detail/'.$transaksi_id);
	}
    
}