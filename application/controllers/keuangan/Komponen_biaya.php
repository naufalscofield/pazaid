<?php 

class Komponen_biaya extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('keuangan/komponen_model', 'm_komponen');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','keuangan');
	}

	public function index(){
		$data   = $this->m_komponen->get_data();
		$this->template->load('layout/template','transaksi/keuangan/komponen_biaya/index', compact('data'));
	}

	public function add(){
        $post = $this->input->post();
        $this->id_komponen          = uniqid();
        $this->nama_komponen        = $post['nama_komponen'];
        $this->tipe_komponen        = $post['tipe_komponen'];
        $this->harga_komponen       = $post['harga_komponen'];
        $this->status_cicil         = $post['status_cicil'];
        $this->form_validation->set_data($post);
        $this->form_validation->set_rules('nama_komponen', 'Nama Komponen', 'required');

        if($this->m_komponen->insert($post)){
            $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
        }else{
            $this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diinput','danger'));
        }
        redirect('keuangan/komponenbiaya');

	}

	public function update()
    {
        $p  = $this->input->post();
		$id = $p['id_komponen'];
		unset($p['id_komponen']);
        
		$this->form_validation->set_data($p);
		// var_dump($p);
		// die;
		$this->form_validation->set_rules('nama_komponen', 'Nama Komponen', 'required');

		if($this->form_validation->run() == TRUE){

			if($this->m_komponen->update($p, $id)){
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diubah','success'));
			}else{
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diubah','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
        }
        redirect('keuangan/komponenbiaya');
    }

	public function delete($id){
		if($this->m_komponen->delete($id)){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dihapus','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dihapus','danger'));
		}

		redirect('keuangan/komponenbiaya');
	}

}