<?php 

class Rekening extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->model('keuangan/Rekening_model', 'm_rek');
		$this->load->model('transaksi_model');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','keuangan');
	}

	public function index(){
		$data['list']      = $this->m_rek->get_data();
		// var_dump($data['list']); die;
		$data['last_code'] = $this->m_rek->generate_code();
		$this->template->load('layout/template','master_data/keuangan/rekening/index', $data);
	}

	public function add(){
		$p = $this->input->post();

		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('kode_rek', 'Kode Rek', 'required|is_unique[k_rek.no_rek]');

		if($this->form_validation->run() == TRUE){

			if($this->m_rek->insert($p)){
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-check"></i> Data berhasil dimasukkan','success'));
			}else{
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dimasukkan','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
		}

		redirect('keuangan/rekening');
	}

	public function update(){
		$p  = $this->input->post();
		$id = $p['id_rekening'];
		$s  = false;

		unset($p['id_rekening']);

		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('no_rek', 'No Rek', 'required');

		$cek = $this->m_rek->get_detail('id', $id)->row_array();
		
		if($p['no_rek'] == $cek['no_rek']){
			$s = true;
		
		}else{
			$cek = $this->m_rek->get_detail('no_rek', $p['no_rek'])->num_rows();
			if($cek == 0){
				$s = true;
			}
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-warning"></i> No Rek sudah ada','warning'));
		}

		if($s){
			if($this->form_validation->run() == TRUE){

				if($this->m_rek->update($p, $id)){
					$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-check"></i> Data berhasil diubah','success'));
				}else{
					$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diubah','danger'));
				}

			}else{
				$this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
			}
		}

		redirect('keuangan/rekening');
	}

	public function delete($rek_id){

		if($this->m_rek->delete($rek_id)){
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-check"></i> Data berhasil dihapus','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dihapus','danger'));
		}

		redirect('keuangan/rekening');
	}

	public function ajaxRekening()
	{
		$data = $this->m_rek->get_data();
		echo json_encode($data);
	}

	public function ajaxNoRekening($kode_rek)
	{
		$data = $this->m_rek->get_detail('kode_rek', $kode_rek)->row();
		echo json_encode($data);
	}
    
}