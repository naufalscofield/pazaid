<?php 

class Jurnal extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->model('keuangan/Rekening_model', 'm_rek');
		$this->load->model('transaksi_model');
		$this->load->model('keuangan/Jurnal_model', 'm_jurnal');
		$this->load->model('keuangan/coa_model', 'm_coa');
		$this->load->model('asset/Penyusutan_model', 'm_penyusutan');

		if(!$this->session->userdata('login')){
			redirect('');
		}
	}

	public function index(){
		$this->m_penyusutan->set_penyusutan_to_jurnal();
		$data['list'] = $this->db->select('jurnal_master.*, SUM(nominal) AS total_nominal')
								 ->order_by('jurnal_master.id', 'DESC')
								 ->join('jurnal', 'jurnal.jurnal_master_id = jurnal_master.id')
								 ->where('posisi', 'debit')
								 ->group_by('jurnal_master.id')
								 ->get('jurnal_master')->result_array();
		$this->template->load('layout/template','transaksi/keuangan/jurnal/index', $data);
	}

	public function add(){
		$data['coa'] = $this->m_coa->get_data();
		$this->template->load('layout/template','transaksi/keuangan/jurnal/add', $data);
	}

	public function post_jurnal($trans_id){
		$p = $this->input->post();

		$this->db->trans_begin();
		$data = [
			'is_post' => '1',
			'waktu_post' => date('Y-m-d H:i:s')
		];
		$this->db->where('id', $trans_id)
				 ->update('jurnal_master', $data);

		if(!empty($p)){
			foreach ($p['coa_id'] as $key => $val) {
				$this->db->where('id', $key)
						 ->update('jurnal', ['coa_id' => $val]);
			}
		}

		if($this->db->trans_status()){
			$this->db->trans_commit();
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Jurnal berhasil diposting', 'success'));

		}else{
			$this->db->trans_rollback();
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-minus-circle"></i></b> Terjadi Kesalahan', 'danger'));
		}
		
		redirect('keuangan/transaksi/jurnal/detail/'.$trans_id);
	}

	public function detail($trans_id){

		$cek = $this->db->where('id', $trans_id)->get('jurnal_master');
		if($cek->num_rows() > 0){
			$data['jurnal']		 = $cek->row_array();
			$data['jurnal_list'] = $this->db->select('*, jurnal.id AS jurnal_id')
											->where('jurnal_master_id', $trans_id)
											->join('coa', 'coa.id = jurnal.coa_id')
											->order_by('jurnal.id', 'ASC')
											->get('jurnal')->result_array();
			$this->template->load('layout/template','transaksi/keuangan/jurnal/detail', $data);

		}else{
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fa fa-minus-circle"></i></b> ID Transaksi tidak ditemukan', 'danger'));
			redirect('keuangan/transaksi/jurnal');
		}
	}
	
	public function insert(){
		$p = $this->input->post();
		$waktu = date('Y-m-d H:i:s');
		$this->db->trans_begin();
		$data = [
			'kode_transaksi' => $this->transaksi_model->generate_code('jurnal'),
			'tanggal_transaksi' => $waktu,
			'jenis'	=> 'none',
			'tipe'  => 'jurnal',
			'level' => '3'
		];
		$this->transaksi_model->insert($data);

		$last = $this->db->where('tipe', 'jurnal')
						 ->order_by('id', 'DESC')
						 ->limit(1)->get('transaksi')->row_array();

		$kode_jurnal = $this->transaksi_model->generate_jurnal();
		$this->db->insert('jurnal_master', [
			'kode_jurnal'   => $kode_jurnal,
			'tanggal_input' => $waktu,
			'keterangan'    => $p['keterangan'],
			'source'		=> 'Manual'
		]);
		$last_jurnal = $this->transaksi_model->last_jurnal();

		$data = [];
		foreach ($p['nominal'] as $key => $value){
			$data[] = [
				'jurnal_master_id' => $last_jurnal['id'],
				'transaksi_id' => $last['id'],
				'coa_id' 	 => $p['akun'][$key],
				'tgl_jurnal' => $waktu,
				'posisi'	 => $p['posisi_akun'][$key],
				'nominal'	 => format_angka($value)
			];
		}
		//echo "<pre>".print_r($data, true)."</pre>";
		$this->db->insert_batch('jurnal', $data);

		if($this->db->trans_status()){
			$this->db->trans_commit();
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan', 'success'));
			redirect('keuangan/transaksi/jurnal');

		}else{
			$this->db->trans_rollback();
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fa fa-minus-circle"></i></b> Terjadi kesalahan', 'danger'));
			redirect('keuangan/transaksi/jurnal/tambah');
		}
	}
    
}