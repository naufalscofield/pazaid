<?php 

class Neraca extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('keuangan/pemilik_model', 'm_pemilik');
		$this->load->model('keuangan/rekening_model', 'm_rekening');
		$this->load->model('keuangan/model_tunai', 'm_tunai');
		$this->load->model('Bank/model_bank','m_bank');
		$this->load->model('Bank/model_bunga','m_bunga');
		$this->load->model('Bank/model_koran','m_koran');
		$this->load->model('keuangan/model_neraca','m_neraca');
		$this->load->model('transaksi_model');
		$this->load->model('keuangan/jurnal_model', 'm_jurnal');
		if(!$this->session->userdata('login')){
			redirect('');
		}
		$this->session->set_userdata('menu','keuangan');
    }

	public function index()
	{
		$req = [
			'bulan'  => $this->input->get('bulan'),
			'tahun'	=> $this->input->get('tahun')
		 ];
		$data = $this->m_jurnal->get_neraca($req);
		$aktivaLancar 		 = [];
		$passivaJangkaPendek = [];
		$aktivaTetap		 = [];
		$passivaJangkaPanjang= [];
		$modal				 = [];

		foreach ($data as $el) {
			if ($el['coa_id'] == 26)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 25)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 41)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 3)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 110)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 29)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 31)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 33)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 35)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 43)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 44)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 45)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 48)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 46)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 9)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 49)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 50)
			{
				$dataLancar = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaLancar, $dataLancar);
			}
			if ($el['coa_id'] == 22)
			{
				$dataPassivaJangkaPendek = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($passivaJangkaPendek, $dataPassivaJangkaPendek);
			}
			if ($el['coa_id'] == 34)
			{
				$dataPassivaJangkaPendek = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($passivaJangkaPendek, $dataPassivaJangkaPendek);
			}
			if ($el['coa_id'] == 27)
			{
				$dataPassivaJangkaPendek = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($passivaJangkaPendek, $dataPassivaJangkaPendek);
			}
			if ($el['coa_id'] == 11)
			{
				$dataPassivaJangkaPendek = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($passivaJangkaPendek, $dataPassivaJangkaPendek);
			}
			if ($el['coa_id'] == 28)
			{
				$dataAktivaTetap = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaTetap, $dataAktivaTetap);
			}
			if ($el['coa_id'] == 2)
			{
				$dataAktivaTetap = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaTetap, $dataAktivaTetap);
			}
			if ($el['coa_id'] == 22)
			{
				$dataAktivaTetap = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($aktivaTetap, $dataAktivaTetap);
			}
			if ($el['coa_id'] == 24)
			{
				$dataAktivaTetap = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($modal, $dataModal);
			}
			if ($el['coa_id'] == 23)
			{
				$dataModal = [
					'coa_id'	=> $el['coa_id'],
					'nama_coa'	=> $el['nama_coa'],
					'nominal'	=> $el['nominal'],
				];

				array_push($modal, $dataModal);
			}
		}
		$this->template->load('layout/template','transaksi/keuangan/laporan/neraca', compact('aktivaLancar', 'passivaJangkaPendek', 'aktivaTetap', 'passivaJangkaPanjang', 'modal'));
	}
	
    
}