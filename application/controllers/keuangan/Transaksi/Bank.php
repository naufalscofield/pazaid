<?php 

class Bank extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('keuangan/Periode_model', 'm_periode');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','keuangan');
    }

	public function index()
	{
		$data['periode'] 	= $this->m_periode->get_data();
		$data['get_code']	= $this->m_periode->generate_code_bank();
		$this->template->load('layout/template','transaksi/keuangan/Siswa/Bank/index', $data);
	}
	
	public function add()
	{
		$post = $this->input->post();
		$this->id_periode       = uniqid();
		$this->kode_periode   	= $post['kode_periode'];
		$this->jumlah_tagihan  	= $post['jumlah_tagihan'];
		$this->status     		= $post['status'];
		$this->form_validation->set_data($post);
		$this->form_validation->set_rules('kode_periode', 'Kode periode', 'required|is_unique[k_periode.kode_periode]');
		$this->form_validation->set_rules('jumlah_tagihan', 'jumlah tagihan', 'required');

		if($this->form_validation->run() == TRUE){

			if($this->m_periode->insert($post)){
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan','success'));
                $data['get_code']   = $this->m_periode->generate_code_bank();
            }else{
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dimasukkan','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
		}

		redirect('keuangan/tagihan/siswa/bank');
	}
	
	public function update()
	{
		$p  = $this->input->post();
		$id = $p['kode_periode'];

		unset($p['kode_periode']);

		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('jumlah_periode', 'Jumlah Periode', 'required');

		if($this->form_validation->run() == TRUE){

			if($this->m_periode->update($p, $id)){
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diubah','success'));
			}else{
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diubah','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
		}

		redirect('keuangan/tagihan/siswa/bank');
	}

	public function delete($id)
	{
		if($this->m_periode->delete($id)){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dihapus','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dihapus','danger'));
		}

		redirect('keuangan/tagihan/siswa/bank');
	}

}