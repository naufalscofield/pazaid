<?php 

class PenerimaanKas extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('akademik/Transaksi/model_transaksi_pendaftaran', 'm_transaksi');
		$this->load->model('akademik/Transaksi/model_transaksi_spp', 'm_transaksi_spp');
		$this->load->model('akademik/MasterData/Model_Daycare', 'm_daycare');
		$this->load->model('Akademik/MasterData/Master_model', 'm_master');
		$this->load->model('Keuangan/Jurnal_model', 'm_jurnal');
		$this->load->model('Keuangan/model_tunai', 'm_tunai');
		$this->load->model('Keuangan/rekening_model', 'm_rekening');


		$this->load->model('Akademik/masterdata/kelas_model', 'm_kelas');
		$this->load->model('Akademik/transaksi/model_tagihan_lainnya', 'm_lainnya');
		$this->load->model('Akademik/MasterData/Master_model', 'm_tahun');
		$this->load->model('akademik/Transaksi/model_siswa', 'm_siswa');
        $this->load->model('Akademik/Transaksi/model_transaksi_pendaftaran', 'm_daftar');


		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','keuangan');
    }

	public function index()
	{
	}

	public function PembayaranPendaftaran()
	{

		$tahun = $this->m_master->get_active();
		$id_tahun = $tahun->id_tahun;
		$data   = $this->m_siswa->get_data();
        $siswa  = $data[0]['id_siswa'];
		$daftar = $this->m_daftar->get_tagihan($siswa,$id_tahun);
		// var_dump($daftar); die;
		$this->template->load('layout/template', 'transaksi/keuangan/penerimaan_kas/pembayaran_pendaftaran', compact('data','daftar'));
	}

	public function PembayaranNaikKelas()
	{
		$tahun = $this->m_master->get_active();
		$id_tahun = $tahun->id_tahun;

		$data   = $this->m_siswa->get_data();
        $siswa  = $data[0]['id_siswa'];
        $daftar = $this->m_daftar->get_tagihan($siswa, $id_tahun);
		$this->template->load('layout/template', 'transaksi/keuangan/penerimaan_kas/pembayaran_pendaftaran_naikkelas', compact('data','daftar'));
	}
	
	public function PembayaranLainnya()
	{
		$data1  = $this->m_lainnya->get_data();
		// var_dump($data1);
		// die;
		$data2  = $this->m_kelas->get_data();
        $data   = [];
        // foreach($data1 as $siswa)
        // {
        //     foreach($data2 as $kelas)
        //     {
        //         if($siswa['id_kelas'] == $kelas['id_kelas'])
        //         {
        //             $tahun  = $this->m_tahun->getById($siswa['id_tahun']);
        //             $lahir  = new DateTime($siswa['tanggal_lahir']);
        //             $today  = new DateTime();
        //             $umur   = $today->diff($lahir)->y;
        //             $arr = [
        //                 'id_siswa'          => $siswa['id_siswa'],
        //                 'id_kelas'          => $siswa['id_kelas'],
        //                 'id_tahun'          => $siswa['id_tahun'],
        //                 'nis'               => $siswa['nis'],
        //                 'nama_siswa'        => $siswa['nama_siswa'],
        //                 'panggilan_siswa'   => $siswa['panggilan_siswa'],
        //                 'panggilan_siswa'   => $siswa['panggilan_siswa'],
        //                 'tempat_lahir'      => $siswa['tempat_lahir'],
        //                 'tanggal_lahir'     => $siswa['tanggal_lahir'],
        //                 'umur'              => $umur,
        //                 'jenis_kelamin'     => $siswa['jenis_kelamin'],
        //                 'agama'             => $siswa['agama'],
        //                 'jumlah_saudara'    => $siswa['jumlah_saudara'],
        //                 'anak_ke'           => $siswa['anak_ke'],
        //                 'alamat'            => $siswa['alamat'],
        //                 'status_ortu'       => $siswa['status'],
        //                 'status_siswa'      => $siswa['status_siswa'],
        //                 'status_bayar'      => $siswa['status_bayar'],
        //                 'kode_kelas'        => $kelas['kode_kelas'],
        //                 'nama_kelas'        => $kelas['nama_kelas'],
        //                 'level_kelas'       => $kelas['level_kelas'],
        //                 'status_kelas'      => $kelas['status'],
        //                 'kode_tahun'        => $tahun[0]->kode_tahun,
        //                 'created_at'        => $tahun[0]->created_at
        //             ];                       
        //         array_push($data, $arr);
        //         }
        //     }
        // }
		$this->template->load('layout/template', 'transaksi/keuangan/penerimaan_kas/pembayaran_lainnya', compact('data1'));
	}

	public function ajaxTagihanLainnya($nis)
	{
		$data = $this->m_lainnya->get_tagihan($nis);

		$overtime = 0;
		$catering = 0;
		$sarapan = 0;
		$mandi = 0;

		foreach($data as $el)
		{
			if ($el['jenis_tambahan'] == 'overtime')
			{
				$overtime += $el['total_tagihan'];
			}
			if ($el['jenis_tambahan'] == 'catering')
			{
				$catering += $el['total_tagihan'];
			}
			if ($el['jenis_tambahan'] == 'sarapan')
			{
				$sarapan += $el['total_tagihan'];
			}
			if ($el['jenis_tambahan'] == 'mandi')
			{
				$mandi += $el['total_tagihan'];
			}
		}

		$resp = [
			'nis' => $nis,
			'nama_siswa' => $data[0]['nama_siswa'],
			'overtime' => $overtime,
			'catering' => $catering,
			'sarapan' => $sarapan,
			'mandi' => $mandi,
			'total' => $overtime+$catering+$sarapan+$mandi
		];

		echo json_encode($resp);
	}

	public function StorePembayaranLainnya()
	{
		// print_r($this->input->post()); die;
		$nis = $this->input->post('nis_input');
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');

		if ($jenis_pembayaran == 'cash')
		{
			$kode_rek = NULL;
		} else {
			$kode_rek = $this->input->post('no_rek');
		}
		$data = [
			'status_lainnya' => 'Lunas',
			'jenis_pembayaran' => $jenis_pembayaran,
			'kode_rek' => $kode_rek,
			'tanggal_transaksi' => date('Y-m-d')
		];

		$insertLainnya 	= $this->m_lainnya->update($nis, $data);
		

		//Jurnal Overtime
		if ($this->input->post('total_overtime') != 0 || $this->input->post('total_overtime') != '')
		{
			$getId			= $this->m_lainnya->get_detail_overtime('nis', $nis)->row();
			$id 			= $getId->id;
			if ($jenis_pembayaran == 'cash')
			{
				$coa = 26;
				$saldoTunai = $this->m_tunai->get_detail('id', 1)->row();
				$saldo = $saldoTunai->saldo;
				$bayar = $this->input->post('total_overtime');
				$saldoNew = $saldo + $bayar;
				$dataUpd = [
					'saldo'	=> $saldoNew
				];
				$update = $this->m_tunai->update($dataUpd, 1);
			} else {
				$coa = 3;
				$getRekening = $this->m_rekening->get_detail('no_rek', $this->input->post('no_rek'))->row();
				// print_r($getRekening);die;
				$saldo = $getRekening->saldo;
				$bayar = $this->input->post('total_overtime');
				$saldoNew = $saldo + $bayar;
				$dataUpd = [
					'saldo'	=> $saldoNew
				];
				$update = $this->m_rekening->update($dataUpd, $getRekening->id);
			}

				$jurnal = [
					'jurnal_master_id'          => NULL,
					'coa_id'                    => $coa,
					'transaksi_lainnya_id'   	=> $id,
					'alt_name'                  => NULL,
					'tgl_jurnal'                => date('Y-m-d h:i:s'),
					'posisi'                    => 'debit',
					'nominal'                   => $this->input->post('total_overtime'),
					'status'                    => 0
				];

				$jurnal1 = [
					'jurnal_master_id'          => NULL,
					'coa_id'                    => 43,
					'transaksi_lainnya_id'  	=> $id,
					'alt_name'                  => NULL,
					'tgl_jurnal'                => date('Y-m-d h:i:s'),
					'posisi'                    => 'kredit',
					'nominal'                   => $this->input->post('total_overtime'),
					'status'                    => 0
				];

				$dataJurnal = [$jurnal, $jurnal1];
		
				$insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);
		}

		//Jurnal Catering
		if ($this->input->post('total_catering') != 0 || $this->input->post('total_catering') != '')
		{
			$getId			= $this->m_lainnya->get_detail_catering('nis', $nis)->row();
			$id 			= $getId->id;
			if ($jenis_pembayaran == 'cash')
			{
				$coa = 26;
				$saldoTunai = $this->m_tunai->get_detail('id', 1)->row();
				$saldo = $saldoTunai->saldo;
				$bayar = $this->input->post('total_catering');
				$saldoNew = $saldo + $bayar;
				$dataUpd = [
					'saldo'	=> $saldoNew
				];
				$update = $this->m_tunai->update($dataUpd, 1);
			} else {
				$coa = 3;
				$getRekening = $this->m_rekening->get_detail('no_rek', $this->input->post('no_rek'))->row();
				// print_r($getRekening);die;
				$saldo = $getRekening->saldo;
				$bayar = $this->input->post('total_catering');
				$saldoNew = $saldo + $bayar;
				$dataUpd = [
					'saldo'	=> $saldoNew
				];
				$update = $this->m_rekening->update($dataUpd, $getRekening->id);
			}

				$jurnal = [
					'jurnal_master_id'          => NULL,
					'coa_id'                    => $coa,
					'transaksi_lainnya_id'   	=> $id,
					'alt_name'                  => NULL,
					'tgl_jurnal'                => date('Y-m-d h:i:s'),
					'posisi'                    => 'debit',
					'nominal'                   => $this->input->post('total_catering'),
					'status'                    => 0
				];

				$jurnal1 = [
					'jurnal_master_id'          => NULL,
					'coa_id'                    => 44,
					'transaksi_lainnya_id'  	=> $id,
					'alt_name'                  => NULL,
					'tgl_jurnal'                => date('Y-m-d h:i:s'),
					'posisi'                    => 'kredit',
					'nominal'                   => $this->input->post('total_catering'),
					'status'                    => 0
				];

				$dataJurnal = [$jurnal, $jurnal1];
		
				$insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);
		}

		//Jurnal Sarapan
		if ($this->input->post('total_sarapan') != 0 || $this->input->post('total_sarapan') != '')
		{
			$getId			= $this->m_lainnya->get_detail_sarapan('nis', $nis)->row();
			$id 			= $getId->id;
			if ($jenis_pembayaran == 'cash')
			{
				$coa = 26;
				$saldoTunai = $this->m_tunai->get_detail('id', 1)->row();
				$saldo = $saldoTunai->saldo;
				$bayar = $this->input->post('total_sarapan');
				$saldoNew = $saldo + $bayar;
				$dataUpd = [
					'saldo'	=> $saldoNew
				];
				$update = $this->m_tunai->update($dataUpd, 1);
			} else {
				$coa = 3;
				$getRekening = $this->m_rekening->get_detail('no_rek', $this->input->post('no_rek'))->row();
				// print_r($getRekening);die;
				$saldo = $getRekening->saldo;
				$bayar = $this->input->post('total_sarapan');
				$saldoNew = $saldo + $bayar;
				$dataUpd = [
					'saldo'	=> $saldoNew
				];
				$update = $this->m_rekening->update($dataUpd, $getRekening->id);
			}

				$jurnal = [
					'jurnal_master_id'          => NULL,
					'coa_id'                    => $coa,
					'transaksi_lainnya_id'   	=> $id,
					'alt_name'                  => NULL,
					'tgl_jurnal'                => date('Y-m-d h:i:s'),
					'posisi'                    => 'debit',
					'nominal'                   => $this->input->post('total_sarapan'),
					'status'                    => 0
				];

				$jurnal1 = [
					'jurnal_master_id'          => NULL,
					'coa_id'                    => 45,
					'transaksi_lainnya_id'  	=> $id,
					'alt_name'                  => NULL,
					'tgl_jurnal'                => date('Y-m-d h:i:s'),
					'posisi'                    => 'kredit',
					'nominal'                   => $this->input->post('total_sarapan'),
					'status'                    => 0
				];

				$dataJurnal = [$jurnal, $jurnal1];
		
				$insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);
		}

		//Jurnal Mandi
		if ($this->input->post('total_mandi') != 0 || $this->input->post('total_mandi') != '')
		{
			$getId			= $this->m_lainnya->get_detail_mandi('nis', $nis)->row();
			$id 			= $getId->id;
			if ($jenis_pembayaran == 'cash')
			{
				$coa = 26;
				$saldoTunai = $this->m_tunai->get_detail('id', 1)->row();
				$saldo = $saldoTunai->saldo;
				$bayar = $this->input->post('total_mandi');
				$saldoNew = $saldo + $bayar;
				$dataUpd = [
					'saldo'	=> $saldoNew
				];
				$update = $this->m_tunai->update($dataUpd, 1);
			} else {
				$coa = 3;
				$getRekening = $this->m_rekening->get_detail('no_rek', $this->input->post('no_rek'))->row();
				// print_r($getRekening);die;
				$saldo = $getRekening->saldo;
				$bayar = $this->input->post('total_mandi');
				$saldoNew = $saldo + $bayar;
				$dataUpd = [
					'saldo'	=> $saldoNew
				];
				$update = $this->m_rekening->update($dataUpd, $getRekening->id);
			}

				$jurnal = [
					'jurnal_master_id'          => NULL,
					'coa_id'                    => $coa,
					'transaksi_lainnya_id'   	=> $id,
					'alt_name'                  => NULL,
					'tgl_jurnal'                => date('Y-m-d h:i:s'),
					'posisi'                    => 'debit',
					'nominal'                   => $this->input->post('total_mandi'),
					'status'                    => 0
				];

				$jurnal1 = [
					'jurnal_master_id'          => NULL,
					'coa_id'                    => 48,
					'transaksi_lainnya_id'  	=> $id,
					'alt_name'                  => NULL,
					'tgl_jurnal'                => date('Y-m-d h:i:s'),
					'posisi'                    => 'kredit',
					'nominal'                   => $this->input->post('total_mandi'),
					'status'                    => 0
				];

				$dataJurnal = [$jurnal, $jurnal1];
		
				$insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);
		}
		

		if($insertLainnya){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diinput','danger'));
		}
        redirect('keuangan/pembayaranlainnya');
	}

	public function PelunasanPendaftaran()
	{
		$this->template->load('layout/template', 'transaksi/keuangan/penerimaan_kas/pelunasan_pendaftaran');
	}

	public function PelunasanNaikKelas()
	{
		$this->template->load('layout/template', 'transaksi/keuangan/penerimaan_kas/pelunasan_naikkelas');
	}

	public function PembayaranSpp()
	{
		$tahun = $this->input->post('id_tahun_ajaran');
		$bulan = $this->input->post('bulan_ke');

		if ($tahun == NULL && $bulan == NULL)
		{
			$this->template->load('layout/template', 'transaksi/keuangan/penerimaan_kas/pembayaran_spp');
		} else {
			$data  = $this->m_siswa->get_data_spp($tahun, $bulan)->result_array();
			$this->template->load('layout/template', 'transaksi/keuangan/penerimaan_kas/pembayaran_spp', compact('data'));

		}
	}

	public function PembayaranDaycare()
	{
		$data = $this->m_daycare->get();
		$this->template->load('layout/template', 'transaksi/keuangan/penerimaan_kas/pembayaran_daycare', compact('data'));

		// $id = $this->input->post('id');
		// $jenis_pembayaran = $this->input->post('jenis_pembayaran');
		// $kode_rek = $this->input->post('kode_rek');
	}

	public function StorePembayaranDaycare()
	{
		$jenis_pembayaran = $this->input->post('jenis_pembayaran');

		if ($jenis_pembayaran == 'cash')
		{
			$no_rek = NULL;
		} else {
			$no_rek = $this->input->post('no_rek');;
		}

		$data = [
			'jenis_pembayaran' => $jenis_pembayaran,
			'no_rek' => $no_rek,
			'status' => 'Sudah Bayar'
		];
		$updateDaycare 	= $this->m_daycare->storePembayaran($this->input->post('id_daycare'), $data);

		$getId			= $this->m_daycare->get_detail('id', $this->input->post('id_daycare'));
		$tagihanDaycare	= $getId->total_tagihan;

		if ($jenis_pembayaran == 'cash')
		{
			$coa = 26;
			$saldoTunai = $this->m_tunai->get_detail('id', 1)->row();
			$saldo = $saldoTunai->saldo;
			$bayar = $tagihanDaycare;
			$saldoNew = $saldo + $bayar;
			$dataUpd = [
				'saldo'	=> $saldoNew
			];
			$update = $this->m_tunai->update($dataUpd, 1);
		} else {
			$coa = 3;
			$getRekening = $this->m_rekening->get_detail('no_rek', $this->input->post('no_rek'))->row();
			// print_r($getRekening);die;
			$saldo = $getRekening->saldo;
			$bayar = $tagihanDaycare;
			$saldoNew = $saldo + $bayar;
			$dataUpd = [
				'saldo'	=> $saldoNew
			];
			$update = $this->m_rekening->update($dataUpd, $getRekening->id);
		}

			$jurnal = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => $coa,
				'transaksi_daycare_id'   	=> $this->input->post('id_daycare'),
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'debit',
				'nominal'                   => $tagihanDaycare,
				'status'                    => 0
			];

			$jurnal2 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => 46,
				'transaksi_daycare_id'  	=> $this->input->post('id_daycare'),
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'kredit',
				'nominal'                   => $tagihanDaycare,
				'status'                    => 0
			];

			$dataJurnal = [$jurnal, $jurnal2];
	
			$insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);

		if($updateDaycare){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diinput','danger'));
		}
        redirect('keuangan/pembayarandaycare');
	}

	public function ajaxDaycare($val)
	{
		$data 		= $this->m_daycare->show($val);

		echo json_encode($data);
	}

	public function ajaxTagihan($val)
	{
		$tahun = $this->m_master->get_active();
		$id_tahun = $tahun->id_tahun;

		$siswa 		= $this->m_siswa->get_detail('kode_pendaftaran', $val);
		if ($siswa->num_rows() == 0)
		{
			$response = [];

		} else {
			$siswaData = $siswa->row();
			$data 		= $this->m_transaksi->get_tagihan($siswaData->id_siswa, $id_tahun);
			$response	= [
				'siswa'		=> $siswaData,
				'tagihan'	=> $data
			];
		}

		echo json_encode($response);
	}

	public function ajaxTagihanNis($val)
	{
		$tahun = $this->m_master->get_active();
		$id_tahun = $tahun->id_tahun;

		$siswa 		= $this->m_siswa->get_detail('nis', $val);
		if ($siswa->num_rows() == 0)
		{
			$response = [];

		} else {
			$siswaData = $siswa->row();
			$data 		= $this->m_transaksi->get_tagihan($siswaData->id_siswa, $id_tahun);
			$response	= [
				'siswa'		=> $siswaData,
				'tagihan'	=> $data
			];
		}

		echo json_encode($response);
	}

	public function ajaxTagihanNaikKelas($val)
	{
		$tahun = $this->m_master->get_active();
		$id_tahun = $tahun->id_tahun;
		
		$siswa 		= $this->m_siswa->get_detail('nis', $val)->row();
		// print_r($siswa); die;
		$data 		= $this->m_transaksi->get_tagihan($siswa->id_siswa, $id_tahun);
		$response	= [
			'siswa'		=> $siswa,
			'tagihan'	=> $data
		];

		echo json_encode($response);
	}
	
	public function ajaxTagihanSpp($val)
	{
		$siswa 		= $this->m_siswa->get_detail_join($val)->row();
		// print_r($siswa->id_siswa); die;
		$tagihan 	= $this->m_transaksi_spp->get_tagihan($siswa->id_siswa)->row();
		$response	= [
			'siswa'		=> $siswa,
			'tagihan'	=> $tagihan
		];

		echo json_encode($response);
	}

	public function PelunasanSpp()
	{
		$id_spp 			= $this->input->post('id_spp');
		$getTagihan			= $this->m_transaksi_spp->get_detail('id', $id_spp)->row();
		$tagihanSpp			= $getTagihan->biaya_spp;
		$jenis_pembayaran 	= $this->input->post('jenis_pembayaran');

		if ($jenis_pembayaran == 'cash')
		{
			$kode_rek = NULL;
		} else{
			$kode_rek = $this->input->post('no_rek');
		}
		$data = [
			'status_spp' => 'Sudah Dibayar',
			'jenis_pembayaran' => $jenis_pembayaran,
			'kode_rek' => $kode_rek,
			'tanggal_transaksi' => date('Y-m-d')
		];
	
		if ($jenis_pembayaran == 'cash')
		{
			$coa = 26;
			$saldoTunai = $this->m_tunai->get_detail('id', 1)->row();
			$saldo = $saldoTunai->saldo;
			$bayar = $tagihanSpp;
			$saldoNew = $saldo + $bayar;
			$dataUpd = [
				'saldo'	=> $saldoNew
			];
			$update = $this->m_tunai->update($dataUpd, 1);
		} else {
			$coa = 3;
			$getRekening = $this->m_rekening->get_detail('no_rek', $this->input->post('no_rek'))->row();
			// print_r($getRekening);die;
			$saldo = $getRekening->saldo;
			$bayar = $tagihanSpp;
			$saldoNew = $saldo + $bayar;
			$dataUpd = [
				'saldo'	=> $saldoNew
			];
			$update = $this->m_rekening->update($dataUpd, $getRekening->id);
		}

			$jurnal = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => $coa,
				'transaksi_spp_id'   		=> $id_spp,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'debit',
				'nominal'                   => $tagihanSpp,
				'status'                    => 0
			];

			$jurnal2 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => 35,
				'transaksi_spp_id'  		=> $id_spp,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'kredit',
				'nominal'                   => $tagihanSpp,
				'status'                    => 0
			];

			$dataJurnal = [$jurnal, $jurnal2];
	
			$insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);

		if($this->m_transaksi_spp->update($id_spp, $data)){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diinput','danger'));
		}

        redirect('keuangan/pembayaranspp');
	}

	public function add()
	{
		//
		$jenisPembayaran = $this->input->post('jenis_pembayaran');
		//Get Tahun Ajaran
        $getCr = $this->m_master->get_active();
        $cr = $getCr->created_at;
        $date =  strtotime($cr);
        $tahun = date("Y", $date);
        $bulan = date("m", $date);
        
        //Get Jumlah Siswa Yang Sudah Bayar
        $getSiswa = $this->m_siswa->get_detail('status_bayar', 'Sudah Bayar')->result_array();
        $jumlahSiswa = count($getSiswa)+1;
        $noUrut = sprintf("%04d", $jumlahSiswa);
		
        //NIS
		$nis = "$tahun"."$bulan"."$noUrut";
		
		$post = $this->input->post();
		$id_siswa 			= $post['id_siswa'];
		$biaya_pendaftaran 	= $post['biaya_pendaftaran_input'];
		$biaya_seragam 		= $post['biaya_seragam_input'];
		$cicilan_dpp 		= $post['cicilan_dpp'];
		$cicilan_dsk 		= $post['cicilan_dsk'];
		$total_tagihan 		= $post['total_tagihan'];
		$status 		= 1;
		
		$total_pembayaran 	= $biaya_pendaftaran+$biaya_seragam+$cicilan_dpp+$cicilan_dsk;
		$sisa_tagihan		= $total_tagihan - $total_pembayaran;

		if ($total_pembayaran == $total_tagihan)
		{
			$status_bayar = 'Sudah Bayar';
			$updateDataSiswa = [
				'status_bayar' => $status_bayar,
				'nis' 			=> $nis,
				'kode_pendaftaran' => ''
			];
		}

		else 
		{
			$status_bayar = 'Belum Lunas';
			$updateDataSiswa = [
				'status_bayar' => $status_bayar,
				'nis' 			=> $nis
			];

		}

		// print_r($total_pembayaran); die;
		
		$dataStore = [
			'cicilan_dpp' 		=> $cicilan_dpp,
			'cicilan_dsk' 		=> $cicilan_dsk,
			'total_pembayaran' 	=> $total_pembayaran,
			'sisa_tagihan' 		=> $sisa_tagihan,
			'status' 			=> $status,
			'jenis_pembayaran'	=> $jenisPembayaran,
			'kode_rek'			=> $this->input->post('kode_rek'),
			'tanggal_transaksi'	=> date('Y-m-d')
		];

		

		$q = $this->m_transaksi->update($id_siswa, $dataStore);
		$getId = $this->m_transaksi->get_detail('id_siswa', $id_siswa);
		$id = $getId->id;
		$u = $this->m_siswa->update($updateDataSiswa, $id_siswa);

		if ($jenisPembayaran == 'cash')
		{
			$coa = 26;
			$saldoTunai = $this->m_tunai->get_detail('id', 1)->row();
			$saldo = $saldoTunai->saldo;
			$bayar = $cicilan_dpp+$cicilan_dsk;
			$saldoNew = $saldo + $bayar;
			$dataUpd = [
				'saldo'	=> $saldoNew
			];
			$update = $this->m_tunai->update($dataUpd, 1);
		} else {
			$coa = 3;
			$getRekening = $this->m_rekening->get_detail('no_rek', $this->input->post('kode_rek'))->row();
			$saldo = $getRekening->saldo;
			$bayar = $cicilan_dpp+$cicilan_dsk;
			$saldoNew = $saldo + $bayar;
			$dataUpd = [
				'saldo'	=> $saldoNew
			];
			$update = $this->m_rekening->update($dataUpd, $getRekening->id);
		}


			$jurnal = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => $coa,
				'transaksi_pendaftaran_id'  => $id,
				'transaksi_pembayaran_id'   => NULL,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'debit',
				'nominal'                   => $total_pembayaran,
				'status'                    => 0
			];
	
			$jurnal1 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => 110,
				'transaksi_pendaftaran_id'  => $id,
				'transaksi_pembayaran_id'   => NULL,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'kredit',
				'nominal'                   => $biaya_pendaftaran,
				'status'                    => 0
			];

			$jurnal3 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => 29,
				'transaksi_pendaftaran_id'  => $id,
				'transaksi_pembayaran_id'   => NULL,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'kredit',
				'nominal'                   => $biaya_seragam,
				'status'                    => 0
			];

			$jurnal5 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => 31,
				'transaksi_pendaftaran_id'  => $id,
				'transaksi_pembayaran_id'   => NULL,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'kredit',
				'nominal'                   => $cicilan_dpp,
				'status'                    => 0
			];

			$jurnal7 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => 33,
				'transaksi_pendaftaran_id'  => $id,
				'transaksi_pembayaran_id'   => NULL,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'kredit',
				'nominal'                   => $cicilan_dsk,
				'status'                    => 0
			];
	
			$dataJurnal = [$jurnal, $jurnal1, $jurnal3, $jurnal5, $jurnal7];
	
			$insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);
		

		
		if($q && $u){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diinput','danger'));
		}
        redirect('keuangan/pembayaranpendaftaran');

	}

	public function addPembayaranNaikKelas()
	{
		//Get Tahun Ajaran
        $getCr = $this->m_master->get_active();
        $cr = $getCr->created_at;
        $date =  strtotime($cr);
        $tahun = date("Y", $date);
        $bulan = date("m", $date);
        
        //Get Jumlah Siswa Yang Sudah Bayar
        $getSiswa = $this->m_siswa->get_detail('status_bayar', 'Sudah Bayar')->result_array();
        $jumlahSiswa = count($getSiswa)+1;
        $noUrut = sprintf("%04d", $jumlahSiswa);
		
        //NIS
		$nis = "$tahun"."$bulan"."$noUrut";
		
		$post = $this->input->post();
		$id_siswa 			= $post['id_siswa'];
		$biaya_pendaftaran 	= $post['biaya_pendaftaran_input'];
		$biaya_seragam 		= $post['biaya_seragam_input'];
		$cicilan_dpp 		= $post['cicilan_dpp'];
		$cicilan_dsk 		= $post['cicilan_dsk'];
		$total_tagihan 		= $post['total_tagihan'];
		$status 			= 1;
		
		$total_pembayaran 	= $biaya_pendaftaran+$biaya_seragam+$cicilan_dpp+$cicilan_dsk;
		$sisa_tagihan		= $total_tagihan - $total_pembayaran;

		if ($total_pembayaran == $total_tagihan)
		{
			$status_bayar = 'Sudah Bayar';
			$updateDataSiswa = [
				'status_bayar' => $status_bayar,
				'nis' 			=> $nis,
				'kode_pendaftaran' => ''
			];
		}
		else 
		{
			$status_bayar = 'Belum Lunas';
			$updateDataSiswa = [
				'status_bayar' => $status_bayar,
				'nis' 			=> $nis
			];
		}

		// print_r($total_pembayaran); die;
		
		$dataStore = [
			'cicilan_dpp' 		=> $cicilan_dpp,
			'cicilan_dsk' 		=> $cicilan_dsk,
			'total_pembayaran' 	=> $total_pembayaran,
			'sisa_tagihan' 		=> $sisa_tagihan,
			'status' 			=> $status
		];

		

		$q = $this->m_transaksi->update($id_siswa, $dataStore);
		$u = $this->m_siswa->update($updateDataSiswa, $id_siswa);

		$dataTransaksi = $this->m_transaksi->get_detail('id', $this->input->post('idtransaksi'));
		if ($dataTransaksi->jenis_pembayaran == 'cash')
		{
			$coa = 26;
			$saldoTunai = $this->m_tunai->get_detail('id', 1)->row();
			$saldo = $saldoTunai->saldo;
			$bayar = $cicilan_dpp+$cicilan_dsk+$biaya_seragam;
			$saldoNew = $saldo + $bayar;
			$dataUpd = [
				'saldo'	=> $saldoNew
			];
			$update = $this->m_tunai->update($dataUpd, 1);
		} else {
			$coa = 3;
			$getRekening = $this->m_rekening->get_detail('no_rek', $this->input->post('kode_rek'))->row();
			$saldo = $getRekening->saldo;
			$bayar = $cicilan_dpp+$cicilan_dsk+$biaya_seragam;
			$saldoNew = $saldo + $bayar;
			$dataUpd = [
				'saldo'	=> $saldoNew
			];
			$update = $this->m_rekening->update($dataUpd, $getRekening->id);
		}


			$jurnal1 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => $coa,
				'transaksi_pendaftaran_id'  => $this->input->post('idtransaksi'),
				'transaksi_pembayaran_id'   => NULL,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'debit',
				'nominal'                   => $cicilan_dpp+$cicilan_dsk+$biaya_seragam,
				'status'                    => 0
			];

			$jurnal2 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => 29,
				'transaksi_pendaftaran_id'  => $this->input->post('idtransaksi'),
				'transaksi_pembayaran_id'   => NULL,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'kredit',
				'nominal'                   => $biaya_seragam,
				'status'                    => 0
			];

			$jurnal3 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => 31,
				'transaksi_pendaftaran_id'  => $this->input->post('idtransaksi'),
				'transaksi_pembayaran_id'   => NULL,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'kredit',
				'nominal'                   => $cicilan_dpp,
				'status'                    => 0
			];

			$jurnal4 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => 33,
				'transaksi_pendaftaran_id'  => $this->input->post('idtransaksi'),
				'transaksi_pembayaran_id'   => NULL,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'kredit',
				'nominal'                   => $cicilan_dsk,
				'status'                    => 0
			];
	
	
			$dataJurnal = [$jurnal1, $jurnal2, $jurnal3, $jurnal4];
	
			$insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);

		if($q && $u){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diinput','danger'));
		}
        redirect('keuangan/pembayarannaikkelas');

	}

	public function addPelunasan()
	{
		$id_siswa = $this->input->post('id_siswa');
		$updateSiswa = [
			'kode_pendaftaran' => '',
			'status_bayar' => 'Sudah Bayar'
		];
		$q = $this->m_siswa->update($updateSiswa, $id_siswa);

		$cicilan_awal_dpp = $this->input->post('sudah_bayar_biaya_dpp_input');
		$cicilan_awal_dsk = $this->input->post('sudah_bayar_biaya_dsk_input');
		$pelunasan_dpp = $this->input->post('cicilan_dpp');
		$pelunasan_dsk = $this->input->post('cicilan_dsk');
		$total_pelunasan = $pelunasan_dpp + $pelunasan_dsk;
		$cicilan_dpp = $cicilan_awal_dpp + $pelunasan_dpp;
		$cicilan_dsk = $cicilan_awal_dsk + $pelunasan_dsk;
		$biaya_pendaftaran = $this->input->post('o_biaya_pendaftaran_input');
		$biaya_seragam = $this->input->post('o_biaya_seragam_input');
		$total_pembayaran = $biaya_pendaftaran + $biaya_seragam + $cicilan_dpp + $cicilan_dsk;
		// print_r($total_pembayaran); die;
		$sisa_tagihan = $this->input->post('sisa_tagihan_total');
		$sisa_tagihan_final = $sisa_tagihan - $total_pelunasan;

		$updateTagihan = [
			'cicilan_dpp' => $cicilan_dpp,
			'cicilan_dsk' => $cicilan_dsk,
			'total_pembayaran' => $total_pembayaran,
			'sisa_tagihan' => $sisa_tagihan_final,
			
		];

		$u = $this->m_transaksi->update($id_siswa, $updateTagihan);
		$getId = $this->m_transaksi->get_detail('id_siswa', $id_siswa);
		$id = $getId->id;
		
		if ($jenisPembayaran == 'cash')
		{
			$coa = 26;
			$saldoTunai = $this->m_tunai->get_detail('id', 1)->row();
			$saldo = $saldoTunai->saldo;
			$bayar = $cicilan_dpp+$cicilan_dsk;
			$saldoNew = $saldo + $bayar;
			$dataUpd = [
				'saldo'	=> $saldoNew
			];
			
			$update = $this->m_tunai->update($dataUpd, 1);
		} else {
			$coa = 3;
			$getRekening = $this->m_rekening->get_detail('no_rek', $this->input->post('kode_rek'))->row();
			$saldo = $getRekening->saldo;
			$bayar = $cicilan_dpp+$cicilan_dsk;
			$saldoNew = $saldo + $bayar;
			$dataUpd = [
				'saldo'	=> $saldoNew
			];
			$update = $this->m_rekening->update($dataUpd, $getRekening->id);
		}

		$jurnal1 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => $coa,
			'transaksi_pendaftaran_id'  => $id,
			'transaksi_pembayaran_id'   => NULL,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'debit',
			'nominal'                   => $total_pelunasan,
			'status'                    => 0
		];

		$jurnal5 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 31,
			'transaksi_pendaftaran_id'  => $id,
			'transaksi_pembayaran_id'   => NULL,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'kredit',
			'nominal'                   => $cicilan_dpp,
			'status'                    => 0
		];

		$jurnal7 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 33,
			'transaksi_pendaftaran_id'  => $id,
			'transaksi_pembayaran_id'   => NULL,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'kredit',
			'nominal'                   => $cicilan_dsk,
			'status'                    => 0
		];

		$dataJurnal = [$jurnal1, $jurnal5, $jurnal7];

		$insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);

		if($q && $u){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diinput','danger'));
		}
        redirect('keuangan/pelunasanpendaftaran');
	}
	
	

}