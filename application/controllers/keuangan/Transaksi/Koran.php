<?php 

class Koran extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('keuangan/pemilik_model', 'm_pemilik');
		$this->load->model('keuangan/rekening_model', 'm_rekening');
		$this->load->model('Bank/model_bunga','m_bunga');
		$this->load->model('Bank/model_koran','m_koran');
		$this->load->model('transaksi_model');
		$this->load->model('akademik/transaksi/model_transaksi_pendaftaran', 'm_daftar');
		$this->load->model('akademik/transaksi/model_tagihan_lainnya', 'm_lainnya');
		$this->load->model('akademik/transaksi/model_transaksi_spp', 'm_spp');
		$this->load->model('akademik/masterdata/model_daycare', 'm_daycare');
		$this->load->model('keuangan/transaksi_setor_modal', 'm_setor');
		$this->load->model('keuangan/transaksi_tarik_bank', 'm_tarik');
		$this->load->model('keuangan/model_bop', 'm_bop');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','keuangan');
    }

	public function index()
	{
		$data = $this->m_koran->get_data();
		// var_dump($data); die;
		$this->template->load('layout/template','transaksi/keuangan/koran', compact('data'));
	}
	
	public function ajaxKoran($id_rekening)
	{
		$data = $this->m_rekening->getBunga($id_rekening);
		echo json_encode($data);
	}
	
	public function ajaxBop()
	{
		$response = [];
		$data = $this->m_bop->get_data();
		foreach ($data as $el) 
			{
				$supply = [
					'tanggal'					=> $el['tanggal_pengeluaran'],
					'nominal'					=> $el['subtotal'],
					'nama_komponen'				=> $el['nama_komponen'],
					'keterangan_pengeluaran'	=> $el['keterangan_pengeluaran'],
				];

				array_push($response, $supply);
			}
		echo json_encode($response);
	}

	public function ajaxPenerimaanBank($no_rek)
	{
		$response = [];

		$getPendaftaran	= $this->m_daftar->get_where('kode_rek', $no_rek);
		if ($getPendaftaran->num_rows() < 0)
		{
			foreach ($getPendaftaran as $pendaftaran) 
			{
				$dataDaftar = [
					'tanggal'	=> $pendaftaran['tanggal_transaksi'],
					'nominal'	=> $pendaftaran['total_pembayaran'],
					'transaksi'	=> 'Transaksi Pendaftaran',
				];

				array_push($response, $dataDaftar);
			}
		}

		$getSpp	= $this->m_spp->get_where('kode_rek', $no_rek);
		if ($getSpp->num_rows() < 0)
		{
			foreach ($getSpp as $spp) 
			{
				$dataSpp = [
					'tanggal'	=> $spp['tanggal_transaksi'],
					'nominal'	=> $spp['biaya_spp'],
					'transaksi'	=> 'Transaksi SPP',
				];

				array_push($response, $dataSpp);
			}
		}

		$getLainnya	= $this->m_lainnya->get_where('kode_rek', $no_rek);
		if ($getLainnya->num_rows() < 0)
		{
			foreach ($getLainnya as $lainnya) 
			{
				$dataLainnya = [
					'tanggal'	=> $lainnya['creatd_at'],
					'nominal'	=> $lainnya['total_tagihan'],
					'transaksi'	=> 'Transaksi Daycare',
				];

				array_push($response, $dataLainnya);
			}
		}

		echo json_encode($response);

	}
    
	public function ajaxSetorBank($id)
	{
		$response = [];

		$getSetor	= $this->m_setor->get_detail('rek_id', $id);
		if ($getSetor->num_rows() < 0)
		{
			foreach ($getSetor as $setor) 
			{
				$dataDaftar = [
					'tanggal'	=> $setor['tanggal_setoran'],
					'nominal'	=> $setor['nominal'],
					'transaksi'	=> 'Transaksi Setor Modal',
				];

				array_push($response, $dataDaftar);
			}
		}


		echo json_encode($response);

	}
    
	public function ajaxPenarikanBank($id)
	{
		$response = [];

		$getPenarikan	= $this->m_tarik->get_detail('id_rekening', $id);
		if ($getPenarikan->num_rows() < 0)
		{
			foreach ($getPenarikan as $penarikan) 
			{
				$dataPenarikan = [
					'tanggal'	=> $penarikan['tanggal_penarikan'],
					'nominal'	=> $penarikan['nominal'],
					'transaksi'	=> 'Transaksi Penarikan Bank',
				];

				array_push($response, $dataPenarikan);
			}
		}


		echo json_encode($response);

	}
    
}