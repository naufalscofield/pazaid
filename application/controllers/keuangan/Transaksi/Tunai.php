<?php 

class Tunai extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('keuangan/periode_model', 'm_periode');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','keuangan');
    }

    public function index(){
        
		$this->template->load('layout/template','transaksi/keuangan/Siswa/Tunai/index');
    }
    
}