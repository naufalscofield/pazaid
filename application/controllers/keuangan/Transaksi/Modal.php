<?php 

class Modal extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('keuangan/pemilik_model', 'm_pemilik');
		$this->load->model('keuangan/rekening_model', 'm_rekening');
		$this->load->model('Bank/model_bank','m_bank');
		$this->load->model('transaksi_model');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','keuangan');
    }

    public function index(){
        
		$this->template->load('layout/template','transaksi/keuangan/');
    }
    
}