<?php 

class Bunga extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('keuangan/pemilik_model', 'm_pemilik');
		$this->load->model('keuangan/rekening_model', 'm_rekening');
		$this->load->model('Bank/model_bank','m_bank');
		$this->load->model('Bank/model_bunga','m_bunga');
		$this->load->model('Bank/model_koran','m_koran');
		$this->load->model('transaksi_model');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','keuangan');
    }

	public function index()
	{

		$data = $this->m_bunga->get_data();
		$rek = $this->m_rekening->get_data();
		$this->template->load('layout/template','transaksi/keuangan/bunga', compact('data', 'rek'));
	}
	
	public function insert()
	{
		$rekId 				= $this->input->post('rek_id');
		$deskripsi	 		= $this->input->post('deskripsi');
		$potonganBunga 		= $this->input->post('potongan_bunga');
		$potonganBungaFix 	= $potonganBunga / 100;
		// var_dump($potonganBungaFix); die;
		$tanggal			= date('Y-m-d');

		$getSaldo			= $this->m_rekening->get_detail('id', $rekId)->row();
		$saldoAkhir			= $getSaldo->saldo;

		$perolehanBunga		= $potonganBungaFix * $saldoAkhir;

		$data = [
			'id_rekening'		=> $rekId,
			'tanggal'			=> $tanggal,
			'saldo_akhir'		=> $saldoAkhir + $perolehanBunga,
			'potongan_bunga'	=> $potonganBunga,
			'perolehan_bunga'	=> $perolehanBunga
		];

		
		$insert = $this->m_bunga->insert($data);
		$insert_id = $this->db->insert_id();

		$data2 = [
			'id_bunga'	=> $insert_id,
			'deskripsi'	=> $deskripsi
		];

		$dataRek = [
			'saldo'		=> $saldoAkhir + $perolehanBunga
		];

		$insertKoran 	= $this->m_koran->insert($data2);
		$updateRek 		= $this->m_rekening->update($dataRek, $rekId);

		if($insert && $insertKoran && $updateRek){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data Diinputkan','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data Gagal Diinputkan','danger'));
		}

		redirect('keuangan/transaksi/bunga');

	}
    
}