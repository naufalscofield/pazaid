<?php 

class Bank extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('keuangan/pemilik_model', 'm_pemilik');
		$this->load->model('keuangan/rekening_model', 'm_rekening');
		$this->load->model('keuangan/model_tunai', 'm_tunai');
		$this->load->model('keuangan/transaksi_tarik_bank', 'm_tarik');
		$this->load->model('transaksi_model');
		$this->load->model('Keuangan/Jurnal_model', 'm_jurnal');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','keuangan');
	}

	public function index(){
		$data['list']      = $this->db->where('tipe', 'setoran_bank')
									  ->join('k_rek', 'k_rek.id = transaksi.rek_id')
									  ->get('transaksi')->result_array();
		$data['last_code'] = $this->transaksi_model->generate_code('setoran_bank');
		$data['rek']	   = $this->m_rekening->get_data();
		$this->template->load('layout/template','transaksi/keuangan/bank/setoran', $data);
	}

	public function add_setoran(){
		$p = $this->input->post();

		$p['total_transaksi'] = format_angka($p['total_transaksi']);
		$p['total_bayar']	  = $p['total_transaksi'];
		$p['tanggal_transaksi'] = date('Y-m-d H:i:s');
		$p['is_created']		= '1';
		$p['tipe']				= 'setoran_bank';
		$p['pembayaran']		= 'Tunai';
		$p['jenis']				= 'keluar';
		$p['metode']			= 'Cash';

		

		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('total_transaksi', 'Nominal', 'required|numeric|greater_than[0]');
		$this->form_validation->set_rules('rek_id', 'Rekening', 'required|numeric');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

		if($this->form_validation->run() == TRUE){

			$this->db->trans_begin();
			$this->transaksi_model->insert($p);

			
			if($this->db->trans_status()){
				$this->db->trans_commit();
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan','success'));

			}else{
				$this->db->trans_rollback();
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fa fa-minus-circle"></i></b> Data gagal dimasukkan'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-warning"><i class="fas fa-info-circle"></i></b> Form tidak valid<br>'.validation_errors(),'warning'));
		}

		redirect('keuangan/bank/setor');

	}

	public function penarikan()
	{
		$data['last_code'] 	= $this->transaksi_model->generate_code('setoran_modal');
		$data['rek']	   	= $this->m_rekening->get_data();
		$data['data'] 		= $this->m_tarik->get_data();
		$data['pemilik'] 	= $this->m_pemilik->get_data();
		
		$this->template->load('layout/template','transaksi/keuangan/bank/penarikan', $data);
	}

	public function add_penarikan()
	{
		$rek_id = $this->input->post('rek_id');
		$nominal = $this->input->post('nominal');

		$rekening = $this->m_rekening->get_detail('id', $rek_id)->row();

		$saldoAwalRek = $rekening->saldo;
		$saldoAkhirRek = $saldoAwalRek - $nominal;

		$dataRekening = [
			'saldo' => $saldoAkhirRek
		];

		$updateRekening = $this->m_rekening->update($dataRekening, $rek_id);
		
		if ($rek_id != 5)
		{
			$tunai = $this->m_tunai->get_detail('id', 1)->row();
	
			$saldoAwalTunai = $tunai->saldo;
			$saldoAkhirTunai = $saldoAwalTunai + $nominal;
	
			$dataTunai = [
				'saldo' => $saldoAkhirTunai
			];
			
			$updateTunai = $this->m_tunai->update($dataTunai, 1);
		} else {
			$tunai = $this->m_tunai->get_detail('id', 2)->row();
	
			$saldoAwalTunai = $tunai->saldo;
			$saldoAkhirTunai = $saldoAwalTunai + $nominal;
	
			$dataTunai = [
				'saldo' => $saldoAkhirTunai
			];
			
			$updateTunai = $this->m_tunai->update($dataTunai, 2);

		}
	
		//
		$dataTransaksi = [
			'tanggal_penarikan' => date('Y-m-d'),
			'id_user_penarik' => $this->session->userdata('user_data')['id'],
			'id_rekening' => $rek_id,
			'nominal' => $nominal
		];

		$insertTransaksi = $this->m_tarik->insert($dataTransaksi);

		$jurnal1 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 25,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'debit',
			'nominal'                   =>  $nominal,
			'status'                    => 0
		];

		$jurnal2 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 38,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'kredit',
			'nominal'                   => $nominal,
			'status'                    => 0
		];


		$dataJurnal = [$jurnal1, $jurnal2];

		$insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);

		if($insertTransaksi && $updateRekening && $updateTunai){
            $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
        }else{
            $this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diinput','danger'));
        }
        redirect('keuangan/bank/tarik');


	}
    
}