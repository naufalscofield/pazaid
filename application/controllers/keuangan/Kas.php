<?php 

class Kas extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->model('transaksi_model');
		$this->load->model('keuangan/Jurnal_model','m_jurnal');
		$this->load->model('keuangan/rekening_model','m_rekening');
		//$this->load->model('asset/Aktivhru_model', 'm_aktiva');
		//$this->load->model('asset/Kategori_model', 'm_kategori');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','keuangan');
	}

	public function kas_keluar(){

		$data['list'] = $this->db->where('is_created', '1')
								 ->where('level >=', '2')
								 ->or_where('level', '0')
								 ->order_by('tanggal_transaksi', 'DESC')
								 ->get('transaksi')->result_array();

		$this->template->load('layout/template','transaksi/keuangan/kas/keluar/index', $data);
	}

	public function kas_keluar_detail($transaksi_id){
		$perolehan = $this->transaksi_model->get_detail('id', $transaksi_id);
		
		if($perolehan->num_rows() > 0){
			$data['transaksi']  = $perolehan->row_array();

			$tipe = '';
			if($data['transaksi']['tipe'] == 'bop'){
				$tipe = 'masuk';
			}

			$data['item']       = $this->transaksi_model->get_list_item($data['transaksi']['tipe'], $data['transaksi']['id'], $tipe);
			$data['pembayaran'] = $this->transaksi_model->get_list_pembayaran($data['transaksi']['id']);
			$data['rek'] = $this->db->get('k_rek')->result_array();
			$this->template->load('layout/template','transaksi/keuangan/kas/keluar/detail', $data);
		
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> ID Perolehan tidak diketahui','danger'));
			redirect('keuangan/kas_keluar');
		}
	}

	public function set_approval_to_vendor($transaksi_id){
		$cek = $this->db->where('id', $transaksi_id)->get('transaksi')->row_array();
		if(($cek['tipe'] == 'pemeliharaan' || $cek['tipe'] == 'perbaikan') && $cek['level'] == '2'){
			$this->db->where('id', $transaksi_id)
					 ->update('transaksi',['level' => '3']);
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-check-circle"></i> Berhasil di acc','success'));
			redirect('keuangan/kas_keluar/detail/'.$transaksi_id);
		}else{
			show_404();
		}
	}

	public function set_approval($set, $transaksi_id){
		if(in_array($set, ['approve', 'deny'])){

			$this->db->trans_begin();
			$tgl_jurnal = date('Y-m-d H:i:s');
			$waktu = date('Y-m-d H:i:s');
			$jurnal = [];

			$cek = $this->db->where('id', $transaksi_id)->get('transaksi')->row_array();

			if($cek['tipe'] == 'perbaikan'){
				if($set == 'approve'){
					$p = $this->input->post();
					$s = true;

					$p['total_bayar'] = format_angka($p['total_bayar']);
					$this->form_validation->set_data($p);
					$this->form_validation->set_rules('pembayaran', 'Pembayaran', 'required');
					$this->form_validation->set_rules('metode', 'Metode', 'required');
					$this->form_validation->set_rules('total_bayar', 'Nominal', 'required|numeric|greater_than[0]');

					if($p['metode'] == 'Transfer'){
						$this->form_validation->set_rules('rek_id', 'Rekening', 'required');
						$current = $this->db->where('id', $p['rek_id'])
											->get('k_rek')->row_array()['saldo'];
						if($current < $p['total_bayar']){
							$s = false;
						}
					}

					if($this->form_validation->run() == TRUE && $s == true){
						$s_upload = true;
						$config['upload_path']          = './assets/bukti_terima/';
			            $config['allowed_types']        = 'jpg|png|jpeg';
			            $config['file_name']			= $transaksi_id."_".time();
			            $config['max_size']             = 10000;
			            $config['max_width']            = 3000;
			            $config['max_height']           = 3000;

			            $this->load->library('upload', $config);

			            if($this->upload->do_upload('file')){
			            	$waktu = date('Y-m-d H:i:s');
			            	$upl = $this->upload->data();
                    		$p['bukti_terima'] = $upl['file_name'];

							$p['level_vendor']   = '3';
							$p['tanggal_konfirmasi_keuangan'] = $waktu;
							$p['sisa_bayar'] = $cek['total_transaksi'] - $p['total_bayar']; 
							$this->transaksi_model->update($p, $transaksi_id);

							$this->transaksi_model->insert_pembayaran([
								'transaksi_id'  => $transaksi_id,
								'jumlah_bayar'  => $p['total_bayar'],
								'tanggal_bayar' => $waktu
							]);

							if($p['metode'] == 'Transfer'){
								$last = $this->transaksi_model->get_last_pembayaran($transaksi_id);
								$rek = [
									'rek_id' 				  => $p['rek_id'],
									'transaksi_pembayaran_id' => $last['id']
								];
								$this->m_rekening->insert_history($rek);

								$this->db->where('id', $p['rek_id'])
										 ->update('k_rek', [
										 	'saldo' => $current - $p['total_bayar']
										 ]);
							}

							if($cek['tipe'] == 'pemeliharaan'){
								$coa   = '7';
								$title = 'Pemeliharaan'; 
							}else{
								$coa = '6';
								$title = "Perbaikan";
							}

							$kode_jurnal = $this->transaksi_model->generate_jurnal();
							$this->db->insert('jurnal_master', [
								'kode_jurnal'   => $kode_jurnal,
								'tanggal_input' => $waktu,
								'keterangan'    => $title." ".$cek['kode_transaksi'],
							]);
							$last_jurnal = $this->transaksi_model->last_jurnal();

							if($p['pembayaran'] == 'Tunai'){
								if($p['metode'] == 'Transfer'){
									$jurnal[0]['coa_id'] 	   = '3'; //Bank
									$jurnal[0]['jurnal_master_id'] = $last_jurnal['id'];
									$jurnal[0]['transaksi_id'] = $transaksi_id;
									$jurnal[0]['tgl_jurnal']   = $waktu;
									$jurnal[0]['posisi']	   = 'debit';
									$jurnal[0]['nominal']      = $p['total_bayar'];

									$jurnal[1]['coa_id'] 	   = $coa; //Perbaikan Pemeliharaan
									$jurnal[1]['jurnal_master_id'] = $last_jurnal['id'];
									$jurnal[1]['transaksi_id'] = $transaksi_id;
									$jurnal[1]['tgl_jurnal']   = $waktu;
									$jurnal[1]['posisi']	   = 'kredit';
									$jurnal[1]['nominal']      = $p['total_bayar'];
								
								}else{
									$jurnal[0]['coa_id'] 	   = '1'; //Kas
									$jurnal[0]['jurnal_master_id'] = $last_jurnal['id'];
									$jurnal[0]['transaksi_id'] = $transaksi_id;
									$jurnal[0]['tgl_jurnal']   = $waktu;
									$jurnal[0]['posisi']	   = 'debit';
									$jurnal[0]['nominal']      = $p['total_bayar'];

									$jurnal[1]['coa_id'] 	   = $coa; //Perbaikan Pemeliharaan
									$jurnal[1]['jurnal_master_id'] = $last_jurnal['id'];
									$jurnal[1]['transaksi_id'] = $transaksi_id;
									$jurnal[1]['tgl_jurnal']   = $waktu;
									$jurnal[1]['posisi']	   = 'kredit';
									$jurnal[1]['nominal']      = $p['total_bayar'];
								}

							}else{
								$jurnal[0]['coa_id'] 	   = $coa; //Perbaikan Pemelihan
								$jurnal[0]['jurnal_master_id'] = $last_jurnal['id'];
								$jurnal[0]['transaksi_id'] = $transaksi_id;
								$jurnal[0]['tgl_jurnal']   = $waktu;
								$jurnal[0]['posisi']	   = 'debit';
								$jurnal[0]['nominal']      = $cek['total_transaksi'];

								$jurnal[1]['coa_id'] 	   = '4'; //Utang
								$jurnal[1]['jurnal_master_id'] = $last_jurnal['id'];
								$jurnal[1]['transaksi_id'] = $transaksi_id;
								$jurnal[1]['tgl_jurnal']   = $waktu;
								$jurnal[1]['posisi']	   = 'kredit';
								$jurnal[1]['nominal']      = $p['sisa_bayar'];


								if($p['metode'] == 'Transfer'){
									
									$jurnal[2]['coa_id'] 	   = '3'; //Bank
									$jurnal[2]['transaksi_id'] = $transaksi_id;
									$jurnal[2]['tgl_jurnal']   = $waktu;
									$jurnal[2]['posisi']	   = 'kredit';
									$jurnal[2]['nominal']      = $p['total_bayar'];
								
								}else{
									$jurnal[2]['coa_id'] 	   = '1'; //Kas
									$jurnal[2]['jurnal_master_id'] = $last_jurnal['id'];
									$jurnal[2]['transaksi_id'] = $transaksi_id;
									$jurnal[2]['tgl_jurnal']   = $waktu;
									$jurnal[2]['posisi']	   = 'kredit';
									$jurnal[2]['nominal']      = $p['total_bayar'];
								}
							}

							$this->db->insert_batch('jurnal', $jurnal);

							if($this->db->trans_status()){
								$this->db->trans_commit();
								$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Berhasil dikonfirmasi','success'));
							}else{
								$this->db->trans_rollback();
								$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fa fa-times-circle"></i></b> Gagal dikonfirmasi','danger'));
							}

			            }else{
			            	$s_upload = false;
			            	$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fa fa-minus-circle"></i></b> '.$this->upload->display_errors(),'danger'));
			            }

					}else{
						$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fa fa-minus-circle"></i></b> '.validation_errors(),'danger'));
					}

				}else{
					if($cek['level'] == '2'){
						$p['level'] = '0';
					}else{
						$p['level_vendor'] = '0';
					}
					$p['keterangan_deny'] = 'Ditolak oleh Bagian Keuangan <br>'.$this->input->post('keterangan');
				}

			}else{

				if($set == 'approve'){
					$p = $this->input->post();

					$s = true;
					$p['metode']  = $this->input->post('metode');
					$p['level']   = '3';
					$p['keterangan_acc'] = $this->input->post('keterangan');

					$trans = $this->transaksi_model->get_detail('id', $transaksi_id)->row_array();

					if($p['metode'] == 'Transfer'){
						$detail_rek = $this->m_rekening->get_detail('id', $p['rek_id'])->row_array();
						if($detail_rek['saldo'] < $trans['total_bayar']){
							$s = false;
						}
					}else{
						$p['rek_id'] = null;
					}

					if($s){
						$s_upload = true;
						$config['upload_path']          = './assets/bukti_terima/';
			            $config['allowed_types']        = 'jpg|png|jpeg';
			            $config['file_name']			= $transaksi_id."_".time();
			            $config['max_size']             = 10000;
			            $config['max_width']            = 3000;
			            $config['max_height']           = 3000;

			            $this->load->library('upload', $config);

			            if($this->upload->do_upload('file')){
			            	$upl = $this->upload->data();
			            	$p['bukti_terima'] = $upl['file_name'];


			            	$this->transaksi_model->insert_pembayaran([
											'transaksi_id'  => $transaksi_id,
											'jumlah_bayar'  => $trans['total_bayar'],
											'tanggal_bayar' => date('Y-m-d H:i:s')
										]);

			            	$title = '';
			            	if($trans['tipe'] == 'setoran_bank'){
			            		$title = 'Setoran Bank';
			            	}else if($trans['tipe'] == 'perolehan'){
			            		$title = 'Perolehan';
			            	}else if($trans['tipe'] == 'transaksi_beban'){
			            		$title = 'Transaksi Beban';
			            	}

							$last = $this->transaksi_model->get_last_pembayaran($transaksi_id);
							$kode_jurnal = $this->transaksi_model->generate_jurnal();
							$this->db->insert('jurnal_master', [
								'kode_jurnal'   => $kode_jurnal,
								'tanggal_input' => $waktu,
								'keterangan'    => $title." ".$cek['kode_transaksi'],
							]);
							$last_jurnal = $this->transaksi_model->last_jurnal();

							if($trans['tipe'] == 'setoran_bank'){
								$rek = [
									'rek_id' 				  => $trans['rek_id'],
									'transaksi_pembayaran_id' => $last['id']
								];
								$this->m_rekening->insert_history($rek);

								$detail_rek = $this->m_rekening->get_detail('id', $trans['rek_id'])->row_array();
								$detail_rek['saldo'] += $trans['total_bayar'];
							
								$this->m_rekening->update(['saldo' => $detail_rek['saldo']], $trans['rek_id']);

								$jurnal[0]['coa_id'] 	       = '3'; //Aktiva Tetap
								$jurnal[0]['alt_name']	 	   = 'Bank '.$detail_rek['nama_bank'];
								$jurnal[0]['jurnal_master_id'] = $last_jurnal['id'];
								$jurnal[0]['transaksi_id']     = $transaksi_id;
								$jurnal[0]['tgl_jurnal']   	   = $tgl_jurnal;
								$jurnal[0]['posisi']	       = 'debit';
								$jurnal[0]['nominal']      	   = $trans['total_transaksi'];

								$jurnal[1]['coa_id'] 	   	   = '1'; //Kas
								$jurnal[1]['alt_name']		   = '';
								$jurnal[1]['jurnal_master_id'] = $last_jurnal['id'];
								$jurnal[1]['transaksi_id'] = $transaksi_id;
								$jurnal[1]['tgl_jurnal']   = $tgl_jurnal;
								$jurnal[1]['posisi']	   = 'kredit';
								$jurnal[1]['nominal']      = $trans['total_transaksi'];

							}else if($p['metode'] == 'Transfer'){
								$rek = [
									'rek_id' 				  => $p['rek_id'],
									'transaksi_pembayaran_id' => $last['id']
								];
								$this->m_rekening->insert_history($rek);

								$detail_rek = $this->m_rekening->get_detail('id', $p['rek_id'])->row_array();
								$detail_rek['saldo'] -= $trans['total_bayar'];
							
								$this->m_rekening->update(['saldo' => $detail_rek['saldo']], $p['rek_id']);

							}

							if($trans['pembayaran'] == 'Tunai'){

								$p['status'] = 'Lunas';

								if($trans['tipe'] == 'perolehan'){

									if($trans['pembayaran'] == 'Hibah'){
										$jurnal[0]['coa_id'] 	   = '2'; //Aktiva Tetap
										$jurnal[0]['jurnal_master_id'] = $last_jurnal['id'];
										$jurnal[0]['transaksi_id'] = $transaksi_id;
										$jurnal[0]['tgl_jurnal']   = $tgl_jurnal;
										$jurnal[0]['posisi']	   = 'debit';
										$jurnal[0]['nominal']      = $trans['total_transaksi'];

										$jurnal[1]['coa_id'] 	   = '8'; //Pendapatan Hibah
										$jurnal[1]['jurnal_master_id'] = $last_jurnal['id'];
										$jurnal[1]['transaksi_id'] = $transaksi_id;
										$jurnal[1]['tgl_jurnal']   = $tgl_jurnal;
										$jurnal[1]['posisi']	   = 'kredit';
										$jurnal[1]['nominal']      = $trans['total_transaksi'];

									}else{
										if($p['metode'] == 'Transfer'){
											$jurnal[0]['coa_id'] 	   = '2'; //Aktiva Tetap
											$jurnal[0]['jurnal_master_id'] = $last_jurnal['id'];
											$jurnal[0]['transaksi_id'] = $transaksi_id;
											$jurnal[0]['tgl_jurnal']   = $tgl_jurnal;
											$jurnal[0]['posisi']	   = 'debit';
											$jurnal[0]['nominal']      = $trans['total_transaksi'];

											$jurnal[1]['coa_id'] 	   = '3'; //Bank
											$jurnal[1]['jurnal_master_id'] = $last_jurnal['id'];
											$jurnal[1]['transaksi_id'] = $transaksi_id;
											$jurnal[1]['tgl_jurnal']   = $tgl_jurnal;
											$jurnal[1]['posisi']	   = 'kredit';
											$jurnal[1]['nominal']      = $trans['total_transaksi'];
										
										}else{
											$jurnal[0]['coa_id'] 	   = '2'; //Aktiva Tetap
											$jurnal[0]['jurnal_master_id'] = $last_jurnal['id'];
											$jurnal[0]['transaksi_id'] = $transaksi_id;
											$jurnal[0]['tgl_jurnal']   = $tgl_jurnal;
											$jurnal[0]['posisi']	   = 'debit';
											$jurnal[0]['nominal']      = $trans['total_transaksi'];

											$jurnal[1]['coa_id'] 	   = '1'; //Kas
											$jurnal[1]['jurnal_master_id'] = $last_jurnal['id'];
											$jurnal[1]['transaksi_id'] = $transaksi_id;
											$jurnal[1]['tgl_jurnal']   = $tgl_jurnal;
											$jurnal[1]['posisi']	   = 'kredit';
											$jurnal[1]['nominal']      = $trans['total_transaksi'];
										}
									}

								}else if($trans['tipe'] == 'pemeliharaan'){
									if($p['metode'] == 'Transfer'){
										$jurnal[0]['coa_id'] 	   = '7'; //Pemeliharaan
										$jurnal[0]['jurnal_master_id'] = $last_jurnal['id'];
										$jurnal[0]['transaksi_id'] = $transaksi_id;
										$jurnal[0]['tgl_jurnal']   = $tgl_jurnal;
										$jurnal[0]['posisi']	   = 'debit';
										$jurnal[0]['nominal']      = $trans['total_transaksi'];

										$jurnal[1]['coa_id'] 	   = '3'; //Bank
										$jurnal[1]['jurnal_master_id'] = $last_jurnal['id'];
										$jurnal[1]['transaksi_id'] = $transaksi_id;
										$jurnal[1]['tgl_jurnal']   = $tgl_jurnal;
										$jurnal[1]['posisi']	   = 'kredit';
										$jurnal[1]['nominal']      = $trans['total_transaksi'];
									
									}else{
										$jurnal[0]['coa_id'] 	   = '7'; //Pemeliharaan
										$jurnal[0]['jurnal_master_id'] = $last_jurnal['id'];
										$jurnal[0]['transaksi_id'] = $transaksi_id;
										$jurnal[0]['tgl_jurnal']   = $tgl_jurnal;
										$jurnal[0]['posisi']	   = 'debit';
										$jurnal[0]['nominal']      = $trans['total_transaksi'];

										$jurnal[1]['coa_id'] 	   = '1'; //Kas
										$jurnal[1]['jurnal_master_id'] = $last_jurnal['id'];
										$jurnal[1]['transaksi_id'] = $transaksi_id;
										$jurnal[1]['tgl_jurnal']   = $tgl_jurnal;
										$jurnal[1]['posisi']	   = 'kredit';
										$jurnal[1]['nominal']      = $trans['total_transaksi'];
									}

								}else if($trans['tipe'] == 'transaksi_beban'){

									$this->db->select('coa_id, SUM(subtotal) AS total')
											->where('transaksi_id', $transaksi_id)
											->join('k_beban', 'k_beban.id = transaksi_beban.beban_id')
											->group_by('k_beban.coa_id');
									$barang = $this->db->get('transaksi_beban')->result_array();

									if($p['metode'] == 'Transfer'){
										$coa_id = '3'; //BANK;
									}else{
										$coa_id = '1'; //KAS;
									}

									foreach ($barang as $row){
										$jurnal[] = [
											'jurnal_master_id' => $last_jurnal['id'],
											'coa_id' 		=> $row['coa_id'], //BEBAN COA
											'transaksi_id'  => $transaksi_id,
											'tgl_jurnal' 	=> $tgl_jurnal,
											'posisi'	 	=> 'debit',
											'nominal'	 	=> $row['total']
										];
										$jurnal[] = [
											'jurnal_master_id' => $last_jurnal['id'],
											'coa_id' 		=> $coa_id,
											'transaksi_id'  => $transaksi_id,
											'tgl_jurnal' 	=> $tgl_jurnal,
											'posisi'	 	=> 'kredit',
											'nominal'	 	=> $row['total']
										];
									}
									
								}
							
							}else{

								$p['status'] = 'Belum Lunas';

								if($trans['tipe'] == 'perolehan'){

									$jurnal[0]['jurnal_master_id'] = $last_jurnal['id'];
									$jurnal[0]['coa_id'] 	   = '2'; //Aktiva Tetap
									$jurnal[0]['transaksi_id'] = $transaksi_id;
									$jurnal[0]['tgl_jurnal']   = $tgl_jurnal;
									$jurnal[0]['posisi']	   = 'debit';
									$jurnal[0]['nominal']      = $trans['total_transaksi'];

									if($trans['metode'] == 'Transfer'){
										$jurnal[1]['coa_id'] 	   = '3'; // Bank
									}else{
										$jurnal[1]['coa_id'] 	   = '1'; // Kas
									}
									$jurnal[1]['jurnal_master_id'] = $last_jurnal['id'];
									$jurnal[1]['transaksi_id'] = $transaksi_id;
									$jurnal[1]['tgl_jurnal']   = $tgl_jurnal;
									$jurnal[1]['posisi']	   = 'kredit';
									$jurnal[1]['nominal']      = $trans['total_bayar'];

									$jurnal[2]['jurnal_master_id'] = $last_jurnal['id'];
									$jurnal[2]['coa_id'] 	   = '4'; //Utang
									$jurnal[2]['transaksi_id'] = $transaksi_id;
									$jurnal[2]['tgl_jurnal'] = $tgl_jurnal;
									$jurnal[2]['posisi']	   = 'kredit';
									$jurnal[2]['nominal']      = $trans['sisa_bayar'];
								
								}else if($trans['tipe'] == 'pemeliharaan'){
									$jurnal[0]['jurnal_master_id'] = $last_jurnal['id'];
									$jurnal[0]['coa_id'] 	   = '7'; //Pemeliharaan
									$jurnal[0]['transaksi_id'] = $transaksi_id;
									$jurnal[0]['tgl_jurnal']   = $tgl_jurnal;
									$jurnal[0]['posisi']	   = 'debit';
									$jurnal[0]['nominal']      = $trans['total_transaksi'];

									if($trans['metode'] == 'Transfer'){
										$jurnal[1]['coa_id'] 	   = '3'; // Bank
									}else{
										$jurnal[1]['coa_id'] 	   = '1'; // Kas
									}
									$jurnal[1]['jurnal_master_id'] = $last_jurnal['id'];
									$jurnal[1]['transaksi_id'] = $transaksi_id;
									$jurnal[1]['tgl_jurnal']   = $tgl_jurnal;
									$jurnal[1]['posisi']	   = 'kredit';
									$jurnal[1]['nominal']      = $trans['total_bayar'];

									$jurnal[2]['jurnal_master_id'] = $last_jurnal['id'];
									$jurnal[2]['coa_id'] 	   = '4'; //Utang
									$jurnal[2]['transaksi_id'] = $transaksi_id;
									$jurnal[2]['tgl_jurnal'] = $tgl_jurnal;
									$jurnal[2]['posisi']	   = 'kredit';
									$jurnal[2]['nominal']      = $trans['sisa_bayar'];

								}else if($trans['tipe'] == 'pinjaman'){
									if($p['metode'] == 'Transfer'){
										$set_coa = '3'; //Bank

									}else{
										$set_coa = '1'; //Kas
									}

									$jurnal[0]['jurnal_master_id'] = $last_jurnal['id'];
									$jurnal[0]['coa_id'] 	   = '9'; //Pinjaman Pegawai
									$jurnal[0]['transaksi_id'] = $transaksi_id;
									$jurnal[0]['tgl_jurnal']   = $tgl_jurnal;
									$jurnal[0]['posisi']	   = 'debit';
									$jurnal[0]['nominal']      = $trans['total_transaksi'];

									$jurnal[1]['jurnal_master_id'] = $last_jurnal['id'];
									$jurnal[1]['coa_id']       = $set_coa;
									$jurnal[1]['transaksi_id'] = $transaksi_id;
									$jurnal[1]['tgl_jurnal']   = $tgl_jurnal;
									$jurnal[1]['posisi']	   = 'kredit';
									$jurnal[1]['nominal']      = $trans['total_transaksi'];
								

								}else if($trans['tipe'] == 'transaksi_beban'){
									$this->db->select('coa_id, SUM(subtotal) AS total')
											->where('transaksi_id', $transaksi_id)
											->join('k_beban', 'k_beban.id = transaksi_beban.beban_id')
											->group_by('k_beban.coa_id');
									$barang = $this->db->get('transaksi_beban')->result_array();

									if($p['metode'] == 'Transfer'){
										$coa_id = '3'; //BANK;
									}else{
										$coa_id = '1'; //KAS;
									}

									foreach ($barang as $row){
										$jurnal[] = [
											'jurnal_master_id' => $last_jurnal['id'],
											'coa_id' 		=> $row['coa_id'], //BEBAN COA
											'transaksi_id'  => $transaksi_id,
											'tgl_jurnal' 	=> $tgl_jurnal,
											'posisi'	 	=> 'debit',
											'nominal'	 	=> $row['total']
										];
										$jurnal[] = [
											'jurnal_master_id' => $last_jurnal['id'],
											'coa_id' 		=> $coa_id, 
											'transaksi_id'  => $transaksi_id,
											'tgl_jurnal' 	=> $tgl_jurnal,
											'posisi'	 	=> 'kredit',
											'nominal'	 	=> $trans['total_bayar']
										];
										$jurnal[] = [
											'jurnal_master_id' => $last_jurnal['id'],
											'coa_id' 		=> '4', //UTANG
											'transaksi_id'  => $transaksi_id,
											'tgl_jurnal' 	=> $tgl_jurnal,
											'posisi'	 	=> 'kredit',
											'nominal'	 	=> $trans['sisa_bayar']
										];
									}

								}

							}

							if(!empty($jurnal)){
								$this->m_jurnal->insert_multiple($jurnal);
							}

							if($trans['tipe'] == 'perolehan'){
								$this->generateAsset($transaksi_id);
							}

			            }else{
			            	$s_upload = false;
			            	$this->session->set_flashdata('alert_message', show_alert('<b class="text-warning"><i class="fa fa-minus-circle"></i></b>'. $this->upload->display_errors(),'danger'));
			            }
						
					}
					

				}else{
					$p['level'] = '0';
					$p['keterangan_deny'] = 'Ditolak oleh Bagian Keuangan <br>'.$this->input->post('keterangan');
				}

			}

			if($s){
				if($s_upload){
					$this->transaksi_model->update($p, $transaksi_id);

					if($this->db->trans_status()){
						$this->db->trans_commit();
						$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Status Berhasil diubah','success'));

					}else{
						$this->db->trans_rollback();
						$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fa fa-minus-circle"></i></b> Status gagal diubah','danger'));
					}
				}

			}else{
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-warning"><i class="fa fa-ban"></i></b> Saldo Rekening tidak mencukupi','warning'));
			}

			//echo "<pre>".print_r($jurnal, true)."</pre>";
			redirect('keuangan/kas_keluar/detail/'.$transaksi_id);

		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> ID Transaksi tidak diketahui','danger'));
			redirect('keuangan/kas_keluar');
		}
	}

		private function generateAsset($transaksi_id){
			$this->db->select('*, transaksi_aset.id AS transaksi_aset_id')
					 ->where('transaksi_id', $transaksi_id)
					 ->join('hru_aset', 'hru_aset.id = transaksi_aset.aset_id');

			$trans = $this->db->get('transaksi_aset')->result_array();

			foreach ($trans as $row) {
				$this->db->select('COUNT(id) AS total')
						 ->where('aset_id', $row['aset_id'])
						 ->group_by('aset_id');

				$start = $this->db->get('hru_aset_detail')->result_array();

				$n = 0;
				foreach ($start as $row2) { $n++; $num = $row2['total'] + 1;
					for ($i = $num; $i < ($num + $row['jumlah']); $i++){ 
						$aset[$i]['transaksi_aset_id'] = $row['transaksi_aset_id'];
						$aset[$i]['aset_id']		   = $row['aset_id'];
						$aset[$i]['kode_detail_aset']  = $row['kode_aset']."-".$num;
					}
				}

				if($n == 0){
					$k = 0;
					for ($i=0; $i < $row['jumlah']; $i++) { $k++;
						$aset[$i]['transaksi_aset_id'] = $row['transaksi_aset_id'];
						$aset[$i]['aset_id']		   = $row['aset_id'];
						$aset[$i]['kode_detail_aset']  = $row['kode_aset']."-".$k;
					}
				}
			}

			$this->db->insert_batch('hru_aset_detail', $aset);
		}
    
}