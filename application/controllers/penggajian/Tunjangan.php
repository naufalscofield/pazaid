<?php 

class Tunjangan extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->model('penggajian/tunjangan_model', 'm_tunjangan');
		$this->load->model('keuangan/Coa_model', 'm_coa');
		
		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','hr');
	}

	public function lain(){
		$data['coa']  = $this->m_coa->get_data();
		$data['list'] = $this->db->select('coa.kode_coa, coa.nama_coa, fdl_tunjangan.*')
								 ->where('tipe !=', 'lembur')
								 ->join('coa', 'coa.id = fdl_tunjangan.coa_id', 'LEFT')
								 ->get('fdl_tunjangan')->result_array();
		$this->template->load('layout/template','master_data/penggajian/lain', $data);
	}

	public function index(){
		$data['list']      =$this->db->where('tipe', 'lembur')->get('fdl_tunjangan')->result_array();
		$this->template->load('layout/template','master_data/penggajian/lembur', $data);
	}

	public function add(){
		$p = $this->input->post();
		$p['nominal'] = format_angka($p['nominal']);
		$this->form_validation->set_data($p);
		$s = false;

		if(isset($p['source'])){
			$s = true;
			$this->form_validation->set_rules('nama_tunjangan', 'Nama Tunjangan', 'required');
			unset($p['source']);
			
		}else{
			$this->form_validation->set_rules('jml', 'Jumlah Jam', 'required');
		}
		
		$this->form_validation->set_rules('nominal', 'Nominal', 'required|numeric|greater_than[0]');

		if($this->form_validation->run() == TRUE){

			if($this->m_tunjangan->insert($p)){
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-check"></i> Data berhasil dimasukkan','success'));
			}else{
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dimasukkan','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
		}

		if($s){
			redirect('penggajian/tunjangan');
		}else{
			redirect('penggajian/lembur');
		}

		
	}

	public function update(){
		$p  = $this->input->post();
		$id = $p['id_tunjangan'];
		unset($p['id_tunjangan']);

		$p['nominal'] = format_angka($p['nominal']);
		$this->form_validation->set_data($p);
		$s = false;

		if(isset($p['source'])){
			$s = true;
			$this->form_validation->set_rules('nama_tunjangan', 'Nama Tunjangan', 'required');
			unset($p['source']);
			
		}else{
			$this->form_validation->set_rules('jml', 'Jumlah Jam', 'required');
		}

		if($this->form_validation->run() == TRUE){

			if($this->m_tunjangan->update($p, $id)){
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-check"></i> Data berhasil diubah','success'));
			}else{
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diubah','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
		}

		if($s){
			redirect('penggajian/tunjangan');
		}else{
			redirect('penggajian/lembur');
		}
	}

	public function delete($tipe, $id){

		if($this->m_tunjangan->delete($id)){
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-check"></i> Data berhasil dihapus','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dihapus','danger'));
		}

		redirect('penggajian/'. $tipe);
	}
    
}