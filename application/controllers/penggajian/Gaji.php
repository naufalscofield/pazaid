<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gaji extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('penggajian/absensi_model', 'm_absensi');
		$this->load->model('penggajian/gaji_model', 'm_gaji');
		$this->load->model('penggajian/tunjangan_model', 'm_tunjangan');
		$this->load->model('penggajian/pegawai_model', 'm_pegawai');
		$this->load->model('transaksi_model');

		date_default_timezone_set("Asia/Jakarta");
		$this->session->set_userdata('menu','hr');
	}

	public function daftar(){
		if($this->input->get('tahun')){
			$data['tahun'] = $this->input->get('tahun');
		}else{
			$data['tahun'] = date('Y');
		}

		$stop = false;

		for ($i=1; $i <= 12; $i++){ 
			$list[$i] = $this->m_gaji->get_list($i, $data['tahun']);
			$list[$i]['next'] = $this->m_gaji->calculateGaji($i, $data['tahun']);
		}

		$data['gaji'] = $list;

		$this->template->load('layout/template','transaksi/penggajian/gaji/daftar', $data);
	}

	public function insert_lembur(){
		$this->form_validation->set_rules('pegawai_id', 'Pegawai', 'required');
		$this->form_validation->set_rules('jml_lembur', 'Jam Lembur', 'required|numeric|greater_than[0]');
		$this->form_validation->set_rules('tgl', 'Tanggal Lembur', 'required');

		if ($this->form_validation->run() == TRUE){

			$lembur = $this->db->query('SELECT * FROM fdl_tunjangan WHERE tipe = "lembur" ORDER BY jml ASC')->result_array();
			$i = 0; $s = true;

			$jml_lembur = (float)$this->input->post('jml_lembur');
			$nominal_lembur = 0;

			foreach ($lembur as $row){ $i++;

				if($row['jml'] == $jml_lembur){
					$nominal_lembur = $row['nominal'];
					$s = false;
					break;
				}else{
					if($jml_lembur > $row['jml']){
						$nominal_lembur = $row['nominal'];
					}
				}
			}

			$data = array(
				'pegawai_id' => $this->input->post('pegawai_id'),
				'tanggal' => $this->input->post('tgl'),
				'total_jam' => $this->input->post('jml_lembur'),
				'nominal_lembur' => $nominal_lembur
			);

			if($this->m_gaji->insert_lembur($data)){
				$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-check"></i> Data berhasil dimasukkan','success'));
			}else{
				$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-warning"></i> Terjadi kesalahan saat input data','danger'));
			}
			
		}else{
			$this->session->set_flashdata('alert_message',show_alert(validation_errors(),'warning'));
		}

		redirect('penggajian/gaji/lembur');

	}

	public function generate_gaji(){
		if($this->input->is_ajax_request()){
			$this->db->trans_begin();

			$req = array(
				'bulan' => $this->input->post('bulan'),
				'tahun' => $this->input->post('tahun')
			);

			$work_day = HARI_KERJA;

			$count = $this->db->where($req)
							  ->get('fdl_gaji')
							  ->num_rows();

			if($count > 0){
				$response['status']  = 'error';
				$response['message'] = show_alert('<i class="fa fa-warning"></i> Gaji bulan ini sudah digenerate','warning');
			
			}else{
				
				$absensi  = $this->m_absensi->get_list($req, 'gaji')->result_array();

				$bulan    = $req['bulan'];
				$tahun    = $req['tahun'];
				$ptkp     = 54000000;
				$jurnal = []; $total_pph = 0;
				$jurnal_pinjaman = [];
				$waktu = date('Y-m-d H:i:s');
				$num = 4;

				$total_piutang = 0;
				$total_lembur  = 0;
				$total_pokok   = 0;

				if($this->m_gaji->insert([
								'bulan' => $bulan,
								'tahun' => $tahun,
								'waktu_generate' => $waktu
							])){

					$history = $this->db->where($req)->get('fdl_gaji')->row_array();
					$i = 0;
					$total_gaji = $total_pajak = 0;

					$waktu = date('Y-m-d H:i:s');

					$get_all_tj = $this->db->where('tipe !=', 'lembur')
										   ->get('fdl_tunjangan')->result_array();
					$all_tj = [];
					foreach ($get_all_tj as $row) {
						$all_tj[$row['coa_id']] = 0;
					}

					$lain = $this->db->where('tipe', 'harian')
						 ->or_where('tipe', 'bulanan')
						 ->get('fdl_tunjangan')->result_array();

					foreach ($absensi as $row){
						$pph = 0;
						$nominal_tunjangan = 0;
						$tunjangan = [];

						$day = [];
						for($d=1; $d<=31; $d++){
			                $time = mktime(12, 0, 0, $bulan, $d, $tahun);          
			                if(date('m', $time) == $bulan){     
			                    $day[]= $d;
			                }
			            }

			            $num_days = 0; $c = 0;
			            foreach ($day as $k){
			            	$c++;
			            	if(filterDay($tahun.'-'.$bulan.'-'.$c, $work_day)){
			            		$num_days++;
			            	}
			            	
			            }

			            $lembur = $this->db->select('SUM(nominal_lembur) AS nominal')
			            				   ->where('pegawai_id', $row['id'])
			            				   ->where('MONTH(tanggal)', $bulan)
			            				   ->where('YEAR(tanggal)', $tahun)
			            				   ->get('fdl_lembur')->row_array();
			            $nominal_lembur = $lembur['nominal'];

			            if($nominal_lembur == ''){
			            	$nominal_lembur = 0;
			            }

						$base = $row['gaji'] / $num_days;
						$kehadiran = $row['total_hadir'] + $row['total_dinas'];

						$n_hadir = ($kehadiran * $base);
						$n_telat = $row['total_terlambat'] * ((50/100) * $base);

						$pokok = $n_hadir + $n_telat;
						$nominal_pokok	   = $pokok;

						//TUNJANGAN LAINNYA
			            foreach ($lain as $rows){
			            	$key = str_replace('_', ' ', strtolower($rows['nama_tunjangan']));
			            	if($rows['tipe'] == 'harian'){
			            		$val = $rows['nominal'] * $kehadiran;
			            	}else{
			            		$val = (int)$rows['nominal'];
			            	}
			            	$nominal_tunjangan += $val;
			            	$tunjangan[] = ['nominal' => $val, 'name' => $rows['nama_tunjangan']];

			            	$all_tj[$rows['coa_id']] += $val;
			            }

			            //TUNJANGAN KHUSUS
			            $tj = $this->db->select('fdl_tunjangan.id AS tunjangan_id, fdl_tunjangan.coa_id AS coa_id, fdl_tunjangan.nama_tunjangan, SUM(total_nominal) AS total_nominal')
			            		 ->where([
			            			'pegawai_id' => $row['id'],
			            			'MONTH(waktu_tunjangan)' => $bulan,
			            			'YEAR(waktu_tunjangan)'  => $tahun
			            		])
			            		->join('fdl_tunjangan', 'fdl_tunjangan.id = fdl_gaji_tunjangan.tunjangan_id')
			            		->group_by('tunjangan_id')
			            		->get('fdl_gaji_tunjangan')->result_array();
			            
			            foreach ($tj as $tjg){
			            	$nominal_tunjangan += $tjg['total_nominal'];
			            	$tunjangan[] = ['nominal' => $tjg['total_nominal'], 'name' => $tjg['nama_tunjangan']];
			            	$all_tj[$rows['coa_id']] += $tjg['total_nominal'];
			            }

			            $nominal_hutang = $this->transaksi_model->get_nominal_pinjaman($row['id']);

			            $this->db->where('tipe', 'pinjaman')
				                 ->where('level', '3')
				                 ->where('status', 'Belum Lunas')
				                 ->where('pegawai_id', $row['id']);
				      	$list_hutang = $this->db->get('transaksi')->result_array();

				      	foreach ($list_hutang as $hutang){
				      		$h['status']      = 'Lunas';
				      		$h['sisa_bayar']  = 0;
				      		$h['total_bayar'] = $hutang['total_transaksi'];

				      		$this->db->insert('transaksi_pembayaran',[
				      						'transaksi_id'	=> $hutang['id'],
				      						'tanggal_bayar' => $waktu,
				      						'jumlah_bayar'  => $hutang['sisa_bayar'],
				      						'keterangan_pembayaran' => 'Dibayar Saat Periode Penggajian '.get_monthname($bulan)." ".$tahun
				      					]);

				      		$this->db->where('id', $hutang['id'])
				      				 ->update('transaksi', $h);

				      		$total_piutang += $hutang['sisa_bayar'];

				      	}

				      	$gaji_bersih = $nominal_pokok + $nominal_lembur + $nominal_tunjangan - $nominal_hutang;

				      	///////////////// PPH21
				      	if($row['is_nikah'] == '1'){
				      		$ptkp += 4500000;
				      		if($row['jml_anak'] > 3){
				      			$ptkp += 4500000 * 3;
				      		}else{
				      			$ptkp += 4500000 * $row['jml_anak'];
				      		}
				      	}

				      	$pkp = ($gaji_bersih * 12) - $ptkp;
				      	if($pkp > 0){
				      		$pph = calculatePPH21($pkp);
				      	}
				      	/////////////////

						$data[$i]['gaji_id']	 		= $history['id'];
						$data[$i]['pegawai_id'] 		= $row['id'];
						$data[$i]['total_masuk'] 		= $row['total_masuk'];
						$data[$i]['total_hadir'] 		= $kehadiran;
						$data[$i]['total_terlambat'] 	= $row['total_terlambat'];
						$data[$i]['maksimal_kehadiran'] = $num_days;
						$data[$i]['gaji_pokok'] 		 = $nominal_pokok;
						$data[$i]['gaji_kehadiran']		 = $n_hadir;
						$data[$i]['gaji_keterlambatan']  = $n_telat;		
						$data[$i]['tunjangan_lembur'] 	 = $nominal_lembur;
						$data[$i]['tunjangan_lain']		 = $nominal_tunjangan;
						$data[$i]['tunjangan_komponen']  = json_encode($tunjangan);
						$data[$i]['hutang']				 = $nominal_hutang;
						$data[$i]['pph'] 				 = $pph;
						$data[$i]['total_gaji']			 = $gaji_bersih - $pph;

						$total_gaji += $data[$i]['total_gaji'];
						$total_pph  += $data[$i]['pph'];
						$total_lembur += $nominal_lembur;
						$total_pokok  += $nominal_pokok;

						$i++;
					}

					$kode_jurnal = $this->transaksi_model->generate_jurnal();
					$this->db->insert('jurnal_master', [
						'kode_jurnal'   => $kode_jurnal,
						'tanggal_input' => $waktu,
						'keterangan'    => 'Peggajian Bulan '.get_monthname($bulan)." ".$tahun,
					]);

					$last_jurnal = $this->transaksi_model->last_jurnal();
					$total_kas = $total_pokok + $total_lembur;
					$jurnal[] = [
						'coa_id' 	 => '10', //BEBAN GAJI,
						'jurnal_master_id' => $last_jurnal['id'],
		      			'keterangan' => 'Peggajian Bulan '.get_monthname($bulan)." ".$tahun,
		      			'tgl_jurnal' => $waktu,
		      			'posisi'	 => 'debit',
		      			'nominal'    => $total_pokok
					];

					$jurnal[] = [
						'coa_id' 	 => '15', //BEBAN LEMBUR,
						'jurnal_master_id' => $last_jurnal['id'],
		      			'keterangan' => 'Peggajian Bulan '.get_monthname($bulan)." ".$tahun,
		      			'tgl_jurnal' => $waktu,
		      			'posisi'	 => 'debit',
		      			'nominal'    => $total_lembur
					];

					foreach ($all_tj as $key => $value) {
						$jurnal[] = [
							'coa_id' 	 => $key, //BEBAN TJ,
							'jurnal_master_id' => $last_jurnal['id'],
			      			'keterangan' => 'Peggajian Bulan '.get_monthname($bulan)." ".$tahun,
			      			'tgl_jurnal' => $waktu,
			      			'posisi'	 => 'debit',
			      			'nominal'    => $value
						];

						$total_kas += $value;
					}

					
					if($total_piutang > 0){
						$total_kas -= ($total_pph + $total_piutang);
						$jurnal[] = [
							'coa_id' 	 => '1', //KAS,
							'jurnal_master_id' => $last_jurnal['id'],
			      			'keterangan' => 'Peggajian Bulan '.get_monthname($bulan)." ".$tahun,
			      			'tgl_jurnal' => $waktu,
			      			'posisi'	 => 'kredit',
			      			'nominal'    => $total_kas
						];

						$jurnal[] = [
							'coa_id' 	 => '8', //PIUTANG PEGAWAI,
							'jurnal_master_id' => $last_jurnal['id'],
			      			'keterangan' => 'Peggajian Bulan '.get_monthname($bulan)." ".$tahun,
			      			'tgl_jurnal' => $waktu,
			      			'posisi'	 => 'kredit',
			      			'nominal'    => $total_piutang
						];

						$jurnal[] = [
							'coa_id' 	 => '11', //UTANG PPH,
							'jurnal_master_id' => $last_jurnal['id'],
			      			'keterangan' => 'Peggajian Bulan '.get_monthname($bulan)." ".$tahun,
			      			'tgl_jurnal' => $waktu,
			      			'posisi'	 => 'kredit',
			      			'nominal'    => $total_pph
						];
					
					}else{
						$total_kas -= $total_pph;
						$jurnal[] = [
							'coa_id' 	 => '1', //KAS,
							'jurnal_master_id' => $last_jurnal['id'],
			      			'keterangan' => 'Peggajian Bulan '.get_monthname($bulan)." ".$tahun,
			      			'tgl_jurnal' => $waktu,
			      			'posisi'	 => 'kredit',
			      			'nominal'    => $total_kas
						];

						$jurnal[] = [
							'coa_id' 	 => '11', //UTANG PPH,
							'jurnal_master_id' => $last_jurnal['id'],
			      			'keterangan' => 'Peggajian Bulan '.get_monthname($bulan)." ".$tahun,
			      			'tgl_jurnal' => $waktu,
			      			'posisi'	 => 'kredit',
			      			'nominal'    => $total_pph
						];
					}
		      		
		      		ksort($jurnal);
		      		$this->db->insert_batch('jurnal', $jurnal);
					$this->db->insert_batch('fdl_gaji_detail', $data);
					
					if($this->db->trans_status()){
						$this->db->trans_commit();
						$response['status']  = 'success';
						$response['message'] = show_alert('<i class="fa fa-check"></i> Gaji berhasil digenerate','success');
					
					}else{
						$this->db->trans_rollback();
						$response['status']  = 'error';
						$response['message'] = show_alert('<i class="fa fa-warning"></i> Terjadi kesalahan saat generate','danger');
					}

				}else{
					$response['status']  = 'error';
					$response['message'] = show_alert('<i class="fa fa-check"></i> Gagal input gaji','danger');
				}

			}

			echo json_encode($response);

		}else{
			show_404();
		}
	}

	public function lembur(){
		$data['list'] = $this->m_gaji->get_lembur_list()->result_array();
		$data['pegawai'] = $this->m_pegawai->get_data();
		$this->template->load('layout/template','transaksi/penggajian/gaji/lembur', $data);
	}

	public function detail($id_gaji){
		if($this->session->userdata('login')){
			$data['gaji'] = $this->m_gaji->get_detail($id_gaji)->row_array();

			if(count($data['gaji']) > 0){

				$data['list'] = $this->m_gaji->get_list_detail($id_gaji)->result_array();

				$this->template->load('layout/template','transaksi/penggajian/gaji/detail',$data);
			}else{
				redirect('penggajian/gaji/daftar');
			}

		}else{
			redirect();
		}
	}

	public function detail_preview($bulan, $tahun){
		if($this->session->userdata('login')){

			$data['bulan'] = $bulan;
			$data['tahun'] = $tahun;
			$data['gaji'] = $this->m_gaji->calculateGaji($bulan, $tahun);
			$this->template->load('layout/template','transaksi/penggajian/gaji/preview',$data);

		}else{
			redirect();
		}
	}

	public function cetak_slip($id_gaji, $id_pegawai){
		if($this->session->userdata('login')){
			$data['gaji'] = $this->m_gaji->get_detail($id_gaji)->row_array();

			if(count($data['gaji']) > 0){
				$data['pegawai'] = $this->m_pegawai->get_detail('fdl_pegawai.id',$id_pegawai)->row_array();
				$data['list']     = $this->m_gaji->get_list_detail($id_gaji, $id_pegawai)->row_array();

				$this->load->view('transaksi/penggajian/gaji/invoice',$data);
			}else{
				redirect('penggajian/gaji/daftar');
			}

		}else{
			redirect();
		}
	}


	public function tunjangan(){
		$data['list'] = $this->m_gaji->get_tunjangan();
		$data['pegawai'] = $this->m_pegawai->get_data();
		$data['tunjangan'] = $this->m_tunjangan->get_detail('tipe', 'per_orang')->result_array();
		$this->template->load('layout/template','transaksi/penggajian/gaji/tunjangan', $data);
	}

	public function insert_tunjangan(){
		$p = $this->input->post();
		$this->form_validation->set_data($p);

		$this->form_validation->set_rules('tunjangan_id', 'Tunjangan', 'required');
		$this->form_validation->set_rules('pegawai_id', 'Pegawai', 'required');
		$this->form_validation->set_rules('jumlah', 'Jumlah Tunjangan', 'required|numeric|greater_than[0]');
		$this->form_validation->set_rules('waktu_tunjangan', 'Tanggal Tunjangan', 'required');

		if ($this->form_validation->run() == TRUE){

			$tj = $this->m_tunjangan->get_detail('id', $p['tunjangan_id'])->row_array();
			$p['total_nominal'] = $tj['nominal'] * $p['jumlah'];

			if($this->m_gaji->insert_tunjangan($p)){
				$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-check"></i> Data berhasil dimasukkan','success'));
			}else{
				$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-warning"></i> Terjadi kesalahan saat input data','danger'));
			}
			
		}else{
			$this->session->set_flashdata('alert_message',show_alert(validation_errors(),'warning'));
		}

		redirect('penggajian/gaji/tunjangan');

	}

	public function insert_pinjaman(){
		$p = $this->input->post();
		$p['total_transaksi'] = format_angka($p['total_transaksi']);
		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('total_transaksi', 'Nominal Pinjaman', 'required|numeric|greater_than[0]');

		if ($this->form_validation->run() == TRUE){
			$p['tanggal_transaksi'] = date('Y-m-d H:i:s');
			$p['status']			= 'Belum Lunas';
			$p['is_created']		= '1';
			$p['pembayaran']		= 'Kredit';
			$p['jenis']				= 'keluar';
			$p['tipe']				= 'pinjaman';
			$p['kode_transaksi']	= $this->transaksi_model->generate_code('pinjaman');
			$p['pegawai_id']		= $this->session->userdata('user_data')['id'];
			$p['sisa_bayar']		= $p['total_transaksi'];

			if($this->transaksi_model->insert($p)){
				$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-check"></i> Pinjaman berhasil diajukan','success'));
			}else{
				$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-warning"></i> Pinjaman gagal diajukan','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message',show_alert(validation_errors(),'warning'));
		}

		redirect('karyawan/pinjaman');
	}
}