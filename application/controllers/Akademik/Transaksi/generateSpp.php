<?php 

class generateSpp extends CI_Controller{

    function __construct()
    {
		parent::__construct();
		$this->load->model('Akademik/Transaksi/model_siswa', 'm_siswa');
		$this->load->model('Akademik/MasterData/Master_model', 'm_master');
		$this->load->model('Akademik/Transaksi/model_transaksi_pendaftaran', 'm_transaksi');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','akademik');
	}

    public function index()
    {
        $this->template->load('layout/template','Akademik/Transaksi/GenerateSpp');
    }
}