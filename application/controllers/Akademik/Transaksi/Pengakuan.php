<?php 

class Pengakuan extends CI_Controller{

    function __construct()
    {
		parent::__construct();
		$this->load->model('Akademik/Transaksi/model_siswa', 'm_siswa');
		$this->load->model('Akademik/Transaksi/model_transaksi_pendaftaran', 'm_t_pendaftaran');
		$this->load->model('Akademik/Transaksi/model_tagihan_lainnya', 'm_t_lainnya');
		$this->load->model('Akademik/Transaksi/model_biaya_umum', 'm_umum');
		$this->load->model('Akademik/MasterData/Master_model', 'm_master');
		$this->load->model('Akademik/MasterData/Kelas_model', 'm_kelas');
		$this->load->model('Akademik/Transaksi/model_transaksi_pendaftaran', 'm_transaksi');
		$this->load->model('keuangan/Jurnal_model', 'm_jurnal');
        $this->load->model('Keuangan/model_tunai', 'm_tunai');
		$this->load->model('Keuangan/rekening_model', 'm_rekening');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','akademik');
	}

    public function index()
    {
		$bulan 		= date('m');
		$getJurnal 	=  $this->m_jurnal->get_where($bulan);

		$totalPYDDpp		= 0;
		$totalPYDDsk		= 0;
		$totalPendapatanDpp	= 0;
		$totalPendapatanDsk	= 0;

		foreach ($getJurnal as $jurnal)
		{
			if ($jurnal['coa_id'] == 32)
			{
				$totalPYDDpp += $jurnal['nominal'];
			}
			if ($jurnal['coa_id'] == 34)
			{
				$totalPYDDsk += $jurnal['nominal'];
			}
			if ($jurnal['coa_id'] == 66)
			{
				$totalPendapatanDpp += $jurnal['nominal'];
			}
			if ($jurnal['coa_id'] == 67)
			{
				$totalPendapatanDsk += $jurnal['nominal'];
			}
		}

		$jurnal1 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 32,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'debit',
			'nominal'                   => $totalPYDDpp / 12,
			'status'                    => 0
		];

		$jurnal2 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 34,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'debit',
			'nominal'                   => $totalPYDDsk / 12,
			'status'                    => 0
		];

		$jurnal3 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 66,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'kredit',
			'nominal'                   => $totalPYDDpp / 12,
			'status'                    => 0
		];

		$jurnal4 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 67,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'kredit',
			'nominal'                   => $totalPYDDsk / 12,
			'status'                    => 0
		];


		$dataJurnal = [$jurnal1, $jurnal2, $jurnal3, $jurnal4];
	
		$insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);
		$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diubah','success'));
		redirect ('keuangan/laporan/jurnal');
	}
	


}