<?php 

class TagihanLainnya extends CI_Controller{

    function __construct()
    {
		parent::__construct();
		$this->load->model('Akademik/Transaksi/model_siswa', 'm_siswa');
		$this->load->model('Akademik/Transaksi/model_tagihan_lainnya', 'm_tagihan');
		$this->load->model('Akademik/masterdata/kelas_model', 'm_kelas');
        $this->load->model('Akademik/MasterData/Master_model', 'm_tahun');
        $this->load->model('Akademik/Transaksi/model_transaksi_pendaftaran', 'm_transaksi');
		$this->load->model('Keuangan/Jurnal_model', 'm_jurnal');
        

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','akademik');
	}

    public function Overtime()
    {
		$data1  = $this->m_siswa->get_data();
        $data2  = $this->m_kelas->get_data();
        $data   = [];
        foreach($data1 as $siswa)
        {
            foreach($data2 as $kelas)
            {
                if($siswa['id_kelas'] == $kelas['id_kelas'])
                {
                    $tahun  = $this->m_tahun->getById($siswa['id_tahun']);
                    $lahir  = new DateTime($siswa['tanggal_lahir']);
                    $today  = new DateTime();
                    $umur   = $today->diff($lahir)->y;
                    $arr = [
                        'id_siswa'          => $siswa['id_siswa'],
                        'id_kelas'          => $siswa['id_kelas'],
                        'id_tahun'          => $siswa['id_tahun'],
                        'nis'               => $siswa['nis'],
                        'nama_siswa'        => $siswa['nama_siswa'],
                        'panggilan_siswa'   => $siswa['panggilan_siswa'],
                        'panggilan_siswa'   => $siswa['panggilan_siswa'],
                        'tempat_lahir'      => $siswa['tempat_lahir'],
                        'tanggal_lahir'     => $siswa['tanggal_lahir'],
                        'umur'              => $umur,
                        'jenis_kelamin'     => $siswa['jenis_kelamin'],
                        'agama'             => $siswa['agama'],
                        'jumlah_saudara'    => $siswa['jumlah_saudara'],
                        'anak_ke'           => $siswa['anak_ke'],
                        'alamat'            => $siswa['alamat'],
                        'status_ortu'       => $siswa['status'],
                        'status_siswa'      => $siswa['status_siswa'],
                        'status_bayar'      => $siswa['status_bayar'],
                        'kode_kelas'        => $kelas['kode_kelas'],
                        'nama_kelas'        => $kelas['nama_kelas'],
                        'level_kelas'       => $kelas['level_kelas'],
                        'status_kelas'      => $kelas['status'],
                        'kode_tahun'        => $tahun[0]->kode_tahun,
                        'created_at'        => $tahun[0]->created_at
                    ];                       
                array_push($data, $arr);
                }
            }
        }
        $this->template->load('layout/template','Akademik/Transaksi/Overtime', compact('data'));
	}

    public function Catering()
    {
		$data1  = $this->m_siswa->get_data();
        $data2  = $this->m_kelas->get_data();
        $data   = [];
        foreach($data1 as $siswa)
        {
            foreach($data2 as $kelas)
            {
                if($siswa['id_kelas'] == $kelas['id_kelas'])
                {
                    $tahun  = $this->m_tahun->getById($siswa['id_tahun']);
                    $lahir  = new DateTime($siswa['tanggal_lahir']);
                    $today  = new DateTime();
                    $umur   = $today->diff($lahir)->y;
                    $arr = [
                        'id_siswa'          => $siswa['id_siswa'],
                        'id_kelas'          => $siswa['id_kelas'],
                        'id_tahun'          => $siswa['id_tahun'],
                        'nis'               => $siswa['nis'],
                        'nama_siswa'        => $siswa['nama_siswa'],
                        'panggilan_siswa'   => $siswa['panggilan_siswa'],
                        'panggilan_siswa'   => $siswa['panggilan_siswa'],
                        'tempat_lahir'      => $siswa['tempat_lahir'],
                        'tanggal_lahir'     => $siswa['tanggal_lahir'],
                        'umur'              => $umur,
                        'jenis_kelamin'     => $siswa['jenis_kelamin'],
                        'agama'             => $siswa['agama'],
                        'jumlah_saudara'    => $siswa['jumlah_saudara'],
                        'anak_ke'           => $siswa['anak_ke'],
                        'alamat'            => $siswa['alamat'],
                        'status_ortu'       => $siswa['status'],
                        'status_siswa'      => $siswa['status_siswa'],
                        'status_bayar'      => $siswa['status_bayar'],
                        'kode_kelas'        => $kelas['kode_kelas'],
                        'nama_kelas'        => $kelas['nama_kelas'],
                        'level_kelas'       => $kelas['level_kelas'],
                        'status_kelas'      => $kelas['status'],
                        'kode_tahun'        => $tahun[0]->kode_tahun,
                        'created_at'        => $tahun[0]->created_at
                    ];                       
                array_push($data, $arr);
                }
            }
        }
        $this->template->load('layout/template','Akademik/Transaksi/Catering', compact('data'));
	}
	
	public function Sarapan()
    {
		$data1  = $this->m_siswa->get_data();
        $data2  = $this->m_kelas->get_data();
        $data   = [];
        foreach($data1 as $siswa)
        {
            foreach($data2 as $kelas)
            {
                if($siswa['id_kelas'] == $kelas['id_kelas'])
                {
                    $tahun  = $this->m_tahun->getById($siswa['id_tahun']);
                    $lahir  = new DateTime($siswa['tanggal_lahir']);
                    $today  = new DateTime();
                    $umur   = $today->diff($lahir)->y;
                    $arr = [
                        'id_siswa'          => $siswa['id_siswa'],
                        'id_kelas'          => $siswa['id_kelas'],
                        'id_tahun'          => $siswa['id_tahun'],
                        'nis'               => $siswa['nis'],
                        'nama_siswa'        => $siswa['nama_siswa'],
                        'panggilan_siswa'   => $siswa['panggilan_siswa'],
                        'panggilan_siswa'   => $siswa['panggilan_siswa'],
                        'tempat_lahir'      => $siswa['tempat_lahir'],
                        'tanggal_lahir'     => $siswa['tanggal_lahir'],
                        'umur'              => $umur,
                        'jenis_kelamin'     => $siswa['jenis_kelamin'],
                        'agama'             => $siswa['agama'],
                        'jumlah_saudara'    => $siswa['jumlah_saudara'],
                        'anak_ke'           => $siswa['anak_ke'],
                        'alamat'            => $siswa['alamat'],
                        'status_ortu'       => $siswa['status'],
                        'status_siswa'      => $siswa['status_siswa'],
                        'status_bayar'      => $siswa['status_bayar'],
                        'kode_kelas'        => $kelas['kode_kelas'],
                        'nama_kelas'        => $kelas['nama_kelas'],
                        'level_kelas'       => $kelas['level_kelas'],
                        'status_kelas'      => $kelas['status'],
                        'kode_tahun'        => $tahun[0]->kode_tahun,
                        'created_at'        => $tahun[0]->created_at
                    ];                       
                array_push($data, $arr);
                }
            }
        }
        $this->template->load('layout/template','Akademik/Transaksi/Sarapan', compact('data'));
	}

	public function Mandi()
    {
		$data1  = $this->m_siswa->get_data();
        $data2  = $this->m_kelas->get_data();
        $data   = [];
        foreach($data1 as $siswa)
        {
            foreach($data2 as $kelas)
            {
                if($siswa['id_kelas'] == $kelas['id_kelas'])
                {
                    $tahun  = $this->m_tahun->getById($siswa['id_tahun']);
                    $lahir  = new DateTime($siswa['tanggal_lahir']);
                    $today  = new DateTime();
                    $umur   = $today->diff($lahir)->y;
                    $arr = [
                        'id_siswa'          => $siswa['id_siswa'],
                        'id_kelas'          => $siswa['id_kelas'],
                        'id_tahun'          => $siswa['id_tahun'],
                        'nis'               => $siswa['nis'],
                        'nama_siswa'        => $siswa['nama_siswa'],
                        'panggilan_siswa'   => $siswa['panggilan_siswa'],
                        'panggilan_siswa'   => $siswa['panggilan_siswa'],
                        'tempat_lahir'      => $siswa['tempat_lahir'],
                        'tanggal_lahir'     => $siswa['tanggal_lahir'],
                        'umur'              => $umur,
                        'jenis_kelamin'     => $siswa['jenis_kelamin'],
                        'agama'             => $siswa['agama'],
                        'jumlah_saudara'    => $siswa['jumlah_saudara'],
                        'anak_ke'           => $siswa['anak_ke'],
                        'alamat'            => $siswa['alamat'],
                        'status_ortu'       => $siswa['status'],
                        'status_siswa'      => $siswa['status_siswa'],
                        'status_bayar'      => $siswa['status_bayar'],
                        'kode_kelas'        => $kelas['kode_kelas'],
                        'nama_kelas'        => $kelas['nama_kelas'],
                        'level_kelas'       => $kelas['level_kelas'],
                        'status_kelas'      => $kelas['status'],
                        'kode_tahun'        => $tahun[0]->kode_tahun,
                        'created_at'        => $tahun[0]->created_at
                    ];                       
                array_push($data, $arr);
                }
            }
        }
        $this->template->load('layout/template','Akademik/Transaksi/Mandi', compact('data'));
	}

	public function add()
	{
		// print_r($this->input->post()); die;
		$jenis_tambahan = $this->input->post('jenis_tambahan');

		if ($jenis_tambahan == 'overtime')
		{
			$jam = $this->input->post('jam');
			$menit = $this->input->post('menit');
			$conv_menit = $menit / 60;
			$total_jam = $jam + $conv_menit;
		} else {
			$total_jam = $this->input->post('jam');
		}

		if($jenis_tambahan == 'catering')
		{
			$kode_pegawai = NULL;
		} else {
			$kode_pegawai = $this->input->post('guru');
		}
		
		$data = [
			'nis' => $this->input->post('nis'),
			'jumlah_jam' => $total_jam,
			'total_tagihan' => $this->input->post('total_tagihan_input'),
			'kode_pegawai' => $kode_pegawai,
			'jenis_tambahan' => $jenis_tambahan,
            'status_lainnya' => 'Belum Lunas',
            'tanggal' => date("Y-m-d")
        ];

        $insertLainnya  = $this->m_tagihan->add_tagihan($data);
        $id             = $this->db->insert_id();
        
        //Jurnal Overtime
        if ($jenis_tambahan == 'overtime')
        {
            $jurnal = [
                'jurnal_master_id'          => NULL,
                'coa_id'                    => 43,
                'transaksi_lainnya_id'   	=> $id,
                'alt_name'                  => NULL,
                'tgl_jurnal'                => date('Y-m-d h:i:s'),
                'posisi'                    => 'debit',
                'nominal'                   => $this->input->post('total_tagihan_input'),
                'status'                    => 0
            ];
    
            $jurnal2 = [
                'jurnal_master_id'          => NULL,
                'coa_id'                    => 68,
                'transaksi_lainnya_id'  	=> $id,
                'alt_name'                  => NULL,
                'tgl_jurnal'                => date('Y-m-d h:i:s'),
                'posisi'                    => 'kredit',
                'nominal'                   => $this->input->post('total_tagihan_input'),
                'status'                    => 0
            ];
        }
        //Jurnal Mandi
        if ($jenis_tambahan == 'catering')
        {
            $jurnal = [
                'jurnal_master_id'          => NULL,
                'coa_id'                    => 44,
                'transaksi_lainnya_id'   	=> $id,
                'alt_name'                  => NULL,
                'tgl_jurnal'                => date('Y-m-d h:i:s'),
                'posisi'                    => 'debit',
                'nominal'                   => $this->input->post('total_tagihan_input'),
                'status'                    => 0
            ];
    
            $jurnal2 = [
                'jurnal_master_id'          => NULL,
                'coa_id'                    => 69,
                'transaksi_lainnya_id'  	=> $id,
                'alt_name'                  => NULL,
                'tgl_jurnal'                => date('Y-m-d h:i:s'),
                'posisi'                    => 'kredit',
                'nominal'                   => $this->input->post('total_tagihan_input'),
                'status'                    => 0
            ];
        }
        //Jurnal Sarapan
        if ($jenis_tambahan == 'sarapan')
        {
            $jurnal = [
                'jurnal_master_id'          => NULL,
                'coa_id'                    => 45,
                'transaksi_lainnya_id'   	=> $id,
                'alt_name'                  => NULL,
                'tgl_jurnal'                => date('Y-m-d h:i:s'),
                'posisi'                    => 'debit',
                'nominal'                   => $this->input->post('total_tagihan_input'),
                'status'                    => 0
            ];
    
            $jurnal2 = [
                'jurnal_master_id'          => NULL,
                'coa_id'                    => 70,
                'transaksi_lainnya_id'  	=> $id,
                'alt_name'                  => NULL,
                'tgl_jurnal'                => date('Y-m-d h:i:s'),
                'posisi'                    => 'kredit',
                'nominal'                   => $this->input->post('total_tagihan_input'),
                'status'                    => 0
            ];
        }
        //Jurnal Mandi
        if ($jenis_tambahan == 'mandi')
        {
            $jurnal = [
                'jurnal_master_id'          => NULL,
                'coa_id'                    => 48,
                'transaksi_lainnya_id'   	=> $id,
                'alt_name'                  => NULL,
                'tgl_jurnal'                => date('Y-m-d h:i:s'),
                'posisi'                    => 'debit',
                'nominal'                   => $this->input->post('total_tagihan_input'),
                'status'                    => 0
            ];
    
            $jurnal2 = [
                'jurnal_master_id'          => NULL,
                'coa_id'                    => 72,
                'transaksi_lainnya_id'  	=> $id,
                'alt_name'                  => NULL,
                'tgl_jurnal'                => date('Y-m-d h:i:s'),
                'posisi'                    => 'kredit',
                'nominal'                   => $this->input->post('total_tagihan_input'),
                'status'                    => 0
            ];
        }

        $dataJurnal = [$jurnal, $jurnal2];

        $insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);
        

		if($insertLainnya){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diinput','danger'));
		}
        redirect('akademik/transaksi/'.$jenis_tambahan);
	}
	

}