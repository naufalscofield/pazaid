<?php 

class PendaftaranSiswa extends CI_Controller{

    function __construct()
    {
		parent::__construct();
		$this->load->model('Akademik/Transaksi/model_siswa', 'm_siswa');
		$this->load->model('Akademik/MasterData/Master_model', 'm_master');
		$this->load->model('Keuangan/Jurnal_model', 'm_jurnal');
		$this->load->model('Akademik/Transaksi/model_transaksi_pendaftaran', 'm_transaksi');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','akademik');
	}

    public function index()
    {
        $this->template->load('layout/template','Akademik/Transaksi/PendaftaranSiswa');
    }

    public function add()
    {
        $post = $this->input->post();
        $this->id_siswa         = uniqid();
		$this->nama_siswa       = $post['nama_siswa'];
		$this->panggilan_siswa  = $post['panggilan_siswa'];
		$this->tempat_lahir     = $post['tempat_lahir'];
		$this->tanggal_lahir    = $post['tanggal_lahir'];
		$this->jenis_kelamin    = $post['jenis_kelamin'];
		$this->agama            = $post['agama'];
		$this->jumlah_saudara   = $post['jumlah_saudara'];
		$this->anak_ke          = $post['anak_ke'];
		$this->alamat           = $post['alamat'];
		$this->id_kelas         = $post['id_kelas'];
		$this->status           = $post['status'];
		$this->status_bayar     = $post['status_bayar'];
		
        
        // $dataInsert = [

        // ]
		$this->form_validation->set_data($post);
        $this->form_validation->set_rules('nama_siswa', 'Nama Lengkap', 'required');

        if($this->form_validation->run() == TRUE)
        {
            if($this->m_siswa->insert($post))
            {
                $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan','success'));
            }else{
                $this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dimasukkan','danger'));
            }
        }else{
            $this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
        }
        redirect('akademik/masterdata/siswacalon');
    }

    public function PembayaranSiswa()
    {
        $id_siswa = $this->input->post('id_siswa');
        

        // $jenis_pembayaran = $this->input->post('jenis_pembayaran');

        // if ($jenis_pembayaran == 'transfer')
        // {
        //     $kode_rek = $this->input->post('rekening');
        // } else {
        //     $kode_rek = NULL;
        // }


        $id_tahun = $this->input->post('id_tahun_ajaran');

        $total_tagihan = $this->input->post('total_tagihan');
        $biaya_dpp = $this->input->post('biaya_dpp');
        $biaya_dsk = $this->input->post('biaya_dsk');
        $biaya_pendaftaran = $this->input->post('biaya_pendaftaran');
        $biaya_seragam = $this->input->post('biaya_seragam');

        $data = [
            'id_siswa' => $id_siswa,
            'id_tahun' => $id_tahun,
            'biaya_pendaftaran' => $biaya_pendaftaran,
            'biaya_seragam' => $biaya_seragam,
            'biaya_dpp' => $biaya_dpp,
            'biaya_dsk' => $biaya_dsk,
            'jenis_pembayaran' => NULL,
            'kode_rek' => NULL,
            'total_tagihan' => $total_tagihan,
            'total_pembayaran' => NULL,
            'sisa_tagihan' => NULL,
            'status' => 2,
            'tanggal_transaksi' => NULL,
            'tanggal_pendaftaran' => date('Y-m-d')
        ];

        $data2 = [
            'id_tahun' => $id_tahun,
        ];

        $this->m_transaksi->store($data);
        $idLast = $this->db->insert_id();
        $this->m_siswa->update($data2, $id_siswa);

        $jurnal1 = [
            'jurnal_master_id'          => NULL,
            'coa_id'                    => 110,
            'transaksi_pendaftaran_id'  => $idLast,
            'transaksi_pembayaran_id'   => NULL,
            'alt_name'                  => NULL,
            'tgl_jurnal'                => date('Y-m-d h:i:s'),
            'posisi'                    => 'debit',
            'nominal'                   => $biaya_pendaftaran,
            'status'                    => 0
        ];

        $jurnal2 = [
            'jurnal_master_id'          => NULL,
            'coa_id'                    => 109,
            'transaksi_pendaftaran_id'  => $idLast,
            'transaksi_pembayaran_id'   => NULL,
            'alt_name'                  => NULL,
            'tgl_jurnal'                => date('Y-m-d h:i:s'),
            'posisi'                    => 'kredit',
            'nominal'                   => $biaya_pendaftaran,
            'status'                    => 0
        ];

        $jurnal3 = [
            'jurnal_master_id'          => NULL,
            'coa_id'                    => 29,
            'transaksi_pendaftaran_id'  => $idLast,
            'transaksi_pembayaran_id'   => NULL,
            'alt_name'                  => NULL,
            'tgl_jurnal'                => date('Y-m-d h:i:s'),
            'posisi'                    => 'debit',
            'nominal'                   => $biaya_seragam,
            'status'                    => 0
        ];

        $jurnal4 = [
            'jurnal_master_id'          => NULL,
            'coa_id'                    => 30,
            'transaksi_pendaftaran_id'  => $idLast,
            'transaksi_pembayaran_id'   => NULL,
            'alt_name'                  => NULL,
            'tgl_jurnal'                => date('Y-m-d h:i:s'),
            'posisi'                    => 'kredit',
            'nominal'                   => $biaya_seragam,
            'status'                    => 0
        ];

        $jurnal5 = [
            'jurnal_master_id'          => NULL,
            'coa_id'                    => 31,
            'transaksi_pendaftaran_id'  => $idLast,
            'transaksi_pembayaran_id'   => NULL,
            'alt_name'                  => NULL,
            'tgl_jurnal'                => date('Y-m-d h:i:s'),
            'posisi'                    => 'debit',
            'nominal'                   => $biaya_dpp,
            'status'                    => 0
        ];

        $jurnal6 = [
            'jurnal_master_id'          => NULL,
            'coa_id'                    => 32,
            'transaksi_pendaftaran_id'  => $idLast,
            'transaksi_pembayaran_id'   => NULL,
            'alt_name'                  => NULL,
            'tgl_jurnal'                => date('Y-m-d h:i:s'),
            'posisi'                    => 'kredit',
            'nominal'                   => $biaya_dpp,
            'status'                    => 0
        ];

        $jurnal7 = [
            'jurnal_master_id'          => NULL,
            'coa_id'                    => 33,
            'transaksi_pendaftaran_id'  => $idLast,
            'transaksi_pembayaran_id'   => NULL,
            'alt_name'                  => NULL,
            'tgl_jurnal'                => date('Y-m-d h:i:s'),
            'posisi'                    => 'debit',
            'nominal'                   => $biaya_dsk,
            'status'                    => 0
        ];

        $jurnal8 = [
            'jurnal_master_id'          => NULL,
            'coa_id'                    => 34,
            'transaksi_pendaftaran_id'  => $idLast,
            'transaksi_pembayaran_id'   => NULL,
            'alt_name'                  => NULL,
            'tgl_jurnal'                => date('Y-m-d h:i:s'),
            'posisi'                    => 'kredit',
            'nominal'                   => $biaya_dsk,
            'status'                    => 0
        ];

        $dataJurnal = [$jurnal1, $jurnal3, $jurnal5, $jurnal7, $jurnal2, $jurnal4, $jurnal6, $jurnal8];

        $insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);
        // $insertJurnal = $this->m_jurnal->insert($jurnal1);
        // $insertJurnal2 = $this->m_jurnal->insert($jurnal2);

        redirect('akademik/masterdata/siswacalon');

    }
}