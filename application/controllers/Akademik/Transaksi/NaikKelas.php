<?php 

class NaikKelas extends CI_Controller{

    function __construct()
    {
		parent::__construct();
		$this->load->model('Akademik/Transaksi/model_siswa', 'm_siswa');
		$this->load->model('Akademik/Transaksi/model_transaksi_pendaftaran', 'm_t_pendaftaran');
		$this->load->model('Akademik/Transaksi/model_tagihan_lainnya', 'm_t_lainnya');
		$this->load->model('Akademik/Transaksi/model_biaya_umum', 'm_umum');
		$this->load->model('Akademik/MasterData/Master_model', 'm_master');
		$this->load->model('Akademik/MasterData/Kelas_model', 'm_kelas');
		$this->load->model('Akademik/Transaksi/model_transaksi_pendaftaran', 'm_transaksi');
		$this->load->model('keuangan/Jurnal_model', 'm_jurnal');
        $this->load->model('Keuangan/model_tunai', 'm_tunai');
		$this->load->model('Keuangan/rekening_model', 'm_rekening');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','akademik');
	}

    public function index()
    {
        $this->template->load('layout/template','Akademik/Transaksi/NaikKelas');
	}
	
	public function ajaxSiswa($nis)
	{
		$tahun = $this->m_master->get_active();
		$id_tahun = $tahun->id_tahun - 1;
		$siswa = $this->m_siswa->get_detail_join($nis)->row();
		$tunggakovertime = $this->m_t_lainnya->get_tagihan_overtime($nis);
		$tunggakUmum = $this->m_t_pendaftaran->get_tagihan($siswa->id_siswa, $id_tahun);
		$tunggakcatering = $this->m_t_lainnya->get_tagihan_catering($nis);
		$tunggakmandi = $this->m_t_lainnya->get_tagihan_mandi($nis);
		$tunggaksarapan = $this->m_t_lainnya->get_tagihan_sarapan($nis);

		$sisaovertime = 0;
		$sisacatering = 0;
		$sisamandi = 0;
		$sisasarapan = 0;


		if ($tunggakovertime->num_rows() != 0)
		{
			foreach ($tunggakovertime->result_array() as $o) {
				$sisaovertime += $o['total_tagihan'];
			}
		}
		if ($tunggakcatering->num_rows() != 0)
		{
			foreach ($tunggakcatering->result_array() as $o) {
				$sisacatering += $o['total_tagihan'];
			}
		}
		if ($tunggakmandi->num_rows() != 0)
		{
			foreach ($tunggakmandi->result_array() as $o) {
				$sisamandi += $o['total_tagihan'];
			}
		}
		if ($tunggaksarapan->num_rows() != 0)
		{
			foreach ($tunggaksarapan->result_array() as $o) {
				$sisasarapan += $o['total_tagihan'];
			}
		}

		if ($tunggakUmum != NULL)
		{
			$sisadpp = $tunggakUmum->biaya_dpp - $tunggakUmum->cicilan_dpp;
			$sisadsk = $tunggakUmum->biaya_dsk - $tunggakUmum->cicilan_dsk;
		} else {
			$sisadpp = 0;
			$sisadsk = 0;
		}
		
		if ($siswa->jenis_kelamin == 'Perempuan')
		{
			$jk = 2;
		} else {
			$jk = 1;
		}

		$biayaumum = $this->m_umum->getByJK($jk);

		if ($siswa->status_ortu == 'Anak Guru')
		{
			$biayadpp = 0.4 * $biayaumum['biaya_dpp'];
			$biayadsk = 0.4 * $biayaumum['biaya_dsk'];
		} else {
			$biayadpp = $biayaumum['biaya_dpp'];
			$biayadsk = $biayaumum['biaya_dsk'];
		}
		$data = [
			'siswa' => $siswa,
			'sisadpp' => $sisadpp,
			'sisadsk' => $sisadsk,
			'sisaovertime' => $sisaovertime,
			'sisacatering' => $sisacatering,
			'sisamandi' => $sisamandi,
			'sisasarapan' => $sisasarapan,
			'dppbaru' => $biayadpp,
			'dskbaru' => $biayadsk,
			'seragambaru' => $biayaumum['biaya_seragam'],
		];

        echo json_encode($data);
	}

	public function ajaxKelas($level)
	{
		if ($level == 'Bayi')
		{
			$next = 'Playgroup';
		} else if ($kelas == 'Playgroup')
		{
			$next = 'TK';
		}

		$kelas = $this->m_kelas->get_detail('level_kelas', $next)->result_array();

		echo json_encode($kelas);
	}

	public function ajaxKelasDetail($id)
	{
		$kelas = $this->m_kelas->get_detail('id_kelas', $id)->row();

		echo json_encode($kelas);
	}
	
	public function aksiNaik()
	{
		$tahun = $this->m_master->get_active();
		$id_tahun = $tahun->id_tahun;
		$datasiswa = [
			'id_tahun' => $id_tahun,
			'id_kelas' => $this->input->post('id_kelas_selanjutnya'),
			'status_bayar' => 'Belum Bayar',
		];

		if ($this->input->post('jenis_pembayaran') == 'cash'){
			$kode_rek = NULL;
		} else {
			$kode_rek = $this->input->post('no_rek');
		}
		
		$updatesiswa = $this->m_siswa->update($datasiswa, $this->input->post('id_siswa'));
		
		$datatransaksi = [
			'id_siswa' => $this->input->post('id_siswa'),
			'id_tahun' => $id_tahun,
			'biaya_pendaftaran' => 0,
			'biaya_seragam' => $this->input->post('seragam_baru'),
			'biaya_dpp' => $this->input->post('dpp_baru'),
			'biaya_dsk' => $this->input->post('dsk_baru'),
			'cicilan_dpp' => 0,
			'cicilan_dsk' => 0,
			'jenis_pembayaran' => $this->input->post('jenis_pembayaran'),
			'kode_rek' => $kode_rek,
			'total_tagihan' => $this->input->post('seragam_baru') + $this->input->post('dpp_baru') + $this->input->post('dsk_baru'),
			'sisa_tagihan' => $this->input->post('seragam_baru') + $this->input->post('dpp_baru') + $this->input->post('dsk_baru'),
			'status' => 2
		];

		$storetransaksi = $this->m_t_pendaftaran->store($datatransaksi);
		$idTransaksi 	= $this->db->insert_id();

		$jurnal1 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 29,
			'transaksi_pendaftaran_id'  => $idTransaksi,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'debit',
			'nominal'                   => $this->input->post('seragam_baru'),
			'status'                    => 0
		];

		$jurnal2 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 31,
			'transaksi_pendaftaran_id'  => $idTransaksi,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'debit',
			'nominal'                   => $this->input->post('dpp_baru'),
			'status'                    => 0
		];

		$jurnal3 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 33,
			'transaksi_pendaftaran_id'  => $idTransaksi,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'debit',
			'nominal'                   => $this->input->post('dsk_baru'),
			'status'                    => 0
		];

		$jurnal4 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 30,
			'transaksi_pendaftaran_id'  => $idTransaksi,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'kredit',
			'nominal'                   => $this->input->post('seragam_baru'),
			'status'                    => 0
		];

		$jurnal5 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 32,
			'transaksi_pendaftaran_id'  => $idTransaksi,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'kredit',
			'nominal'                   => $this->input->post('dpp_baru'),
			'status'                    => 0
		];

		$jurnal6 = [
			'jurnal_master_id'          => NULL,
			'coa_id'                    => 34,
			'transaksi_pendaftaran_id'  => $idTransaksi,
			'alt_name'                  => NULL,
			'tgl_jurnal'                => date('Y-m-d h:i:s'),
			'posisi'                    => 'kredit',
			'nominal'                   => $this->input->post('dsk_baru'),
			'status'                    => 0
		];

		$dataJurnal = [$jurnal1, $jurnal2, $jurnal3, $jurnal4, $jurnal5, $jurnal6];
	
		$insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);

		if($updatesiswa && $storetransaksi){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diinput','danger'));
		}
        redirect('akademik/transaksi/naikkelas');


	}


}