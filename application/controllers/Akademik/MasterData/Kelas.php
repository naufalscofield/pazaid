<?php 

class Kelas extends CI_Controller{

    function __construct()
    {
		parent::__construct();
		$this->load->model('Akademik/MasterData/Kelas_model', 'm_kelas');
		$this->load->model('Akademik/MasterData/Master_model', 'm_tahun');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','akademik');
	}

    public function index()
    {
        $list                   = $this->m_kelas->get_data();
        // var_dump($list); die;
        $generateBB             = $this->m_kelas->generate_code_bayi();
        $data['generatePG']     = $this->m_kelas->generate_code_pg();
        $data['generateTK']     = $this->m_kelas->generate_code_tk();
        $tahun                  = $this->m_tahun->get_active();
        $this->template->load('layout/template','Akademik/MasterData/Kelas', compact('data','tahun','list','generateBB'));
    }

    public function ajaxKelas()
    {
        $data = $this->m_kelas->get_data();
        echo json_encode($data);
    }

    public function add()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_data($post);
        
        $this->form_validation->set_rules('nama_kelas', 'Nama Kelas', 'required');
        $this->form_validation->set_rules('level_kelas', 'Level Kelas', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        
        if($this->form_validation->run() == TRUE)
        {
            if($this->input->post('level_kelas') == 'Bayi')
            {
                $kode   = $this->m_kelas->generate_code_bayi();
                $data   = [
                'id_kelas'         => uniqid(),
                'kode_kelas'   	    => $kode,
                'nama_kelas'     	=> $post['nama_kelas'],
                'level_kelas'     	=> $post['level_kelas'],
                'status'     		=> $post['status']
                ];
                $this->m_kelas->insert($data);
                $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan','success'));
            }elseif($this->input->post('level_kelas') == 'Playgroup'){
                $kode   = $this->m_kelas->generate_code_pg();
                $data   = [
                    'id_kelas'         => uniqid(),
                    'kode_kelas'   	    => $kode,
                    'nama_kelas'     	=> $post['nama_kelas'],
                    'level_kelas'     	=> $post['level_kelas'],
                    'status'     		=> $post['status']
                    ];
                    $this->m_kelas->insert($data);
                $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan','success'));
            }elseif($this->input->post('level_kelas') == 'TK'){
                $kode   = $this->m_kelas->generate_code_tk();
                $data   = [
                    'id_kelas'         => uniqid(),
                    'kode_kelas'   	    => $kode,
                    'nama_kelas'     	=> $post['nama_kelas'],
                    'level_kelas'     	=> $post['level_kelas'],
                    'status'     		=> $post['status']
                    ];
                    $this->m_kelas->insert($data);
                $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan','success'));
            }else{
            $this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dimasukkan','danger'));
            }
        }else{
        $this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
        }
        redirect('akademik/masterdata/kelas');
    }

    public function delete($id){
		if($this->m_kelas->delete($id)){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dihapus','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dihapus','danger'));
		}
        redirect('akademik/kelas');
	}
}