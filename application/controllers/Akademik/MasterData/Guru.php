<?php 

class Guru extends CI_Controller{

    function __construct()
    {
		parent::__construct();
		$this->load->model('Akademik/MasterData/Model_guru', 'm_guru');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','akademik');
    }
    
    public function ajaxGuru()
    {
        $data = $this->m_guru->get_data();

        echo json_encode($data);
    }

}