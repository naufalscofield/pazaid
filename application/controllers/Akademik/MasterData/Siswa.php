<?php 

Class Siswa extends CI_controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Akademik/Transaksi/model_siswa', 'm_siswa');
        $this->load->model('Akademik/Transaksi/model_biaya_umum', 'm_biaya_umum');
        $this->load->model('Akademik/Transaksi/model_biaya_spp', 'm_biaya_spp');
        $this->load->model('Akademik/Transaksi/model_transaksi_spp', 'm_transaksi_spp');
        $this->load->model('Akademik/Transaksi/model_transaksi_pendaftaran', 'm_daftar');
        $this->load->model('Akademik/MasterData/Kelas_model', 'm_kelas');
        $this->load->model('Akademik/MasterData/Master_model', 'm_tahun');
        $this->load->model('keuangan/Jurnal_model', 'm_jurnal');
        $this->load->model('Keuangan/model_tunai', 'm_tunai');
		$this->load->model('Keuangan/rekening_model', 'm_rekening');


        if(!$this->session->userdata('login')){
            redirect('');
        }

        $this->session->set_userdata('menu','akademik');
    }
        public function index()
    {
        $tahun = $this->m_tahun->get_active();
        $id_tahun = $tahun->id_tahun;
        
        $data   = $this->m_siswa->get_data();
        $siswa  = $data[0]['id_siswa'];
        $daftar = $this->m_daftar->get_tagihan($siswa, $id_tahun);
        $this->template->load('layout/template','Akademik/MasterData/CalonSiswa', compact('data','daftar'));
    }

    public function deleteUndurDiri($id)
    {
            $post   = $this->input->post();
            $this->status_siswa = $post['status_siswa'];
    
            $this->form_validation->set_data($post);
            $this->form_validation->set_rules('id_siswa', 'id Siswa', 'required');
    
            if($this->form_validation->run() == TRUE){
    
                if($this->m_siswa->deleteUndur($post, $id)){
                    $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diubah','success'));
                }else{
                    $this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diubah','danger'));
                }
    
            }else{
                $this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
            }
    
            redirect('akademik/masterdata/siswacalon');

    }

    public function deleteSiswaTetap($id)
    {
            $post   = $this->input->post();
            $this->status_siswa = $post['status_siswa'];
    
            $this->form_validation->set_data($post);
            $this->form_validation->set_rules('id_siswa', 'id Siswa', 'required');

            $dataTransaksi = $this->m_daftar->get_detail('id_siswa', $id);

            if ($dataTransaksi->cicilan_dpp == 0)
            {
                $sisaDpp = 0;
                $sisaDsk = 0;
                $cashDone = $dataTransaksi->biaya_dpp + $dataTransaksi->biaya_dsk;
            } else {
                $sisaDpp = $dataTransaksi->biaya_dpp - $dataTransaksi->cicilan_dpp;
                $sisaDsk = $dataTransaksi->biaya_dsk - $dataTransaksi->cicilan_dsk;
                $cashDone = $dataTransaksi->cicilan_dpp + $dataTransaksi->cicilan_dsk;
            }

            if ($dataTransaksi->jenis_pembayaran == 'cash')
            {
                $coa = 26;
                $saldoTunai = $this->m_tunai->get_detail('id', 1)->row();
                $saldo = $saldoTunai->saldo;
                $bayar = $cashDone;
                $saldoNew = $saldo + $bayar;
                $dataUpd = [
                    'saldo'	=> $saldoNew
                ];
                $update = $this->m_tunai->update($dataUpd, 1);
            } else {
                $coa = 3;
                $getRekening = $this->m_rekening->get_detail('no_rek', $dataTransaksi->no_rek)->row();
                $saldo = $getRekening->saldo;
                $bayar = $cashDone;
                $saldoNew = $saldo + $bayar;
                $dataUpd = [
                    'saldo'	=> $saldoNew
                ];
                $update = $this->m_rekening->update($dataUpd, $getRekening->id);
            }

            $jurnal1 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => 32,
				'transaksi_pendaftaran_id'  => $dataTransaksi->id,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'debit',
				'nominal'                   => $dataTransaksi->biaya_dpp,
				'status'                    => 0
			];

            $jurnal2 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => 34,
				'transaksi_pendaftaran_id'  => $dataTransaksi->id,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'debit',
				'nominal'                   => $dataTransaksi->biaya_dsk,
				'status'                    => 0
			];

            $jurnal3 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => 31,
				'transaksi_pendaftaran_id'  => $dataTransaksi->id,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'kredit',
				'nominal'                   => $sisaDpp,
				'status'                    => 0
			];

            $jurnal4 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => 33,
				'transaksi_pendaftaran_id'  => $dataTransaksi->id,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'kredit',
				'nominal'                   => $sisaDsk,
				'status'                    => 0
			];
	
            $jurnal5 = [
				'jurnal_master_id'          => NULL,
				'coa_id'                    => $coa,
				'transaksi_pendaftaran_id'  => $dataTransaksi->id,
				'alt_name'                  => NULL,
				'tgl_jurnal'                => date('Y-m-d h:i:s'),
				'posisi'                    => 'kredit',
				'nominal'                   => $cashDone,
				'status'                    => 0
			];
	
			$dataJurnal = [$jurnal1, $jurnal2, $jurnal3, $jurnal4, $jurnal5];
	
            $insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);
            
            if($this->form_validation->run() == TRUE){
    
                if($this->m_siswa->deleteUndur($post, $id)){
                    $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diubah','success'));
                }else{
                    $this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diubah','danger'));
                }
    
            }else{
                $this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
            }
    
            redirect('akademik/masterdata/siswatetap');

    }

    public function getUndurDiri()
    {
        $data = $this->m_siswa->get_data();
        $this->template->load('layout/template','Akademik/MasterData/SiswaUndur', compact('data'));
    }

    public function deleteSiswa($id)
    {
        if($this->m_siswa->delete($id)){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dihapus','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dihapus','danger'));
		}
        redirect('akademik/masterdata/siswaundurdiri');
    }

    public function getSiswaTetap()
    {
        $data1  = $this->m_siswa->get_data();
        $data2  = $this->m_kelas->get_data();
        $data   = [];
        foreach($data1 as $siswa)
        {
            foreach($data2 as $kelas)
            {
                if($siswa['id_kelas'] == $kelas['id_kelas'])
                {
                    $tahun  = $this->m_tahun->getById($siswa['id_tahun']);
                    $lahir  = new DateTime($siswa['tanggal_lahir']);
                    $today  = new DateTime();
                    $umur   = $today->diff($lahir)->y;
                    if (isset($tahun[0]->kode_tahun))
                    {
                        $kodeTahun = $tahun[0]->kode_tahun;
                    } else {
                        $kodeTahun = '-';
                    }
                    if (isset($tahun[0]->created_at))
                    {
                        $createdAt = $tahun[0]->created_at;
                    } else {
                        $createdAt = '-';
                    }
                    $arr = [
                        'id_siswa'          => $siswa['id_siswa'],
                        'id_kelas'          => $siswa['id_kelas'],
                        'id_tahun'          => $siswa['id_tahun'],
                        'nis'               => $siswa['nis'],
                        'nama_siswa'        => $siswa['nama_siswa'],
                        'panggilan_siswa'   => $siswa['panggilan_siswa'],
                        'panggilan_siswa'   => $siswa['panggilan_siswa'],
                        'tempat_lahir'      => $siswa['tempat_lahir'],
                        'tanggal_lahir'     => $siswa['tanggal_lahir'],
                        'umur'              => $umur,
                        'jenis_kelamin'     => $siswa['jenis_kelamin'],
                        'agama'             => $siswa['agama'],
                        'jumlah_saudara'    => $siswa['jumlah_saudara'],
                        'anak_ke'           => $siswa['anak_ke'],
                        'alamat'            => $siswa['alamat'],
                        'status_ortu'       => $siswa['status'],
                        'status_siswa'      => $siswa['status_siswa'],
                        'status_bayar'      => $siswa['status_bayar'],
                        'kode_kelas'        => $kelas['kode_kelas'],
                        'nama_kelas'        => $kelas['nama_kelas'],
                        'level_kelas'       => $kelas['level_kelas'],
                        'status_kelas'      => $kelas['status'],
                        'kode_tahun'        => $kodeTahun,
                        'created_at'        => $createdAt
                    ];                       
                array_push($data, $arr);
                }
            }
        }
        $this->template->load('layout/template','Akademik/MasterData/SiswaTetap', compact('data'));
    }

    public function ajaxOrangTuaSiswa($idSiswa)
    {
        $data = $this->m_siswa->getOrtu($idSiswa);
        echo json_encode($data);
    }
   
    public function ajaxSiswaTetap($nis)
    {
        $data = $this->m_siswa->get_detail('nis', $nis)->row();
        echo json_encode($data);
    }
    
    public function ajaxBiayaByJK($jk, $idSiswa)
    {
        $biaya = $this->m_biaya_umum->getByJK($jk);
        $siswa = $this->m_siswa->get_detail('id_siswa', $idSiswa)->row();

        $data = [
            'biaya' => $biaya,
            'siswa' => $siswa,
        ];
        echo json_encode($data);
    }

    public function ajaxBiayaSpp($idSiswa)
    {
        $siswa = $this->m_siswa->get_detail('id_siswa', $idSiswa)->row();
        $id_kelas = $siswa->id_kelas;
        $kelas = $this->m_kelas->get_detail('id_kelas', $id_kelas)->row();
        $biaya = $this->m_biaya_spp->get_where($kelas)->row();

        if ($siswa->status == 'Anak Guru')
        {
            $biayaFix = 0.40 * $biaya->biaya;
        } else {
            $biayaFix = $biaya->biaya;
        }
        $bulan  = $this->m_transaksi_spp->get_bulan($idSiswa);
        $totalBulan = $bulan->num_rows();
        if ($totalBulan == 0)
        {
            $bulanKe = 1;
        } else {
            $bulanKe = $totalBulan+1;
        }

        $data = [
            'biaya' => $biayaFix,
            'siswa' => $siswa,
            'kelas' => $kelas,
            'bulan' => $bulanKe
        ];
        echo json_encode($data);
    }

    public function BuatTagihanSpp()
    {
        $siswa      = $this->m_siswa->get_data();
        $totalSpp   = 0;
            foreach($siswa as $s)
            {
                $id_siswa   = $s['id_siswa'];
                $id_tahun   = $s['id_tahun'];

                $bulan  = $this->m_transaksi_spp->get_bulan($id_siswa);
                $totalBulan = $bulan->num_rows();
                if ($totalBulan == 0)
                {
                    $bulanKe = 1;
                } else {
                    $bulanKe = $totalBulan+1;
                }

                $kelas = $this->m_kelas->get_detail('id_kelas', $s['id_kelas'])->row();

                $biaya = $this->m_biaya_spp->get_where($kelas)->row();

                if ($s['status'] == 'Anak Guru')
                {
                    $biayaFix = 0.60 * $biaya->biaya;
                } else {
                    $biayaFix = $biaya->biaya;
                }

                $totalSpp += $biayaFix;

                $data = [
                    'id_siswa' => $id_siswa,
                    'id_tahun' => $id_tahun,
                    'bulan_ke' => $bulanKe,
                    'biaya_spp' => $biayaFix,
                    'status_spp'    => 'Belum Dibayar'
                ];

                $this->m_transaksi_spp->add_tagihan($data);
            }


            $jurnal = [
                'jurnal_master_id'          => NULL,
                'coa_id'                    => 35,
                'transaksi_pembayaran_id'   => NULL,
                'alt_name'                  => NULL,
                'tgl_jurnal'                => date('Y-m-d h:i:s'),
                'posisi'                    => 'debit',
                'nominal'                   => $totalSpp,
                'status'                    => 0
            ];
    
            $jurnal1 = [
                'jurnal_master_id'          => NULL,
                'coa_id'                    => 36,
                'transaksi_pembayaran_id'   => NULL,
                'alt_name'                  => NULL,
                'tgl_jurnal'                => date('Y-m-d h:i:s'),
                'posisi'                    => 'kredit',
                'nominal'                   => $totalSpp,
                'status'                    => 0
            ];

            $dataJurnal = [$jurnal, $jurnal1];
	
			$insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);

            $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
            redirect('akademik/masterdata/siswatetap');

        $data = [
            'id_siswa' => $this->input->post('id_siswa'),
            'id_tahun' => $this->input->post('id_tahun_ajaran'),
            'bulan_ke' => $this->input->post('bulan_ke'),
            'biaya_spp' => $this->input->post('biaya_spp'),
            'jenis_pembayaran' => $this->input->post('jenis_pembayaran'),
            'kode_rek' => $this->input->post('kode_rek'),
        ];

        

        if($this->m_transaksi_spp->add_tagihan($data)){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diinput','danger'));
		}
        redirect('akademik/masterdata/siswatetap');
    }
}