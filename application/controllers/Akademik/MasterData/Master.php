<?php 

class Master extends CI_Controller{

    function __construct()
    {
		parent::__construct();
		$this->load->model('Akademik/MasterData/Master_model', 'm_master');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','akademik');
	}

    public function index()
    {
        $data['list']       = $this->m_master->get_data();
        $data['generate']   = $this->m_master->generate_code();
        $this->template->load('layout/template','Akademik/MasterData/TahunAjaran',$data);
    }

    public function ajaxTahunAjaran()
    {
        $data = $this->m_master->get_active();
        echo json_encode($data);
    }

    public function add()
    {
        $post = $this->input->post();
        $this->id_tahun     = uniqid();
        $this->created_at   = $post['created_at'];
        $this->status       = $post['status'];
        $this->form_validation->set_data($post);
        $this->form_validation->set_rules('created_at', 'Tahun Ajaran', 'required');

        if($this->form_validation->run() == TRUE)
        {
            if($this->m_master->insert($post))
            {
                $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan','success'));
                $data['generate']   = $this->m_master->generate_code();
            }else{
                $this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dimasukkan','danger'));
            }
        }else{
            $this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
        }
        redirect('akademik/masterdata/tahunajaran');
    }

    public function delete($id){
		if($this->m_master->delete($id)){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dihapus','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dihapus','danger'));
		}
        redirect('akademik/masterdata/tahunajaran');
    }
    
    public function updateaktif($id, $num)
    {
        if(in_array($num, ['1', '0'])){

			$this->db->update('ak_tahun_ajaran', ['status' => '0']);
			if($this->m_master->update(['status' => $num], $id)){
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-check"></i> Data berhasil diubah','success'));
			}else{
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diubah','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Jenis Approval tidak diketahui','danger'));
        }
        redirect('akademik/masterdata/tahunajaran');
    }
}