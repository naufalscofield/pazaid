<?php 

Class Daycare extends CI_controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Akademik/MasterData/Model_Daycare', 'm_daycare');
		$this->load->model('Keuangan/Jurnal_model', 'm_jurnal');

        // $this->load->model('Akademik/Transaksi/model_siswa', 'm_siswa');
        // $this->load->model('Akademik/Transaksi/model_biaya_umum', 'm_biaya_umum');
        // $this->load->model('Akademik/Transaksi/model_biaya_spp', 'm_biaya_spp');
        // $this->load->model('Akademik/Transaksi/model_transaksi_spp', 'm_transaksi_spp');
        // $this->load->model('Akademik/Transaksi/model_transaksi_pendaftaran', 'm_daftar');
        // $this->load->model('Akademik/MasterData/Kelas_model', 'm_kelas');
        // $this->load->model('Akademik/MasterData/Master_model', 'm_tahun');

        if(!$this->session->userdata('login')){
            redirect('');
        }

        $this->session->set_userdata('menu','akademik');
    }
    public function index()
    {
        $this->template->load('layout/template','Akademik/MasterData/Daycare');
    }

    public function store()
    {
        $lahir  = new DateTime($this->input->post('tanggal_lahir'));
        $today  = new DateTime();
        $umur   = $today->diff($lahir)->y;

        if ($this->input->post('jenis_daycare') == 'fullday')
        {
            $total_tagihan = 125000;
        } else {
            $total_tagihan = 75000;
        }

        $data = [
            'kode_transaksi' => generateRandom(8),
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'nama_orang_tua' => $this->input->post('nama_orang_tua'),
            'no_telp'       => $this->input->post('no_telp'),
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'tanggal_lahir' => $this->input->post('tanggal_lahir'),
            'umur'          => $umur,
            'jenis_daycare' => $this->input->post('jenis_daycare'),
            'total_tagihan' => $total_tagihan,
            'status'        => 'Belum Bayar',
            'created_at'    => date("Y-m-d")
        ];

        $storeDaycare = $this->m_daycare->store($data);
        $id             = $this->db->insert_id();

        $jurnal = [
            'jurnal_master_id'          => NULL,
            'coa_id'                    => 45,
            'transaksi_lainnya_id'   	=> $id,
            'alt_name'                  => NULL,
            'tgl_jurnal'                => date('Y-m-d h:i:s'),
            'posisi'                    => 'debit',
            'nominal'                   => $total_tagihan,
            'status'                    => 0
        ];

        $jurnal2 = [
            'jurnal_master_id'          => NULL,
            'coa_id'                    => 40,
            'transaksi_lainnya_id'  	=> $id,
            'alt_name'                  => NULL,
            'tgl_jurnal'                => date('Y-m-d h:i:s'),
            'posisi'                    => 'kredit',
            'nominal'                   => $total_tagihan,
            'status'                    => 0
        ];

    $dataJurnal = [$jurnal, $jurnal2];

    $insertJurnal = $this->m_jurnal->insert_multiple($dataJurnal);

        if($storeDaycare){
            $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
        }else{
            $this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diinput','danger'));
        }
        redirect('akademik/masterdata/daycare');

    }

    public function deleteUndurDiri($id)
    {
            $post   = $this->input->post();
            $this->status_siswa = $post['status_siswa'];
    
            $this->form_validation->set_data($post);
            $this->form_validation->set_rules('id_siswa', 'id Siswa', 'required');
    
            if($this->form_validation->run() == TRUE){
    
                if($this->m_siswa->deleteUndur($post, $id)){
                    $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diubah','success'));
                }else{
                    $this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diubah','danger'));
                }
    
            }else{
                $this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
            }
    
            redirect('akademik/masterdata/siswacalon');

    }

    public function deleteSiswaTetap($id)
    {
            $post   = $this->input->post();
            $this->status_siswa = $post['status_siswa'];
    
            $this->form_validation->set_data($post);
            $this->form_validation->set_rules('id_siswa', 'id Siswa', 'required');
    
            if($this->form_validation->run() == TRUE){
    
                if($this->m_siswa->deleteUndur($post, $id)){
                    $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diubah','success'));
                }else{
                    $this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diubah','danger'));
                }
    
            }else{
                $this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
            }
    
            redirect('akademik/masterdata/siswatetap');

    }

    public function getUndurDiri()
    {
        $data = $this->m_siswa->get_data();
        $this->template->load('layout/template','Akademik/MasterData/SiswaUndur', compact('data'));
    }

    public function deleteSiswa($id)
    {
        if($this->m_siswa->delete($id)){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dihapus','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dihapus','danger'));
		}
        redirect('akademik/masterdata/siswaundurdiri');
    }

    public function getSiswaTetap()
    {
        $data1  = $this->m_siswa->get_data();
        $data2  = $this->m_kelas->get_data();
        $data   = [];
        foreach($data1 as $siswa)
        {
            foreach($data2 as $kelas)
            {
                if($siswa['id_kelas'] == $kelas['id_kelas'])
                {
                    $tahun  = $this->m_tahun->getById($siswa['id_tahun']);
                    $lahir  = new DateTime($siswa['tanggal_lahir']);
                    $today  = new DateTime();
                    $umur   = $today->diff($lahir)->y;
                    $arr = [
                        'id_siswa'          => $siswa['id_siswa'],
                        'id_kelas'          => $siswa['id_kelas'],
                        'id_tahun'          => $siswa['id_tahun'],
                        'nis'               => $siswa['nis'],
                        'nama_siswa'        => $siswa['nama_siswa'],
                        'panggilan_siswa'   => $siswa['panggilan_siswa'],
                        'panggilan_siswa'   => $siswa['panggilan_siswa'],
                        'tempat_lahir'      => $siswa['tempat_lahir'],
                        'tanggal_lahir'     => $siswa['tanggal_lahir'],
                        'umur'              => $umur,
                        'jenis_kelamin'     => $siswa['jenis_kelamin'],
                        'agama'             => $siswa['agama'],
                        'jumlah_saudara'    => $siswa['jumlah_saudara'],
                        'anak_ke'           => $siswa['anak_ke'],
                        'alamat'            => $siswa['alamat'],
                        'status_ortu'       => $siswa['status'],
                        'status_siswa'      => $siswa['status_siswa'],
                        'status_bayar'      => $siswa['status_bayar'],
                        'kode_kelas'        => $kelas['kode_kelas'],
                        'nama_kelas'        => $kelas['nama_kelas'],
                        'level_kelas'       => $kelas['level_kelas'],
                        'status_kelas'      => $kelas['status'],
                        'kode_tahun'        => $tahun[0]->kode_tahun,
                        'created_at'        => $tahun[0]->created_at
                    ];                       
                array_push($data, $arr);
                }
            }
        }
        $this->template->load('layout/template','Akademik/MasterData/SiswaTetap', compact('data'));
    }

    public function ajaxOrangTuaSiswa($idSiswa)
    {
        $data = $this->m_siswa->getOrtu($idSiswa);
        echo json_encode($data);
    }
    
    public function ajaxBiayaByJK($jk, $idSiswa)
    {
        $biaya = $this->m_biaya_umum->getByJK($jk);
        $siswa = $this->m_siswa->get_detail('id_siswa', $idSiswa)->row();

        $data = [
            'biaya' => $biaya,
            'siswa' => $siswa,
        ];
        echo json_encode($data);
    }

    public function ajaxBiayaSpp($idSiswa)
    {
        $siswa = $this->m_siswa->get_detail('id_siswa', $idSiswa)->row();
        $id_kelas = $siswa->id_kelas;
        $kelas = $this->m_kelas->get_detail('id_kelas', $id_kelas)->row();
        $biaya = $this->m_biaya_spp->get_where($kelas)->row();

        if ($siswa->status == 'Anak Guru')
        {
            $biayaFix = 0.40 * $biaya->biaya;
        } else {
            $biayaFix = $biaya->biaya;
        }
        $bulan  = $this->m_transaksi_spp->get_bulan($idSiswa);
        $totalBulan = $bulan->num_rows();
        if ($totalBulan == 0)
        {
            $bulanKe = 1;
        } else {
            $bulanKe = $totalBulan+1;
        }

        $data = [
            'biaya' => $biayaFix,
            'siswa' => $siswa,
            'kelas' => $kelas,
            'bulan' => $bulanKe
        ];
        echo json_encode($data);
    }

    public function BuatTagihanSpp()
    {
        $siswa = $this->m_siswa->get_data();

            foreach($siswa as $s)
            {
                $id_siswa   = $s['id_siswa'];
                $id_tahun   = $s['id_tahun'];

                $bulan  = $this->m_transaksi_spp->get_bulan($id_siswa);
                $totalBulan = $bulan->num_rows();
                if ($totalBulan == 0)
                {
                    $bulanKe = 1;
                } else {
                    $bulanKe = $totalBulan+1;
                }

                $kelas = $this->m_kelas->get_detail('id_kelas', $s['id_kelas'])->row();

                $biaya = $this->m_biaya_spp->get_where($kelas)->row();

                if ($s['status'] == 'Anak Guru')
                {
                    $biayaFix = 0.40 * $biaya->biaya;
                } else {
                    $biayaFix = $biaya->biaya;
                }

                $data = [
                    'id_siswa' => $id_siswa,
                    'id_tahun' => $id_tahun,
                    'bulan_ke' => $bulanKe,
                    'biaya_spp' => $biayaFix,
                    'status'    => 'Belum Dibayar'
                ];

                $this->m_transaksi_spp->add_tagihan($data);
            }

            $this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
            redirect('akademik/masterdata/siswatetap');

        $data = [
            'id_siswa' => $this->input->post('id_siswa'),
            'id_tahun' => $this->input->post('id_tahun_ajaran'),
            'bulan_ke' => $this->input->post('bulan_ke'),
            'biaya_spp' => $this->input->post('biaya_spp'),
            'jenis_pembayaran' => $this->input->post('jenis_pembayaran'),
            'kode_rek' => $this->input->post('kode_rek'),
        ];

        if($this->m_transaksi_spp->add_tagihan($data)){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diinput','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diinput','danger'));
		}
        redirect('akademik/masterdata/siswatetap');
    }
}