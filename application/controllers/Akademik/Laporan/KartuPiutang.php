<?php 

class KartuPiutang extends CI_Controller{

    function __construct()
    {
		parent::__construct();
		$this->load->model('Akademik/Transaksi/model_transaksi_pendaftaran', 'm_transaksi');
        $this->load->model('Akademik/Transaksi/model_transaksi_spp', 'm_spp');
        $this->load->model('Akademik/Transaksi/model_tagihan_lainnya', 'm_lainnya');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','akademik');
	}

    public function pendaftaran()
    {
        $data   = $this->m_transaksi->getDataSiswa();
        // var_dump($data); die;
        $this->template->load('layout/template','Akademik/Laporan/piutang', compact('data'));
    }

    public function spp()
    {
        $data = $this->m_spp->getDataSiswa();
        // var_dump($data); die;
        $this->template->load('layout/template','Akademik/Laporan/spp', compact('data'));
    }

    public function lainnya()
    {
        $data = $this->m_lainnya->getDataSiswa();
        // var_dump($data); die;
        $this->template->load('layout/template','Akademik/Laporan/lainnya', compact('data'));
    }
    
    public function ajaxLainnya($id_siswa)
    {
        $data = $this->m_lainnya->getDataSiswa();
        echo json_encode($data);
    }
}