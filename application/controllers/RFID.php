<?php 

class RFID extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('penggajian/pegawai_model', 'm_pegawai');
		$this->load->model('penggajian/absensi_model', 'm_absensi');	
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index(){
		$this->load->view('rfid');
	}

	public function insert(){
		$rfid = $this->input->post('rfid');
		$cek  = $this->m_pegawai->get_detail('rfid', $rfid);

		if($cek->num_rows() > 0){
			$pegawai = $cek->row_array();

			$jam  	 = date('H:i:s');
			$tanggal = date('Y-m-d');

			if(filterDay($tanggal, HARI_KERJA)){

				$tanggal = date('Y-m-d');
				$absen   = $this->m_absensi->get_detail([
												'DATE(waktu_masuk)' => $tanggal,
												'pegawai_id'		=> $pegawai['pegawai_id']
											]);

				if($absen->num_rows() > 0){
					$p = $absen->row_array();

					$s = true;

					if($p['waktu_masuk'] && !(strtotime($jam) > strtotime(JAM_ABSEN_PULANG) && strtotime($jam) < strtotime(JAM_AKHIR_PULANG))){
						$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-warning"></i> Absen Masuk sudah dilakukan hari ini','warning'));
						$s = false;

					}else if($p['waktu_keluar'] != ''){
						$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-warning"></i> Absen Pulang sudah dilakukan hari ini','warning'));
						$s = false;
					}

					if($s){
						if($this->m_absensi->update(['waktu_keluar' => $tanggal." ".$jam], $p['id'])){
				        		$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-check"></i> Absensi Pulang Berhasil Dilakukan','success'));

			        	}else{
			        		$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-times"></i> Absen Gagal Dimasukkan','danger'));
			        	}
					}

				}else{
					if(strtotime($jam) > strtotime(JAM_MULAI_ABSEN) && strtotime($jam) < strtotime(JAM_SELESAI_ABSEN)){
						if(strtotime($jam) > strtotime(JAM_TERLAMBAT_ABSEN)){
			        		$data['status'] = 'terlambat';
			        		
			        	}else{
			        		$data['status'] = 'hadir';
			        	}

			        	$data['waktu_masuk'] = $tanggal." ".$jam;
			        	$data['pegawai_id']	 = $pegawai['pegawai_id'];

			        	if($this->m_absensi->insert($data)){
			        		$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-check"></i> Absensi Masuk Berhasil Dilakukan','success'));

			        	}else{
			        		$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-times"></i> Absen Gagal Dimasukkan','danger'));
			        	}

			        }else{
			        	$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-warning"></i> Bukan Jam Absen','warning'));
			        }
			        
				}

			}else{
				$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-warning"></i> Bukan Masa Hari Kerja','warning'));
			}

		}else{
			$this->session->set_flashdata('alert_message',show_alert('<i class="fa fa-times"></i> RFID tidak ditemukan','danger'));
		}

		redirect('RFID');
	}

}