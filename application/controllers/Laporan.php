<?php 

class Laporan extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->model('keuangan/Coa_model','coa_model');
		$this->load->model('keuangan/Pemilik_model','m_pemilik');
		$this->load->model('asset/Penyusutan_model', 'm_penyusutan');
		$this->load->model('asset/Aset_model', 'm_aset');
		$this->load->model('penggajian/gaji_model', 'm_gaji');
		$this->load->model('laporan_model');
		$this->load->model('transaksi_model');
		$this->load->model('Akademik/Transaksi/model_transaksi_pendaftaran', 'm_pendaftaran');
		$this->load->model('Akademik/Transaksi/model_tagihan_lainnya', 'm_lainnya');
		$this->load->model('Akademik/Transaksi/model_transaksi_spp', 'm_spp');
		$this->load->model('Akademik/MasterData/Model_Daycare', 'm_daycare');
		$this->load->model('keuangan/model_tunai', 'm_tunai');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu', 'keuangan');
	}

	public function jurnal(){
		$this->m_penyusutan->set_penyusutan_to_jurnal();
		$req = [];

		if($this->input->get('start')){
			$req = [
				'start' => $this->input->get('start'),
				'end'	=> $this->input->get('end')
			];
		}

		// print_r($req); die;
		$data['jurnal']      = $this->laporan_model->get_jurnal($req);
		$this->template->load('layout/template','laporan/keuangan/jurnal', $data);
	}

	public function ayat_jurnal_penutup(){
		$req = [];
		$data = [];
		if($this->input->get('bulan')){
			$req = [
				'bulan' => $this->input->get('bulan'),
				'tahun'	=> $this->input->get('tahun')
			];

			$data['jurnal']      = $this->laporan_model->get_ajp($req);
		}
		
		$this->template->load('layout/template','laporan/keuangan/ayat_jurnal_penutup', $data);
	}

	public function buku_besar(){
		$req = [];

		if($this->input->get('bulan')){
			$req = [
				'bulan'  => $this->input->get('bulan'),
				'tahun'	 => $this->input->get('tahun'),
				'coa_id' => $this->input->get('coa_id')
			];
		}

		$data['akun']		 = $this->coa_model->get_data();
		$data['jurnal']      = $this->laporan_model->get_jurnal($req);
		$this->template->load('layout/template','laporan/keuangan/buku_besar', $data);
	}

	public function laba_rugi(){

		$data[] = [];

		if($this->input->get('bulan')){
			$req = [
				'MONTH(tanggal_transaksi)'  => $this->input->get('bulan'),
				'YEAR(tanggal_transaksi)'	=> $this->input->get('tahun')
			];
			// var_dump($req); die;

			$data['pendapatan'] = $this->db->select('*, SUM(total_bayar) AS total_pendapatan')
								   ->where('jenis', 'masuk')
								   ->where('level', '3')
								   ->where($req)
								   ->group_by('tipe')
								   ->get('transaksi')->result_array();
			
			$data['pengeluaran'] = $this->db->select('*, SUM(total_bayar) AS total_pengeluaran')
								   ->where('jenis', 'keluar')
								   ->where('level', '3')
								   ->where($req)
								   ->or_group_start()
								   ->where('jenis', 'keluar')
								   ->where('level', '3')
								   ->where('level_vendor', '3')
								   ->group_end()
								   ->group_by('tipe')
								   ->get('transaksi')->result_array();
									   
			$pendaftaran 		= $this->m_pendaftaran->getSemua();
			$totalPendaftaran 	= 0;
			$totalSeragam 		= 0;
			$totalDpp	 		= 0;
			$totalDsk	 		= 0;
			foreach($pendaftaran as $pend)
			{
				$totalPendaftaran 	+= $pend['biaya_pendaftaran'];
				$tipeDaftar			= 'Pendaftaran';
				$totalSeragam 		+= $pend['biaya_seragam'];
				$tipeSeragam		= 'Seragam';
				$totalDpp	 		+= $pend['biaya_dpp'];
				$tipeDpp			= 'DPP';
				$totalDsk	 		+= $pend['biaya_dsk'];
				$tipeDsk			= 'DSK';
			}
			$data['pendaftaran'] = ['totalPendaftaran' => $totalPendaftaran, 'tipe' => $tipeDaftar];
			$data['seragam'] 	 = ['totalSeragam' => $totalSeragam, 'tipe' => $tipeSeragam];
			$data['dpp'] 	 	 = ['totalDpp' => $totalDpp, 'tipe' => $tipeDpp];
			$data['dsk'] 	 	 = ['totalDsk' => $totalDsk, 'tipe' => $tipeDsk];
			$lainnya 		= $this->m_lainnya->getSemua();
			$totalTagihan 	= 0;
			foreach($lainnya as $uy)
			{
				if($uy['status_lainnya'] == "Lunas")
				{
					$totalTagihan += $uy['total_tagihan'];
				}
			}
			$data['lainnya'] = ['totalTagihan' => $totalTagihan, 'tipe' => 'Lainnya'];
			
			$daycare		= $this->m_daycare->getSemua();
			$tagihDaycare	= 0;
			foreach($daycare as $mant)
			{
				if($mant['status'] == "Sudah Bayar")
				{
					$tagihDaycare += $mant['total_tagihan'];
				}	
			}
			$data['daycare'] = ['tagihDaycare' => $tagihDaycare, 'tipe' => 'Daycare'];
			// var_dump($data['daycare']); die;

		}

		$this->template->load('layout/template','laporan/keuangan/laba_rugi', $data);
	}
    

    public function kartu_aktiva(){
    	$req = [];
    	$this->session->set_userdata('menu','aset');
    	if($this->input->get('id_aset')){
	  		$data['detail_aset']  = $this->m_aset->get_list_detail($this->input->get('id_aset'));
	  		$req['id_detail_aset'] = $this->input->get('id_detail_aset');
	  		$data['detail'] = $this->db->where('hru_aset_detail.id', $req['id_detail_aset'])
	  								 ->join('hru_aset', 'hru_aset.id = hru_aset_detail.aset_id')
	  								 ->get('hru_aset_detail')->row_array();

	  		$data['perbaikan'] = $this->db->where('tipe', 'perbaikan')
	  									  ->where('aset_detail_id', $req['id_detail_aset'])
	  									  ->join('transaksi', 'transaksi.id = transaksi_perawatan.transaksi_id')
	  									  ->join('hru_vendor', 'hru_vendor.id = transaksi.vendor_id')
	  									  ->get('transaksi_perawatan')->result_array();

	  		$data['pemeliharaan'] = $this->db->where('tipe', 'pemeliharaan')
	  									  ->where('aset_detail_id', $req['id_detail_aset'])
	  									  ->join('transaksi', 'transaksi.id = transaksi_perawatan.transaksi_id')
	  									  ->join('hru_vendor', 'hru_vendor.id = transaksi.vendor_id')
	  									  ->get('transaksi_perawatan')->result_array();

	  		$this->load->library('ciqrcode');
			$params['data']  = $data['detail']['kode_detail_aset'];
			$params['level'] = 'H';
			$params['size']  = 10;
			$params['savename'] = FCPATH.'assets/aset/detail_aset/'.$data['detail']['kode_detail_aset'].'_'.time().'.png';
			$this->ciqrcode->generate($params);

	  	}
	  	$data['penyusutan'] = $this->m_penyusutan->get_dataPenyusutan($req);
	  	$data['aset'] = $this->m_aset->get_data();
    	$this->template->load('layout/template','laporan/asset/kartu_aktiva', $data);
    }

    public function modal_pemilik(){
    	$data['pemilik'] = $this->m_pemilik->get_data();

    	if($this->input->get('pemilik_id')){
    		$pemilik_id = $this->input->get('pemilik_id');
    		$data['detail'] = $this->m_pemilik->get_detail('id', $pemilik_id)->row_array();
    		$data['list'] = $this->db->where('pemilik_id', $pemilik_id)
    								 ->group_start()
	    								 ->where('tipe', 'setoran')
	    								 ->or_where('tipe', 'penarikan')
    								 ->group_end()
    								 ->where('level', '3')
    								 ->order_by('tanggal_transaksi', 'ASC')
    								 ->get('transaksi')->result_array();
    	}
    	$this->template->load('layout/template','laporan/keuangan/modal_pemilik', $data);
    }

    public function perubahan_modal(){
    	$data['pemilik'] = $this->m_pemilik->get_data();

    	if($this->input->get('bulan')){
    		$bulan = $this->input->get('bulan');
    		$tahun = $this->input->get('tahun');

    		$time  = strtotime($tahun."-".$bulan."-01");
    		$final = date("Y-m-d", strtotime("+1 month", $time));
    		
    		$modal = $this->laporan_model->get_saldo($final, '24');

    		$prive = $this->db->where('level','3')
    						  ->where('tipe', 'penarikan')
    						  ->select('SUM(total_transaksi) AS total')
    						  ->where('MONTH(tanggal_transaksi)', $bulan)
    						  ->where('YEAR(tanggal_transaksi)', $tahun)
    				 		  ->get('transaksi')->row_array()['total'];

    		$laba_rugi = $this->db->select('(SUM(CASE WHEN jenis = "masuk" THEN total_transaksi END) - SUM(CASE WHEN jenis = "keluar" THEN total_transaksi END)) AS total')
    							  ->where('level', '3')
    							  ->where('MONTH(tanggal_transaksi)', $bulan)
    						  	  ->where('YEAR(tanggal_transaksi)', $tahun)
    							  ->get('transaksi')->row_array()['total'];
    		if($laba_rugi == ''){
    			$laba_rugi = 0;
    		}

    		$data['modal'] = $modal;
    		$data['prive'] = $prive;
    		$data['laba_rugi'] = $laba_rugi;
    	}
    	$this->template->load('layout/template','laporan/keuangan/perubahan_modal', $data);
    }

    public function aset($page){
    	$this->session->set_userdata('menu','aset');
    	if(in_array($page, ['perolehan', 'perbaikan', 'pemeliharaan'])){
    		$data = [];
    		if($this->input->get('bulan')){
    			$p = $this->input->get();
	    		if($page == 'perbaikan'){
					$find = [
						'tipe'		   => 'perbaikan',
						'level_vendor >=' => '3',
						'MONTH(tanggal_transaksi)' => $p['bulan'],
						'YEAR(tanggal_transaksi)'  => $p['tahun']
					];

				}else{
					$find = [
						'tipe'  => $page,
						'level' => '3',
						'MONTH(tanggal_transaksi)' => $p['bulan'],
						'YEAR(tanggal_transaksi)'  => $p['tahun']
					];
				}
				$data['list'] = $this->transaksi_model->get_detail($find, '', $page)
												      ->result_array();
			}

			$this->template->load('layout/template','laporan/asset/'.$page, $data);

    	}else{
    		show_404();
    	}
    }


    public function gaji(){
		$req = [];

		if($this->input->get('gaji_id')){
			$data['detail'] = $this->m_gaji->get_detail($this->input->get('gaji_id'))->row_array();
			$data['list'] = $this->m_gaji->get_list_detail($this->input->get('gaji_id'))->result_array();
		}

		$data['gaji'] = $this->db->order_by('tahun', 'DESC')
								 ->order_by('bulan', 'ASC')
								 ->get('fdl_gaji')->result_array();

		$this->session->set_userdata('menu', 'hr');
		$this->template->load('layout/template','laporan/penggajian/gaji', $data);
	}

	public function absensi(){
		$req = [];
		$data = [];

		if($this->input->get('bulan')){
			$pegawai = $this->db->get('fdl_pegawai')->result_array();

			foreach ($pegawai as $row){
				$this->db->select('SUM(CASE WHEN status = "hadir" THEN 1 ELSE 0 END) AS hadir,
					               SUM(CASE WHEN status = "terlambat" THEN 1 ELSE 0 END) AS terlambat,
								   SUM(CASE WHEN status = "cuti" THEN 1 ELSE 0 END) AS cuti,
								   SUM(CASE WHEN status = "dinas" THEN 1 ELSE 0 END) AS dinas,
								   SUM(CASE WHEN status = "izin" THEN 1 ELSE 0 END) AS izin,
								   SUM(CASE WHEN status = "sakit" THEN 1 ELSE 0 END) AS sakit
								   ')
						 ->where('pegawai_id', $row['id'])
						 ->where('MONTH(waktu_masuk)', $this->input->get('bulan'))
						 ->where('YEAR(waktu_masuk)', $this->input->get('tahun'));
						 
				$res = $this->db->get('fdl_absensi')->row_array();
				$absensi[] = [
					'pegawai' => $row,
					'absensi' => $res
				];
			}

			$data['absensi'] = $absensi;
			
		}

		$this->session->set_userdata('menu', 'hr');
		$this->template->load('layout/template','laporan/penggajian/absensi', $data);
	}

	public function kas_keluar(){
		$req = [];
		$data = [];

		if($this->input->get('bulan')){
			$req = [
				'MONTH(tanggal_transaksi)'  => $this->input->get('bulan'),
				'YEAR(tanggal_transaksi)'	=> $this->input->get('tahun'),
				'level'	=> '3',
				'is_created' => '1'
			];

			$this->db->where($req)
					 ->where('jenis', 'keluar');
			$data['list'] = $this->db->get('transaksi')->result_array();
		}

		$this->template->load('layout/template','laporan/keuangan/kas_keluar', $data);
	}

	public function laporanCash()
	{
		$data['pendaftaran'] = $this->m_pendaftaran->get_data();
		$data['spp']		 = $this->m_spp->get_data();
		$data['lainnya']	 = $this->m_lainnya->getDataCash();
		$data['daycare']	 = $this->m_daycare->get_data();

		$this->template->load('layout/template','laporan/keuangan/cash', $data);
	}
	
	public function laporanBank()
	{
		$data['pendaftaran'] = $this->m_pendaftaran->get_data_transfer();
		$data['spp'] 		 = $this->m_spp->get_data_transfer();
		$data['lainnya'] 	 = $this->m_lainnya->get_data_transfer();
		$data['daycare'] 	 = $this->m_daycare->get_data_transfer();

		$this->template->load('layout/template','laporan/keuangan/bank', $data);
	}

	public function saldoKas()
	{
		$data	= $this->m_tunai->getAll();
		
		$this->template->load('layout/template','laporan/keuangan/saldoKas', compact('data'));
	}
}