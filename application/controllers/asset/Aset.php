<?php 

class Aset extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->model('asset/aset_model', 'm_aset');
		$this->load->model('asset/Kategori_model', 'm_kategori');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','aset');
	}

	public function index(){
		$data['list']      = $this->m_aset->get_data();
		$data['kategori']  = $this->m_kategori->get_data();
		$data['last_code'] = $this->m_aset->generate_code();
		$this->template->load('layout/template','master_data/asset/aset/index', $data);
	}

	public function detail($aset_id){
		
		$cek = $this->m_aset->get_detail('id', $aset_id);

		if($cek->num_rows() > 0){
			$data['aset'] = $cek->row_array();
			$data['list'] = $this->m_aset->get_list_detail($aset_id);

			$this->template->load('layout/template','master_data/asset/aset/detail', $data);

		}else{
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fa fa-minus-circle"></i></b> ID Aset tidak diketahui','success'));
			redirect('aset/daftar');
		}
	}

	public function add(){
		$p = $this->input->post();
		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('nama_aset', 'Nama Aset', 'required');
		$this->form_validation->set_rules('kategori_id', 'Kategori', 'required');

		if($this->form_validation->run() == TRUE){

			if($this->m_aset->insert($p)){
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan','success'));
			}else{
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dimasukkan','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
		}

		redirect('aset/daftar');
	}

	public function update(){
		$p  = $this->input->post();
		$id = $p['id_aset'];

		unset($p['id_aset']);

		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('nama_aset', 'Nama Aset', 'required');
		$this->form_validation->set_rules('kategori_id', 'Kategori', 'required');

		if($this->form_validation->run() == TRUE){

			if($this->m_aset->update($p, $id)){
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diubah','success'));
			}else{
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diubah','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
		}

		redirect('aset/daftar');
	}

	public function delete($id){

		if($this->m_aset->delete($id)){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dihapus','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dihapus','danger'));
		}

		redirect('aset/daftar');
	}
    
}