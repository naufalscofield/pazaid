<?php 

class Perolehan extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->model('transaksi_model');
		$this->load->model('asset/Aset_model', 'm_aset');
		$this->load->model('asset/Vendor_model', 'm_vendor');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','aset');
	}

	public function index(){

		$req = [
			'is_created' => '1',
			'tipe'   	 => 'perolehan'
		];

		$data['list']      = $this->transaksi_model->get_detail($req, '', 'perolehan')->result_array();
		$this->template->load('layout/template','transaksi/asset/perolehan/index', $data);
	}

	public function detail($transaksi_id){
		$perolehan = $this->transaksi_model->get_detail('transaksi.id', $transaksi_id, 'perolehan');
		
		if($perolehan->num_rows() > 0){
			$data['transaksi']  = $perolehan->row_array();
			$data['item']       = $this->transaksi_model->get_list_item('perolehan', $data['transaksi']['id']);
			$data['pembayaran'] = $this->transaksi_model->get_list_pembayaran($data['transaksi']['id']);
			$this->template->load('layout/template','transaksi/asset/perolehan/detail', $data);
		
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> ID Perolehan tidak diketahui','danger'));
			redirect('transaksi/asset/perolehan');
		}
		
	}

	public function add(){
		$last = $this->transaksi_model->last_data('perolehan');
		if($last['is_created'] != '0'){
			$data = [
				'kode_transaksi' => $this->transaksi_model->generate_code('perolehan'),
				'tipe'			 => 'perolehan'
			];
			$this->transaksi_model->insert($data);

			$last = $this->transaksi_model->last_data('perolehan');
		}

		$data['vendor']    = $this->m_vendor->get_data();
		$data['aset']      = $this->m_aset->get_data();
		$data['perolehan'] = $last;
		$data['item']	   = $this->transaksi_model->get_list_item('perolehan',$last['id']);

		$this->template->load('layout/template','transaksi/asset/perolehan/add', $data);
	}

	public function insert($transaksi_id){
		$p = $this->input->post();
		$p['total_bayar'] = format_angka($p['total_bayar']);
		$p['jenis']		  = 'keluar';

		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('kode_transaksi', 'Kode Transaksai', 'required');
		$this->form_validation->set_rules('total_bayar', 'Total Dibayar', 'required|numeric|greater_than[0]');
		if(isset($p['pembayaran'])){
			$this->form_validation->set_rules('pembayaran', 'Tipe Pembayaran', 'required');
		}

		if($this->form_validation->run() == TRUE){

			$total_transaksi = $this->transaksi_model->get_total_price('perolehan',$transaksi_id);

			if($total_transaksi > 0){
				$waktu = date('Y-m-d H:i:s');
				$p['total_transaksi'] 	= $total_transaksi;
				$p['sisa_bayar'] 		= $p['total_transaksi'] - $p['total_bayar'];
				$p['tanggal_transaksi'] = $waktu;
				$p['is_created'] = '1';

				if($p['pembayaran'] == 'Kredit'){
					$p['status'] = 'Belum Lunas';
				}else{
					$p['status'] = 'Lunas';
				}

				$this->db->trans_begin();
				$check = $this->db->where('umur >', 0)
								  ->where('transaksi_id', $transaksi_id)
								  ->get('transaksi_aset');
				
				if($check->num_rows() > 0){
					$p['pembayaran'] = 'Hibah';
					$p['level']      = '3';

					$pby = [
						'transaksi_id'  => $transaksi_id,
						'jumlah_bayar'  => $p['total_transaksi'],
						'tanggal_bayar' => $waktu
					];
					$this->transaksi_model->insert_pembayaran($pby);
					$this->generateAsset($transaksi_id);

				}

				$this->transaksi_model->update($p, $transaksi_id);

				if($this->db->trans_status()){
					$this->db->trans_commit();
					$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan','success'));
					redirect('aset/perolehan');

				}else{
					$this->db->trans_rollback();
					$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fa fa-minus-circle"></i></b> Data gagal dimasukkan','danger'));
					redirect('aset/perolehan/tambah');
				}

			}else{
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-warning"><i class="fa fa-ban"></i></b> Tidak ada Produk yang dipilih','warning'));
				redirect('aset/perolehan/tambah');
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-warning"><i class="fa fa-ban"></i> Form Tidak Valid</b><br>'.validation_errors(),'warning'));

			redirect('aset/perolehan/tambah');
		}

	}

	public function insert_produk(){
		if($this->input->is_ajax_request()){
			$s = false;
			$p = $this->input->post();
			$p['harga'] = format_angka($p['harga']);
			$p['nilai_residu'] = format_angka($p['nilai_residu']);

			$this->form_validation->set_data($p);
			$this->form_validation->set_rules('aset_id', 'Aset', 'required');
			$this->form_validation->set_rules('harga', 'Harga', 'required|numeric|greater_than[0]');
			$this->form_validation->set_rules('jumlah', 'Jumlah', 'required|numeric|greater_than[0]');
			$this->form_validation->set_rules('nilai_residu', 'Nilai Residu', 'required|numeric|greater_than[0]');

			if($this->form_validation->run() == TRUE){

				$p['subtotal'] = $p['jumlah'] * $p['harga'];

				if($this->transaksi_model->insert_item('perolehan',$p)){
					$response['message'] = 'Data berhasil ditambahkan';
					$response['data']    = $this->transaksi_model->get_list_item('perolehan',$p['transaksi_id']);
					$response['status']  = true;
				
				}else{
					$response['message'] = 'Data gagal ditambahkan';
					$response['status']  = false;
				}

			}else{
				$response['message'] = validation_errors();
				$response['status']  = false;
			}

			echo json_encode($response);

		}else{
			show_404();
		}
	}

	public function update_produk($perolehan_id){
		if($this->input->is_ajax_request()){
			$p = $this->input->post();

			$this->form_validation->set_data($p);
			$this->form_validation->set_rules('jumlah', 'Jumlah', 'required|numeric|greater_than[0]');

			if($this->form_validation->run() == TRUE){

					if($this->transaksi_model->update_item(['qty' => $p['qty']], $p['perolehan_detail_id'])){
						$response['message'] = 'Data berhasil diubah';
						$response['data']    = $this->transaksi_model->get_list_item($perolehan_id);
						$response['status']  = true;
					
					}else{
						$response['message'] = 'Data gagal diubah';
						$response['status']  = false;
					}

			}else{
				$response['message'] = validation_errors();
				$response['status']  = false;
			}

			echo json_encode($response);

		}else{
			show_404();
		}
	}

	public function delete_produk($transaksi_id){

		if($this->input->is_ajax_request()){
			if($this->transaksi_model->delete_item('perolehan', $this->input->post('transaksi_aset_id'))){
				$response['message'] = 'Data berhasil diubah';
				$response['data']    = $this->transaksi_model->get_list_item('perolehan', $transaksi_id);
				$response['status']  = true;

			}else{
				$response['message'] = 'Data gagal dihapus';
				$response['status']  = false;
			}

			echo json_encode($response);

		}else{
			show_404();
		}
	}
    

    public function pelunasan(){

    	$find = [
    		'is_created' => '1',
    		'status'     => 'Belum Lunas'
    	];
    	$data['list'] = $this->transaksi_model->get_detail($find)->result_array();

    	if(!$this->input->get('id')){
    		$this->template->load('layout/template','pesanan/perolehan/pelunasan', $data);
    	
    	}else{
    		$id = $this->input->get('id');
    		$find = [
	    		'is_created'   => '1',
	    		'status'       => 'Belum Lunas',
	    		'perolehan.id' => $id
	    	];

    		$perolehan = $this->transaksi_model->get_detail($find);
    		if($perolehan->num_rows() > 0){
    			$data['perolehan'] = $perolehan->row_array();
    			$data['item']      = $this->transaksi_model->get_list_item($data['perolehan']['perolehan_id']);
    			$data['pembayaran'] = $this->transaksi_model->get_list_pembayaran($data['perolehan']['perolehan_id']);
    			$this->template->load('layout/template','pesanan/perolehan/pelunasan', $data);

    		}else{
    			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> ID Perolehan tidak diketahui','danger'));
				redirect('perolehan/pesanan/pelunasan');
    		}
    	}
    }

    public function insert_pembayaran($perolehan_id){
    	$status = false;
    	$perolehan = $this->transaksi_model->get_detail('perolehan.id', $perolehan_id)->row_array();
    	$p = $this->input->post();

    	$p['perolehan_id'] = $perolehan_id;
    	$p['jumlah_bayar'] = format_angka($p['jumlah_bayar']);

    	$this->form_validation->set_data($p);
		$this->form_validation->set_rules('jumlah_bayar', 'Jumlah Bayar', 'required|numeric|greater_than[0]');

		if($this->form_validation->run() == TRUE){
			if($p['jumlah_bayar'] <= $perolehan['sisa_bayar']){

	    		$sisa_bayar  = $perolehan['sisa_bayar'] - $p['jumlah_bayar'];
	    		$total_bayar = $perolehan['total_bayar'] + $p['jumlah_bayar'];

	    		$this->transaksi_model->update([
	    								'total_bayar' => $total_bayar,
	    								'sisa_bayar'  => $sisa_bayar
	    							], $perolehan_id);

	    		$this->transaksi_model->insert_pembayaran([
	    									'perolehan_id'  => $perolehan_id,
	    									'jumlah_bayar'  => $p['jumlah_bayar'],
	    									'tanggal_bayar' => date('Y-m-d H:i:s')
	    							]);	

	    		if($sisa_bayar == 0){
	    			$status = true;
	    			$this->transaksi_model->update(['status' => 'Lunas'], $perolehan_id);
	    			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-check"></i> Pelunasan pembayaran berhasil dilakukan','success'));
	    		}else{
	    			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-check"></i> Data berhasil dimasukkan','success'));
	    		}

	    	}else{
	    		$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-warning"></i> Jumlah bayar tidak boleh melebihi sisa pembayaran','warning'));
	    	}

		}else{
			$this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
		}

		if($status){
			redirect('perolehan/pelunasan');
		}else{
			redirect('perolehan/pelunasan?id='.$perolehan_id);
		}
    	
    }


    	private function generateAsset($transaksi_id){
			$this->db->select('*, transaksi_aset.id AS transaksi_aset_id')
					 ->where('transaksi_id', $transaksi_id)
					 ->join('hru_aset', 'hru_aset.id = transaksi_aset.aset_id');

			$trans = $this->db->get('transaksi_aset')->result_array();
			$jurnal = [];
			foreach ($trans as $row) {
				$this->db->select('COUNT(id) AS total')
						 ->where('aset_id', $row['aset_id'])
						 ->group_by('aset_id');

				$start = $this->db->get('hru_aset_detail')->result_array();

				$n = 0;
				foreach ($start as $row2){ $n++; $num = $row2['total'] + 1;
					$kode = $num;
					for ($i = $num; $i < ($num + $row['jumlah']); $i++){ 
						$aset[$i]['transaksi_aset_id'] = $row['transaksi_aset_id'];
						$aset[$i]['aset_id']		   = $row['aset_id'];
						$aset[$i]['kode_detail_aset']  = $row['kode_aset']."-".$kode;
						$kode++;
					}
				}

				if($n == 0){
					$k = 0;
					for ($i=0; $i < $row['jumlah']; $i++) { $k++;
						$aset[$i]['transaksi_aset_id'] = $row['transaksi_aset_id'];
						$aset[$i]['aset_id']		   = $row['aset_id'];
						$aset[$i]['kode_detail_aset']  = $row['kode_aset']."-".$k;
					}
				}
			}

			$this->db->insert_batch('hru_aset_detail', $aset);
		}
}