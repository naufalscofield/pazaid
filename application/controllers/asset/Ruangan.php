<?php 

class Ruangan extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->model('asset/Ruangan_model', 'm_ruangan');
		$this->load->model('asset/Aset_model', 'm_aset');
		$this->load->model('transaksi_model');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','aset');
	}

	public function index(){
		$data['list']      = $this->m_ruangan->get_data();
		$data['last_code'] = $this->m_ruangan->generate_code();
		$this->template->load('layout/template','master_data/asset/ruangan', $data);
	}

	public function add(){
		$p = $this->input->post();
		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('nama_ruangan', 'Nama ruangan', 'required');

		if($this->form_validation->run() == TRUE){

			if($this->m_ruangan->insert($p)){
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan','success'));
			}else{
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dimasukkan','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
		}

		redirect('aset/ruangan');
	}

	public function update(){
		$p  = $this->input->post();
		$id = $p['id_ruangan'];

		unset($p['id_ruangan']);

		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('nama_ruangan', 'Nama ruangan', 'required');

		if($this->form_validation->run() == TRUE){

			if($this->m_ruangan->update($p, $id)){
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil diubah','success'));
			}else{
				$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal diubah','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert(validation_errors(),'warning'));
		}

		redirect('aset/ruangan');
	}

	public function delete($id){

		if($this->m_ruangan->delete($id)){
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dihapus','success'));
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> Data gagal dihapus','danger'));
		}

		redirect('aset/ruangan');
	}

	public function penempatan(){
		$data['aset'] = $this->m_aset->get_data();
		$data['ruangan'] = $this->m_ruangan->get_data();

		$aset_id = $this->input->get('aset_id');

		if($aset_id){
			$data['list'] = $this->m_aset->get_unlocated($aset_id);
		}

		$this->template->load('layout/template','transaksi/asset/pengalokasian/penempatan', $data);
	}

	public function penerimaan(){
		$trans_id = $this->input->get('transaksi_id');
		$data = [];
		$s = true;

		$find = [
			'is_received' => '0',
			'level' 	  => '3',
			'tipe'		  => 'perolehan'
		];	
		$cek = $this->transaksi_model->get_detail($find);
		$data['transaksi'] = $cek->result_array();

		if($trans_id){
			$find['id'] = $trans_id;
			$cek = $this->transaksi_model->get_detail($find);

			if($cek->num_rows() > 0){
				$data['list'] = $this->m_aset->get_received($trans_id);
			}else{
				$s = false;
			}
		}

		if($s){
			$this->template->load('layout/template','transaksi/asset/pengalokasian/penerimaan', $data);
		}else{
			show_404();
		}
	}

	public function insert_asset_location($aset_id){
		$this->form_validation->set_rules('ruangan_id', 'ruangan', 'required');

		if($this->form_validation->run() == TRUE){
			
			foreach ($this->input->post('detail_aset') as $row) {
				$data[] = $row;
			}

			if($this->m_aset->insert_location($data, $this->input->post('ruangan_id'))){
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> aset berhasil ditempatkan','success'));
			}else{
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fas fa-info-circle"></i></b> Harap Pilih aset','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fa fa-ban"></i>Form Tidak Valid</b><br>'.validation_errors(),'warning'));
		}

		redirect('aset/penempatan?aset_id='.$aset_id);
	}
    
    public function insert_asset_received($transaksi_id){
    	$p     = $this->input->post();
    	$count = 0;

    	$url = 'aset/penerimaan?transaksi_id='.$transaksi_id;
		if(!empty($p)){
			$terima = date('Y-m-d');
			$this->db->trans_begin();
			if(isset($p['is_terima'])){
				foreach ($p['is_terima'] as $key => $value) {
					$this->db->where('id', $key)
							 ->update('hru_aset_detail', ['is_terima' => '1', 'tanggal_terima' => $terima]);
				}
			}

			if(isset($p['is_retur'])){
				foreach ($p['is_retur'] as $key => $value) {
					$this->db->where('id', $key)
							 ->update('hru_aset_detail', ['is_retur' => '1', 'tanggal_terima' => $terima]);
				}
			}

			$receive   = $this->m_aset->get_received($transaksi_id);
			$num = count($receive); $i = 0;
			foreach ($receive as $row) {
				if($row['is_terima'] == '0' && $row['is_retur'] == '0'){
					$i++;
				}
			}

			if($i == 0){
				$this->transaksi_model->update(['is_received' => '1'], $transaksi_id);
			}

			if($this->db->trans_status()){
				$this->db->trans_commit();

				if($i == 0){
					$url = 'aset/penerimaan';
					$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Semua Aset berhasil diterima','success'));
				}else{
					$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Aset berhasil diterima','success'));
				}
				

			}else{
				$this->db->trans_rollback();
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fas fa-info-circle"></i></b> Harap Pilih aset','danger'));
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fa fa-ban"></i>Harap pilih aset yang diterima</b><br>','warning'));
		}

		redirect($url);
	}
}