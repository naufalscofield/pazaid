<?php class perbaikan extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->model('asset/Kategori_model', 'm_kategori');
		$this->load->model('asset/Aset_model', 'm_aset');
		$this->load->model('asset/Vendor_model', 'm_vendor');
		$this->load->model('transaksi_model');
	}

	public function index(){
		$find = [
			'is_created' => '1',
			'tipe'		 => 'perbaikan'
		];
		$data['list']      = $this->transaksi_model->get_detail($find)->result_array();
		$this->template->load('layout/template','transaksi/asset/perawatan/perbaikan/index', $data);
	}

	public function add(){
		$last = $this->transaksi_model->last_data('perbaikan');
		if($last['is_created'] != '0'){
			$data = [
				'kode_transaksi' => $this->transaksi_model->generate_code('perbaikan'),
				'tipe'			 => 'perbaikan'
			];
			$this->transaksi_model->insert($data);

			$last = $this->transaksi_model->last_data('perbaikan');
		}

		$data['vendor'] 	  = $this->m_vendor->get_data();
		$data['aset']      	  = $this->m_aset->get_data();
		$data['perbaikan'] = $last;
		$data['item']	  	  = $this->transaksi_model->get_list_item('perbaikan',$last['id']);

		$this->template->load('layout/template','transaksi/asset/perawatan/perbaikan/add', $data);
	}

	public function insert_produk(){
		if($this->input->is_ajax_request()){
			$s = false;
			$p = $this->input->post();
			unset($p['aset_id']);

			$this->form_validation->set_data($p);
			$this->form_validation->set_rules('aset_detail_id', 'Detail Aset', 'required|numeric|greater_than[0]');

			if($this->form_validation->run() == TRUE){

				$config['upload_path']          = './assets/aset/perbaikan/';
	            $config['allowed_types']        = 'jpg|png|jpeg';
	            $config['file_name']			= $p['aset_detail_id']."_".time();
	            $config['max_size']             = 10000;
	            $config['max_width']            = 3000;
	            $config['max_height']           = 3000;

	            $this->load->library('upload', $config);

	            if($this->upload->do_upload('gambar')){
                    $upl = $this->upload->data();
                    $p['gambar'] = $upl['file_name'];

                   	$cek = $this->db->where('aset_detail_id', $p['aset_detail_id'])
								->where('transaksi_id', $p['transaksi_id'])
								->get('transaksi_perawatan');

					if($cek->num_rows() == 0){
						if($this->transaksi_model->insert_item('transaksi_perawatan', $p)){
							$response['message'] = 'Data berhasil ditambahkan';
							$response['data']    = $this->transaksi_model->get_list_item('perbaikan', $p['transaksi_id']);
							$response['status']  = true;
						
						}else{
							$response['message'] = 'Data gagal ditambahkan';
							$response['status']  = false;
						}

					}else{
						$response['message'] = 'Aset sudah masuk daftar perbaikan';
						$response['status']  = false;
					}
	            
	            }else{
	            	$response['message'] = $this->upload->display_errors();
					$response['status']  = false;
	            }

			}else{
				$response['message'] = validation_errors();
				$response['status']  = false;
			}

			echo json_encode($response);

		}else{
			show_404();
		}
	}
    
    public function delete_produk($transaksi_id){

		if($this->input->is_ajax_request()){
			$this->db->where('id', $this->input->post('perbaikan_id'));
			$img  = $this->db->get('transaksi_perawatan')->row_array()['gambar'];
			$link = './assets/aset/perbaikan/'.$img;
			if(file_exists($link)){
				unlink($link);
			}
			
			if($this->transaksi_model->delete_item('perbaikan', $this->input->post('perbaikan_id'))){
				$response['message'] = 'Data berhasil dihapus';
				$response['data']    = $this->transaksi_model->get_list_item('perbaikan', $transaksi_id);
				$response['status']  = true;

			}else{
				$response['message'] = 'Data gagal dihapus';
				$response['status']  = false;
			}

			echo json_encode($response);

		}else{
			show_404();
		}
	}

	public function insert($transaksi_id){
		$p = $this->input->post();
		$p['jenis']		  = 'keluar';

		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('kode_transaksi', 'Kode Transaksai', 'required');
		if($this->form_validation->run() == TRUE){

			$cek = $this->db->where('transaksi_id', $transaksi_id)
							->get('transaksi_perawatan')->num_rows();

			if($cek > 0){

				$p['tanggal_transaksi'] = date('Y-m-d H:i:s');
				$p['is_created'] = '1';

				if($this->transaksi_model->update($p, $transaksi_id)){
					$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan','success'));
					redirect('aset/transaksi/perbaikan');

				}else{
					$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fa fa-minus-circle"></i></b> Data gagal dimasukkan','danger'));
					redirect('aset/transaksai/perbaikan/tambah');
				}

			}else{
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-warning"><i class="fa fa-ban"></i></b> Tidak ada Produk yang dipilih','warning'));
				redirect('aset/transaksi/perbaikan/tambah');
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-warning"><i class="fa fa-ban"></i> Form Tidak Valid</b><br>'.validation_errors(),'warning'));

			redirect('aset/transaksi/perbaikan');
		}
	}

	public function detail($transaksi_id){
		$perolehan = $this->transaksi_model->get_detail('id', $transaksi_id);
		
		if($perolehan->num_rows() > 0){
			$data['transaksi']  = $perolehan->row_array();
			$data['item']       = $this->transaksi_model->get_list_item('perbaikan', $data['transaksi']['id']);
			$data['pembayaran'] = $this->transaksi_model->get_list_pembayaran($data['transaksi']['id']);
			$this->template->load('layout/template','transaksi/asset/perawatan/perbaikan/detail', $data);
		
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> ID Perolehan tidak diketahui','danger'));
			redirect('transaksi/asset/perbaikan');
		}
	}
}

