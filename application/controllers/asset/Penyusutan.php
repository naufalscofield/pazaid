<?php 

class Penyusutan extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->model('transaksi_model');
		$this->load->model('asset/penyusutan_model', 'm_penyusutan');
		$this->load->model('asset/aset_model', 'm_aset');

		if(!$this->session->userdata('login')){
			redirect('');
		}

		$this->session->set_userdata('menu','aset');
	}

	public function index(){
		$this->m_penyusutan->set_penyusutan_to_jurnal();
		$req = $res = [];

	  	if($this->input->get('id_aset')){
	  		$data['detail_aset']  = $this->m_aset->get_list_detail($this->input->get('id_aset'));
	  		$req['id_detail_aset'] = $this->input->get('id_detail_aset');
	  	}

	  	$data['penyusutan'] = $this->m_penyusutan->get_dataPenyusutan($req);
		
	  	/**$log = $this->db->where('tahun', date('Y'))
	  					->order_by('bulan','ASC')
	  					->get('hru_penyusutan_log')
	  					->result_array();

	  	foreach ($log as $val) {
	  		$res[] = $val['bulan'];
	  	}**/

	  	$data['aset'] = $this->m_aset->get_data();
	  	$data['log']  = $res;

		$this->template->load('layout/template','transaksi/asset/penyusutan/index', $data);
	}

	
	public function call_detail_aset(){
	    
	    $data = $this->db->where([
	    				'aset_id' => $this->input->post('id_aset'),
	    				'ruangan_id !=' => null
	    			])->get('hru_aset_detail');

	    if($data->num_rows() > 0){
	      $response['status'] = 'success';
	      $response['data']   = $data->result_array();
	    
	    }else{
	      $response['status']  = 'error';
	      $response['message'] = 'Aset Kosong';
	    }

	    echo json_encode($response);
  	}

  	public function call_aset_fix(){
	    
	    $data = $this->db->where([
	    				'aset_id' => $this->input->post('id_aset'),
	    				'is_fix'  => '0'
	    			])->get('hru_aset_detail');

	    if($data->num_rows() > 0){
	      $response['status'] = 'success';
	      $response['data']   = $data->result_array();
	    
	    }else{
	      $response['status']  = 'error';
	      $response['message'] = 'Aset Kosong';
	    }

	    echo json_encode($response);
  	}

}