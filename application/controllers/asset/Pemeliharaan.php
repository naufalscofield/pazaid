<?php class Pemeliharaan extends CI_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->model('asset/Kategori_model', 'm_kategori');
		$this->load->model('asset/Aset_model', 'm_aset');
		$this->load->model('asset/Vendor_model', 'm_vendor');
		$this->load->model('transaksi_model');
	}

	public function index(){
		$find = [
			'is_created' => '1',
			'tipe'		 => 'pemeliharaan'
		];
		$data['list']      = $this->transaksi_model->get_detail($find, '', 'pemeliharaan')->result_array();
		$this->template->load('layout/template','transaksi/asset/perawatan/pemeliharaan/index', $data);
	}

	public function add(){
		$last = $this->transaksi_model->last_data('pemeliharaan');
		if($last['is_created'] != '0'){
			$data = [
				'kode_transaksi' => $this->transaksi_model->generate_code('pemeliharaan'),
				'tipe'			 => 'pemeliharaan'
			];
			$this->transaksi_model->insert($data);

			$last = $this->transaksi_model->last_data('pemeliharaan');
		}

		$data['vendor'] 	  = $this->m_vendor->get_data();
		$data['aset']      	  = $this->m_aset->get_data();
		$data['pemeliharaan'] = $last;
		$data['item']	  	  = $this->transaksi_model->get_list_item('pemeliharaan',$last['id']);

		$this->template->load('layout/template','transaksi/asset/perawatan/pemeliharaan/add', $data);
	}

	public function insert_produk(){
		if($this->input->is_ajax_request()){
			$s = false;
			$p = $this->input->post();
			$p['harga_perawatan'] = format_angka($p['harga_perawatan']);
			unset($p['aset_id']);

			$this->form_validation->set_data($p);
			$this->form_validation->set_rules('harga_perawatan', 'Harga Pemeliharaan', 'required|numeric|greater_than[0]');
			$this->form_validation->set_rules('aset_detail_id', 'Detail Aset', 'required|numeric|greater_than[0]');

			if($this->form_validation->run() == TRUE){

				$cek = $this->db->where('aset_detail_id', $p['aset_detail_id'])
								->where('transaksi_id', $p['transaksi_id'])
								->get('transaksi_perawatan');

				if($cek->num_rows() == 0){
					if($this->transaksi_model->insert_item('transaksi_perawatan', $p)){
						$response['message'] = 'Data berhasil ditambahkan';
						$response['data']    = $this->transaksi_model->get_list_item('pemeliharaan', $p['transaksi_id']);
						$response['status']  = true;
					
					}else{
						$response['message'] = 'Data gagal ditambahkan';
						$response['status']  = false;
					}

				}else{
					$response['message'] = 'Aset sudah masuk daftar pemeliharaan';
					$response['status']  = false;
				}

			}else{
				$response['message'] = validation_errors();
				$response['status']  = false;
			}

			echo json_encode($response);

		}else{
			show_404();
		}
	}
    
    public function delete_produk($transaksi_id){

		if($this->input->is_ajax_request()){
			if($this->transaksi_model->delete_item('pemeliharaan', $this->input->post('pemeliharaan_id'))){
				$response['message'] = 'Data berhasil dihapus';
				$response['data']    = $this->transaksi_model->get_list_item('pemeliharaan', $transaksi_id);
				$response['status']  = true;

			}else{
				$response['message'] = 'Data gagal dihapus';
				$response['status']  = false;
			}

			echo json_encode($response);

		}else{
			show_404();
		}
	}

	public function insert($transaksi_id){
		$p = $this->input->post();
		$p['total_bayar'] = format_angka($p['total_bayar']);
		$p['jenis']		  = 'keluar';

		$this->form_validation->set_data($p);
		$this->form_validation->set_rules('kode_transaksi', 'Kode Transaksai', 'required');
		$this->form_validation->set_rules('pembayaran', 'Tipe Pembayaran', 'required');
		$this->form_validation->set_rules('total_bayar', 'Total Dibayar', 'required|numeric|greater_than[0]');

		if($this->form_validation->run() == TRUE){

			$total_transaksi = $this->transaksi_model->get_total_price('pemeliharaan',$transaksi_id);

			if($total_transaksi > 0){

				$p['total_transaksi'] 	= $total_transaksi;
				$p['sisa_bayar'] 		= $p['total_transaksi'] - $p['total_bayar'];
				$p['tanggal_transaksi'] = date('Y-m-d H:i:s');
				$p['is_created'] = '1';

				if($p['pembayaran'] == 'Kredit'){
					$p['status'] = 'Belum Lunas';
				}else{
					$p['status'] = 'Lunas';
				}

				if($this->transaksi_model->update($p, $transaksi_id)){
					$this->session->set_flashdata('alert_message', show_alert('<b class="text-success"><i class="fa fa-check-circle"></i></b> Data berhasil dimasukkan','success'));
					redirect('aset/transaksi/pemeliharaan');

				}else{
					$this->session->set_flashdata('alert_message', show_alert('<b class="text-danger"><i class="fa fa-minus-circle"></i></b> Data gagal dimasukkan','danger'));
					redirect('aset/transaksai/pemeliharaan/tambah');
				}

			}else{
				$this->session->set_flashdata('alert_message', show_alert('<b class="text-warning"><i class="fa fa-ban"></i></b> Tidak ada Produk yang dipilih','warning'));
				redirect('aset/transaksi/pemeliharaan/tambah');
			}

		}else{
			$this->session->set_flashdata('alert_message', show_alert('<b class="text-warning"><i class="fa fa-ban"></i> Form Tidak Valid</b><br>'.validation_errors(),'warning'));
			
			//echo "<pre>".print_r($p,true)."</pre>";

			redirect('aset/transaksi/pemeliharaan');
		}
	}

	public function detail($transaksi_id){
		$perolehan = $this->transaksi_model->get_detail('transaksi.id', $transaksi_id, 'pemeliharaan');
		
		if($perolehan->num_rows() > 0){
			$data['transaksi']  = $perolehan->row_array();
			$data['item']       = $this->transaksi_model->get_list_item('pemeliharaan', $data['transaksi']['id']);
			$data['pembayaran'] = $this->transaksi_model->get_list_pembayaran($data['transaksi']['id']);
			$this->template->load('layout/template','transaksi/asset/perawatan/pemeliharaan/detail', $data);
		
		}else{
			$this->session->set_flashdata('alert_message', show_alert('<i class="fa fa-close"></i> ID Perolehan tidak diketahui','danger'));
			redirect('transaksi/asset/pemeliharaan');
		}
	}
}

