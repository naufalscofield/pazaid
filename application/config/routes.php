<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'Auth/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//----------------------------------------------------//
//                       ASSET                        //
//----------------------------------------------------//

	######################
	//	MASTER DATA 	//
	######################
	
		## Kategori
		$route['aset/kategori']   			   = 'asset/Kategori';
		$route['insert_kategori']			   = 'asset/Kategori/add';
		$route['update_kategori']			   = 'asset/Kategori/update';
		$route['delete_kategori/(:any)'] 	   = 'asset/Kategori/delete/$1';

		## aset
		$route['aset/daftar']                = 'asset/Aset';
		$route['aset/daftar/detail/(:any)']  = 'asset/Aset/detail/$1';
		$route['insert_aset']			     = 'asset/Aset/add';
		$route['update_aset']			     = 'asset/Aset/update';
		$route['delete_aset/(:any)'] 	     = 'asset/Aset/delete/$1';

		## Lokasi
		$route['aset/ruangan']   			 = 'asset/Ruangan';
		$route['insert_ruangan']			 = 'asset/Ruangan/add';
		$route['update_ruangan']			 = 'asset/Ruangan/update';
		$route['delete_ruangan/(:any)'] 	 = 'asset/Ruangan/delete/$1';

		## Vendor
		$route['aset/vendor']   			 = 'asset/Vendor';
		$route['insert_vendor']			     = 'asset/Vendor/add';
		$route['update_vendor']			     = 'asset/Vendor/update';
		$route['delete_vendor/(:any)'] 	     = 'asset/Vendor/delete/$1';

	######################
	//	TRANSAKSI 	    //
	######################

		## Perolehan
		$route['aset/perolehan']               = 'asset/Perolehan';
		$route['aset/perolehan/tambah']		   = 'asset/Perolehan/add';
		$route['aset/perolehan/detail/(:any)'] = 'asset/Perolehan/detail/$1';

		$route['insert_transaksi/(:any)']  		  = 'asset/Perolehan/insert/$1';
		$route['insertProdukPenjualan']    		  = 'asset/Perolehan/insert_produk';
		$route['updateProdukPenjualan/(:any)']    = 'asset/Perolehan/update_produk/$1';
		$route['deleteProdukPenjualan/(:any)']    = 'asset/Perolehan/delete_produk/$1';

		$route['insertPembayaran/(:any)'] = 'Perolehan/insert_pembayaran/$1';

		## Penempatan aset
		$route['aset/penempatan']   = 'asset/Ruangan/penempatan';
		$route['insert_asset_location/(:any)'] = 'asset/Ruangan/insert_asset_location/$1';

		## Penerimaan
		$route['aset/penerimaan']   = 'asset/Ruangan/penerimaan';
		$route['insert_asset_received/(:any)'] = 'asset/Ruangan/insert_asset_received/$1';

		## Penyusutan
		$route['aset/penyusutan']    = 'asset/Penyusutan';
		$route['get_detail_aset/(:any)/(:any)'] = 'asset/Penyusutan/call_detail_aset/$1/$2';
		$route['get_aset_fix'] = 'asset/Penyusutan/call_aset_fix';

		## Pemeliharaan
		$route['aset/transaksi/pemeliharaan']  				= 'asset/Pemeliharaan';
		$route['aset/transaksi/pemeliharaan/tambah']  		= 'asset/Pemeliharaan/add';
		$route['insertProdukPemeliharaan']					= 'asset/Pemeliharaan/insert_produk';
		$route['deleteProdukPemeliharaan/(:any)']			= 'asset/Pemeliharaan/delete_produk/$1';
		$route['insert_pemeliharaan/(:any)']				= 'asset/Pemeliharaan/insert/$1';
		$route['aset/transaksi/pemeliharaan/detail/(:any)'] = 'asset/Pemeliharaan/detail/$1';

		## Perbaikan
		$route['aset/transaksi/perbaikan']  			 = 'asset/Perbaikan';
		$route['aset/transaksi/perbaikan/tambah']  		 = 'asset/Perbaikan/add';
		$route['insertProdukPerbaikan']					 = 'asset/Perbaikan/insert_produk';
		$route['deleteProdukPerbaikan/(:any)']			 = 'asset/Perbaikan/delete_produk/$1';
		$route['insert_perbaikan/(:any)']				 = 'asset/Perbaikan/insert/$1';
		$route['aset/transaksi/perbaikan/detail/(:any)'] = 'asset/Perbaikan/detail/$1';


//----------------------------------------------------//
//                     KEUANGAN                       //
//----------------------------------------------------//

	######################
	//	MASTER DATA 	//
	######################

		## Beban
		$route['keuangan/beban'] 			= 'keuangan/Beban';
		$route['insert_beban']			   	= 'keuangan/Beban/add';
		$route['update_beban']			   	= 'keuangan/Beban/update';
		$route['delete_beban/(:any)'] 	   	= 'keuangan/Beban/delete/$1';

		## COA
		$route['keuangan/coa'] 				= 'keuangan/Coa';
		$route['insert_coa']			   	= 'keuangan/Coa/add';
		$route['update_coa']			   	= 'keuangan/Coa/update';
		$route['delete_coa/(:any)'] 	   	= 'keuangan/Coa/delete/$1';

		## Beban
		$route['keuangan/rekening'] 			= 'keuangan/Rekening';
		$route['insert_rekening']			   	= 'keuangan/Rekening/add';
		$route['update_rekening']			   	= 'keuangan/Rekening/update';
		$route['delete_rekening/(:any)'] 	   	= 'keuangan/Rekening/delete/$1';

		## Beban
		$route['keuangan/pemilik'] 			= 'keuangan/Pemilik';
		$route['insert_pemilik']			= 'keuangan/Pemilik/add';
		$route['update_pemilik']			= 'keuangan/Pemilik/update';
		$route['delete_pemilik/(:any)'] 	= 'keuangan/Pemilik/delete/$1';

		## Beban
		$route['keuangan/bop/(:any)'] 		= 'keuangan/Bop/get_page/$1';
		$route['keuangan/bop/keluar/detail/(:any)'] = 'Keuangan/Bop/detail/$1';
		$route['insert_bop']			   	= 'keuangan/Bop/add';
		$route['insert_pengeluaran_bop/(:any)']		= 'Keuangan/Bop/add_bop_keluar/$1';

		## REKENING
		$route['keuangan/rekening/ajaxRekening']			= 'keuangan/Rekening/ajaxRekening';
		$route['keuangan/rekening/ajaxNoRekening/(:any)']	= 'keuangan/Rekening/ajaxNoRekening/$1';

		## Komponen Biaya
		$route['keuangan/komponenbiaya']				= 'keuangan/Komponen_biaya';
		$route['keuangan/komponenbiaya/add']			= 'keuangan/Komponen_biaya/add';
		$route['keuangan/komponenbiaya/update']			= 'keuangan/Komponen_biaya/update';
		$route['keuangan/komponenbiaya/delete/(:any)']	= 'keuangan/Komponen_biaya/delete/$1';

	######################
	//	TRANSAKSI 	    //
	######################

		## KAS KELUAR
		$route['keuangan/kas_keluar'] 					= 'keuangan/Kas/kas_keluar';
		$route['keuangan/kas_keluar/detail/(:any)'] 	= 'keuangan/Kas/kas_keluar_detail/$1';
		$route['insert_approve_keuangan/(:any)/(:any)'] = 'keuangan/Kas/set_approval/$1/$2';
		$route['set_approval_to_vendor/(:any)']	= 'keuangan/Kas/set_approval_to_vendor/$1';
		## MODAL
		$route['keuangan/modal/penarikan'] = 'keuangan/Modal';
		$route['insert_penarikan']		   = 'keuangan/Modal/add_penarikan';
		$route['keuangan/modal/setor']	   = 'keuangan/Modal/setor';
		$route['insert_setoran_modal']      = 'Keuangan/modal/add_setoran';

		## BANK
		$route['keuangan/bank/setor']	   = 'keuangan/Bank';
		$route['keuangan/bank/tarik']	   = 'keuangan/Bank/penarikan';
		$route['insert_setoran_bank']      = 'Keuangan/Bank/add_setoran';
		$route['insert_penarikan_bank']    = 'Keuangan/Bank/add_penarikan';

		## TRANSAKSI BEBAN
		$route['keuangan/transaksi_beban']          		= 'keuangan/Beban/keluar';
		$route['keuangan/transaksi_beban/add']		 		= 'keuangan/Beban/keluar_add';
		$route['keuangan/transaksi_beban/detail/(:any)'] 	= 'keuangan/Beban/keluar_detail/$1';
		$route['keuangan/transaksi/bunga']          		= 'keuangan/Transaksi/Bunga';
		$route['insert_bunga']  							= 'Keuangan/Transaksi/Bunga/insert';
		$route['keuangan/transaksi/koran']          		= 'keuangan/Transaksi/Koran';
		$route['keuangan/transaksi/koran/ajaxPenerimaanBank/(:any)']          		= 'keuangan/Transaksi/Koran/ajaxPenerimaanBank/$1';
		$route['keuangan/transaksi/koran/ajaxSetorBank/(:any)']          			= 'keuangan/Transaksi/Koran/ajaxSetorBank/$1';
		$route['keuangan/transaksi/koran/ajaxPenarikanBank/(:any)']          		= 'keuangan/Transaksi/Koran/ajaxPenarikanBank/$1';
		$route['keuangan/transaksi/koran/ajaxBop']          						= 'keuangan/Transaksi/Koran/ajaxBop';
		$route['keuangan/transaksi/ajaxKoran/(:any)']       = 'keuangan/Transaksi/Koran/ajaxKoran/$1';

		$route['insert_transaksi_keluar/(:any)']  	= 'Keuangan/Beban/keluar_insert/$1';

		$route['insertProdukKeluar']    		  	= 'keuangan/Beban/insert_produk';
		$route['updateProdukKeluar/(:any)']    		= 'keuangan/Beban/update_produk/$1';
		$route['deleteProdukKeluar/(:any)']    		= 'keuangan/Beban/delete_produk/$1';

		## POST JURNAL
		$route['keuangan/transaksi/jurnal']				  = 'keuangan/Jurnal';
		$route['keuangan/transaksi/jurnal/tambah']		  = 'keuangan/Jurnal/add';
		$route['keuangan/transaksi/jurnal/detail/(:any)'] = 'keuangan/Jurnal/detail/$1';
		$route['post_jurnal/(:any)']					  = 'keuangan/Jurnal/post_jurnal/$1';
		$route['insert_jurnal']	= 'keuangan/Jurnal/insert';

		## Pembayaran
		$route['keuangan/pembayaranpendaftaran']					= 'keuangan/Transaksi/PenerimaanKas/PembayaranPendaftaran';
		$route['keuangan/pembayaranpendaftaran/add']				= 'keuangan/Transaksi/PenerimaanKas/add';
		$route['keuangan/pembayaranpendaftaran/ajaxTagihan/(:any)']	= 'keuangan/Transaksi/PenerimaanKas/ajaxTagihan/$1';
		$route['keuangan/pembayaranpendaftaran/ajaxTagihanNis/(:any)']	= 'keuangan/Transaksi/PenerimaanKas/ajaxTagihanNis/$1';
		$route['keuangan/pelunasanpendaftaran']						= 'keuangan/Transaksi/PenerimaanKas/PelunasanPendaftaran';
		$route['keuangan/pelunasanpendaftaran/add']					= 'keuangan/Transaksi/PenerimaanKas/addPelunasan';
		
		$route['keuangan/pembayarannaikkelas']						= 'keuangan/Transaksi/PenerimaanKas/PembayaranNaikKelas';
		$route['keuangan/pembayarannaikkelas/add']					= 'keuangan/Transaksi/PenerimaanKas/addPembayaranNaikKelas';
		$route['keuangan/pembayarannaikkelas/ajaxTagihan/(:any)']	= 'keuangan/Transaksi/PenerimaanKas/ajaxTagihanNaikKelas/$1';
		$route['keuangan/pelunasannaikkelas']						= 'keuangan/Transaksi/PenerimaanKas/PelunasanNaikKelas';
		$route['keuangan/pelunasannaikkelas/add']					= 'keuangan/Transaksi/PenerimaanKas/addPelunasanNaikKelas';
		$route['keuangan/pembayaranspp']							= 'keuangan/Transaksi/PenerimaanKas/PembayaranSpp';
		$route['keuangan/pembayaranspp/ajaxTagihanSpp/(:any)']		= 'keuangan/Transaksi/PenerimaanKas/ajaxTagihanSpp/$1';
		$route['keuangan/pembayaranspp/add']						= 'keuangan/Transaksi/PenerimaanKas/PelunasanSpp';
		$route['keuangan/pembayarandaycare']						= 'keuangan/Transaksi/PenerimaanKas/PembayaranDaycare';
		$route['keuangan/pembayarandaycare/ajaxDaycare/(:any)']		= 'keuangan/Transaksi/PenerimaanKas/ajaxDaycare/$1';
		$route['keuangan/pembayarandaycare/StorePembayaranDaycare']	= 'keuangan/Transaksi/PenerimaanKas/StorePembayaranDaycare';
		$route['keuangan/pembayaranlainnya']						= 'keuangan/Transaksi/PenerimaanKas/PembayaranLainnya';
		$route['keuangan/pembayaranlainnya/ajaxTagihanLainnya/(:any)']		= 'keuangan/Transaksi/PenerimaanKas/ajaxTagihanLainnya/$1';
		$route['keuangan/pembayaranlainnya/StorePembayaranLainnya']	= 'keuangan/Transaksi/PenerimaanKas/StorePembayaranLainnya';
		// $route['keuangan/pembayaranpendaftaran/ajaxTagihan/(:any)']	= 'keuangan/Transaksi/PenerimaanKas/ajaxTagihan/$1';

		#################
		//   LAPORAN   //
		#################
		$route['keuangan/transaksi/neraca']			= 'keuangan/Transaksi/Neraca';

//----------------------------------------------------//
//                     KEPALA SEKOLAH                 //
//----------------------------------------------------//

	######################
	//	MASTER DATA 	//
	######################


	######################
	//	TRANSAKSI 	    //
	######################

		## REVIEW PENDANAAN
		$route['acc/kas'] 						= 'Acc/kas';
		$route['acc/kas/detail/(:any)'] 		= 'Acc/detail_kas/$1';
		$route['acc/kas/detail_bop/(:any)'] 	= 'Acc/detail_kas_bop/$1';
		$route['set_trans_level/(:any)/(:any)']	= 'Acc/set_approval/$1/$2';
		$route['set_trans_level_bop/(:any)/(:any)']	= 'Acc/set_approval_bop/$1/$2';


//----------------------------------------------------//
//                   HUMAN RESOURCE                   //
//----------------------------------------------------//

	######################
	//	MASTER DATA 	//
	######################
	
		## Jabatan
		$route['penggajian/jabatan']   	   	   = 'penggajian/Jabatan';
		$route['insert_jabatan']			   = 'penggajian/Jabatan/add';
		$route['update_jabatan']			   = 'penggajian/Jabatan/update';
		$route['delete_jabatan/(:any)'] 	   = 'penggajian/Jabatan/delete/$1';

		## Karyawan
		$route['penggajian/pegawai']      	   = 'penggajian/Pegawai';
		$route['insert_pegawai']			   = 'penggajian/Pegawai/add';
		$route['update_pegawai']			   = 'penggajian/Pegawai/update';
		$route['delete_pegawai/(:any)'] 	   = 'penggajian/Pegawai/delete/$1';

		## Tunjangan
		$route['penggajian/lembur']     	   = 'penggajian/Tunjangan';
		$route['insert_tunjangan']			   = 'penggajian/Tunjangan/add';
		$route['update_tunjangan']			   = 'penggajian/Tunjangan/update';
		$route['delete_tunjangan/(:any)/(:any)'] 	   = 'penggajian/Tunjangan/delete/$1/$2';

		$route['penggajian/tunjangan']     	   = 'penggajian/Tunjangan/lain';


	######################
	//	  TRANSAKSI   	//
	######################

		## ABSENSI
		$route['penggajian/presensi/cuti']   	= 'penggajian/Absensi/cuti';
		$route['penggajian/presensi/daftar']	= 'penggajian/Absensi/daftar';
		$route['insert_absen']			        = 'penggajian/Absensi/insert_absen';
		$route['set_izin/(:any)/(:any)']  		= 'penggajian/Absensi/set_izin/$1/$2';

		## GAJI
		$route['penggajian/gaji/lembur']   		= 'penggajian/Gaji/lembur';
		$route['insert_lembur']					= 'penggajian/Gaji/insert_lembur';
		$route['penggajian/gaji/daftar']		= 'penggajian/Gaji/daftar';
		$route['penggajian/gaji/daftar/detail/(:any)'] = 'penggajian/Gaji/detail/$1';
		$route['penggajian/gaji/daftar/detail/(:any)/cetak/(:any)'] = 'penggajian/Gaji/cetak_slip/$1/$2';
		$route['generate_gaji']					= 'penggajian/Gaji/generate_gaji';

		$route['penggajian/gaji/tunjangan']   	= 'penggajian/Gaji/tunjangan';
		$route['insert_gaji_tunjangan']			= 'penggajian/Gaji/insert_tunjangan';

		##RFID
		$route['RFID'] = 'RFID';
		$route['insert_rfid'] = 'RFID/insert';

	######################
	//	  LAPORAN   	//
	######################
		$route['penggajian/laporan/gaji'] = 'Laporan/gaji';
		$route['penggajian/laporan/absensi'] = 'Laporan/absensi';

//----------------------------------------------------//
//                  	KARYAWAN  	                  //
//----------------------------------------------------//

$route['karyawan']		= 'Dashboard/karyawan';
$route['karyawan/izin'] = 'Dashboard/izin';
$route['insert_izin']	= 'penggajian/Absensi/insert_izin';

$route['karyawan/gaji'] = 'Dashboard/gaji';
$route['karyawan/gaji/detail/(:any)'] = 'Dashboard/gaji_detail/$1';

$route['karyawan/pinjaman'] = 'Dashboard/pinjaman';
$route['insert_pinjaman'] = 'penggajian/Gaji/insert_pinjaman';


//----------------------------------------------------//
//                  	VENDOR  	                  //
//----------------------------------------------------//

$route['vendor']		= 'Dashboard/vendor';
$route['vendor/(:any)'] = 'Dashboard/perawatan/$1';
$route['vendor/(:any)/detail/(:any)'] = 'Dashboard/perawatan_detail/$1/$2';
$route['insert_price/(:any)/(:any)']  = 'Dashboard/insert_price/$1/$2';

$route['fix_done/(:any)/(:any)/(:any)'] = 'Dashboard/set_done/$1/$2/$3';

//laporan

$route['keuangan/laporan/jurnal']   			= 'Laporan/jurnal';
$route['keuangan/laporan/ayat_jurnal_penutup']  = 'Laporan/ayat_jurnal_penutup';
$route['keuangan/laporan/buku_besar']   		= 'Laporan/buku_besar';
$route['keuangan/laporan/modal_pemilik']   		= 'Laporan/modal_pemilik';
$route['keuangan/laporan/perubahan_modal']  	= 'Laporan/perubahan_modal';
$route['keuangan/laporan/kas_keluar']			= 'Laporan/kas_keluar';

$route['laporan/aset/(:any)']	 				= 'Laporan/aset/$1';
$route['keuangan/laporan/laba_rugi'] 			= 'Laporan/laba_rugi';

$route['keuangan/laporan/cash']					= 'Laporan/laporanCash';
$route['keuangan/laporan/bank']					= 'Laporan/laporanBank';

$route['keuangan/transaksi/saldokas']			= 'Laporan/saldoKas';

//----------------------------------------------------//
//                     AKADEMIK                       //
//----------------------------------------------------//
######################
//	  MasterData    //
######################
$route['akademik/masterdata/tahunajaran']				= 'Akademik/MasterData/Master';
$route['akademik/masterdata/ajaxTahunAjaran']			= 'Akademik/MasterData/Master/ajaxTahunAjaran';
$route['updateStatus/(:any)/(:any)']					= 'Akademik/MasterData/Master/updateaktif/$1/$2';
$route['akademik/masterdata/tahunajaran/tambah']		= 'Akademik/MasterData/Master/add';
$route['akademik/masterdata/tahunajaran/hapus/(:any)']	= 'Akademik/MasterData/Master/delete/$1';

$route['akademik/masterdata/ajaxGuru']	  					= 'Akademik/MasterData/Guru/ajaxGuru';
$route['akademik/masterdata/siswacalon']	  				= 'Akademik/MasterData/Siswa';
$route['akademik/masterdata/siswacalon/edit']	  			= 'Akademik/MasterData/Siswa/getcalonsiswa';
$route['akademik/masterdata/siswacalon/delete/(:any)']		= 'Akademik/MasterData/Siswa/deleteUndurDiri/$1';
$route['akademik/transaksi/siswacalon/bayar']				= 'Akademik/Transaksi/PendaftaranSiswa/PembayaranSiswa';
$route['akademik/transaksi/siswatetap/bayarspp']			= 'Akademik/MasterData/Siswa/BuatTagihanSpp';
$route['akademik/masterdata/siswatetap/delete/(:any)']		= 'Akademik/MasterData/Siswa/deleteUndurDiri/$1';
$route['akademik/masterdata/siswatetap/deleteU/(:any)']		= 'Akademik/MasterData/Siswa/deleteSiswaTetap/$1';
$route['akademik/masterdata/siswaundurdiri']	  			= 'Akademik/MasterData/Siswa/getUndurDiri';
$route['akademik/masterdata/siswaundurdiri/delete/(:any)']	= 'Akademik/MasterData/Siswa/deleteSiswa/$1';
$route['akademik/masterdata/siswatetap']					= 'Akademik/MasterData/Siswa/getSiswaTetap';
$route['akademik/masterdata/ajaxSiswaTetap/(:any)']			= 'Akademik/MasterData/Siswa/ajaxSiswaTetap/$1';
$route['akademik/masterdata/daycare']						= 'Akademik/MasterData/Daycare';
$route['akademik/masterdata/daycare/store']					= 'Akademik/MasterData/Daycare/store';
$route['akademik/transaksi/naikkelas']						= 'Akademik/Transaksi/NaikKelas';
$route['akademik/transaksi/naikkelas/ajaxSiswa/(:any)']		= 'Akademik/Transaksi/NaikKelas/ajaxSiswa/$1';
$route['akademik/transaksi/naikkelas/ajaxKelas/(:any)']		= 'Akademik/Transaksi/NaikKelas/ajaxKelas/$1';
$route['akademik/transaksi/naikkelas/ajaxKelasDetail/(:any)']		= 'Akademik/Transaksi/NaikKelas/ajaxKelasDetail/$1';
$route['akademik/transaksi/naikkelas/aksinaik']				= 'Akademik/Transaksi/NaikKelas/aksiNaik';

$route['akademik/masterdata/kelas/add']						= 'Akademik/MasterData/Kelas/add';
$route['akademik/masterdata/kelas']							= 'Akademik/MasterData/Kelas';
$route['akademik/masterdata/kelas/add']						= 'Akademik/MasterData/Kelas/add';
$route['akademik/masterdata/ajaxKelas']						= 'Akademik/MasterData/Kelas/ajaxKelas';
$route['akademik/masterdata/ajaxOrangTuaSiswa/(:any)']		= 'Akademik/MasterData/Siswa/ajaxOrangTuaSiswa/$1';
$route['akademik/masterdata/ajaxBiayaByJK/(:any)/(:any)']	= 'Akademik/MasterData/Siswa/ajaxBiayaByJK/$1/$2';
$route['akademik/masterdata/ajaxBiayaSpp/(:any)']			= 'Akademik/MasterData/Siswa/ajaxBiayaSpp/$1';

			#####################
			//	    SPP        //
			#####################
$route['transaksi/generatespp']								= 'Akademik/Transaksi/generateSpp';


			#####################
			//	    SISWA      //
			#####################
$route['akademik/transaksi/pendaftaransiswa']		= 'Akademik/Transaksi/PendaftaranSiswa';
$route['akademik/transaksi/pendaftaransiswa/add']	= 'Akademik/Transaksi/PendaftaranSiswa/add';
$route['akademik/transaksi/tagihanlainnya/add']		= 'Akademik/Transaksi/TagihanLainnya/add';
$route['akademik/transaksi/overtime']				= 'Akademik/Transaksi/TagihanLainnya/Overtime';
$route['akademik/transaksi/catering']				= 'Akademik/Transaksi/TagihanLainnya/Catering';
$route['akademik/transaksi/sarapan']				= 'Akademik/Transaksi/TagihanLainnya/Sarapan';
$route['akademik/transaksi/mandi']					= 'Akademik/Transaksi/TagihanLainnya/Mandi';
$route['akademik/transaksi/pengakuan']				= 'Akademik/Transaksi/Pengakuan';

			#####################
			//	   Laporan     //
			#####################
$route['akademik/laporan/pendaftaran']				= 'Akademik/Laporan/KartuPiutang/pendaftaran';
$route['akademik/laporan/ajaxPendaftaran/(:any)']	= 'Akademik/Laporan/KartuPiutang/ajaxPendaftaran/$1';
$route['akademik/laporan/spp']						= 'Akademik/Laporan/KartuPiutang/spp';
$route['akademik/laporan/lainnya']					= 'Akademik/Laporan/KartuPiutang/lainnya';
$route['akademik/laporan/ajaxLainnya/(:any)']		= 'Akademik/Laporan/KartuPiutang/ajaxLainnya/$1';