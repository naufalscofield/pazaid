<?php
 
Class transaksi_tarik_bank extends CI_Model{

   protected $table = 'transaksi_penarikan_bank';
    
   public function get_data(){
      $this->db->join('user b', 'b.id = a.id_user_penarik');
      $this->db->join('k_rek c', 'c.id = a.id_rekening');
      return $this->db->get("$this->table a")->result_array();
   }

   public function get_detail($key, $val = ''){
      if(is_array($key)){
         $this->db->where($key);
      
      }else{
         $this->db->where($key, $val);
      }

      return $this->db->get($this->table);
   }


   public function insert($data){
      $this->db->insert($this->table, $data);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

   public function update($data, $id){
      $this->db->where('id', $id)
               ->update($this->table, $data);
      return true;
   }

   public function delete($id){
      $this->db->where('id', $id)
               ->delete($this->table);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

}
