<?php
 
Class Transaksi_setor_modal extends CI_Model{

   protected $table = 'transaksi_setor_modal';
    
   public function get_data(){
      $this->db->join('k_pemilik b', 'b.id = a.id_pemilik');
      $this->db->join('k_rek c', 'c.id = a.rek_id');
      return $this->db->get("$this->table a")->result_array();
   }

   public function get_detail($key, $val = ''){
      if(is_array($key)){
         $this->db->where($key);
      
      }else{
         $this->db->where($key, $val);
      }

      return $this->db->get($this->table);
   }


   public function insert($data){
      $this->db->insert($this->table, $data);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

   public function update($data, $id){
      $this->db->where('id', $id)
               ->update($this->table, $data);
      return true;
   }

   public function delete($id){
      $this->db->where('id', $id)
               ->delete($this->table);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

}
