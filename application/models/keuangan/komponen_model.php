<?php
 
Class komponen_model extends CI_Model{

   protected $table = 'k_komponenbiaya';
    
   public function get_data(){
        return $this->db->get($this->table)->result_array();
   }

   public function get_detail($key, $val = ''){
      if(is_array($key)){
         $this->db->where($key);
         
      }else{
         $this->db->where($key, $val);
      }

      return $this->db->get($this->table)->row();
   }

   public function insert($data){
      $this->db->insert($this->table, $data);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

   public function update($data, $id){
      $this->db->where('id_komponen', $id)
               ->update($this->table, $data);
      return true;
   }

   public function delete($id){
      $this->db->where('id_komponen', $id)
               ->delete($this->table);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }
}
