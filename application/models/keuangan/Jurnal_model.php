<?php
 
Class Jurnal_model extends CI_Model{

   protected $table = 'jurnal';

   public function insert_single($data){
      $this->db->insert($this->table, $data);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

   public function insert_multiple($data){
      $this->db->insert_batch($this->table, $data);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

   public function insert($data)
   {
      return $this->db->insert($this->table, $data);
   }

   public function get_where($bulan)
   {
      return $this->db->query("SELECT * FROM `jurnal` where month(`tgl_jurnal`) =  $bulan")->result_array();
   }
   
   public function get_neraca($req)
   {
      return $this->db->query("SELECT a.coa_id, b.nama_coa, SUM(`nominal`) as nominal FROM jurnal a INNER join coa b on b.id = a.coa_id where month(a.tgl_jurnal) = '".$req['bulan']."' and year(a.tgl_jurnal) = '".$req['tahun']."' Group By a.coa_id")->result_array();
   }

}
