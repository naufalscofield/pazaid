<?php

class model_siswa extends CI_Model{

	protected $table = 'ak_siswa';
    
	public function get_data(){
	   return $this->db->get($this->table)->result_array();
	}

	public function get_data_spp($tahun, $bulan)
	{
		$this->db->join('transaksi_spp b', 'a.id_siswa = b.id_siswa');
		$this->db->join('ak_kelas c', 'a.id_kelas = c.id_kelas');
		$this->db->join('ak_tahun_ajaran d', 'a.id_tahun = d.id_tahun');
		$this->db->where('b.id_tahun', $tahun);
		$this->db->where('b.bulan_ke', $bulan);
		$this->db->select('b.*, b.status_spp AS status_bayar_spp');
		$this->db->select('a.*, a.status AS status_ortu');
		$this->db->select('c.*, c.status AS status_kelas');
		$this->db->select('d.*');
		return $this->db->get("$this->table a");
	}
 
	public function get_detail($key, $val = ''){
	   if(is_array($key)){
		  $this->db->where($key);
	   
	   }else{
		  $this->db->where($key, $val);
	   }
 
	   return $this->db->get($this->table);
	}

 
	public function get_detail_join($nis){

		$this->db->where('a.nis', $nis);
		$this->db->join('ak_kelas b', 'b.id_kelas = a.id_kelas');
		$this->db->join('ak_tahun_ajaran c', 'c.id_tahun = a.id_tahun');
		$this->db->select('a.*,a.status as status_ortu');
		$this->db->select('b.*,b.status as status_kelas');
		return $this->db->get("$this->table a");

	}

    public function get_undur(){
        $query = "SELECT * from ak_siswa WHERE status_siswa == 'Undur Diri'";
		return true;
    }
    
	public function insert($data){
		$this->db->insert($this->table, $data);
  
		if($this->db->affected_rows() > 0){
		   return true;
		}
		return false;
	}
	public function update($data, $id){
		$this->db->where('id_siswa', $id)
				 ->update($this->table, $data);
		return true;
	}
 
    public function deleteUndur($data, $id){
		
		$this->db->where('id_siswa', $id)
					->update($this->table, $data);
		return true;
	}

 	public function delete($id){
		$this->db->where('id_siswa', $id)
				 ->delete($this->table);
  
		if($this->db->affected_rows() > 0){
		   return true;
		}
		return false;
	}

	public function getOrtu($id_siswa)
	{
		$q = $this->db->get_where($this->table, array('id_siswa' => $id_siswa));
		$q = $q->row();
		return $q;
	}

}
