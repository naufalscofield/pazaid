<?php

class model_transaksi_spp extends CI_Model{

	protected $table = 'transaksi_spp';
	
	public function add_tagihan($data)
	{
		return $this->db->insert($this->table, $data);
	}
	
	public function get_detail($key, $val = ''){
		if(is_array($key)){
		   $this->db->where($key);
		
		}else{
		   $this->db->where($key, $val);
		}
  
		return $this->db->get($this->table);
	 }
 

	public function get_tagihan($idsiswa)
	{
		return $this->db->get_where($this->table, array('id_siswa' => $idsiswa, 'status_spp' => 'Belum Dibayar'));
	}
	public function get_bulan($id_siswa)
	{
		return $this->db->get_where($this->table, array('id_siswa' => $id_siswa));
	}


	public function update($id_spp, $data)
	{
		$this->db->where('id', $id_spp)
			     ->update($this->table, $data);
		return true;
	}
 
	public function getDataSiswa()
	{
		$this->db->join('ak_siswa b', 'b.id_siswa = a.id_siswa');
		$this->db->join('ak_kelas c', 'c.id_kelas = b.id_kelas');
		return $this->db->get("$this->table a")->result_array();
	}
	
	public function get_data()
	{
		return $this->db->get_where("$this->table", ["jenis_pembayaran" => "cash"])->result_array();
	}

	public function get_data_transfer()
	{
		return $this->db->get_where("$this->table", ["jenis_pembayaran" => "transfer"])->result_array();
	}

	public function getSemua()
	{
		return $this->db->get($this->table)->result_array();
	}

	public function get_where($key, $val)
	{
		return $this->db->get_where($this->table, [$key => $val]);
	}
}
