<?php

class model_transaksi_pendaftaran extends CI_Model{

	protected $table = 'transaksi_pendaftaran';
    
	public function store($data)
	{
		return $this->db->insert($this->table, $data);
	}
	
	public function getSemua()
	{
		return $this->db->get($this->table)->result_array();
	}

	public function get_data()
	{
			$this->db->join("ak_siswa b", "b.id_siswa = a.id_siswa");
	return	$this->db->get_where("$this->table a", ['jenis_pembayaran' => 'cash'])->result_array();
	
	}

	public function get_data_transfer()
	{
			$this->db->join("ak_siswa b", "b.id_siswa = a.id_siswa");
	return	$this->db->get_where("$this->table a", ['jenis_pembayaran' => 'transfer'])->result_array();
	
	}

	public function getDataSiswa()
	{
		$this->db->join('ak_siswa b', 'a.id_siswa = b.id_siswa');
		return $this->db->get("$this->table a")->result_array();
	}

	public function get_tagihan($id_siswa, $id_tahun)
	{
		return $this->db->get_where($this->table, array('id_siswa' => $id_siswa, 'id_tahun' => $id_tahun))->row();
	}

	public function update($id_siswa, $data)
	{
		$this->db->where('id_siswa', $id_siswa);
		$q = $this->db->update($this->table, $data);
		// return true;
		return $q;
	}

	public function get_detail($key, $val)
	{
		return $this->db->get_where($this->table, [$key => $val])->row();
	}

	public function get_where($key, $val)
	{
		return $this->db->get_where($this->table, [$key => $val]);
	}
 
	
}
