<?php

class model_biaya_spp extends CI_Model{

	protected $table = 'biaya_spp';

	public function get_where($kelas)
	{
		return $this->db->get_where($this->table, array('level_kelas' => $kelas->level_kelas, 'jenis_kelas' => $kelas->status));
	}
    
	public function getByJK($jk){
		if ($jk == 1)
		{
			$j = $this->db->get_where($this->table, array('nama_biaya' => 'Seragam Laki-Laki'))->row();
		} else {
			$j = $this->db->get_where($this->table, array('nama_biaya' => 'Seragam Perempuan'))->row();
		}

		$q = $this->db->get_where($this->table, array('nama_biaya' => 'Biaya Pendaftaran'))->row();
		$dp = $this->db->get_where($this->table, array('nama_biaya' => 'DPP (Pembangunan)'))->row();
		$ds = $this->db->get_where($this->table, array('nama_biaya' => 'DSK (Semester)'))->row();

		$res = [
			'biaya_daftar' => $q->biaya,
			'biaya_seragam' => $j->biaya,
			'biaya_dpp' => $dp->biaya,
			'biaya_dsk' => $ds->biaya,
		];

		return $res;
	}
 
	public function get_detail($key, $val = ''){
	   if(is_array($key)){
		  $this->db->where($key);
	   
	   }else{
		  $this->db->where($key, $val);
	   }
 
	   return $this->db->get($this->table);
	}

    public function get_undur(){
        $query = "SELECT * from ak_siswa WHERE status_siswa == 'Undur Diri'";
		return true;
    }
    
	public function insert($data){
		$this->db->insert($this->table, $data);
  
		if($this->db->affected_rows() > 0){
		   return true;
		}
		return false;
	}
	public function update($data, $id){
		$this->db->where('id_siswa', $id)
				 ->update($this->table, $data);
		return true;
	}
 
    public function deleteUndur($data, $id){
		
		$this->db->where('id_siswa', $id)
					->update($this->table, $data);
		return true;
	}

 	public function delete($id){
		$this->db->where('id_siswa', $id)
				 ->delete($this->table);
  
		if($this->db->affected_rows() > 0){
		   return true;
		}
		return false;
	}

	public function getOrtu($id_siswa)
	{
		$q = $this->db->get_where($this->table, array('id_siswa' => $id_siswa));
		$q = $q->row();
		return $q;
	}

}
