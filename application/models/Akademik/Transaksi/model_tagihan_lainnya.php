<?php

class model_tagihan_lainnya extends CI_Model{

	protected $table = 'transaksi_lainnya';
	
	public function get_data()
	{
		return $this->db->get($this->table)->result_array();
	}
	
	public function getDataCash()
	{
		return $this->db->get_where("$this->table", ['jenis_pembayaran' => 'cash'])->result_array();
	}

	public function get_detail_overtime($key, $value)
	{
		return $this->db->get_where($this->table, [$key => $value, 'jenis_tambahan' => 'overtime']);
	}
	
	public function get_detail_mandi($key, $value)
	{
		return $this->db->get_where($this->table, [$key => $value, 'jenis_tambahan' => 'mandi']);
	}
	
	public function get_detail_sarapan($key, $value)
	{
		return $this->db->get_where($this->table, [$key => $value, 'jenis_tambahan' => 'sarapan']);
	}
	
	public function get_detail_catering($key, $value)
	{
		return $this->db->get_where($this->table, [$key => $value, 'jenis_tambahan' => 'catering']);
	}

	public function getDataSiswa()
	{
		$this->db->join('ak_siswa b', 'b.nis = a.nis');
		return $this->db->get("$this->table a")->result_array();
	}

	public function getTagSiswa($id_siswa)
	{
		$this->db->join('ak_siswa b', 'b.id_siswa = a.id_siswa');
		return $this->db->get("$this->table a")->row();
	}

	public function add_tagihan($data)
	{
		return $this->db->insert($this->table, $data);
	}
	
	public function get_tagihan($nis)
	{
		$this->db->join('ak_siswa b', 'b.nis = a.nis');
		return $this->db->get_where("$this->table a" , array('a.nis' => $nis, 'a.status_lainnya' => 'Belum Lunas'))->result_array();
	}
	
	public function get_tagihan_overtime($nis)
	{
		return $this->db->get_where("$this->table" , array('nis' => $nis, 'status_lainnya' => 'Belum Lunas', 'jenis_tambahan' => 'overtime'));
	}
	
	public function get_tagihan_catering($nis)
	{
		return $this->db->get_where("$this->table" , array('nis' => $nis, 'status_lainnya' => 'Belum Lunas', 'jenis_tambahan' => 'catering'));
	}
	
	public function get_tagihan_mandi($nis)
	{
		return $this->db->get_where("$this->table" , array('nis' => $nis, 'status_lainnya' => 'Belum Lunas', 'jenis_tambahan' => 'mandi'));
	}
	
	public function get_tagihan_sarapan($nis)
	{
		return $this->db->get_where("$this->table" , array('nis' => $nis, 'status_lainnya' => 'Belum Lunas', 'jenis_tambahan' => 'sarapan'));
	}


	public function update($nis, $data)
	{
		$this->db->where('nis', $nis)
			     ->update($this->table, $data);
		return true;
	}
 
	public function get_data_transfer()
	{
		return $this->db->get_where("$this->table", ["jenis_pembayaran" => "transfer"])->result_array();
	}

	public function getSemua()
	{
		return $this->db->get($this->table)->result_array();
	}

	public function get_where($key, $val)
	{
		return $this->db->get_where($this->table, [$key => $val]);
	}
}
