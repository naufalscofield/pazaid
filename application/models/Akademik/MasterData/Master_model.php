<?php

class master_model extends CI_Model{

	protected $table = 'ak_tahun_ajaran';
    
	public function get_data(){
	   return $this->db->get($this->table)->result_array();
	}

	public function get_active()
	{
		return $this->db->get_where($this->table, array('status' => 1))->row();
	}
 
	public function get_detail($key, $val = ''){
	   if(is_array($key)){
		  $this->db->where($key);
	   
	   }else{
		  $this->db->where($key, $val);
	   }
 
	   return $this->db->get($this->table);
	}

	public function getById($id)
	{
		$data = $this->db->select('*')->from($this->table)->where('id_tahun',$id)->get();
		return $data->result();
	}

	public function generate_code(){
		$this->db->select('RIGHT(kode_tahun,4) as kode', FALSE)
				 ->order_by('kode_tahun','DESC')
				 ->limit(1);    
		
		$query = $this->db->get($this->table);  
		if($query->num_rows() <> 0){
		   $data = $query->row();      
		   $kode = intval($data->kode) + 1;    
		}else{
		   $kode = 1;    
		}
  
		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
		$code = "TAH-20-".$kodemax;
		return $code;
	}

	public function insert($data){
		$this->db->insert($this->table, $data);
  
		if($this->db->affected_rows() > 0){
		   return true;
		}
		return false;
	}
	public function update($data, $id){
		$this->db->where('id_tahun', $id)
				 ->update($this->table, $data);
		return true;
	 }
  
 	public function delete($id){
		$this->db->where('id_tahun', $id)
				 ->delete($this->table);
  
		if($this->db->affected_rows() > 0){
		   return true;
		}
		return false;
	}

}
