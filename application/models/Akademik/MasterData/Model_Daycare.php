<?php

class Model_Daycare extends CI_Model{

	protected $table = 'ak_daycare';
    
	public function store($data)
	{
		return $this->db->insert($this->table, $data);
	}

	public function show($kode)
	{
		return $this->db->get_where($this->table, array('kode_transaksi' => $kode))->row();
	}

	public function get_detail($key, $val)
	{
		return $this->db->get_where($this->table, array($key => $val))->row();
	}

	public function storePembayaran($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update($this->table, $data);
	}

	public function get()
	{
		return $this->db->get($this->table)->result_array();
	}

	public function get_data()
	{
		return $this->db->get_where("$this->table", ['jenis_pembayaran' => 'cash'])->result_array();
	}

	public function get_data_transfer()
	{
		return $this->db->get_where("$this->table", ["jenis_pembayaran" => "transfer"])->result_array();
	}

	public function getSemua()
	{
		return $this->db->get($this->table)->result_array();
	}

}
