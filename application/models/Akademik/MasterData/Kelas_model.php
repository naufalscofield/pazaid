<?php

class Kelas_model extends CI_Model{

	protected $table = 'ak_kelas';
    
	public function get_data(){
	   return $this->db->get($this->table)->result_array();
	}

	public function get_detail($key, $val)
	{
		return $this->db->get_where($this->table, array($key => $val));
	}

	public function generate_code_bayi(){
		$this->db->select('RIGHT(kode_kelas,4) as kode', FALSE)
				 ->order_by('kode_kelas','DESC')
				 ->limit(1);    
		$query = $this->db->get($this->table);  
		if($query->num_rows() <> 0){
		   $data = $query->row();      
		   $kode = intval($data->kode) + 1;    
		}else{
		   $kode = 1;    
		}

		
		
		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
		$code = "BB-".$kodemax;
		return $code;
    }
    
    public function generate_code_pg(){
		$this->db->select('RIGHT(kode_kelas,4) as kode', FALSE)
				 ->order_by('kode_kelas','DESC')
				 ->limit(1);    
		
		$query = $this->db->get($this->table);  
		if($query->num_rows() <> 0){
		   $data = $query->row();      
		   $kode = intval($data->kode) + 1;    
		}else{
		   $kode = 1;    
		}
  
		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
		$code = "PG-".$kodemax;
		return $code;
	}

	public function generate_code_tk(){
		$this->db->select('RIGHT(kode_kelas,4) as kode', FALSE)
				 ->order_by('kode_kelas','DESC')
				 ->limit(1);    
		
		$query = $this->db->get($this->table);  
		if($query->num_rows() <> 0){
		   $data = $query->row();      
		   $kode = intval($data->kode) + 1;    
		}else{
		   $kode = 1;    
		}
  
		$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
		$code = "TK-".$kodemax;
		return $code;
	}

	public function insert($data){
		$this->db->insert($this->table, $data);
  
		if($this->db->affected_rows() > 0){
		   return true;
		}
		return false;
	}
	public function update($data, $id){
		$this->db->where('kode_kelas', $id)
				 ->update($this->table, $data);
		return true;
	 }
  
 	public function delete($id){
		$this->db->where('id_tahun', $id)
				 ->delete($this->table);
  
		if($this->db->affected_rows() > 0){
		   return true;
		}
		return false;
	}

}
