<?php
 
Class Penyusutan_model extends CI_Model{

   protected $table = 'hru_aset';
    
   public function get_data(){
      $this->db->select('*, hru_aset.id AS id_aset, hru_aset.id AS id_kategori')
               ->join('hru_aset', 'hru_aset.id = hru_aset.kategori_id');
      return $this->db->get($this->table)->result_array();
   }

   public function get_detail($key, $val = ''){
      if(is_array($key)){
         $this->db->where($key);
      
      }else{
         $this->db->where($key, $val);
      }

      return $this->db->get($this->table);
   }

   public function generate_code(){
      $this->db->select('RIGHT(kode_aset,4) as kode', FALSE)
               ->order_by('kode_aset','DESC')
               ->limit(1);    
      
      $query = $this->db->get('hru_aset');  
      if($query->num_rows() <> 0){
         $data = $query->row();      
         $kode = intval($data->kode) + 1;    
      }else{
         $kode = 1;    
      }

      $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
      $code = "AT-".$kodemax;
      return $code;
   }

   public function insert($data){
      $this->db->insert($this->table, $data);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

   public function update($data, $id){
      $this->db->where('id', $id)
               ->update($this->table, $data);
      return true;
   }

   public function delete($id){
      $this->db->where('id', $id)
               ->delete($this->table);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

   public function get_dataPenyusutan($req = []){
      if(array_key_exists('start_date', $req)){
         $this->db->where('tanggal_penempatan >=',$req['start_date']);     
      }

      if(array_key_exists('end_date', $req)){
         $this->db->where('tanggal_penempatan <=',$req['end_date']);
      }

      if(array_key_exists('bulan', $req)){
         $this->db->where('MONTH(tanggal) <=',$req['bulan']);      
      }

      if(array_key_exists('tahun', $req)){
         $this->db->where('YEAR(tanggal) <=',$req['tahun']);
      }

      if(array_key_exists('id_aset', $req)){
         $this->db->where('detail_aset.aset_id',$req['id_aset']);
      }

      if(array_key_exists('id_detail_aset', $req)){
         $this->db->where('detail_aset.id',$req['id_detail_aset']);
      }

      $this->db->join('hru_aset aset','aset.id = detail_aset.aset_id')
               ->join('hru_kategori kategori','kategori.id = aset.kategori_id')
               ->join('transaksi_aset', 'transaksi_aset.id = detail_aset.transaksi_aset_id')
               ->join('transaksi', 'transaksi.id = transaksi_aset.transaksi_id');

      return $this->db->get('hru_aset_detail detail_aset')->result_array();
   }

   public function get_dataPenyusutan_by_kategori($id){

      $this->db->where('kategori.id', $id)
             ->join('aset','aset.id = detail_aset.aset_id')
             ->join('kategori','kategori.id = aset.id_kategori');

      return $this->db->get('detail_aset')->result_array();
   }

   public function calculatePenyusutan($harga_asset, $residu, $masa_pakai, $tgl_perolehan, $jumlah){
       $num = 0;

       $tgl = substr($tgl_perolehan, 8, 2);
       $bln = substr($tgl_perolehan, 5, 2);
       $num = $bln;

       if($tgl >= '15'){
         $num = $num + 1;

         if($bln == '12'){
           $num = 1;
         }
       }

       $num = 13 - $num;

       $harga_penyusutan = ($num / 12) * ($harga_asset - $residu) / $masa_pakai;
       $total_penyusutan = ($harga_asset - $harga_penyusutan) * $jumlah;
       return $total_penyusutan; 
  }


  public function set_penyusutan_to_jurnal(){
    $day  = date('d');
    $date = date('Y-m-d');
    $this->db->select('*, aset_detail.id AS aset_detail_id')
             ->where('DAY(tanggal_transaksi) <=', $day)
             ->where('aset_detail.id NOT IN(
                          SELECT aset_detail_id FROM hru_penyusutan_log 
                          WHERE tanggal_penyusutan <= "'.$date.'"
                    )')
             ->where('aset_detail.is_active', '1')
             ->where('aset_detail.is_terima', '1')
             ->where('aset_detail.ruangan_id !=', null)
             ->join('transaksi_aset', 'transaksi_aset.transaksi_id = transaksi.id')
             ->join('hru_aset_detail aset_detail', 'aset_detail.transaksi_aset_id = transaksi_aset.id')
             ->join('hru_aset aset', 'aset.id = aset_detail.aset_id')
             ->join('hru_kategori kategori', 'kategori.id = aset.kategori_id');
    $cek = $this->db->get('transaksi')->result_array();
    $waktu = date('Y-m-d H:i:s');

    if(count($cek) > 0){
      $kode_jurnal = $this->transaksi_model->generate_jurnal();
      $this->db->insert('jurnal_master', [
        'kode_jurnal'   => $kode_jurnal,
        'tanggal_input' => $waktu,
        'keterangan'    => 'Penyusutan Aset',
      ]);
      $last_jurnal = $this->transaksi_model->last_jurnal();
    }
    
    foreach ($cek as $row){
      $log[] = [
        'aset_detail_id' => $row['aset_detail_id'],
        'tanggal_penyusutan' => $date
      ];
      $prb =$this->db
         ->select('*, 
                  (CASE 
                    WHEN DAY(tanggal_transaksi) > 15 
                    THEN CONCAT(
                          MONTH(DATE_ADD(tanggal_transaksi, INTERVAL 1 MONTH)), "-", 
                          YEAR(DATE_ADD(tanggal_transaksi, INTERVAL 1 MONTH))
                        ) 
                    ELSE CONCAT(
                          MONTH(tanggal_transaksi), "-", 
                          YEAR(tanggal_transaksi))
                   END )AS waktu_perbaikan')
         ->where('aset_detail_id', $row['aset_detail_id'])
         ->join('transaksi', 'transaksi.id = transaksi_perawatan.transaksi_id')
         ->where('tipe', 'perbaikan')
         ->get('transaksi_perawatan')->result_array();

      $akumulasi = 0;
      $num = 0;
      $tgl = substr($row['tanggal_penempatan'], 8, 2);
      $bln = substr($row['tanggal_penempatan'], 5, 2);
      $thn = substr($row['tanggal_penempatan'], 0, 4);
      $num = $bln;

      $masa_pakai = $row['masa_pakai'];

      if($tgl >= '15'){
        $num = $num + 1;

        if($bln == '12'){
          $num = 1;
        }
      }

      $num = $num;

      $selisih = diffMonth(date('Y-m-')."1", $thn."-".$bln."-1");

      if(date('d', strtotime($row['tanggal_penempatan'])) > 15){
        $time = strtotime($row['tanggal_penempatan']);
        $row['tanggal_penempatan'] = date("Y-m-2", strtotime("+1 month", $time));
      }

      $cek_tgl = date('n-Y', strtotime($row['tanggal_penempatan']));
      $search = search($prb, ['waktu_perbaikan' => $cek_tgl]);

      if(!empty($search)){
        $akm_total = 0;
        foreach ($search as $akm){
          $akm_total += $akm['harga_perbaikan'];
        }
        $row['harga'] += $akm_total;
      }

      $total_penyusutan = calculatePenyusutan(
                            $row['harga'], 
                            $row['nilai_residu'],
                            $masa_pakai, 
                            $row['tanggal_penempatan'],1, 0, $prb);

      $akumulasi  += $total_penyusutan;
      $total_buku =  $row['harga'] - $total_penyusutan;

      for($i = 1; $i < $selisih; $i++){
        $new_thn = date('Y', strtotime("+".$i." months", strtotime($row['tanggal_penempatan'])));
        $new_bln = date('n', strtotime("+".$i." months", strtotime($row['tanggal_penempatan'])));

        $cek_tgl = $new_bln."-".$new_thn;
        $search = search($prb, ['waktu_perbaikan' => $cek_tgl]);

        if(!empty($search)){
          $akm_total = 0;
          foreach ($search as $akm){
            $akm_total += $akm['harga_perawatan'];
          }

          $current = $total_buku;
          $row['harga'] = $current + $akm_total;
          $total_penyusutan = calculatePenyusutan(
                      $row['harga'], 
                      $row['nilai_residu'],
                      $masa_pakai, 
                      $row['tanggal_penempatan'],1, 0, $prb);
          $total_buku = $row['harga'];
        }

        $akumulasi  += $total_penyusutan;
        $total_buku -= $total_penyusutan;
      }

      $jurnal[] = [
        'jurnal_master_id' => $last_jurnal['id'],
        'coa_id'           => '21', //BEBAN PENYUSUTAN
        'tgl_jurnal'       => $waktu,
        'posisi'           => 'debit',
        'nominal'          => $akumulasi,
        'alt_name'         => 'Beban Penyusutan '.$row['nama_aset']
      ];
      $jurnal[] = [
        'jurnal_master_id' => $last_jurnal['id'],
        'coa_id'           => '22', //AKUMULASI PENYUSUTAN
        'tgl_jurnal'       => $waktu,
        'posisi'         => 'kredit',
        'nominal'        => $akumulasi,
        'alt_name'       => 'Akumulasi Penyusutan '.$row['nama_aset']
      ];
    }

    if(!empty($jurnal)){
      $this->db->trans_begin();

      $this->db->insert_batch('jurnal', $jurnal);
      $this->db->insert_batch('hru_penyusutan_log', $log);

      if($this->db->trans_status()){
        $this->db->trans_commit();
      }else{
        $this->db->trans_rollback();
      }
    }
    
  }

}
