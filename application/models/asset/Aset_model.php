<?php
 
Class Aset_model extends CI_Model{

   protected $table = 'hru_aset';
    
   public function get_data(){
      $this->db->select('*, hru_aset.id AS id_aset, hru_kategori.id AS id_kategori')
               ->join('hru_kategori', 'hru_kategori.id = hru_aset.kategori_id');
      return $this->db->get($this->table)->result_array();
   }

   public function get_detail($key, $val = ''){
      if(is_array($key)){
         $this->db->where($key);
      
      }else{
         $this->db->where($key, $val);
      }

      return $this->db->get($this->table);
   }

   public function generate_code(){
      $this->db->select('RIGHT(kode_aset,4) as kode', FALSE)
               ->order_by('kode_aset','DESC')
               ->limit(1);    
      
      $query = $this->db->get('hru_aset');  
      if($query->num_rows() <> 0){
         $data = $query->row();      
         $kode = intval($data->kode) + 1;    
      }else{
         $kode = 1;    
      }

      $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
      $code = "AT-".$kodemax;
      return $code;
   }

   public function insert($data){
      $this->db->insert($this->table, $data);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

   public function update($data, $id){
      $this->db->where('id', $id)
               ->update($this->table, $data);
      return true;
   }

   public function delete($id){
      $this->db->where('id', $id)
               ->delete($this->table);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

   public function get_list_detail($aset_id){
      $this->db->select('*, hru_aset_detail.id AS id_detail_aset')
               ->where('hru_aset_detail.aset_id', $aset_id)
               ->join('transaksi_aset', 'transaksi_aset.id = hru_aset_detail.transaksi_aset_id')
               ->join('transaksi', 'transaksi.id = transaksi_aset.transaksi_id')
               ->join('hru_ruangan', 'hru_ruangan.id = hru_aset_detail.ruangan_id', 'LEFT');
      return $this->db->get('hru_aset_detail')->result_array();
   }

   public function get_unlocated($aset_id = ''){
      $this->db->select('*, hru_aset_detail.id AS aset_detail_id');

      if($aset_id != ''){
         $this->db->where('aset_id', $aset_id);
      }

      $this->db->join('hru_aset', 'hru_aset.id = hru_aset_detail.aset_id')
               ->where('is_active', '1')
               ->where('is_terima', '1')
               ->where('ruangan_id', null);
      return $this->db->get('hru_aset_detail')->result_array();
   }

   public function insert_location($aset_id, $location_id){
      $this->db->where_in('id',$aset_id)
               ->update('hru_aset_detail', [
                                 'ruangan_id' => $location_id,
                                 'tanggal_penempatan' => date('Y-m-d')
                              ]);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

   public function get_received($transaksi_id){
      $this->db->select('*, hru_aset_detail.id AS aset_detail_id')
               ->where('transaksi_id', $transaksi_id)
               ->where('tipe', 'perolehan')
               ->join('transaksi_aset', 'transaksi_aset.transaksi_id = transaksi.id')
               ->join('hru_aset_detail', 'hru_aset_detail.transaksi_aset_id = transaksi_aset.id')
               ->join('hru_aset', 'hru_aset.id = hru_aset_detail.aset_id');
      return $this->db->get('transaksi')->result_array();
   }
}
