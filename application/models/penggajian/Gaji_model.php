<?php
class Gaji_model extends CI_model{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function get_list($bulan, $tahun, $pegawai_id = ''){

		$this->db->select('gaji.*, gaji_detail.*, gaji.id AS gaji_id,
					   COUNT(pegawai_id) AS total_pegawai,
					   SUM(total_gaji) AS total_gaji')
			 ->order_by('gaji.id','DESC')
			 ->group_by('gaji.id')
			 ->join('fdl_gaji_detail gaji_detail','gaji_detail.gaji_id = gaji.id')
			 ->where('bulan', $bulan)
			 ->where('tahun', $tahun);

		if($pegawai_id != ''){
			$this->db->where('pegawai_id', $pegawai_id);
		}

		$get = $this->db->get('fdl_gaji gaji')->row_array();

		$data['id_gaji'] 		= $get['gaji_id'];
		$data['bulan'] 	 		= $bulan;
		$data['tahun'] 			= $tahun;
		$data['total_gaji'] 	= $get['total_gaji'];
		$data['total_pegawai'] = $get['total_pegawai'];

		return $data;
	}

	public function calculateGaji($bulan, $tahun, $pegawai_id = ''){

		$get_waktu = $this->db->where('is_aktif','1')
				 			  ->get('fdl_waktu')->result_array();
		$work_day = [];

		foreach ($get_waktu as $row) {
			$work_day[] = $row['hari'];
		}

		$req = array(
				'bulan' => $bulan,
				'tahun' => $tahun
			);

		if($pegawai_id != ''){
			$req['pegawai.id'] = $pegawai_id;
		}

		$absensi  = $this->m_absensi->get_list($req, 'gaji')->result_array();

		$bulan    = $bulan;
		$tahun    = $tahun;
		$i = 0;
		$total_gaji = $total_pajak = 0;
		$data = [];

		foreach ($absensi as $row){

			$day = [];
			for($d=1; $d<=31; $d++){
                $time = mktime(12, 0, 0, $bulan, $d, $tahun);          
                if(date('m', $time) == $bulan){     
                    $day[]= $d;
                }
            }

            $num_days = 0; $c = 0;
            foreach ($day as $k){
            	$c++;
            	if(filterDay($tahun.'-'.$bulan.'-'.$c, $work_day)){
            		$num_days++;
            	}
            	
            }

            $lembur = $this->db->select('SUM(nominal_lembur) AS nominal')
            				   ->where('pegawai_id', $row['id'])
            				   ->where('MONTH(tanggal)', $bulan)
            				   ->where('YEAR(tanggal)', $tahun)
            				   ->get('fdl_lembur')->row_array();
            $nominal_lembur = $lembur['nominal'];

			$base = $row['gaji'] / $num_days;
			$kehadiran = $row['total_hadir'] + $row['total_dinas'];

			$n_hadir = ($kehadiran * $base);
			$n_telat = $row['total_terlambat'] * ((50/100) * $base);

			$pokok = $n_hadir + $n_telat;
			$nominal_pokok	   = $pokok;

			$data[$i]['pegawai_id'] 		= $row['id'];
			$data[$i]['kode_pegawai']		= $row['kode_pegawai'];
			$data[$i]['nama_pegawai']		= $row['nama_pegawai'];
			$data[$i]['total_masuk'] 		= $row['total_masuk'];
			$data[$i]['total_hadir'] 		= $kehadiran;
			$data[$i]['total_terlambat'] 	= $row['total_terlambat'];
			$data[$i]['maksimal_kehadiran'] = $num_days;
			$data[$i]['gaji_pokok'] 		 = $nominal_pokok;
			$data[$i]['gaji_kehadiran']		 = $n_hadir;
			$data[$i]['gaji_keterlambatan']  = $n_telat;		
			$data[$i]['tunjangan_lembur'] 	 = $nominal_lembur;
			$data[$i]['total_gaji']			 = $nominal_pokok + $nominal_lembur;
			
			$total_gaji += $data[$i]['total_gaji'];

			$i++;
		}

		return array(
				'pph' => $total_pajak,
				'total_gaji' => $total_gaji,
				'total_pegawai' => $i,
				'list' => $data
			);
	}

	public function insert_lembur($data){
		$this->db->insert('fdl_lembur',$data);

		if($this->db->affected_rows() > 0){
			return true;
		}
		return false;
	}

	public function get_lembur_list(){
		$this->db->join('fdl_pegawai pegawai', 'pegawai.id = lembur.pegawai_id');
		return $this->db->get('fdl_lembur lembur');
	}

	public function get_list_detail($id_gaji, $pegawai_id = ''){

		$this->db->where('gaji_id',$id_gaji)
				 ->join('fdl_pegawai pegawai','pegawai.id = fdl_gaji_detail.pegawai_id');

		if($pegawai_id != ''){
			$this->db->where('pegawai_id',$pegawai_id);
		}

		return $this->db->get('fdl_gaji_detail');
	}

	public function get_detail($id_gaji, $pegawai_id = ''){
		$this->db->where('gaji.id',$id_gaji);
		$this->db->select('gaji.*, gaji_detail.*, gaji.id AS gaji_id,
						   COUNT(pegawai_id) AS total_pegawai,
						   SUM(total_gaji) AS total_gaji')
				 ->order_by('gaji.id','DESC')
				 ->join('fdl_gaji_detail gaji_detail','gaji_detail.gaji_id = gaji.id');	
				 
		if($pegawai_id != ''){
			$this->db->where('pegawai_id',$pegawai_id);
		}

		return $this->db->get('fdl_gaji gaji');
	}

	public function get_condition($condition){
		$this->db->where($condition);
		return $this->db->get('gaji');
	}

	public function insert($data){
		$this->db->insert('fdl_gaji',$data);

		if($this->db->affected_rows() > 0){
			return true;
		}
		return false;
	}

	public function delete($id_gaji){
		$this->db->where('id',$id_gaji)
				 ->delete('fdl_gaji');

		if($this->db->affected_rows() > 0){
			return true;
		}
		return false;
	}

	public function update($data, $id_gaji){
		$this->db->where('id',$id_gaji)
				 ->update('fdl_gaji', $data);

		return true;
	}

	public function get_list_pegawai($id){
		return $this->db->select('gaji.*, gaji_detail.*, gaji.id AS gaji_id')
						->where('pegawai_id',$id)
						->join('fdl_gaji_detail gaji_detail','gaji_detail.id_gaji = gaji.id')
						->group_by('pegawai_id')
				 		->get('fdl_gaji gaji');
	}

	public function get_tunjangan(){
		$this->db->join('fdl_pegawai', 'fdl_pegawai.id = fdl_gaji_tunjangan.pegawai_id')
				 ->join('fdl_tunjangan', 'fdl_tunjangan.id = fdl_gaji_tunjangan.tunjangan_id')
				 ->order_by('fdl_gaji_tunjangan.id', 'DESC');
		return $this->db->get('fdl_gaji_tunjangan')->result_array();
	}

	public function insert_tunjangan($data){
		$this->db->insert('fdl_gaji_tunjangan',$data);

		if($this->db->affected_rows() > 0){
			return true;
		}
		return false;
	}
	
}
