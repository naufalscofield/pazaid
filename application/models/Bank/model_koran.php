<?php

class model_koran extends CI_Model{

	protected $table = 'k_rek_koran';
    
	public function get_data(){
		$this->db->join('k_bunga b', 'b.id = a.id_bunga', 'left');
		$this->db->join('k_rek c', 'c.id = b.id_rekening', 'right');
		return $this->db->get("$this->table a")->result_array();
	}

	public function getBunga()
	{
		$this->db->join('k_rek b','b.id = c.id_rekening');
		$this->db->join('k_bunga c','c.id = a.id_bunga');
		$baris = $this->db->get("$this->table a");
		$baris = $baris->result_array();
		return $baris;
	}
 
	// public function get_detail($key, $val = ''){
	//    if(is_array($key)){
	// 	  $this->db->where($key);
	   
	//    }else{
	// 	  $this->db->where($key, $val);
	//    }
 
	//    return $this->db->get($this->table);
	// }

	// public function generate_code(){
	// 	$this->db->select('RIGHT(kode_bank,4) as kode', FALSE)
	// 			 ->order_by('kode_bank','DESC')
	// 			 ->limit(1);    
		
	// 	$query = $this->db->get($this->table);  
	// 	if($query->num_rows() <> 0){
	// 	   $data = $query->row();      
	// 	   $kode = intval($data->kode) + 1;    
	// 	}else{
	// 	   $kode = 1;    
	// 	}
  
	// 	$kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
	// 	$code = "BNK-".$kodemax;
	// 	return $code;
	// }

	public function insert($data){
		$this->db->insert($this->table, $data);
  
		if($this->db->affected_rows() > 0){
		   return true;
		}
		return false;
	}
	// public function update($data, $id){
	// 	$this->db->where('kode_bank', $id)
	// 			 ->update($this->table, $data);
	// 	return true;
	//  }
  
 	// public function delete($id){
	// 	$this->db->where('id_bank', $id)
	// 			 ->delete($this->table);
  
	// 	if($this->db->affected_rows() > 0){
	// 	   return true;
	// 	}
	// 	return false;
	// }

}
?>
