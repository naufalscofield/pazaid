<?php
 
Class Transaksi_model extends CI_Model{

   protected $table = 'transaksi';
    
   public function get_data(){
      $this->db->select('*, transaksi.id AS transaksi_id');
      return $this->db->get($this->table)->result_array();
   }

   public function get_list_item($tipe, $id, $by = ''){

      $res = [];

      $this->db->select('*, transaksi.id AS transaksi_id')
               ->where('transaksi.id', $id);

      if($tipe == 'perolehan'){
         $this->db->select('transaksi_aset.id AS transaksi_aset_id, hru_aset.id AS aset_id')
                  ->join('transaksi_aset','transaksi_aset.transaksi_id = transaksi.id')
                  ->join('hru_aset','hru_aset.id = transaksi_aset.aset_id');
      
      }else if($tipe == 'transaksi_beban'){
         $this->db->select('transaksi_beban.id AS transaksi_beban_id, transaksi_beban.qty AS jumlah')
                  ->join('transaksi_beban','transaksi_beban.transaksi_id = transaksi.id')
                  ->join('k_beban','k_beban.id = transaksi_beban.beban_id');
      
      }else if($tipe == 'pinjaman'){
         $this->db->join('fdl_pegawai', 'fdl_pegawai.id = transaksi.pegawai_id');
      
      }else if($tipe == 'pemeliharaan' || $tipe == 'perbaikan'){
         $this->db->select('transaksi_perawatan.id AS transaksi_perawatan_id')
                  ->join('transaksi_perawatan', 'transaksi_perawatan.transaksi_id = transaksi.id')
                  ->join('hru_aset_detail', 'hru_aset_detail.id = transaksi_perawatan.aset_detail_id')
                  ->join('hru_aset', 'hru_aset.id = hru_aset_detail.aset_id');
      
      }else if($tipe == 'setoran_bank'){
         $this->db->join('k_rek', 'k_rek.id = transaksi.rek_id');
      
      }else if($tipe == 'bop'){
         $this->db->join('transaksi_bop', 'transaksi_bop.transaksi_id = transaksi.id', 'LEFT')
                  ->join('k_rek', 'k_rek.id = transaksi_bop.rek_id', 'LEFT');
      }

      $data = $this->db->get($this->table)->result_array();
      $i = 0;

      foreach ($data as $row) {
         if($tipe == 'perolehan'){
            $res[$i]['detail_id']     = $row['transaksi_aset_id'];
            $res[$i]['nama_komponen'] = $row['kode_aset']." / ".$row['nama_aset'];
            $res[$i]['nilai_residu'] = $row['nilai_residu'];
            $res[$i]['harga']    = $row['harga']; 
            $res[$i]['jumlah']   = $row['jumlah']; 
            $res[$i]['subtotal'] = $row['subtotal'];
            $res[$i]['umur']     = $row['umur'];

         }else if($tipe == 'transaksi_beban'){
            $res[$i]['detail_id']     = $row['transaksi_beban_id'];
            $res[$i]['kode_beban']    = $row['kode_beban'];
            $res[$i]['nama_beban']    = $row['nama_beban'];
            $res[$i]['nama_komponen'] = $row['kode_beban']." / ".$row['nama_beban'];
            $res[$i]['harga']    = $row['harga']; 
            $res[$i]['jumlah']   = $row['jumlah']; 
            $res[$i]['qty']      = $row['jumlah'];
            $res[$i]['subtotal'] = $row['subtotal'];
         
         }else if($tipe == 'pinjaman'){
            $res[$i]['detail_id']     = $row['transaksi_id'];
            $res[$i]['nama_komponen'] = $row['kode_pegawai']." / ".$row['nama_pegawai'];
            $res[$i]['harga']    = ''; 
            $res[$i]['jumlah']   = '1';
            $res[$i]['subtotal'] = $row['total_transaksi'];
         
         }else if($tipe == 'pemeliharaan' || $tipe == 'perbaikan'){
            $res[$i]['detail_id']     = $row['transaksi_perawatan_id'];
            $res[$i]['nama_komponen'] = $row['nama_aset']." / ".$row['kode_detail_aset'];
            $res[$i]['harga']         = $row['harga_perawatan']; 
            $res[$i]['subtotal']      = $row['harga_perawatan'];
            $res[$i]['jumlah']        = '1';
            $res[$i]['keterangan']    = $row['keterangan_perawatan'];
            $res[$i]['keterangan_vendor'] = $row['keterangan_vendor'];
            $res[$i]['gambar']        = $row['gambar'];
            $res[$i]['is_done']       = $row['is_done'];
         
         }else if($tipe == 'setoran_bank'){
            $res[$i]['nama_komponen'] = $row['nama_bank']." - ".$row['no_rek']." a/n ".$row['atas_nama'];
            $res[$i]['harga']    = ''; 
            $res[$i]['jumlah']   = '1';
            $res[$i]['subtotal'] = $row['total_transaksi'];
         
         }else if($tipe == 'bop'){
            if($by == 'masuk'){
               $res[$i]['nama_komponen'] = 'Penerimaan Dana BOP';
               $res[$i]['harga']         = ''; 
               $res[$i]['jumlah']        = '1';
               $res[$i]['subtotal']      = $row['total_transaksi'];
               $res[$i]['keterangan']    = $row['keterangan'];

            }else{
               $res[$i]['nama_komponen'] = $row['nama_komponen'];
               $res[$i]['harga']         = ''; 
               $res[$i]['jumlah']        = '1';
               $res[$i]['subtotal']      = $row['subtotal'];
               $res[$i]['subtotal_acc']  = $row['subtotal_acc'];
               $res[$i]['keterangan']    = $row['keterangan_pengeluaran'];
               $res[$i]['tanggal_pengeluaran'] = $row['tanggal_pengeluaran'];
               $res[$i]['is_acc']        = $row['is_acc'];
               $res[$i]['pembayaran']    = $row['metode_pengeluaran'];
               $res[$i]['rekening']      = $row['nama_bank']." - ( ".$row['no_rek']." ) a/n ".$row['atas_nama'];
            }
         }
         
         $i++;
      }

      return $res;
   }

   public function get_list_pembayaran($id){
      $this->db->where('transaksi_id', $id);
      return $this->db->get('transaksi_pembayaran')->result_array();
   }

   public function get_item_by($tipe, $transaksi_id, $component_id){
      $this->db->select('*, transaksi.id AS transaksi_id')
               ->where('transaksi.id', $transaksi_id);

      if($tipe == 'perolehan'){
         $this->db->select('transaksi_aset.id AS transaksi_aset_id')
                  ->join('transaksi_aset', 'transaksi_aset.transaksi_id = transaksi.id')
                  ->where('transaksi_aset.aset_id', $component_id);
      
      }else if($tipe == 'transaksi_beban'){
         $this->db->select('transaksi_beban.id AS transaksi_beban_id')
                  ->join('transaksi_beban','transaksi_beban.id = transaksi.id')
                  ->join('k_beban','k_beban.id = transaksi_beban.beban_id')
                  ->where('transaksi_beban.transaksi_id', $component_id);
      }

      return $this->db->get($this->table);
   }

   public function get_detail($key, $val = '', $tipe = ''){
      $this->db->select('transaksi.*');

      if(is_array($key)){
         $this->db->where($key);
      
      }else{
         $this->db->where($key, $val);
      }

      if(in_array($tipe, ['perolehan', 'pemeliharaan', 'perbaikan'])){
         $this->db->select('vendor.kode_vendor, vendor.nama_vendor')
                  ->join('hru_vendor vendor', 'vendor.id = transaksi.vendor_id');
      }

      $this->db->order_by("tanggal_transaksi", 'DESC');

      return $this->db->get($this->table);
   }

   public function last_data($tipe){
      $this->db->order_by('id','DESC')
               ->limit(1)
               ->where('tipe',$tipe);

      return $this->db->get($this->table)->row_array();
   }

   public function generate_code($tipe){
      $this->db->select('RIGHT(kode_transaksi,4) as kode', FALSE)
               ->order_by('kode_transaksi','DESC')
               ->where('tipe', $tipe)
               ->where('DATE(tanggal_transaksi)', date('Y-m-d'))
               ->limit(1);
      
      $query = $this->db->get('transaksi');  
      if($query->num_rows() <> 0){
         $data = $query->row();      
         $kode = intval($data->kode) + 1;    
      }else{
         $kode = 1;    
      }

      if($tipe == 'perolehan'){
         $prefix = "PRL";
      
      }else if($tipe == 'transaksi_beban'){
         $prefix = "TBBN";
      }else if($tipe == 'pinjaman'){
         $prefix = 'PNJM';
      }else if($tipe == 'pemeliharaan'){
         $prefix = 'PML';
      }else if($tipe == 'perbaikan'){
         $prefix = 'PRB';
      }else if($tipe == 'penarikan'){
         $prefix = 'PNRK';
      }else if($tipe == 'setoran_bank'){
         $prefix = 'SBNK';
      }else if($tipe == 'jurnal_master'){
         $prefix = 'JRNL';
      }else if($tipe == 'bop'){
         $prefix = 'BOP';
      } else if($tipe == 'setoran_modal'){
         $prefix = 'STRM';
      }

      $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
      $code = $prefix."-".date('dmY')."-".$kodemax;
      return $code;
   }

   public function last_jurnal(){
      $this->db->limit(1)->order_by('id', 'DESC');
      return $this->db->get('jurnal_master')->row_array();
   }

   public function generate_jurnal(){
      $this->db->select('RIGHT(kode_jurnal,4) as kode', FALSE)
               ->order_by('kode_jurnal','DESC')
               ->limit(1);
      
      $query = $this->db->get('jurnal_master');  
      if($query->num_rows() <> 0){
         $data = $query->row();      
         $kode = intval($data->kode) + 1;    
      }else{
         $kode = 1;    
      }

      $kodemax = str_pad($kode, 4, "0", STR_PAD_LEFT);
      $code = 'JRNL'."-".$kodemax;
      return $code;
   }

   public function insert($data){
      $this->db->insert($this->table, $data);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

   public function insert_pembayaran($data){
      $this->db->insert('transaksi_pembayaran', $data);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

   public function get_last_pembayaran($transaksi_id){
      $this->db->limit(1)
               ->where('transaksi_id', $transaksi_id);
      return $this->db->get('transaksi_pembayaran')->row_array();
   }

   public function insert_item($tipe, $data){
      $tbl = $tipe;

      if($tipe == 'perolehan'){
         $tbl = 'transaksi_aset';
      }else if($tipe == 'beban'){
         $tbl = 'transaksi_beban';
      }

      $this->db->insert($tbl, $data);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

   public function update($data, $id){
      $this->db->where('id', $id)
               ->update($this->table, $data);
      return true;
   }

   public function update_item($tipe, $data, $id){
      $this->db->where('id', $id);

      if($tipe == 'perolehan'){
         $tbl = 'transaksi_aset';
      }else if($tipe == 'transaksi_beban'){
         $tbl = 'transaksi_beban';
      }

      $this->db->update($tbl, $data);

      return true;
   }

   public function delete_item($tipe, $id){
      $this->db->where('id', $id);

      if($tipe == 'perolehan'){
         $this->db->delete('transaksi_aset');

      }else if($tipe == 'transaksi_beban'){
         $this->db->delete('transaksi_beban');
      
      }else if($tipe == 'pemeliharaan' || $tipe == 'perbaikan'){
         $this->db->delete('transaksi_perawatan');
      }

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }

   public function get_total_price($tipe ,$transaksi_id){
      $this->db->where('transaksi.id', $transaksi_id);

      if($tipe == 'perolehan'){
         $this->db->select('SUM(transaksi_aset.subtotal) AS total')
                  ->join('transaksi_aset', 'transaksi_aset.transaksi_id = transaksi.id');
      
      }else if($tipe == 'transaksi_beban'){
         $this->db->select('SUM(transaksi_beban.subtotal) AS total')
                  ->join('transaksi_beban', 'transaksi_beban.transaksi_id = transaksi.id');

      }else if($tipe == 'pemeliharaan' || $tipe == 'perbaikan'){
         $this->db->select('SUM(transaksi_perawatan.harga_perawatan) AS total')
                  ->join('transaksi_perawatan', 'transaksi_perawatan.transaksi_id = transaksi.id');
      }

      return $this->db->get('transaksi')->row_array()['total'];
   }

   public function get_nominal_pinjaman($karyawan_id){
      $this->db->select('SUM(sisa_bayar) AS nominal')
               ->where('tipe', 'pinjaman')
               ->where('level', '3')
               ->where('status', 'Belum Lunas')
               ->where('pegawai_id', $karyawan_id);

      return $this->db->get('transaksi')->row_array()['nominal'];
   }

   public function get_total_list_item($tipe, $id){
      $this->db->select('*, transaksi.id AS transaksi_id')
               ->where('transaksi.id', $id);

      if($tipe == 'perolehan'){
         $this->db->select('transaksi_aset.id AS transaksi_aset_id, a_aset.id AS aset_id')
                  ->join('transaksi_aset','transaksi_aset.transaksi_id = transaksi.id')
                  ->join('a_aset','a_aset.id = transaksi_aset.aset_id');

      }else if($tipe == 'setoran' || $tipe == 'penarikan'){
         $this->db->select('total_transaksi AS subtotal, 1 AS jumlah')
                  ->join('k_pemilik', 'k_pemilik.id = transaksi.pemilik_id');
      
      }else if($tipe == 'bop'){
         $this->db->select('SUM(transaksi_bop.subtotal) AS total')
                  ->join('transaksi_bop', 'transaksi_bop.transaksi_id = transaksi.id')
                  ->group_start()
                     ->where('is_acc', '1')
                     ->or_where('is_acc', null)
                  ->group_end();
      
      }

      $nominal = $this->db->get($this->table)->row_array()['total'];

      if($nominal == ''){
         return 0;
      }

      return $nominal;
   }


   public function insert_bop_keluar($data){
      $this->db->insert('transaksi_bop', $data);

      if($this->db->affected_rows() > 0){
         return true;
      }
      return false;
   }
}
