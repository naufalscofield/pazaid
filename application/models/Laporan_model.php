<?php
 
Class Laporan_model extends CI_Model{
    
   public function get_jurnal($req = array()){

      $waktu = ''; $coa_id = '';

      if(isset($req['start'])){
         $this->db->where('tgl_jurnal >=', $req['start'])
                  ->where('tgl_jurnal <=', $req['end']);
      
      }

      if(isset($req['bulan'])){
         $this->db->where('MONTH(tgl_jurnal)', $req['bulan'])
                  ->where('YEAR(tgl_jurnal)', $req['tahun']);
         $waktu = $req['tahun'].'-'.$req['bulan'].'-01';
      }

      if(isset($req['coa_id'])){
         if($req['coa_id'] != 'all'){
            $this->db->where('coa_id', $req['coa_id']);
            $coa_id = $req['coa_id'];
         }
      }

      // $this->db->select('*, jurnal.id AS jurnal_id')
      //          ->join('coa','coa.id = jurnal.coa_id')
      //          ->join('jurnal_master', 'jurnal_master.id = jurnal.jurnal_master_id')
      //          ->where('is_post', '1')
      //          ->order_by('jurnal.id', 'ASC');
      $this->db->select('*, jurnal.id AS jurnal_id')
               ->join('coa','coa.id = jurnal.coa_id')
               // ->join('jurnal_master', 'jurnal_master.id = jurnal.jurnal_master_id')
               // ->where('is_post', '1')
               ->order_by('jurnal.id', 'ASC');

      return [
         'list' => $this->db->get('jurnal')->result_array(),
         'saldo_awal' => $this->get_saldo_awal($waktu, $coa_id)
      ];
   }

   public function get_saldo($min_date, $coa_id){
       return $this->get_saldo_awal($min_date, $coa_id);
   }

   public function get_ajp($req){
      $result = [];
      $item   = [];

      $beban = $this->db->like('kode_coa', '5', 'after')->get('coa')->result_array();
      $time = strtotime($req['tahun'].'-'.$req['bulan'].'-01');
      $final = date("Y-m-d", strtotime("+1 month", $time));

      $beban_saldo = 0;
      foreach ($beban as $row){
         $saldo = $this->get_saldo_awal($final, $row['id']);
         if($saldo == ''){
            $saldo = 0;
         }

         $beban_saldo += $saldo;
         $item[] = [
            'kode_coa' => $row['kode_coa'],
            'nama_coa' => $row['nama_coa'],
            'saldo'    => $saldo
         ];
      }

      $result[] = [
         'nama'  => 'Ikhtisar Laba Rugi',
         'saldo' => $beban_saldo,
         'item'  => $item
      ];

      $pendapatan = $this->db->select('*, SUM(total_bayar) AS total_pendapatan')
                        ->where('jenis', 'masuk')
                        ->where('level', '3')
                        ->where('MONTH(tanggal_transaksi)', $req['bulan'])
                        ->where('YEAR(tanggal_transaksi)', $req['tahun'])
                        ->get('transaksi')->row_array()['total_pendapatan'];

      $pengeluaran = $this->db->select('*, SUM(total_bayar) AS total_pengeluaran')
                        ->where('jenis', 'keluar')
                        ->where('level', '3')
                        ->where('MONTH(tanggal_transaksi)', $req['bulan'])
                        ->where('YEAR(tanggal_transaksi)', $req['tahun'])
                           ->or_group_start()
                              ->where('jenis', 'keluar')
                              ->where('level', '3')
                              ->where('level_vendor', '3')
                           ->group_end()
                        ->get('transaksi')->row_array()['total_pengeluaran'];
      $saldo_akhir = $pendapatan - $pengeluaran;
      if($saldo_akhir == ''){
         $saldo_akhir = 0;
      }

      if($saldo_akhir >= 0){
         $title_1 = 'Saldo Laba';
         $title_2 = 'Modal';
      }else{
         $title_1 = 'Modal';
         $title_2 = 'Saldo Rugi';
      }

      $item   = [];
      $item[] = [
         'kode_coa' => '',
         'nama_coa' => $title_2,
         'saldo'    => $saldo_akhir
      ];

      $result[] = [
         'nama'  => $title_1,
         'saldo' => $saldo_akhir,
         'item'  => $item
      ];

      $prive = $this->db->select('SUM(total_transaksi) AS total')
                        ->where('tipe', 'penarikan')
                        ->where('MONTH(tanggal_transaksi)', $req['bulan'])
                        ->where('YEAR(tanggal_transaksi)', $req['tahun'])
                        ->get('transaksi')->row_array()['total'];
      if($prive == ''){
         $prive = 0;
      }
      $item   = [];
      $item[] = [
         'kode_coa' => '',
         'nama_coa' => 'Prive',
         'saldo'    => $prive
      ];

      $result[] = [
         'nama'  => 'Modal',
         'saldo' => $prive,
         'item'  => $item
      ];

      $jasa = $this->db->like('kode_coa', '4', 'after')->get('coa')->result_array();
      $nominal_jasa = 0;
      foreach ($beban as $row){
         $saldo = $this->get_saldo_awal($final, $row['id']);
         if($saldo == ''){
            $saldo = 0;
         }

         $nominal_jasa += $saldo;
      }

      $item   = [];
      $item[] = [
         'kode_coa' => '',
         'nama_coa' => 'Ikhtisar Laba / Rugi',
         'saldo'    => $nominal_jasa
      ];

      $result[] = [
         'nama'  => 'Pendapatan Jasa',
         'saldo' => $nominal_jasa,
         'item'  => $item
      ];
      return $result;
   }

      private function get_saldo_awal($waktu, $coa_id){
         $debit  = $this->_saldo('debit', $waktu, $coa_id);
         $kredit = $this->_saldo('kredit', $waktu, $coa_id);

         return $debit - $kredit;
      }

      private function _saldo($tipe, $waktu, $coa_id){
         $this->db->select('SUM(nominal) AS nominal')
                  ->join('jurnal_master', 'jurnal_master.id = jurnal.jurnal_master_id')
                  ->where('is_post', '1')
                  ->where('posisi', $tipe)
                  ->where('coa_id', $coa_id)
                  ->where('tgl_jurnal <', $waktu);

         return $this->db->get('jurnal')->row_array()['nominal'];
      }

      // public function getJoinCash()
      // {
      //    $this->db->join('transaksi_pendaftaran a')
      // }

}
