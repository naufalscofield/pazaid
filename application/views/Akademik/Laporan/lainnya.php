<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Daftar Kartu Piutang</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item"><a href="#">Kartu Piutang</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Lainnya</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Kartu Piutang Lainnya</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>Tanggal Transaksi<th>
									<th>NIS<th>
									<th>Nama Siswa<th>
									<th class="text-center"><i class="fa fa-cog"></i></th>
									<!-- <th>Jumlah Jam<th>
									<th>Kode Pegawai<th>
									<th>Total Tagihan<th>
									<th>Jenis Tambahan<th>
									<th>Jenis Pembayaran<th>
									<th>No Rekening<th>
									<th>Status<th> -->
								</tr>
							</thead>
							<tbody>
                                <?php $n = -1; $totalTagihan = 0;
                                foreach ($data as $row) { $n++; ?>

                                <tr>
                                    <td style="width: 5%"> <?php echo $n+1 ?> </td>
                                    <td> <?php echo $row['tanggal_transaksi'] ?> </td>
                                    <td></td>
                                    <td> <?php echo $row['nis'] ?> </td>
                                    <td></td>
                                    <td> <?php echo $row['nama_siswa'] ?> </td>
                                    <td></td>
                                    <td class="text-center">
                                    <button data-id="<?= $row['id_siswa'];?>" type="button" id="btnRincianOrtu" class="btn btn-warning"><i class="fa fa-circle-o"></i></button>
                                    </td>
                                    <!-- <td></td>
                                    <td> <?php echo $row['jumlah_jam'] ?> Jam </td>
                                    <td></td>
                                    <td> <?php echo $row['kode_pegawai'] ?> </td>
                                    <td></td>
                                    <td> <?php echo format_rp($row['total_tagihan']) ?> </td>
                                    <td></td>
                                    <td> <?php echo $row['jenis_tambahan'] ?> </td>
                                    <td></td>
                                    <td> <?php if($row['jenis_pembayaran'] == "transfer"){ ?> 
                                        <t class="badge badge-success">Transfer</t>
                                    <?php }elseif($row['jenis_pembayaran'] == 'cash'){ ?>
                                        <t class="badge badge-success">Cash</t>
                                    <?php } ?>
                                    </td>
                                    <td></td>
                                    <td> <?php echo $row['kode_rek'] ?> </td>
                                    <td></td>
                                    <td> <?php if($row['status_lainnya'] == "Lunas"){ ?> 
                                        <t class="badge badge-success"><?php echo $row['status_lainnya'] ?></t>
                                    <?php }else{ ?>
                                        <t class="badge badge-danger"><?php echo $row['status_lainnya'] ?></t>
                                    <?php } ?>
                                    </td>
                                    <td></td> -->
                                </tr>
                                <?php } ?>
							</tbody>
                                <!-- <tr>
                                    <td style="width: 5%"><b>Total</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-center"><b><?php echo format_rp($totalTagihan) ?> </b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr> -->
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="modalRincianOrtu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Orang Tua Siswa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h6><b>IBU</b></h6><br>
                                <div id="ibu">
                                    <div class="form-group">
                                    <b><label for="name">Nama Lengkap</label></b>
                                    <p id="nama_lengkap_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="lname">Tempat Tanggal Lahir</label></b>
                                    <p id="ttl_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Agama</label></b>
                                    <p id="agama_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">No Telp</label></b>
                                    <p id="no_telp_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Pendidikan Terakhir</label></b>
                                    <p id="pendidikan_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Pekerjaan</label></b>
                                    <p id="pekerjaan_ibu"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h6><b>BAPAK</b></h6><br>
                                <div id="ibu">
                                    <div class="form-group">
                                    <b><label for="name">Nama Lengkap</label></b>
                                    <p id="nama_lengkap_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="lname">Tempat Tanggal Lahir</label></b>
                                    <p id="ttl_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Agama</label></b>
                                    <p id="agama_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">No Telp</label></b>
                                    <p id="no_telp_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Pendidikan Terakhir</label></b>
                                    <p id="pendidikan_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Pekerjaan</label></b>
                                    <p id="pekerjaan_bapak"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).on('click', '#btnRincianOrtu', function() {
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    var idSiswa = $(this).data('id')
    $('#modalRincianOrtu').modal('show')

    $.get(baseUrl+"/akademik/laporan/ajaxLainnya/"+idSiswa, function(data, status){
        var element = JSON.parse(data)    
        console.log(element)        
        $('#nama_lengkap_ibu').text(element.ot_ib_nama_lengkap)
        $('#ttl_ibu').text(element.ot_ib_tempat_lahir+', '+element.ot_ib_tanggal_lahir)
        $('#no_telp_ibu').text(element.ot_ib_no_telp)
        $('#agama_ibu').text(element.ot_ib_agama)
        $('#pekerjaan_ibu').text(element.ot_ib_pekerjaan)
        $('#pendidikan_ibu').text(element.ot_ib_pendidikan)

        $('#nama_lengkap_bapak').text(element.ot_bpk_nama_lengkap)
        $('#ttl_bapak').text(element.ot_bpk_tempat_lahir+', '+element.ot_bpk_tanggal_lahir)
        $('#no_telp_bapak').text(element.ot_bpk_no_telp)
        $('#agama_bapak').text(element.ot_bpk_agama)
        $('#pekerjaan_bapak').text(element.ot_bpk_pekerjaan)
        $('#pendidikan_bapak').text(element.ot_bpk_pendidikan)
    });

})


 function edit( kode, tgl){

  $('#e_kode').val(kode);
  $('#e_tgl').val(tgl);


  $('#modalEditKTG').modal('show'); 
}
</script>

