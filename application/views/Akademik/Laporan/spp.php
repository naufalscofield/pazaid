<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Daftar Kartu Piutang</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item"><a href="#">Kartu Piutang</a></li>
                    <li class="breadcrumb-item active" aria-current="page">SPP</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Kartu Piutang SPP</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>Tanggal Transaksi<th>
									<th>NIS<th>
									<th>Nama Siswa<th>
									<th>Kelas<th>
									<th>Biaya SPP<th>
									<th>Jenis Pembayaran<th>
									<th>No Rekening<th>
									<th>Status<th>
								</tr>
							</thead>
							<tbody>
                                <?php $n = -1; $totalSpp = 0;
                                foreach ($data as $row) { $n++; ?>

                                <tr>
                                    <td style="width: 5%"> <?php echo $n+1 ?> </td>
                                    <td> <?php echo $row['tanggal_transaksi'] ?> </td>
                                    <td></td>
                                    <td> <?php echo $row['nis'] ?> </td>
                                    <td></td>
                                    <td> <?php echo $row['nama_siswa'] ?> </td>
                                    <td></td>
                                    <td> <?php echo $row['nama_kelas'] ?> </td>
                                    <td></td>
                                    <td> <?php echo format_rp($row['biaya_spp']) ?> </td>
                                    <td></td>
                                    <td> <?php if($row['jenis_pembayaran'] == "transfer"){ ?> 
                                        <t class="badge badge-success">Transfer</t>
                                    <?php }elseif($row['jenis_pembayaran'] == 'cash'){ ?>
                                        <t class="badge badge-success">Cash</t>
                                    <?php } ?>
                                    </td>
                                    <td></td>
                                    <td> <?php echo $row['kode_rek'] ?> </td>
                                    <td></td>
                                    <td> <?php if($row['status_spp'] == "Sudah Dibayar"){ ?> 
                                        <t class="badge badge-success"><?php echo $row['status_spp'] ?></t>
                                    <?php }else{ ?>
                                        <t class="badge badge-danger"><?php echo $row['status_spp'] ?></t>
                                    <?php } ?>
                                    </td>
                                    <td></td>
                                </tr>
                                    <?php $totalSpp   += $row['biaya_spp'] ?>
                                <?php } ?>
							</tbody>
                                <tr>
                                    <td style="width: 5%"><b>Total</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-center"><b><?php echo format_rp($totalSpp) ?> </b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>

<script type="text/javascript">
 function edit( kode, tgl){

  $('#e_kode').val(kode);
  $('#e_tgl').val(tgl);


  $('#modalEditKTG').modal('show'); 
}
</script>

