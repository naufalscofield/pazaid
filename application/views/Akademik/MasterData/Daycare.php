<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Input Data Daycare</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Bagian Akademik</a></li>
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Daycare</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>

<div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">


    <div class="row">
        <div class="col-md-12">

        <div class="card">
            <div class="card-body">

            <div class="row">
                <div class="col-md-12">
                <br>
                <table class="table">
                    <form action="<?=base_url();?>akademik/masterdata/daycare/store" method="POST">
                        <tr>
                        <th>Nama Lengkap</th>
                        <td id="nama_lengkap">
                        <input required type="text" class="form-control" name="nama_lengkap">
                        </td>
                        
                        <th>Nama Orang Tua / Wali</th>
                        <td id="nama_orang_tua">
                        <input required type="text" class="form-control" name="nama_orang_tua">
                        </td>
                        </tr>

                        <tr>
                        <th>Tempat Lahir</th>
                        <td id="tempat_lahir">
                        <input required type="text" class="form-control" name="tempat_lahir">
                        </td>
                        
                        <th>Tanggal Lahir</th>
                        <td id="tanggal_lahir">
                        <input required type="date" class="form-control" name="tanggal_lahir">
                        </td>
                        </tr>
                        
                        <tr>
                        <th>No Telp Orang Tua / Wali</th>
                        <td id="no_telp">
                        <input required type="number" class="form-control" name="no_telp">
                        </td>
                        
                        <th>Jenis Daycare</th>
                        <td id="jenis_daycare">
                        <select required name="jenis_daycare" id="" class="form-control">
                            <option value="">Pilih Jenis Daycare</option>
                            <option value="fullday">Fullday - Rp.125.000</option>
                            <option value="halfday">Halfday - Rp.75.000</option>
                        </select>
                        </td>
                        </tr>

                    <!-- <div class="form-group"> -->

                </table>
                        <center>
                         <input type="submit" class="btn btn-primary" value="Input Data">
                        </center>
                    </form>
                    
                </div>
            </div>
        </div>

    </div>

<div class="modal" id="modalBulan" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Pilih Bulan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url();?>keuangan/pembayaranspp" method="POST">
            <div class="form-group">
            <b><label for="lname">Tahun Ajaran</label></b><br>
            <input readonly type="text" id="tahun_ajaran" name="tahun_ajaran" class="form-control">
            <input readonly type="hidden" id="id_tahun_ajaran" name="id_tahun_ajaran" class="form-control">
            </div>

            <div class="form-group">
            <b><label for="lname">Bulan Ke</label></b><br>
            <select name="bulan_ke" id="" class="form-control">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
            </select>
            </div>        

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">OK</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>

    </div>
  </div>
</div>

    
</main>

<div class="modal fade" id="modalBayar" tabindex="-1" role="dialog" aria-lableledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Bayar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="form">
                                    <form action="<?= base_url();?>/akademik/transaksi/siswacalon/bayar" method="POST">
                                        <input type="hidden" id="id_siswa" name="id_siswa">

                                        <div class="form-group">
                                        <b><label for="name">Biaya Pendaftaran</label></b><br>
                                        Rp. <label id="biaya_pendaftaran"></label>
                                        <input type="hidden" readonly name="biaya_pendaftaran" id="biaya_pendaftaran_input"></input>
                                        </div>

                                        <div class="form-group">
                                        <b><label for="lname">Biaya Seragam</label>&nbsp<label id="bayarJK"></label></b><br>
                                        Rp. <label id="biaya_seragam"></label>
                                        <input id="biaya_seragam_input" type="hidden" readonly name="biaya_seragam"></input>
                                        </div>
                                        
                                        <div class="form-group">
                                        <b><label for="name">Biaya DPP (Pembangunan)</label></b><br>
                                        Rp. <label id="biaya_dpp"></label>
                                        <!-- <input type="hidden" readonly name="biaya_dpp" id="biaya_dpp_input"></input>&nbsp &nbsp<input name="cb_dpp" value="true" id="cbDpp"type="checkbox">Cicil -->
                                        <input type="hidden" readonly name="biaya_dpp" id="biaya_dpp_input"></input>
                                        <input placeholder="Nominal cicilan awal" type="number"class="form-control" name="biaya_dpp_cicil" id="biaya_dpp_cicil">
                                        </div>
                                        
                                        <div class="form-group">
                                        <b><label for="name">Biaya DSK (Semester)</label></b><br>
                                        Rp. <label id="biaya_dsk"></label>
                                        <!-- <input type="hidden" readonly name="biaya_dsk" id="biaya_dsk_input"></input>&nbsp &nbsp<input value="true" id="cbDsk"type="checkbox">Cicil -->
                                        <input type="hidden" readonly name="biaya_dsk" id="biaya_dsk_input"></input>
                                        <input placeholder="Nominal cicilan awal" type="number" class="form-control" name="biaya_dsk_cicil" id="biaya_dsk_cicil">
                                        </div>
                                        
                                        <div class="form-group">
                                        <b><label for="lname">Tahun Ajaran</label></b><br>
                                        <input readonly type="text" id="tahun_ajaran" name="tahun_ajaran" class="form-control">
                                        <input readonly type="hidden" id="id_tahun_ajaran" name="id_tahun_ajaran" class="form-control">
                                        </div>

                                        <div class="form-group">
                                        <b><label for="lname">Jenis Pembayaran</label></b><br>
                                        <select required class="form-control" name="jenis_pembayaran" id="jenis_pembayaran">
                                            <option value="">--Pilih Jenis Pembayaran--</option>
                                            <option value="cash">Cash</option>
                                            <option value="transfer">Transfer</option>
                                        </select>
                                        </div>

                                        <div class="form-group" id="divRekening">
                                        <b><label for="lname">Rekening</label></b><br>
                                        <select disabled class="form-control" name="rekening" id="rekening">
                                            <option value="">--Pilih Rekening--</option>
                                        </select>
                                        </div>

                                        <div class="form-group" id="">
                                        <b><label for="lname">No Rekening</label></b><br>
                                        <input name="no_rek" type="number" readonly class="form-control" id="no_rek">
                                        </div>
                                        
                                        <hr>

                                        <div class="form-group" id="">
                                        <b><h4 for="lname" style="color:red">Total Tagihan:  Rp. <label id="total_tagihan"></label></h4></b><br>
                                        <input id="total_tagihan_input" type="hidden" readonly name="total_tagihan"></input>

                                        <b><h4 for="lname" style="color:blue">Total Pembayaran:  Rp. <label id="total_pembayaran"></label></h4></b><br>
                                        <input id="total_pembayaran_input" type="hidden" readonly name="total_pembayaran"></input>
                                        </div>
                                        
                                        <hr>
                                        <div class="form-group" id="">
                                        <b><h4 for="lname" style="color:green">Sisa Tagihan:  Rp. <label id="sisa_tagihan"></label></h4></b><br>
                                        <input id="sisa_tagihan_input" type="hidden" readonly name="sisa_tagihan"></input>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">OK</button>
      </div>
      </form>

    </div>
  </div>
</div>
<!-- end::main content -->

<script type="text/javascript" src="<?=base_url();?>assets/js/autonumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    $.get(baseUrl+"/akademik/masterdata/ajaxTahunAjaran", function(data, status){
        console.log(data)
        var element = JSON.parse(data)
        $('#tahun_ajaran').val(element.kode_tahun)
        $('#id_tahun_ajaran').val(element.id_tahun)
    });
    $(document).on('click', '#btn_cari', function() {
        let val = $('#nis').val()
        const getUrl = window.location;
        const baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
            $.get(baseUrl+"/keuangan/pembayaranspp/ajaxTagihanSpp/"+val, function(data, status){
                let element = JSON.parse(data)
                console.log(element.siswa)

                //kode                
                $('#o_nis').text(element.siswa.nis)

                //siswa
                $('#nama_lengkap').text(element.siswa.nama_s
                <input type="text" class="form-control" name="nama_lengkap">
                iswa)
                $('#nama_orang_tua').text(element.siswa.kode_kelas)
                $('#o_tahun_ajaran').text(element.siswa.kode_tahun)
                $('#o_bulan_ke').text(element.tagihan.bulan_ke)
                $('#o_biaya_spp').text(element.tagihan.biaya_spp)
                $('#id_spp').val(element.tagihan.id)
                $("#o_biaya_spp").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });

            })
    
    })

    $(document).on('change', '#jenis_pembayaran', function() {
    let val = $(this).val()
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    if (val == 'transfer')
    {
        $('#rekening').prop("disabled", false)
        $('#no_rek').prop("disabled", false)
        $.get(baseUrl+"/keuangan/rekening/ajaxRekening", function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            $('#rekening').empty()
            $('<option>').val("").text("--Pilih Rekening--").appendTo('#rekening');
            $.each(element, function(index, el){
                $('<option>').val(el.kode_rek).text(el.nama_bank).appendTo('#rekening');
            })
        })
    } else {
        $('#rekening').prop("disabled", true)
        $('#no_rek').prop("disabled", true)
        $('#rekening').empty()
        $('#no_rek').val("")

    }
});

$(document).on('change', '#rekening', function() {
    let val = $(this).val()
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        $.get(baseUrl+"/keuangan/rekening/ajaxNoRekening/"+val, function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            $('#no_rek').empty()
            $('#no_rek').val(element.no_rek)
        })
});

})

</script>

