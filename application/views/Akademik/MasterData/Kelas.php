<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Daftar Kelas</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Kelas</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Kelas</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<button data-toggle="modal" data-target="#modalTambahKTG" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> &nbsp; Tambah Kelas </button>
						<br><br>

					<div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>Kode Kelas</th>
									<th>Nama Kelas<th>
									<th>Level Kelas<th>
									<th>Status Kelas<th>
									<th class="text-center"><i class="fa fa-cog"></i></th>
								</tr>
							</thead>
							<tbody>
                                <?php $n = -1;
                                foreach ($list as $row) { $n++; ?>

                                <tr>
                                    <td> <?php echo $n+1 ?> </td>
                                    <td> <?php echo $row['kode_kelas'] ?> </td>
                                    <td> <?php echo $row['nama_kelas'] ?> </td>
                                    <td></td>
                                    <td> <?php echo $row['level_kelas'] ?> </td>
                                    <td></td>
                                    <td> <?php echo $row['status'] ?> </td>
                                    <td></td>
                                    <td class="text-center">
                                        <!-- <a href="javascript:void(0)" data-toggle="tooltip" title="Ubah" class="btn btn-warning btn-sm"
                                                onclick="
                                                edit(		                                              
                                                )">
                                            <i class="fa fa-edit"></i>
                                        </a> -->
                                        &nbsp;
                                        <a onclick="return confirm('Hapus data ini ?')" href="<?php echo site_url('akademik/tahunajaran/hapus/'.$row['id_kelas']) ?>" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>

                                <?php } ?>
							</tbody>
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->

<form action="<?php echo site_url('akademik/masterdata/kelas/add') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalTambahKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Kelas</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <div class="modal-body">
                <div class="form-group">
                   <label>Nama Kelas</label>
                   <input autocomplete="off" type="text" name="nama_kelas" id="nama_kelas" class="form-control" placeholder="" required/>
                </div>
                <div class="form-group">
                   <label>Level Kelas</label>
                   <select name="level_kelas" id="" class='form-control' required>
                        <option value="">Pilih Kelas</option>
                        <option value="Bayi">Bayi</option>
                        <option value="Playgroup">Playgroup</option>
                        <option value="TK">TK</option>
                   </select>
                </div>
                <div class="form-group">
                   <label>Status Kelas</label>
                   <select name="status" id="" class='form-control' required>
                        <option value="">Status Kelas</option>
                        <option value="Fullday">Fullday</option>
                        <option value="Halfday">Halfday</option>
                   </select>
                </div>
             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>

   <form action="<?php echo site_url('update_bank') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalEditKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Ubah bank</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>


             <div class="modal-body">
                <div class="form-group">
                   <label>Kode Tahun</label>
                   <input type="text" name="kode_tahun" id="e_kode" class="form-control" placeholder="Kode Aktiva..." value="" readonly="" autocomplete="off" required />
                </div>
                <div class="form-group">
                   <label>Dibuat Pada</label>
                   <input autocomplete="off" type="text" name="created_at" id="e_tgl" class="form-control" placeholder="Nama Aktiva..." required/>
                </div>
             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i> Ubah</button>
             </div>

           </div>
        </div>
     </div>
   </form>

<script type="text/javascript">
function edit( kode, tgl){

  $('#e_kode').val(kode);
  $('#e_tgl').val(tgl);


  $('#modalEditKTG').modal('show'); 
}
</script>

