<style>
.paging_full_numbers a.paginate_button {
    color: #000 !important;
}
.paging_full_numbers a.paginate_active {
    color: #000 !important;
}
</style>
<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Siswa Tetap</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item"><a href="#">Siswa</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Siswa Tetap</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Siswa Tetap</h5>
                    <form action="<?= base_url();?>akademik/transaksi/siswatetap/bayarspp" method="POST">
                        <button type="submit" class="btn btn-success">Generate Tagihan SPP</button>
                    </form>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<div class="table-responsive dataTables_paginate paging_full_numbers" >
						<table style="width:100%" id="example" class="display nowrap" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>NIS Siswa</th>
									<th>Nama Siswa</th>
									<th>Panggilan Siswa</th>
									<th>Tempat Lahir</th>
									<th>Tanggal Lahir</th>
									<th>Usia</th>
									<th>Jenis Kelamin</th>
									<th>Agama</th>
									<th>Jumlah Saudara</th>
									<th>Anak Ke-</th>
									<th>Alamat</th>
									<th>Status Siswa</th>
									<th>Status Bayar</th>
									<th>Nama Kelas</th>
									<th>Level Kelas</th>
									<th>Tahun Ajaran</th>
									<th class="text-center"><i class="fa fa-cog"></i></th>
								</tr>
							</thead>
							<tbody>
                                <?php $n = 1; 
                                    foreach($data as $row){ 
                                        if($row['nis'] != 0 && $row['status_siswa'] != "Undur Diri" && $row['status_bayar'] != "Belum Bayar"){?>
                                <tr>
                                    <td><?php echo $n++?>.</td>
                                    <td><?php echo $row['nis']?></td>
                                    <td><?php echo $row['nama_siswa']?></td>
                                    <td><?php echo $row['panggilan_siswa']?></td>
                                    <td><?php echo $row['tempat_lahir']?></td>
                                    <td><?php echo date("d-M-Y", strtotime($row['tanggal_lahir']))?></td>
                                    <td><?php echo $row['umur']?> Tahun</td>
                                    <td><?php echo $row['jenis_kelamin']?></td>
                                    <td><?php echo $row['agama']?></td>
                                    <td><?php echo $row['jumlah_saudara']?> Bersaudara</td>
                                    <td><?php echo $row['anak_ke']?></td>
                                    <td><?php echo $row['alamat']?></td>
                                    <td><?php echo $row['status_ortu']?></td>
                                    <td><?php if($row['status_bayar'] == "Belum Bayar"){?>
                                        <t class="badge badge-danger"><?php echo $row['status_bayar'] ?></t>
                                    <?php }else{ ?>
                                        <t class="badge badge-success"><?php echo $row['status_bayar']  ?></t>
                                    <?php } ?>
                                    </td>
                                    <td><?php echo $row['nama_kelas']?></td>
                                    <td><?php echo $row['level_kelas']?></td>
                                    <td><?php echo date("d-M-Y", strtotime($row['created_at']))?></td>
                                    <form action="<?php echo site_url('akademik/masterdata/siswatetap/deleteU/'.$row['id_siswa']) ?>" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="status_siswa" value="Undur Diri">
                                    <input type="hidden" name="id_siswa" value="<?php echo $row['id_siswa']?>">
                                    <td class="text-center">
                                        <!-- <a href="javascript:void(0)" data-toggle="tooltip" title="Ubah" class="btn btn-warning btn-sm"
                                                onclick="
                                                edit(		                                              
                                                )">
                                            <i class="fa fa-edit"></i>
                                        </a> -->
                                        <button data-id="<?= $row['id_siswa'];?>" type="button" id="btnRincianOrtu" class="btn btn-warning"><i class="fa fa-user"></i> Rincian Orang Tua</button>
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>           
                                    </form>
                                    <!-- <button data-id="<?= $row['id_siswa'];?>" type="button" id="btnBayar" class="btn btn-success"><i class="fa fa-money"></i> Bayar SPP</button> -->
                                    </td>
                                </tr>    
                                    <?php }?>                  
                                <?php } ?>
							</tbody>
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->

<div class="modal fade" id="modalRincianOrtu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Orang Tua Siswa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h6><b>IBU</b></h6><br>
                                <div id="ibu">
                                    <div class="form-group">
                                    <b><label for="name">Nama Lengkap</label></b>
                                    <p id="nama_lengkap_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="lname">Tempat Tanggal Lahir</label></b>
                                    <p id="ttl_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Agama</label></b>
                                    <p id="agama_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">No Telp</label></b>
                                    <p id="no_telp_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Pendidikan Terakhir</label></b>
                                    <p id="pendidikan_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Pekerjaan</label></b>
                                    <p id="pekerjaan_ibu"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h6><b>BAPAK</b></h6><br>
                                <div id="ibu">
                                    <div class="form-group">
                                    <b><label for="name">Nama Lengkap</label></b>
                                    <p id="nama_lengkap_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="lname">Tempat Tanggal Lahir</label></b>
                                    <p id="ttl_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Agama</label></b>
                                    <p id="agama_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">No Telp</label></b>
                                    <p id="no_telp_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Pendidikan Terakhir</label></b>
                                    <p id="pendidikan_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Pekerjaan</label></b>
                                    <p id="pekerjaan_bapak"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


   <div class="modal fade" id="modalBayar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Bayar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="form">
                                    <form action="<?= base_url();?>akademik/transaksi/siswatetap/bayarspp" method="POST">
                                        <input type="hidden" id="id_siswa" name="id_siswa">

                                        <div class="form-group">
                                        <b><label for="name">Biaya SPP</label></b><br>
                                        Rp. <label id="biaya_spp"></label>
                                        <input type="hidden" readonly name="biaya_spp" id="biaya_spp_input"></input>
                                        </div>

                                        <div class="form-group">
                                        <b><label for="lname">Bulan Ke</label></b><br>
                                        <label id="bulan_ke"></label>
                                        <input id="bulan_ke_input" type="hidden" readonly name="bulan_ke"></input>
                                        </div>

                                        <div class="form-group">
                                        <b><label for="lname">Tahun Ajaran</label></b><br>
                                        <input readonly type="text" id="tahun_ajaran" name="tahun_ajaran" class="form-control">
                                        <input readonly type="hidden" id="id_tahun_ajaran" name="id_tahun_ajaran" class="form-control">
                                        </div>
                                        
                                        <div class="form-group">
                                        <b><label for="lname">Jenis Pembayaran</label></b><br>
                                        <select required class="form-control" name="jenis_pembayaran" id="jenis_pembayaran">
                                            <option value="">--Pilih Jenis Pembayaran--</option>
                                            <option value="cash">Cash</option>
                                            <option value="transfer">Transfer</option>
                                        </select>
                                        </div>

                                        <div class="form-group" id="divRekening">
                                        <b><label for="lname">Rekening</label></b><br>
                                        <select  class="form-control" name="rekening" id="rekening">
                                            <option value="">--Pilih Rekening--</option>
                                        </select>
                                        </div>

                                        <div class="form-group" id="">
                                        <b><label for="lname">No Rekening</label></b><br>
                                        <input name="no_rek" readonly type="number"  class="form-control" id="no_rek">
                                        </div>
                                        
                                        <hr>

                                        <div class="form-group" id="">
                                        <b><h4 for="lname" style="color:red">Total Tagihan:  Rp. <label id="total_tagihan"></label></h4></b><br>
                                        <input id="total_tagihan_input" type="hidden" readonly name="total_tagihan"></input>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">OK</button>
      </div>
      </form>

    </div>
  </div>
</div>
<script type="text/javascript" src="<?=base_url();?>assets/js/autonumeric/autoNumeric.js"></script>

<script type="text/javascript">

$(document).ready(function(){
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                className: 'btn btn-primary',
                extend: 'copy',
            },
            {
                className: 'btn btn-primary',
                extend: 'print',
            },
            {
                className: 'btn btn-primary',
                extend: 'pdf',
                title: 'Data Siswa',
                orientation: 'landscape',
                pageSize: 'A4',
                exportOptions: {
                    columns: ':not(:last-child)',
                },
            },
            {
                className: 'btn btn-primary',
                extend: 'excel',
                title: 'Data Siswa',
                orientation: 'landscape',
                pageSize: 'A4',
                exportOptions: {
                    columns: ':not(:last-child)',
                },
            },
            {
                className: 'btn btn-primary',
                extend: 'csv',
                title: 'Data Siswa',
                orientation: 'landscape',
                pageSize: 'A4',
                exportOptions: {
                    columns: ':not(:last-child)',
                },
            },
        ]
    } );
    
    $(document).on('click', '#btnBayar', function() {
    $('#modalBayar').modal('show')
    $('#biaya_dpp_cicil').hide()
    $('#biaya_dsk_cicil').hide()
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    var id  = $(this).data('id')
    var jk  = $(this).data('jk')

    $('#id_siswa').val(id)
    if (jk == 'Perempuan')
    {
        var idJK = 2
    } else {
        var idJK = 1
    }

    $.get(baseUrl+"/akademik/masterdata/ajaxKelas", function(data, status){
        console.log(data)
        var element = JSON.parse(data)
        $('#kelas').empty()
        $('<option>').val("").text("--Pilih Kelas--").appendTo('#kelas');
        $.each(element, function(index, el){
            $('<option>').val(el.id_kelas).text(el.nama_kelas+'-'+el.level_kelas+'-'+el.status).appendTo('#kelas');
        })
    });
    
    $.get(baseUrl+"/akademik/masterdata/ajaxTahunAjaran", function(data, status){
        console.log(data)
        var element = JSON.parse(data)
        $('#tahun_ajaran').val(element.kode_tahun)
        $('#id_tahun_ajaran').val(element.id_tahun)
    });

    $.get(baseUrl+"/akademik/masterdata/ajaxBiayaSpp/"+id, function(data, status){
        var element = JSON.parse(data)
        console.log(element)
        
        $('#biaya_spp').text(element.biaya)
        $('#biaya_spp_input').val(element.biaya)
        $('#total_tagihan').text(element.biaya)
        $('#bulan_ke').text(element.bulan)
        $('#bulan_ke_input').val(element.bulan)
        $("#biaya_spp").autoNumeric('init', {
            aSep: '.', 
            aDec: ',',
            aForm: true,
            vMax: '999999999',
            vMin: '-999999999'
        });

         
    });
});

    $(document).on('click', '#btnRincianOrtu', function() {
        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        var idSiswa = $(this).data('id')
        $('#modalRincianOrtu').modal('show')

        $.get(baseUrl+"/akademik/masterdata/ajaxOrangTuaSiswa/"+idSiswa, function(data, status){
            var element = JSON.parse(data)    
            console.log(element)        
            $('#nama_lengkap_ibu').text(element.ot_ib_nama_lengkap)
            $('#ttl_ibu').text(element.ot_ib_tempat_lahir+', '+element.ot_ib_tanggal_lahir)
            $('#no_telp_ibu').text(element.ot_ib_no_telp)
            $('#agama_ibu').text(element.ot_ib_agama)
            $('#pekerjaan_ibu').text(element.ot_ib_pekerjaan)
            $('#pendidikan_ibu').text(element.ot_ib_pendidikan)

            $('#nama_lengkap_bapak').text(element.ot_bpk_nama_lengkap)
            $('#ttl_bapak').text(element.ot_bpk_tempat_lahir+', '+element.ot_bpk_tanggal_lahir)
            $('#no_telp_bapak').text(element.ot_bpk_no_telp)
            $('#agama_bapak').text(element.ot_bpk_agama)
            $('#pekerjaan_bapak').text(element.ot_bpk_pekerjaan)
            $('#pendidikan_bapak').text(element.ot_bpk_pendidikan)
        });

    })
    function edit( kode, tgl){

    $('#e_kode').val(kode);
    $('#e_tgl').val(tgl);


    $('#modalEditKTG').modal('show'); 
    }

});
</script>

