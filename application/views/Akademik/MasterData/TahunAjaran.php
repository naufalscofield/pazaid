<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Tahun Ajaran</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tahun Ajaran</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Tahun Ajaran</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<button data-toggle="modal" data-target="#modalTambahKTG" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> &nbsp; Tambah Tahun Ajaran </button>
						<br><br>

					<div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>Kode Tahun</th>
									<th>Mulai Ajaran Pada ..</th>
									<th>Selesai Ajaran Pada ..</th>
									<th>Status <th>
									<th class="text-center"><i class="fa fa-cog"></i></th>
								</tr>
							</thead>
							<tbody>
                            <?php $n = 0;
		                          foreach ($list as $row) { $n++; ?>
                                    <tr>
		                              <td><?php echo $n ?></td>
		                              <td><?php echo $row['kode_tahun'] ?></td>
		                              <td><?php echo date("d-M-y", strtotime($row['created_at'])) ?></td>
		                              <td><?php echo date("d-M-y", strtotime($row['ended_at'])) ?></td>
		                              <td>
                                        <?php if($row['status'] == '1'){ ?>
                                            <a onclick="return confirm('Non Aktifkan Tahun Ajaran Ini')" href='<?php echo site_url('updateStatus/'.$row['id_tahun'].'/0') ?>' class='badge badge-success'><i class='fa fa-check'></i> Aktif</a>
                                    
                                        <?php }else{ ?>
                                            <a onclick="return confirm('Aktifkan Tahun Ajaran Ini')" href='<?php echo site_url('updateStatus/'.$row['id_tahun'].'/1') ?>' class='badge badge-danger'><i class='fa fa-ban'></i>Non Aktif</a>
                                        <?php } ?>
                                      </td>
                                      <td></td>

		                              <td class="text-center">
		                                  <!-- <a href="javascript:void(0)" data-toggle="tooltip" title="Ubah" class="btn btn-warning btn-sm"
		                                          onclick="
		                                            edit(		                                              
		                                            )">
		                                      <i class="fa fa-edit"></i>
		                                  </a> -->
		                                  &nbsp;
		                                  <a onclick="return confirm('Hapus data ini ?')" href="<?php echo site_url('akademik/masterdata/tahunajaran/hapus/'.$row['id_tahun']) ?>" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm">
		                                    <i class="fa fa-trash"></i>
		                                  </a>
		                              </td>
		                            </tr>

		                    <?php } ?>
							</tbody>
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->

<form action="<?php echo site_url('akademik/masterdata/tahunajaran/tambah') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalTambahKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Tahun Ajaran</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <div class="modal-body">
                <div class="form-group">
                   <input type="hidden" name="kode_tahun" id="kode" class="form-control" placeholder="" value="<?php echo $generate;?>" readonly="" autocomplete="off" required />
                </div>
                <div class="form-group">
                   <input type="hidden" name="status" id="stat" class="form-control" placeholder=" " value="1" readonly="" autocomplete="off" required />
                </div>
                <div class="form-group">
                   <label>Tahun Mulai Ajaran</label>
                   <input autocomplete="off" type="date" name="created_at" id="nama" class="form-control" placeholder="Tahun Ajaran..." required/>
                </div>
                <div class="form-group">
                   <label>Tahun Selesai Ajaran</label>
                   <input autocomplete="off" type="date" name="ended_at" id="nama" class="form-control" placeholder="Tahun Ajaran..." required/>
                </div>
             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>


<script type="text/javascript">
 function edit( kode, tgl){

  $('#e_kode').val(kode);
  $('#e_tgl').val(tgl);


  $('#modalEditKTG').modal('show'); 
}
</script>

