<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Siswa Undur Diri</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item"><a href="#">Siswa</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Siswa Undur Diri</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Calon Siswa Undur Diri</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>Nama Siswa</th>
									<th>Panggilan Siswa</th>
									<th>Tempat Lahir</th>
									<th>Tanggal Lahir</th>
									<th>Usia</th>
									<th>Jenis Kelamin</th>
									<th>Agama</th>
									<th>Jumlah Saudara</th>
									<th>Anak Ke-</th>
									<th>Alamat</th>
									<th>Status</th>
									<th>Status Pembayaran</th>
									<th class="text-center"><i class="fa fa-cog"></i></th>
								</tr>
							</thead>
							<tbody>
                                <?php $n = 1; 
                                    foreach($data as $row){ 
                                        if($row['status_siswa'] == "Undur Diri"){?>
                                <tr>
                                    <td><?php echo $n++?>.</td>
                                    <td><?php echo $row['nama_siswa']?></td>
                                    <td><?php echo $row['panggilan_siswa']?></td>
                                    <td><?php echo $row['tempat_lahir']?></td>
                                    <?php 
                                        $lahir  = new DateTime($row['tanggal_lahir']);
                                        $today  = new DateTime(); 
                                        $umur   = $today->diff($lahir)->y;
                                    ?>
                                    <td><?php echo date("d-M-Y", strtotime($row['tanggal_lahir']))?></td>
                                    <td><?php echo $umur?> Tahun</td>
                                    <td><?php echo $row['jenis_kelamin']?></td>
                                    <td><?php echo $row['agama']?></td>
                                    <td><?php echo $row['jumlah_saudara']?></td>
                                    <td><?php echo $row['anak_ke']?></td>
                                    <td><?php echo $row['alamat']?></td>
                                    <td><?php echo $row['status']?></td>
                                    <td><?php echo $row['status_bayar']?></td>
                                    <form action="<?php echo site_url('akademik/siswacalon/delete/'.$row['id_siswa']) ?>" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="status_siswa" value="Undur Diri">
                                    <input type="hidden" name="id_siswa" value="<?php echo $row['id_siswa']?>">
                                    <td class="text-center">
                                        <!-- <a href="javascript:void(0)" data-toggle="tooltip" title="Ubah" class="btn btn-warning btn-sm"
                                                onclick="
                                                edit(		                                              
                                                )">
                                            <i class="fa fa-edit"></i>
                                        </a> -->
                                        &nbsp;
                                        <a onclick="return confirm('Hapus data ini ?')" href="<?php echo site_url('akademik/masterdata/siswaundurdiri/delete/'.$row['id_siswa']) ?>" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm">
		                                    <i class="fa fa-trash"></i>
                                        </a>
                                    </form>
                                    </td>
                                </tr>    
                                    <?php }?>                  
                                <?php } ?>
							</tbody>
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->

   <form action="<?php echo site_url('update_bank') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalEditKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Ubah bank</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>


             <div class="modal-body">
                <div class="form-group">
                   <label>Kode Tahun</label>
                   <input type="text" name="kode_tahun" id="e_kode" class="form-control" placeholder="Kode Aktiva..." value="" readonly="" autocomplete="off" required />
                </div>
                <div class="form-group">
                   <label>Dibuat Pada</label>
                   <input autocomplete="off" type="text" name="created_at" id="e_tgl" class="form-control" placeholder="Nama Aktiva..." required/>
                </div>
             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i> Ubah</button>
             </div>

           </div>
        </div>
     </div>
   </form>

<script type="text/javascript">
 function edit( kode, tgl){

  $('#e_kode').val(kode);
  $('#e_tgl').val(tgl);


  $('#modalEditKTG').modal('show'); 
}
</script>

