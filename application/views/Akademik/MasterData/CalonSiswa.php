<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Calon Siswa</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item"><a href="#">Siswa</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Calon Siswa</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Calon Siswa</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>Kode Pendaftaran</th>
									<th>NIS</th>
									<th>Nama Siswa</th>
									<th>Panggilan Siswa</th>
									<th>Tempat Lahir</th>
									<th>Tanggal Lahir</th>
									<th>Usia</th>
									<th>Jenis Kelamin</th>
									<th>Agama</th>
									<th>Jumlah Saudara</th>
									<th>Anak Ke-</th>
									<th>Alamat</th>
									<th>Status</th>
									<th>Status Pembayaran</th>
									<th class="text-center"><i class="fa fa-cog"></i></th>
								</tr>
							</thead>
							<tbody>
                                <?php $n = 1; 
                                    foreach($data as $row){ 
                                        if($row['id_kelas'] != 0 && $row['status_siswa'] != "Undur Diri" && $row['status_bayar'] != 'Sudah Bayar') {?>
                                <tr>
                                    <td><?php echo $n++?>.</td>
                                    <td><t class="badge badge-warning"><?php echo $row['kode_pendaftaran']?></t></td>
                                    <td><?php echo $row['nis']?></td>
                                    <td><?php echo $row['nama_siswa']?></td>
                                    <td><?php echo $row['panggilan_siswa']?></td>
                                    <td><?php echo $row['tempat_lahir']?></td>
                                    <?php 
                                        $lahir  = new DateTime($row['tanggal_lahir']);
                                        $today  = new DateTime(); 
                                        $umur   = $today->diff($lahir)->y;
                                    ?>
                                    <td><?php echo date("d-M-Y", strtotime($row['tanggal_lahir']))?></td>
                                    <td><?php echo $umur?> Tahun</td>
                                    <td><?php echo $row['jenis_kelamin']?></td>
                                    <td><?php echo $row['agama']?></td>
                                    <td><?php echo $row['jumlah_saudara']?></td>
                                    <td><?php echo $row['anak_ke']?></td>
                                    <td><?php echo $row['alamat']?></td>
                                    <td><?php echo $row['status']?></td>
                                    <td><?php if($row['status_bayar'] == "Belum Bayar") { ?>
                                        <t class="badge badge-danger"><?php echo $row['status_bayar']; ?></t>
                                        <?php }else{ ?>
                                        <t class="badge badge-success"><?php echo $row['status_bayar'];} ?></t>
                                    </td>
                                    <form action="<?php echo site_url('akademik/masterdata/siswacalon/delete/'.$row['id_siswa']) ?>" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="status_siswa" value="Undur Diri">
                                    <input type="hidden" name="id_siswa" value="<?php echo $row['id_siswa']?>">
                                    <td class="text-center">
                                        <!-- <a href="javascript:void(0)" data-toggle="tooltip" title="Ubah" class="btn btn-warning btn-sm"
                                                onclick="
                                                edit(		                                              
                                                )">
                                            <i class="fa fa-edit"></i>
                                        </a> -->
                                        <button data-id="<?= $row['id_siswa'];?>" type="button" id="btnRincianOrtu" class="btn btn-warning"><i class="fa fa-user"></i>&nbsp Rincian Orang Tua</button>
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp Hapus</button>  
                                        <?php
                                        if ($row['status_bayar'] == 'Belum Bayar') { ?>        
                                        <button data-id="<?= $row['id_siswa'];?>" data-jk="<?= $row['jenis_kelamin'];?>" type="button" id="btnBayar" class="btn btn-info"><i class="fa fa-money"></i>&nbsp Buat Biaya</button>
                                        <?php } ?>
                                    </form>
                                    </td>
                                </tr>      
                                    <?php } ?>                          
                                <?php } ?>
							</tbody>
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>

<div class="modal fade" id="modalBayar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Bayar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="form">
                                    <form action="<?= base_url();?>/akademik/transaksi/siswacalon/bayar" method="POST">
                                        <input type="hidden" id="id_siswa" name="id_siswa">
                                        <div class="form-group">
                                        <b><label for="name">Biaya Pendaftaran</label></b><br>
                                        Rp. <label id="biaya_pendaftaran"></label>
                                        <input type="hidden" readonly name="biaya_pendaftaran" id="biaya_pendaftaran_input"></input>
                                        </div>

                                        <div class="form-group">
                                        <b><label for="lname">Biaya Seragam</label>&nbsp<label id="bayarJK"></label></b><br>
                                        Rp. <label id="biaya_seragam"></label>
                                        <input id="biaya_seragam_input" type="hidden" readonly name="biaya_seragam"></input>
                                        </div>
                                        
                                        <div class="form-group">
                                        <b><label for="name">Biaya DPP (Pembangunan)</label></b><br>
                                        Rp. <label id="biaya_dpp"></label>
                                        <!-- <input type="hidden" readonly name="biaya_dpp" id="biaya_dpp_input"></input>&nbsp &nbsp<input name="cb_dpp" value="true" id="cbDpp"type="checkbox">Cicil -->
                                        <input type="hidden" readonly name="biaya_dpp" id="biaya_dpp_input"></input>
                                        <input placeholder="Nominal cicilan awal" type="number"class="form-control" name="biaya_dpp_cicil" id="biaya_dpp_cicil">
                                        </div>
                                        
                                        <div class="form-group">
                                        <b><label for="name">Biaya DSK (Semester)</label></b><br>
                                        Rp. <label id="biaya_dsk"></label>
                                        <!-- <input type="hidden" readonly name="biaya_dsk" id="biaya_dsk_input"></input>&nbsp &nbsp<input value="true" id="cbDsk"type="checkbox">Cicil -->
                                        <input type="hidden" readonly name="biaya_dsk" id="biaya_dsk_input"></input>
                                        <input placeholder="Nominal cicilan awal" type="number" class="form-control" name="biaya_dsk_cicil" id="biaya_dsk_cicil">
                                        </div>

                                        <!-- <div class="form-group">
                                        <b><label for="lname">Pilih Kelas</label></b><br>
                                        <select required class="form-control" name="kelas" id="kelas">
                                            <option value="">--Pilih Kelas--</option>
                                        </select>
                                        </div> -->
                                        
                                        <div class="form-group">
                                        <b><label for="lname">Tahun Ajaran</label></b><br>
                                        <input readonly type="text" id="tahun_ajaran" name="tahun_ajaran" class="form-control">
                                        <input readonly type="hidden" id="id_tahun_ajaran" name="id_tahun_ajaran" class="form-control">
                                        </div>

                                        <!-- <div class="form-group">
                                        <b><label for="lname">Jenis Pembayaran</label></b><br>
                                        <select required class="form-control" name="jenis_pembayaran" id="jenis_pembayaran">
                                            <option value="">--Pilih Jenis Pembayaran--</option>
                                            <option value="cash">Cash</option>
                                            <option value="transfer">Transfer</option>
                                        </select>
                                        </div>

                                        <div class="form-group" id="divRekening">
                                        <b><label for="lname">Rekening</label></b><br>
                                        <select class="form-control" name="rekening" id="rekening">
                                            <option value="">--Pilih Rekening--</option>
                                        </select>
                                        </div>

                                        <div class="form-group" id="">
                                        <b><label for="lname">No Rekening</label></b><br>
                                        <input name="no_rek" type="number" readonly class="form-control" id="no_rek">
                                        </div> -->
                                        
                                        <hr>

                                        <div class="form-group" id="">
                                        <b><h4 for="lname" style="color:red">Total Tagihan:  Rp. <label id="total_tagihan"></label></h4></b><br>
                                        <input id="total_tagihan_input" type="hidden" readonly name="total_tagihan"></input>

                                        <!-- <b><h4 for="lname" style="color:blue">Total Pembayaran:  Rp. <label id="total_pembayaran"></label></h4></b><br>
                                        <input id="total_pembayaran_input" type="hidden" readonly name="total_pembayaran"></input>
                                        </div> -->
                                        
                                        <!-- <hr>
                                        <div class="form-group" id="">
                                        <b><h4 for="lname" style="color:green">Sisa Tagihan:  Rp. <label id="sisa_tagihan"></label></h4></b><br>
                                        <input id="sisa_tagihan_input" type="hidden" readonly name="sisa_tagihan"></input>
                                        </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">OK</button>
      </div>
      </form>

    </div>
  </div>
</div>
</div>

<div class="modal fade" id="modalRincianOrtu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Orang Tua Siswa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h6><b>IBU</b></h6><br>
                                <div id="ibu">
                                    <div class="form-group">
                                    <b><label for="name">Nama Lengkap</label></b>
                                    <p id="nama_lengkap_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="lname">Tempat Tanggal Lahir</label></b>
                                    <p id="ttl_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Agama</label></b>
                                    <p id="agama_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">No Telp</label></b>
                                    <p id="no_telp_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Pendidikan Terakhir</label></b>
                                    <p id="pendidikan_ibu"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Pekerjaan</label></b>
                                    <p id="pekerjaan_ibu"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h6><b>BAPAK</b></h6><br>
                                <div id="ibu">
                                    <div class="form-group">
                                    <b><label for="name">Nama Lengkap</label></b>
                                    <p id="nama_lengkap_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="lname">Tempat Tanggal Lahir</label></b>
                                    <p id="ttl_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Agama</label></b>
                                    <p id="agama_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">No Telp</label></b>
                                    <p id="no_telp_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Pendidikan Terakhir</label></b>
                                    <p id="pendidikan_bapak"></p>
                                    </div>

                                    <div class="form-group">
                                    <b><label for="contact">Pekerjaan</label></b>
                                    <p id="pekerjaan_bapak"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- end::main content -->

   <form action="<?php echo site_url('update_bank') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalEditKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Ubah bank</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>


             <div class="modal-body">
                <div class="form-group">
                   <label>Kode Tahun</label>
                   <input type="text" name="kode_tahun" id="e_kode" class="form-control" placeholder="Kode Aktiva..." value="" readonly="" autocomplete="off" required />
                </div>
                <div class="form-group">
                   <label>Dibuat Pada</label>
                   <input autocomplete="off" type="text" name="created_at" id="e_tgl" class="form-control" placeholder="Nama Aktiva..." required/>
                </div>
             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i> Ubah</button>
             </div>

           </div>
        </div>
     </div>
   </form>
<script type="text/javascript" src="<?=base_url();?>assets/js/autonumeric/autoNumeric.js"></script>
<script type="text/javascript">

$(document).on('click', '#btnRincianOrtu', function() {
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    var id = $(this).data('id')
    $('#modalRincianOrtu').modal('show')

    $.get(baseUrl+"/akademik/masterdata/ajaxOrangTuaSiswa/"+id, function(data, status){
        var element = JSON.parse(data)    
        console.log(element)

        $('#nama_lengkap_ibu').text(element.ot_ib_nama_lengkap)
        $('#ttl_ibu').text(element.ot_ib_tempat_lahir+', '+element.ot_ib_tanggal_lahir)
        $('#no_telp_ibu').text(element.ot_ib_no_telp)
        $('#agama_ibu').text(element.ot_ib_agama)
        $('#pekerjaan_ibu').text(element.ot_ib_pekerjaan)
        $('#pendidikan_ibu').text(element.ot_ib_pendidikan)

        $('#nama_lengkap_bapak').text(element.ot_bpk_nama_lengkap)
        $('#ttl_bapak').text(element.ot_bpk_tempat_lahir+', '+element.ot_bpk_tanggal_lahir)
        $('#no_telp_bapak').text(element.ot_bpk_no_telp)
        $('#agama_bapak').text(element.ot_bpk_agama)
        $('#pekerjaan_bapak').text(element.ot_bpk_pekerjaan)
        $('#pendidikan_bapak').text(element.ot_bpk_pendidikan)
    });
})

$(document).on('click', '#btnBayar', function() {
    $('#modalBayar').modal('show')
    $('#biaya_dpp_cicil').hide()
    $('#biaya_dsk_cicil').hide()
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    var id  = $(this).data('id')
    var jk  = $(this).data('jk')

    $('#id_siswa').val(id)
    if (jk == 'Perempuan')
    {
        var idJK = 2
    } else {
        var idJK = 1
    }

    $.get(baseUrl+"/akademik/masterdata/ajaxKelas", function(data, status){
        console.log(data)
        var element = JSON.parse(data)
        $('#kelas').empty()
        $('<option>').val("").text("--Pilih Kelas--").appendTo('#kelas');
        $.each(element, function(index, el){
            $('<option>').val(el.id_kelas).text(el.nama_kelas+'-'+el.level_kelas+'-'+el.status).appendTo('#kelas');
        })
    });
    
    $.get(baseUrl+"/akademik/masterdata/ajaxTahunAjaran", function(data, status){
        console.log(data)
        var element = JSON.parse(data)
        $('#tahun_ajaran').val(element.kode_tahun)
        $('#id_tahun_ajaran').val(element.id_tahun)
    });

    $.get(baseUrl+"/akademik/masterdata/ajaxBiayaByJK/"+idJK+"/"+id, function(data, status){
        var element = JSON.parse(data)
        console.log(element)
        if (element.siswa.status == 'Anak Guru')
        {
            $('#biaya_pendaftaran').text(0)
            $('#biaya_dpp').text(0)
            $('#biaya_dsk').text(0)
            $('#biaya_pendaftaran_input').val(0)
            $('#biaya_dpp_input').val(0)
            $('#biaya_dsk_input').val(0)
            var intDaftar = 0
            var intSeragam = parseInt(element.biaya.biaya_seragam)
            var intDpp = 0
            var intDsk = 0
        }
        else 
        {
            $('#biaya_pendaftaran').text(element.biaya.biaya_daftar)
            $('#biaya_dpp').text(element.biaya.biaya_dpp)
            $('#biaya_dsk').text(element.biaya.biaya_dsk)
            $('#biaya_pendaftaran_input').val(element.biaya.biaya_daftar)
            $('#biaya_dpp_input').val(element.biaya.biaya_dpp)
            $('#biaya_dsk_input').val(element.biaya.biaya_dsk)
            var intDaftar = parseInt(element.biaya.biaya_daftar)
            var intSeragam = parseInt(element.biaya.biaya_seragam)
            var intDpp = parseInt(element.biaya.biaya_dpp)
            var intDsk = parseInt(element.biaya.biaya_dsk)
        }
        $('#biaya_seragam').text(element.biaya.biaya_seragam)
        $('#biaya_seragam_input').val(element.biaya.biaya_seragam)
        // $('#biaya_dpp_cicil').val(element.biaya_dpp)
        // $('#biaya_dsk_cicil').val(element.biaya_dsk)

        // let intDppCicil = parseInt(element.biaya_dpp)
        // let intDskCicil = parseInt(element.biaya_dsk)
        
        let totalTagihan    = intDaftar + intSeragam + intDpp + intDsk
        // let totalPembayaran = intDaftar + intSeragam + intDppCicil + intDskCicil
        // let sisaTagihan = totalTagihan - totalPembayaran

        $('#total_tagihan').text(totalTagihan)
        // $('#total_pembayaran').text(totalPembayaran)
        // $('#sisa_tagihan').text(sisaTagihan)
        $('#total_tagihan_input').val(totalTagihan)
        // $('#total_pembayaran_input').val(totalPembayaran)
        // $('#sisa_tagihan_input').val(sisaTagihan)

        $('#bayarJK').text(jk)
        $("#sisa_tagihan").autoNumeric('init', {
            aSep: '.', 
            aDec: ',',
            aForm: true,
            vMax: '999999999',
            vMin: '-999999999'
        });
        $("#total_tagihan").autoNumeric('init', {
            aSep: '.', 
            aDec: ',',
            aForm: true,
            vMax: '999999999',
            vMin: '-999999999'
        });
        $("#total_pembayaran").autoNumeric('init', {
            aSep: '.', 
            aDec: ',',
            aForm: true,
            vMax: '999999999',
            vMin: '-999999999'
        });
        $("#biaya_pendaftaran").autoNumeric('init', {
            aSep: '.', 
            aDec: ',',
            aForm: true,
            vMax: '999999999',
            vMin: '-999999999'
        });
        $("#biaya_seragam").autoNumeric('init', {
            aSep: '.', 
            aDec: ',',
            aForm: true,
            vMax: '999999999',
            vMin: '-999999999'
        });
        $("#biaya_dpp").autoNumeric('init', {
            aSep: '.', 
            aDec: ',',
            aForm: true,
            vMax: '999999999',
            vMin: '-999999999'
        });
        $("#biaya_dsk").autoNumeric('init', {
            aSep: '.', 
            aDec: ',',
            aForm: true,
            vMax: '999999999',
            vMin: '-999999999'
        });

         
    });
});

$(document).on('change', '#jenis_pembayaran', function() {
    let val = $(this).val()
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    if (val == 'transfer')
    {
        $('#rekening').prop("disabled", false)
        $('#no_rek').prop("disabled", false)
        $.get(baseUrl+"/keuangan/rekening/ajaxRekening", function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            $('#rekening').empty()
            $('<option>').val("").text("--Pilih Rekening--").appendTo('#rekening');
            $.each(element, function(index, el){
                $('<option>').val(el.kode_rek).text(el.nama_bank).appendTo('#rekening');
            })
        })
    } else {
        $('#rekening').prop("disabled", true)
        $('#no_rek').prop("disabled", true)
        $('#rekening').empty()
        $('#no_rek').val("")

    }
});

$(document).on('change', '#rekening', function() {
    let val = $(this).val()
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        $.get(baseUrl+"/keuangan/rekening/ajaxNoRekening/"+val, function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            $('#no_rek').empty()
            $('#no_rek').val(element.no_rek)
        })
});

$(document).on('click', '#cbDpp', function() {
    if($(this). prop("checked") == true){
        $('#biaya_dpp_cicil').show()
    } else {
        $('#biaya_dpp_cicil').hide()
        let biayaDpp = $('#biaya_dpp').text()
        let IntBiayaDpp = biayaDpp.replace('.', '')
        let IntBiayaDpp2 = IntBiayaDpp.replace('.', '')
        $('#biaya_dpp_cicil').val(IntBiayaDpp2)

        let totalPembayaran = parseInt($('#biaya_pendaftaran_input').val()) + parseInt($('#biaya_seragam_input').val()) + parseInt(IntBiayaDpp2) + parseInt($('#biaya_dsk_cicil').val())
        let sisaTagihan = $('#total_tagihan_input').val() - totalPembayaran

        $('#total_pembayaran').text(totalPembayaran)
        $('#sisa_tagihan').text(sisaTagihan)
        $('#total_pembayaran_input').val(totalPembayaran)
        $('#sisa_tagihan_input').val(sisaTagihan)

        $("#total_pembayaran").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
        });
        $("#sisa_tagihan").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
        });
    }
});

$(document).on('click', '#cbDsk', function() {
    if($(this). prop("checked") == true){
        $('#biaya_dsk_cicil').show()
    } else {
        $('#biaya_dsk_cicil').hide()
        let biayaDsk = $('#biaya_dsk').text()
        let IntBiayaDsk = biayaDsk.replace('.', '')
        let IntBiayaDsk2 = IntBiayaDsk.replace('.', '')
        $('#biaya_dsk_cicil').val(IntBiayaDsk2)

        let totalPembayaran = parseInt($('#biaya_pendaftaran_input').val()) + parseInt($('#biaya_seragam_input').val()) + parseInt(IntBiayaDsk2) + parseInt($('#biaya_dpp_cicil').val())
        let sisaTagihan = $('#total_tagihan_input').val() - totalPembayaran

        $('#total_pembayaran').text(totalPembayaran)
        $('#sisa_tagihan').text(sisaTagihan)
        $('#total_pembayaran_input').val(totalPembayaran)
        $('#sisa_tagihan_input').val(sisaTagihan)

        $("#total_pembayaran").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
        });
        $("#sisa_tagihan").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
        });
    }
});

$(document).on('change', '#biaya_dpp_cicil', function() {
    let totalPembayaran = parseInt($('#biaya_pendaftaran_input').val()) + parseInt($('#biaya_seragam_input').val()) + parseInt($(this).val()) + parseInt($('#biaya_dsk_cicil').val())
    console.log($('#total_tagihan_input').val())
    let sisaTagihan = $('#total_tagihan_input').val() - totalPembayaran

    $('#total_pembayaran').text(totalPembayaran)
    $('#sisa_tagihan').text(sisaTagihan)
    $('#total_pembayaran_input').val(totalPembayaran)
    $('#sisa_tagihan_input').val(sisaTagihan)

    $("#total_pembayaran").autoNumeric('init', {
                aSep: '.', 
                aDec: ',',
                aForm: true,
                vMax: '999999999',
                vMin: '-999999999'
    });
    $("#sisa_tagihan").autoNumeric('init', {
                aSep: '.', 
                aDec: ',',
                aForm: true,
                vMax: '999999999',
                vMin: '-999999999'
    });
});

$(document).on('change', '#biaya_dsk_cicil', function() {
    let totalPembayaran = parseInt($('#biaya_pendaftaran_input').val()) + parseInt($('#biaya_seragam_input').val()) + parseInt($(this).val()) + parseInt($('#biaya_dpp_cicil').val())
    console.log($('#total_tagihan_input').val())
    let sisaTagihan = $('#total_tagihan_input').val() - totalPembayaran

    $('#total_pembayaran').text(totalPembayaran)
    $('#sisa_tagihan').text(sisaTagihan)
    $('#total_pembayaran_input').val(totalPembayaran)
    $('#sisa_tagihan_input').val(sisaTagihan)

    $("#total_pembayaran").autoNumeric('init', {
                aSep: '.', 
                aDec: ',',
                aForm: true,
                vMax: '999999999',
                vMin: '-999999999'
    });
    $("#sisa_tagihan").autoNumeric('init', {
                aSep: '.', 
                aDec: ',',
                aForm: true,
                vMax: '999999999',
                vMin: '-999999999'
    });
});



 function edit( kode, tgl){

  $('#e_kode').val(kode);
  $('#e_tgl').val(tgl);


  $('#modalEditKTG').modal('show'); 
}
</script>

