<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Pendaftaran Siswa</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Pendaftaran Siswa</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">
    <div class="row">
        <div class="col-md-12">
            <?php echo $this->session->flashdata('alert_message') ?>
        </div>
    </div>

    <div class="wizard-body">
    <form id="formDaftar" method="POST" action="<?php echo site_url('akademik/transaksi/pendaftaransiswa/add') ?>">
        <div class="row">

            <ul class="wizard-menu nav nav-pills nav-primary">
            <li class="step">
                <a class="nav-link active final-step" href="#about" data-toggle="tab" aria-expanded="true"><i class="fa fa-user mr-0"></i> Data Pendaftar</a>
            </li>
            <li class="step">
                <a class="nav-link final-step" href="#account" data-toggle="tab"><i class="fa fa-file mr-2"></i> Data Orang Tua</a>
            </li>
            <li class="step">
                <a class="nav-link final-step" href="#address" data-toggle="tab"><i class="fa fa-map-signs mr-2"></i> Konfirmasi</a>
            </li>
            </ul>
        </div>
        <br>
        <div class="tab-content">
            <div class="tab-pane active" id="about">
            <div class="row">
                <div class="col-md-12 text-center">
                <div class="card">
                <div class="card-header text-center bg-primary">
                <h4 class="info-text">BIODATA CALON SISWA</h4>
                </div>
                <div class="card-body">
                <div class="col-sm-12 pl-0">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Nama Lengkap</label>
                            <input required class="form-control" name="nama_siswa" id="nama_siswa" type="text" placeholder="Nama Lengkap..." required="required">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Nama panggilan</label>
                            <input class="form-control" name="panggilan_siswa" id="panggilan_siswa" type="text" placeholder="Nama Panggilan..." required="required">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group">
                        <label for="lname">Tempat Lahir</label>
                        <input required="" class="form-control" name="tempat_lahir" id="tempat_lahir" type="text" placeholder="Tempat Lahir...">
                    </div>
                    </div>
                    <div class="col-md-6">
                    <div class="form-group">
                        <label for="contact">Tanggal Lahir</label>
                        <input type="date" required="" id="datefield" class="form-control" name="tanggal_lahir" data-date-format="YYYY MMMM DD" id="tanggal_lahir" min="2001-01-01" placeholder="Tanggal Lahir...">
                    </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">Jenis Kelamin</label>
                    <select required="" name="jenis_kelamin" id="jk" class="form-control">
                    <option value="">Pilih</option>
                    <option value="Laki - Laki">Laki - Laki</option>
                    <option value="Perempuan">Perempuan</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Agama</label>
                    <select required="" id="agama" name="agama" class="form-control">
                    <option value="">Pilih</option>
                    <option value="Islam">Islam</option>
                    <option value="Kristen">Kristen</option>
                    <option value="Budha">Budha</option>
                    <option value="Hindu">Hindu</option>
                    </select>
                </div>

                
                <div class="form-group">
                    <label for="name">Status</label>
                    <select required="" id="status" name="status" class="form-control">
                    <option value="">Pilih</option>
                    <option value="Anak Guru">Anak Guru</option>
                    <option value="Bukan Anak Guru">Bukan Anak Guru</option>
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="name">Kelas</label>
                    <select required="" id="kelas" name="id_kelas" class="form-control">
                    <option value="">Pilih</option>
                    </select>
                </div>

                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group">
                        <label for="lname">Jumlah Saudara</label>
                        <input type="number" min="1" class="form-control" required="" id="jumlah_saudara" name="jumlah_saudara" type="text" placeholder="Jumlah Saudara...">
                    </div>
                    </div>

                    <div class="col-md-6">
                    <div class="form-group">
                        <label for="contact">Anak Ke</label>
                        <input type="number" class="form-control" required="" name="anak_ke" id="anak_ke" placeholder="Anak Ke...">
                    </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name">Alamat</label>
                    <input class="form-control" name="alamat" id="alamat" type="text" placeholder="Alamat..." required="required">
                    <input class="form-control" name="status_bayar" id="status_bayar" type="hidden" value="Belum Bayar" required="required">
                    
                </div>
                </div>
                </div>
                </div>

                </div>
            </div>
            </div>
            <div class="tab-pane" id="account">
            <div class="text-center">
            <h4 class="info-text">Data Orang Tua </h4></div>
            <div class="text-center mb-3">
                <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" id="yatim" name="radio" value="1">
                <label class="custom-control-label" for="yatim">Yatim</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" id="piatu" name="radio" value="2">
                <label class="custom-control-label" for="piatu">Piatu</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" id="yatimPiatu" name="radio" value="3">
                <label class="custom-control-label" for="yatimPiatu">Yatim Piatu</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" id="lengkap" name="radio" value="4">
                <label class="custom-control-label" for="lengkap">Lengkap</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                        <h6><b>AYAH</b></h6><br>
                        <div id="ayah">
                            <div class="form-group">
                            <label for="name">Nama Lengkap</label>
                            <input required class="form-control" name="ot_bpk_nama_lengkap" id="bpk_nama_lengkap" type="text" placeholder="Nama Lengkap...">
                            </div>

                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="lname">Tempat Lahir</label>
                                <input class="form-control" name="ot_bpk_tempat_lahir" id="bpk_tempat_lahir" type="text" placeholder="Tempat Lahir...">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="contact">Tanggal Lahir</label>
                                <input class="form-control date-picker" type="text" name="ot_bpk_tanggal_lahir" id="bpk_tanggal_lahir" placeholder="Tanggal Lahir...">
                                </div>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="contact">Agama</label>
                            <select id="bpk_agama" name="ot_bpk_agama" class="form-control">
                                <option value="">Pilih</option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Budha">Budha</option>
                                <option value="Hindu">Hindu</option>
                            </select>
                            </div>

                            <div class="form-group">
                            <label for="contact">No Telp</label>
                            <input type="text" class="form-control" name="ot_bpk_no_telp" id="bpk_no_telp" placeholder="No Telp...">
                            </div>

                            <div class="form-group">
                            <label for="contact">Pendidikan Terakhir</label>
                            <input type="text" class="form-control" name="ot_bpk_pendidikan" id="bpk_pendidikan" placeholder="Pendidikan terakhir...">
                            </div>

                            <div class="form-group">
                            <label for="contact">Pekerjaan</label>
                            <input type="text" class="form-control" name="ot_bpk_pekerjaan" id="bpk_pekerjaan" placeholder="Pekerjaan...">
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>

                <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                        <h6><b>IBU</b></h6><br>
                        <div id="ibu">
                            <div class="form-group">
                            <label for="name">Nama Lengkap</label>
                            <input required class="form-control" name="ot_ib_nama_lengkap" id="ib_nama_lengkap" type="text" placeholder="Nama Lengkap...">
                            </div>

                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="lname">Tempat Lahir</label>
                                <input class="form-control" name="ot_ib_tempat_lahir" id="ib_tempat_lahir" type="text" placeholder="Tempat Lahir...">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="contact">Tanggal Lahir</label>
                                <input type="text" class="form-control date-picker" name="ot_ib_tanggal_lahir" id="ib_tanggal_lahir" placeholder="Tanggal Lahir...">
                                </div>
                            </div>
                            </div>

                            <div class="form-group">
                            <label for="contact">Agama</label>
                            <select id="ib_agama" name="ot_ib_agama" class="form-control">
                                <option value="">Pilih</option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Budha">Budha</option>
                                <option value="Hindu">Hindu</option>
                            </select>
                            </div>

                            <div class="form-group">
                            <label for="contact">No Telp</label>
                            <input type="text" class="form-control" name="ot_ib_no_telp" id="ib_no_telp" placeholder="No Telp...">
                            </div>

                            <div class="form-group">
                            <label for="contact">Pendidikan Terakhir</label>
                            <input type="text" class="form-control" name="ot_ib_pendidikan" id="ib_pendidikan" placeholder="Pendidikan Terakhir...">
                            </div>

                            <div class="form-group">
                            <label for="contact">Pekerjaan</label>
                            <input type="text" class="form-control" name="ot_ib_pekerjaan" id="ib_pekerjaan" placeholder="Pekerjaan...">
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <div class="tab-pane" id="address">
            <div class="row">
                <div class="col-md-12">

                <div class="card">
                    <div class="card-header text-center bg-primary">
                    <h4 class="card-title text-white">Konfirmasi Pendaftaran</h4>
                    </div>
                    <div class="card-body">

                    <div class="col-md-12 text-center">
                        <small>KODE PENDAFTARAN</small>
                        <?php $kode = generateRandom(8) ?>
                        <h4><b><?php echo $kode ?></b></h4>
                        <input type="hidden" name="kode_pendaftaran" value="<?php echo $kode ?>">
                      </div>

                    <div class="row">
                        <div class="col-md-12">
                        <h6><b>Data Siswa</b></h6>
                        <br>
                        <table class="table">
                            <tr>
                            <th>Nama Lengkap</th>
                            <td id="o_nama_lengkap"></td>
                           
                            <th>Nama Panggilan</th>
                            <td id="o_panggilan_siswa"></td>
                            </tr>

                            <tr>
                            <th>Tempat / Tanggal Lahir</th>
                            <td id="o_ttl"></td>
                            <th>Anak Ke</th>
                            <td id="o_anak_ke"></td>
                            </tr>

                            <tr>
                            <th>Jenis Kelamin</th>
                            <td id="o_jk"></td>
                            <th>Jumlah Saudara</th>
                            <td id="o_jumlah_saudara"></td>
                            </tr>

                            <tr>
                            <th>Agama</th>
                            <td id="o_agama"></td>
                            <th>Alamat</th>
                            <td id="o_alamat" colspan="3"></td>


                            </tr>
                            <tr>

                            <th>Status Orang Tua</th>
                            <td id="o_status"></td>
                            </tr>
                        </table>

                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-6">
                        <h6><b>Data Ayah</b></h6>
                        <br>
                        <table class="table">
                            <tr>
                            <th>Nama Lengkap</th>
                            <td id="o_bpk_nama_lengkap"></td>
                            </tr>

                            <tr>
                            <th>Tempat / Tanggal Lahir</th>
                            <td id="o_bpk_ttl"></td>
                            </tr>

                            <tr>
                            <th>No Telepon</th>
                            <td id="o_bpk_no_telp"></td>
                            </tr>

                            <tr>
                            <th>Agama</th>
                            <td id="o_bpk_agama"></td>
                            </tr>

                            <tr>
                            <th>Pendidikan</th>
                            <td id="o_bpk_pendidikan"></td>
                            </tr>

                            <tr>

                            <th>Pekerjaan</th>
                            <td id="o_bpk_pekerjaan"></td>
                            </tr>
                        </table>
                        </div>

                        <div class="col-md-6">
                        <h6><b>Data Ibu</b></h6>
                        <br>
                        <table class="table">
                            <tr>
                            <th>Nama Lengkap</th>
                            <td id="o_ib_nama_lengkap"></td>
                            </tr>

                            <tr>
                            <th>Tempat / Tanggal Lahir</th>
                            <td id="o_ib_ttl"></td>
                            </tr>

                            <tr>
                            <th>No Telepon</th>
                            <td id="o_ib_no_telp"></td>
                            </tr>

                            <tr>
                            <th>Agama</th>
                            <td id="o_ib_agama"></td>
                            </tr>

                            <tr>
                            <th>Pendidikan</th>
                            <td id="o_ib_pendidikan"></td>
                            </tr>

                            <tr>

                            <th>Pekerjaan</th>
                            <td id="o_ib_pekerjaan"></td>
                            </tr>
                        </table>
                        </div>


                    </div>

                    </div>

                </div>
                </div>

            </div>
            </div>
        </div>
    </div>

    <div class="wizard-action">
        <div class="pull-left">
          <!-- <input type="button" class="btn btn-previous btn-fill btn-warning" name="previous" value="Previous"> -->
        </div>
        <div class="pull-right">
          <!-- <input type="button" class="btn btn-next btn-danger final-step" name="next" value="Next"> -->
          <input id="finalBtn" type="button" class="btn btn-finish btn-primary" name="finish" value="Finish" style="">
        </div>
        <div class="clearfix"></div>
      </div>
    </form>
</main>
<!-- end::main content -->

<script type="text/javascript">
  $('.date-picker').datepicker({
    dateFormat: "yy-mm-dd",
    changeMonth: true,
    changeYear: true
  });

$(document).ready(function(){
    $('#jumlah_saudara').on('change', function(){
        var jumlah_saudara = $(this).val()
        var max = parseInt(jumlah_saudara)+1
      $('#anak_ke').attr('max', max)  
      $('#anak_ke').attr('min', 1)  
    })

    $('#anak_ke').on('change', function(){
        var max = $(this).attr('max')
        var min = $(this).attr('min')
        var val = $(this).val()
        if (val > max || val < min)
        {
            alert('Harap masukan kolom Anak Ke dengan benar')
            $(this).val("")
        }
    })


var getUrl = window.location;
var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
  $.get(baseUrl+"/akademik/masterdata/ajaxKelas", function(data, status){
        console.log(data)
        var element = JSON.parse(data)
        $('#kelas').empty()
        $('<option>').val("").text("--Pilih Kelas--").appendTo('#kelas');
        $.each(element, function(index, el){
            $('<option>').val(el.id_kelas).text(el.nama_kelas+'-'+el.level_kelas+'-'+el.status).appendTo('#kelas');
        })
    });
})


  $("input[type='radio']").on("click", function() {
    // let value = $("input:checked").val();
    let value = $(this).val();
    console.log(value)
    let idAyah = $('#ayah');
    let idIbu = $('#ibu');
    let inputAyah = idAyah.find("input[type='text'],input[type='url'],input[type='date'],select");
    let inputIbu = idIbu.find("input[type='text'],input[type='url'],input[type='date'],select");

    if (value == '2') {
      for (var i = 0; i < inputAyah.length; i++) {
        $(inputAyah[i]).closest(".form-group input").attr("disabled", "true");
        $(inputAyah[i]).closest(".form-group select").attr("disabled", "true");
      }
      for (var i = 0; i < inputIbu.length; i++) {
        $(inputIbu[i]).closest(".form-group input").attr("required", "true");
        $(inputIbu[i]).closest(".form-group select").attr("required", "true");
        $(inputIbu[i]).closest(".form-group input").removeAttr("disabled", "true");
        $(inputIbu[i]).closest(".form-group select").removeAttr("disabled", "true");
      }

    } else if (value == '1') {
      for (var i = 0; i < inputAyah.length; i++) {
        $(inputAyah[i]).closest(".form-group input").attr("required", "true");
        $(inputAyah[i]).closest(".form-group select").attr("required", "true");
        $(inputAyah[i]).closest(".form-group input").removeAttr("disabled", "true");
        $(inputAyah[i]).closest(".form-group select").removeAttr("disabled", "true");
      }
      for (var i = 0; i < inputIbu.length; i++) {
        $(inputIbu[i]).closest(".form-group input").attr("disabled", "true");
        $(inputIbu[i]).closest(".form-group select").attr("disabled", "true");
      }

    } else if (value == '3') {
      for (var i = 0; i < inputAyah.length; i++) {
        $(inputAyah[i]).closest(".form-group input").removeAttr("required", "true");
        $(inputAyah[i]).closest(".form-group select").removeAttr("required", "true");
        $(inputAyah[i]).closest(".form-group input").attr("disabled", "true");
        $(inputAyah[i]).closest(".form-group select").attr("disabled", "true");
      }
      for (var i = 0; i < inputIbu.length; i++) {
        $(inputIbu[i]).closest(".form-group input").removeAttr("required", "true");
        $(inputIbu[i]).closest(".form-group select").removeAttr("required", "true");
        $(inputIbu[i]).closest(".form-group input").attr("disabled", "true");
        $(inputIbu[i]).closest(".form-group select").attr("disabled", "true");
      }

    } else if (value == '4') {
      for (var i = 0; i < inputAyah.length; i++) {
        $(inputAyah[i]).closest(".form-group input").attr("required", "true");
        $(inputAyah[i]).closest(".form-group select").attr("required", "true");
        $(inputAyah[i]).closest(".form-group input").removeAttr("disabled", "true");
        $(inputAyah[i]).closest(".form-group select").removeAttr("disabled", "true");
      }
      for (var i = 0; i < inputIbu.length; i++) {
        $(inputIbu[i]).closest(".form-group input").attr("required", "true");
        $(inputIbu[i]).closest(".form-group select").attr("required", "true");
        $(inputIbu[i]).closest(".form-group input").removeAttr("disabled", "true");
        $(inputIbu[i]).closest(".form-group select").removeAttr("disabled", "true");
      }

    }


  });

  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; //January is 0!
  var yyyy = today.getFullYear() - 5;
  var yyyyM = today.getFullYear() - 6;
  if (dd < 10) {
    dd = '0' + dd
  }
  if (mm < 10) {
    mm = '0' + mm
  }

  today = yyyy + '-' + mm + '-' + dd;
  todayM = yyyyM + '-' + mm + '-' + dd;
  console.log(todayM);
  document.getElementById("datefield").setAttribute("max", today);
  document.getElementById("datefield").setAttribute("min", todayM);

  $(document).on('click', '.final-step', function() {

    $('#o_nama_lengkap').text($('#nama_siswa').val());
    $('#o_panggilan_siswa').text($('#panggilan_siswa').val());
    $('#o_alamat').text($('#alamat').val());
    $('#o_anak_ke').text($('#anak_ke').val());
    $('#o_jumlah_saudara').text($('#jumlah_saudara').val());
    $('#o_jk').text($('#jk').val());
    $('#o_ttl').text($('#tempat_lahir').val() + " / " + $('#tanggal_lahir').val());
    $('#o_agama').text($('#agama').val());
    $('#o_status').text($('#status').val());
    // $('#o_status').text($('#status').val());

    $('#o_bpk_nama_lengkap').text($('#bpk_nama_lengkap').val());
    $('#o_bpk_ttl').text($('#bpk_tempat_lahir').val() + " / " + $('#bpk_tanggal_lahir').val());
    $('#o_bpk_agama').text($('#bpk_agama').val());
    $('#o_bpk_no_telp').text($('#bpk_no_telp').val());
    $('#o_bpk_pendidikan').text($('#bpk_pendidikan').val());
    $('#o_bpk_pekerjaan').text($('#bpk_pekerjaan').val());

    $('#o_ib_nama_lengkap').text($('#ib_nama_lengkap').val());
    $('#o_ib_ttl').text($('#ib_tempat_lahir').val() + " / " + $('#ib_tanggal_lahir').val());
    $('#o_ib_agama').text($('#ib_agama').val());
    $('#o_ib_no_telp').text($('#ib_no_telp').val());
    $('#o_ib_pendidikan').text($('#ib_pendidikan').val());
    $('#o_ib_pekerjaan').text($('#ib_pekerjaan').val());

  });

  $(document).on('click', '#finalBtn', function() {

    r = confirm('Apakah kamu yakin ?');

    if (r) {
      $('#formDaftar').submit();
    }

  })
</script>