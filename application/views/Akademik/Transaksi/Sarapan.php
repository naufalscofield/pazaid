<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Input Tagihan Sarapan</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Bagian Akademik</a></li>
                    <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tagihan - Sarapan</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Siswa</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
                        <div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>NIS Siswa</th>
									<th>Nama Siswa</th>
									<th>Panggilan Siswa</th>
									<th>Tempat Lahir</th>
									<th>Tanggal Lahir</th>
									<th>Usia</th>
									<th>Jenis Kelamin</th>
									<th>Agama</th>
									<th>Jumlah Saudara</th>
									<th>Anak Ke-</th>
									<th>Alamat</th>
									<th>Status Siswa</th>
									<th>Status Bayar</th>
									<th>Nama Kelas</th>
									<th>Level Kelas</th>
									<th>Tahun Ajaran</th>
									<th class="text-center"><i class="fa fa-cog"></i></th>
								</tr>
							</thead>
							<tbody>
                                <?php $n = 1; 
                                    foreach($data as $row){ 
                                        if($row['nis'] != 0 && $row['status_siswa'] != "Undur Diri" && $row['status_bayar'] != "Belum Bayar"){?>
                                <tr>
                                    <td><?php echo $n++?>.</td>
                                    <td><?php echo $row['nis']?></td>
                                    <td><?php echo $row['nama_siswa']?></td>
                                    <td><?php echo $row['panggilan_siswa']?></td>
                                    <td><?php echo $row['tempat_lahir']?></td>
                                    <td><?php echo date("d-M-Y", strtotime($row['tanggal_lahir']))?></td>
                                    <td><?php echo $row['umur']?> Tahun</td>
                                    <td><?php echo $row['jenis_kelamin']?></td>
                                    <td><?php echo $row['agama']?></td>
                                    <td><?php echo $row['jumlah_saudara']?> Bersaudara</td>
                                    <td><?php echo $row['anak_ke']?></td>
                                    <td><?php echo $row['alamat']?></td>
                                    <td><?php echo $row['status_ortu']?></td>
                                    <td><?php if($row['status_bayar'] == "Belum Bayar"){?>
                                        <t class="badge badge-danger"><?php echo $row['status_bayar'] ?></t>
                                    <?php }else{ ?>
                                        <t class="badge badge-success"><?php echo $row['status_bayar']  ?></t>
                                    <?php } ?>
                                    </td>
                                    <td><?php echo $row['nama_kelas']?></td>
                                    <td><?php echo $row['level_kelas']?></td>
                                    <td><?php echo date("d-M-Y", strtotime($row['created_at']))?></td>
                                    <form action="<?php echo site_url('akademik/masterdata/siswatetap/delete/'.$row['id_siswa']) ?>" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="status_siswa" value="Undur Diri">
                                    <input type="hidden" name="id_siswa" value="<?php echo $row['id_siswa']?>">
                                    <td class="text-center">
                                        <!-- <a href="javascript:void(0)" data-toggle="tooltip" title="Ubah" class="btn btn-warning btn-sm"
                                                onclick="
                                                edit(		                                              
                                                )">
                                            <i class="fa fa-edit"></i>
                                        </a> -->
                                    </form>
                                    <!-- <button data-id="<?= $row['id_siswa'];?>" type="button" id="btnBayar" class="btn btn-success"><i class="fa fa-money"></i> Bayar SPP</button> -->
                                    </td>
                                </tr>    
                                    <?php }?>                  
                                <?php } ?>
							</tbody>
						</table>
					</div>
						</div>
					</div>

                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">NIS</h5>
                    <input type="text" class="form-control" id="nis_cari"placeholder="NIS"><br>
                    <button id="btn_cari" type="button" class="btn btn-primary">Cari</button>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">

        <div class="card">
            <div class="card-body">

            <div class="col-md-12 text-center">
                <b>BUAT TAGIHAN</b>
                <br>
                <b><h2 id="o_total_tagihan"></h2></b>
                </div>

            <div class="row">
                <div class="col-md-12">
                <h6><b>Data Tagihan</b></h6>
                <br>
                <table class="table">
                <form method="POST" action="<?php echo site_url('akademik/transaksi/tagihanlainnya/add') ?>">
                <input type="hidden" value="sarapan" name="jenis_tambahan">
                    <tr>
                    <th>NIS</th>
                    <td>
                    <input id="nis" name="nis" class="form-control" readonly></input>
                    </td>
                    <th>Nama Siswa</th>
                    <td>
                    <input id="nama_siswa" name="nama_siswa" class="form-control" readonly></input>
                    </td>
                    </tr>
                    
                    <tr>
                    <th>Jumlah</th>
                    <td>
                    <input id="jam" name="jam" value="0" type="number" class="form-control" ></input>
                    <th>Nama Guru</th>
                    <td>
                    <select name="guru" id="guru" width="100%" class="form-control">
                    </select>
                    </tr>
                    
                    <tr>
                    <th>Total Tagihan</th>
                    <td>
                    <input id="total_tagihan" value="15.000"name="total_tagihan" class="form-control" readonly></input>
                    <input id="total_tagihan_input" value="15000" name="total_tagihan_input" type="hidden"class="form-control" readonly></input>
                    </td>
                    </tr>
                    

  
                </table>
                    <center><button class="btn btn-primary" type="submit">Buat Tagihan</button></center>
                    </form>

                </div>
            </div>

            <hr>

            </div>

        </div>
        </div>

    </div>
    
</main>

<!-- end::main content -->

<script type="text/javascript" src="<?=base_url();?>assets/js/autonumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    const getUrl = window.location;
        const baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
            $.get(baseUrl+"/akademik/masterdata/ajaxGuru", function(data, status){
                let element = JSON.parse(data)
                $('#guru').empty()
                $('<option>').val("").text("--Pilih Guru--").appendTo('#guru');
                $.each(element, function(index, el){
                    $('<option>').val(el.kode_pegawai).text(el.nama_pegawai).appendTo('#guru');
                })

            })

    $(document).on('click', '#btn_cari', function() {
        let val = $('#nis_cari').val()
        const getUrl = window.location;
        const baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
            $.get(baseUrl+"/akademik/masterdata/ajaxSiswaTetap/"+val, function(data, status){
                let element = JSON.parse(data)
                $('#nama_siswa').val(element.nama_siswa)
                $('#nis').val(element.nis)


            })
    })

    // $(document).on('change', '#jam', function() {
    //     if ($(this).val() == '')
    //     {
    //         $(this).val(0) 
    //         var val = $(this).val()
    //         var conv_menit = val * 60
    //         var menit = $('#menit').val()
    //         var total_menit = parseInt(conv_menit) + parseInt(menit)

    //         var total_tagihan = total_menit / 30
    //         total_tagihan = total_tagihan * 10000
    //         total_tagihan = total_tagihan + 5000

    //         $('#total_tagihan').val(total_tagihan)
    //         $('#total_tagihan_input').val(total_tagihan)

    //     } else 
    //     {
    //         var val = $(this).val()
    //         var conv_menit = val * 60
    //         var menit = $('#menit').val()
    //         var total_menit = parseInt(conv_menit) + parseInt(menit)

    //         var total_tagihan = total_menit / 30
    //         total_tagihan = total_tagihan * 10000
    //         total_tagihan = total_tagihan + 5000

    //         $('#total_tagihan').val(total_tagihan)
    //         $('#total_tagihan_input').val(total_tagihan)

    //     }
    //     $("#total_tagihan").autoNumeric('init', {
    //                 aSep: '.', 
    //                 aDec: ',',
    //                 aForm: true,
    //                 vMax: '999999999',
    //                 vMin: '-999999999'
    //             });
    // });

    $(document).on('change', '#menit', function() {
        if ($(this).val() == '')
        {
            $(this).val(0) 
            var val = $(this).val()
            var jam = $('#jam').val()
            var conv_menit = jam * 60
            var total_menit = parseInt(conv_menit) + parseInt(val)

            var total_tagihan = total_menit / 30
            total_tagihan = total_tagihan * 10000
            total_tagihan = total_tagihan + 5000
            
            $('#total_tagihan').val(total_tagihan)
            $('#total_tagihan_input').val(total_tagihan)

        } else 
        {
            var val = $(this).val()
            var jam = $('#jam').val()
            var conv_menit = jam * 60
            var total_menit = parseInt(conv_menit) + parseInt(val)

            var total_tagihan = total_menit / 30
            total_tagihan = total_tagihan * 10000
            total_tagihan = total_tagihan + 5000

            $('#total_tagihan').val(total_tagihan)
            $('#total_tagihan_input').val(total_tagihan)

        }
        $("#total_tagihan").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });
    });

})

</script>

