<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Generate SPP</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Akademik</a></li>
                    <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Generate SPP Siswa</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">NIS</h5>
                    <input type="text" class="form-control" id="nis"placeholder="NIS"><br>
                    <button id="btn_cari" type="button" class="btn btn-info">Cari</button>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

        <div class="card">
            <div class="card-body">

            <div class="col-md-12 text-center">
                <b>NIS</b>
                <br>
                <b><h2 id="o_nis"></h2></b>
                </div>

            <div class="row">
                <div class="col-md-12">
                <h6><b>Data Siswa</b></h6>
                <br>
                <table class="table">
                    <tr>
                    <th>Nama Lengkap</th>
                    <td id="o_nama_lengkap"></td>
                    
                    <th>Nama Panggilan</th>
                    <td id="o_panggilan_siswa"></td>
                    </tr>

                    <tr>
                    <th>Tempat / Tanggal Lahir</th>
                    <td id="o_ttl"></td>
                    <th>Anak Ke</th>
                    <td id="o_anak_ke"></td>
                    </tr>

                    <tr>
                    <th>Jenis Kelamin</th>
                    <td id="o_jk"></td>
                    <th>Jumlah Saudara</th>
                    <td id="o_jumlah_saudara"></td>
                    </tr>

                    <tr>
                    <th>Agama</th>
                    <td id="o_agama"></td>
                    <th>Alamat</th>
                    <td id="o_alamat" colspan="3"></td>


                    </tr>
                    <tr>

                    <th>Status Orang Tua</th>
                    <td id="o_status"></td>
                    </tr>
                </table>

                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-md-6">
                <h6><b>Data Ayah</b></h6>
                <br>
                <table class="table">
                    <tr>
                    <th>Nama Lengkap</th>
                    <td id="o_bpk_nama_lengkap"></td>
                    </tr>

                    <tr>
                    <th>Tempat / Tanggal Lahir</th>
                    <td id="o_bpk_ttl"></td>
                    </tr>

                    <tr>
                    <th>No Telepon</th>
                    <td id="o_bpk_no_telp"></td>
                    </tr>

                    <tr>
                    <th>Agama</th>
                    <td id="o_bpk_agama"></td>
                    </tr>

                    <tr>
                    <th>Pendidikan</th>
                    <td id="o_bpk_pendidikan"></td>
                    </tr>

                    <tr>

                    <th>Pekerjaan</th>
                    <td id="o_bpk_pekerjaan"></td>
                    </tr>
                </table>
                </div>

                <div class="col-md-6">
                <h6><b>Data Ibu</b></h6>
                <br>
                <table class="table">
                    <tr>
                    <th>Nama Lengkap</th>
                    <td id="o_ib_nama_lengkap"></td>
                    </tr>

                    <tr>
                    <th>Tempat / Tanggal Lahir</th>
                    <td id="o_ib_ttl"></td>
                    </tr>

                    <tr>
                    <th>No Telepon</th>
                    <td id="o_ib_no_telp"></td>
                    </tr>

                    <tr>
                    <th>Agama</th>
                    <td id="o_ib_agama"></td>
                    </tr>

                    <tr>
                    <th>Pendidikan</th>
                    <td id="o_ib_pendidikan"></td>
                    </tr>

                    <tr>

                    <th>Pekerjaan</th>
                    <td id="o_ib_pekerjaan"></td>
                    </tr>
                </table>
                </div>


            </div>

            </div>

        </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">

        <div class="card">
            <div class="card-body">

            <div class="col-md-12 text-center">
                <b>TOTAL TAGIHAN</b>
                <br>
                <b><h2 id="o_total_tagihan"></h2></b>
                </div>

            <div class="row">
                <div class="col-md-12">
                <h6><b>Data Tagihan</b></h6>
                <br>
                <table class="table">
                <form method="POST" action="<?php echo site_url('keuangan/pembayaranpendaftaran/add') ?>">
                    <tr>
                    <th>Biaya Pendaftaran</th>
                    <td>
                    <input id="o_biaya_pendaftaran" class="form-control" readonly></input>
                    </td>
                    <th>Biaya Seragam</th>
                    <td>
                    <input id="o_biaya_seragam" class="form-control" readonly></input>
                    </td>
                    </tr>

                    <tr>
                    <th>Biaya DPP</th>
                    <td>
                    Rp. <label id="o_biaya_dpp"></label>
                    <br>
                    <br>
                    <input name="cicilan_dpp" class="form-control" type="number"></input>
                    </td>
                    <th>Biaya DSK</th>
                    <td>
                    Rp. <label id="o_biaya_dsk"></label>
                    <br>
                    <br>
                    <input name="cicilan_dsk" class="form-control" type="number"></input>
                    <input id="o_id_siswa" name="id_siswa" class="form-control" type="hidden"></input>
                    <input id="total_tagihan" name="total_tagihan" class="form-control" type="hidden"></input>
                    <input id="biaya_pendaftaran_input" name="biaya_pendaftaran_input" class="form-control" type="hidden"></input>
                    <input id="biaya_seragam_input" name="biaya_seragam_input" class="form-control" type="hidden"></input>
                    </td>
                    </tr>
  
                </table>
                    <center><button class="btn btn-primary" type="submit">Bayar</button></center>
                    </form>

                </div>
            </div>

            <hr>

            </div>

        </div>
        </div>

    </div>
    
</main>

<div class="modal fade" id="modalBayar" tabindex="-1" role="dialog" aria-lableledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Bayar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="form">
                                    <form action="<?= base_url();?>/akademik/transaksi/siswacalon/bayar" method="POST">
                                        <input type="hidden" id="id_siswa" name="id_siswa">

                                        <div class="form-group">
                                        <b><label for="name">Biaya Pendaftaran</label></b><br>
                                        Rp. <label id="biaya_pendaftaran"></label>
                                        <input type="hidden" readonly name="biaya_pendaftaran" id="biaya_pendaftaran_input"></input>
                                        </div>

                                        <div class="form-group">
                                        <b><label for="lname">Biaya Seragam</label>&nbsp<label id="bayarJK"></label></b><br>
                                        Rp. <label id="biaya_seragam"></label>
                                        <input id="biaya_seragam_input" type="hidden" readonly name="biaya_seragam"></input>
                                        </div>
                                        
                                        <div class="form-group">
                                        <b><label for="name">Biaya DPP (Pembangunan)</label></b><br>
                                        Rp. <label id="biaya_dpp"></label>
                                        <!-- <input type="hidden" readonly name="biaya_dpp" id="biaya_dpp_input"></input>&nbsp &nbsp<input name="cb_dpp" value="true" id="cbDpp"type="checkbox">Cicil -->
                                        <input type="hidden" readonly name="biaya_dpp" id="biaya_dpp_input"></input>
                                        <input placeholder="Nominal cicilan awal" type="number"class="form-control" name="biaya_dpp_cicil" id="biaya_dpp_cicil">
                                        </div>
                                        
                                        <div class="form-group">
                                        <b><label for="name">Biaya DSK (Semester)</label></b><br>
                                        Rp. <label id="biaya_dsk"></label>
                                        <!-- <input type="hidden" readonly name="biaya_dsk" id="biaya_dsk_input"></input>&nbsp &nbsp<input value="true" id="cbDsk"type="checkbox">Cicil -->
                                        <input type="hidden" readonly name="biaya_dsk" id="biaya_dsk_input"></input>
                                        <input placeholder="Nominal cicilan awal" type="number" class="form-control" name="biaya_dsk_cicil" id="biaya_dsk_cicil">
                                        </div>
                                        
                                        <div class="form-group">
                                        <b><label for="lname">Tahun Ajaran</label></b><br>
                                        <input readonly type="text" id="tahun_ajaran" name="tahun_ajaran" class="form-control">
                                        <input readonly type="hidden" id="id_tahun_ajaran" name="id_tahun_ajaran" class="form-control">
                                        </div>

                                        <div class="form-group">
                                        <b><label for="lname">Jenis Pembayaran</label></b><br>
                                        <select required class="form-control" name="jenis_pembayaran" id="jenis_pembayaran">
                                            <option value="">--Pilih Jenis Pembayaran--</option>
                                            <option value="cash">Cash</option>
                                            <option value="transfer">Transfer</option>
                                        </select>
                                        </div>

                                        <div class="form-group" id="divRekening">
                                        <b><label for="lname">Rekening</label></b><br>
                                        <select disabled class="form-control" name="rekening" id="rekening">
                                            <option value="">--Pilih Rekening--</option>
                                        </select>
                                        </div>

                                        <div class="form-group" id="">
                                        <b><label for="lname">No Rekening</label></b><br>
                                        <input name="no_rek" type="number" readonly class="form-control" id="no_rek">
                                        </div>
                                        
                                        <hr>

                                        <div class="form-group" id="">
                                        <b><h4 for="lname" style="color:red">Total Tagihan:  Rp. <label id="total_tagihan"></label></h4></b><br>
                                        <input id="total_tagihan_input" type="hidden" readonly name="total_tagihan"></input>

                                        <b><h4 for="lname" style="color:blue">Total Pembayaran:  Rp. <label id="total_pembayaran"></label></h4></b><br>
                                        <input id="total_pembayaran_input" type="hidden" readonly name="total_pembayaran"></input>
                                        </div>
                                        
                                        <hr>
                                        <div class="form-group" id="">
                                        <b><h4 for="lname" style="color:green">Sisa Tagihan:  Rp. <label id="sisa_tagihan"></label></h4></b><br>
                                        <input id="sisa_tagihan_input" type="hidden" readonly name="sisa_tagihan"></input>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">OK</button>
      </div>
      </form>

    </div>
  </div>
</div>
<!-- end::main content -->

<script type="text/javascript" src="<?=base_url();?>assets/js/autonumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(document).on('click', '#btn_cari', function() {
        let val = $('#nis').val()
        const getUrl = window.location;
        const baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
            $.get(baseUrl+"/keuangan/pembayaranpendaftaran/ajaxTagihan/"+val, function(data, status){
                let element = JSON.parse(data)
                console.log(element.siswa)

                //kode                
                $('#o_nis').text(element.siswa.kode_pendaftaran)

                //siswa
                $('#o_nama_lengkap').text(element.siswa.nama_siswa)
                $('#o_panggilan_siswa').text(element.siswa.panggilan_siswa)
                $('#o_ttl').text(element.siswa.tempat_lahir + "/" + element.siswa.tanggal_lahir)
                $('#o_anak_ke').text(element.siswa.anak_ke)
                $('#o_jk').text(element.siswa.jenis_kelamin)
                $('#o_jumlah_saudara').text(element.siswa.jumlah_saudara)
                $('#o_agama').text(element.siswa.agama)
                $('#o_alamat').text(element.siswa.alamat)
                $('#o_status').text(element.siswa.status)
                
                //bapak
                $('#o_bpk_nama_lengkap').text(element.siswa.ot_bpk_nama_lengkap)
                $('#o_bpk_ttl').text(element.siswa.ot_bpk_tempat_lahir + "/" + element.siswa.ot_bpk_tanggal_lahir)
                $('#o_bpk_no_telp').text(element.siswa.ot_bpk_no_telp)
                $('#o_bpk_agama').text(element.siswa.ot_bpk_agama)
                $('#o_bpk_pendidikan').text(element.siswa.ot_bpk_pendidikan)
                $('#o_bpk_pekerjaan').text(element.siswa.ot_bpk_pekerjaan)

                //ibu
                $('#o_ib_nama_lengkap').text(element.siswa.ot_ib_nama_lengkap)
                $('#o_ib_ttl').text(element.siswa.ot_ib_tempat_lahir + "/" + element.siswa.ot_ib_tanggal_lahir)
                $('#o_ib_no_telp').text(element.siswa.ot_ib_no_telp)
                $('#o_ib_agama').text(element.siswa.ot_ib_agama)
                $('#o_ib_pendidikan').text(element.siswa.ot_ib_pendidikan)
                $('#o_ib_pekerjaan').text(element.siswa.ot_ib_pekerjaan)

                //tagihan
                $('#o_id_siswa').val(element.tagihan.id_siswa)
                $('#total_tagihan').val(element.tagihan.total_tagihan)
                $('#o_biaya_pendaftaran').val(element.tagihan.biaya_pendaftaran)
                $('#o_biaya_seragam').val(element.tagihan.biaya_seragam)
                $('#biaya_pendaftaran_input').val(element.tagihan.biaya_pendaftaran)
                $('#biaya_seragam_input').val(element.tagihan.biaya_seragam)
                $('#o_biaya_dpp').text(element.tagihan.biaya_dpp)
                $('#o_biaya_dsk').text(element.tagihan.biaya_dsk)
                $("#o_biaya_dpp").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });
                $("#o_biaya_dsk").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });
                $("#o_biaya_seragam").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });
                $("#o_biaya_pendaftaran").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });

            })
    
    })

})

</script>

