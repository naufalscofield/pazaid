<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Kenaikan Kelas</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Akademik</a></li>
                    <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Kenaikan Kelas</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">NIS</h5>
                    <input type="text" class="form-control" id="nis_cari" placeholder="NIS"><br>
                    <button id="btn_cari" type="button" class="btn btn-info">Cari</button>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

        <div class="card">
            <div class="card-body">
            <form method="POST" action="<?php echo site_url('akademik/transaksi/naikkelas/aksinaik') ?>">

            <div class="col-md-12 text-center">
                <b>NIS</b>
                <br>
                <b><h2 id="o_nis"></h2></b>
                <input type="hidden" name="id_siswa" id="id_siswa">
                </div>

            <div class="row">
                <div class="col-md-12">
                <h6><b>Data Siswa</b></h6>
                <br>
                <table class="table">
                    <tr>
                    <th>Nama Lengkap</th>
                    <td id="o_nama_lengkap"></td>
                    
                    <th>Nama Panggilan</th>
                    <td id="o_panggilan_siswa"></td>
                    </tr>

                    <tr>
                    <th>Tempat / Tanggal Lahir</th>
                    <td id="o_ttl"></td>
                    <th>Anak Ke</th>
                    <td id="o_anak_ke"></td>
                    </tr>

                    <tr>
                    <th>Jenis Kelamin</th>
                    <td id="o_jk"></td>
                    <th>Jumlah Saudara</th>
                    <td id="o_jumlah_saudara"></td>
                    </tr>

                    <tr>
                    <th>Agama</th>
                    <td id="o_agama"></td>
                    <th>Alamat</th>
                    <td id="o_alamat" colspan="3"></td>


                    </tr>
                    <tr>

                    <th>Status Orang Tua</th>
                    <td id="o_status"></td>
                    </tr>
                </table>

                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-md-6">
                <h6><b>Data Kelas Saat Ini</b></h6>
                <br>
                <table class="table">
                    <tr>
                    <th>Kode Kelas</th>
                    <td id="kode_kelas"></td>
                    </tr>

                    <tr>
                    <th>Nama Kelas</th>
                    <td id="nama_kelas"></td>
                    </tr>

                    <tr>
                    <th>Level Kelas</th>
                    <td id="level_kelas"></td>
                    </tr>

                    <tr>
                    <th>Status Kelas</th>
                    <td id="status_kelas"></td>
                    </tr>

                </table>
                </div>
                <div class="col-md-6">
                <h6><b>Data Kelas Selanjutnya</b></h6>
                <br>
                <table class="table">
                    <tr>
                    <th>Kode Kelas</th>
                    <td>
                    <select style="height:10%"class="form-control" name="id_kelas_selanjutnya" id="id_kelas_selanjutnya">
                    
                    </select>
                    </td>
                    </tr>

                    <tr>
                    <th>Nama Kelas</th>
                    <td id="nama_kelas_selanjutnya"></td>
                    </tr>

                    <tr>
                    <th>Level Kelas</th>
                    <td id="level_kelas_selanjutnya"></td>
                    </tr>

                    <tr>
                    <th>Status Kelas</th>
                    <td id="status_kelas_selanjutnya"></td>
                    </tr>

                </table>
                </div>



            </div>

            </div>

        </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">

        <div class="card">
            <div class="card-body">

            <div class="col-md-12 text-center">
                <b>TUNGGAKAN</b>
                <br>
                <b><h2 id="o_total_tagihan"></h2></b>
                </div>

            <div class="row">
                <div class="col-md-12">
                <h6><b>Data Tunggakan</b></h6>
                <br>
                <table class="table">
                <form method="POST" action="<?php echo site_url('keuangan/pembayaranpendaftaran/add') ?>">
                    <tr>
                    <th>Biaya DPP</th>
                    <td>
                    Rp. <label id="o_biaya_dpp"></label>
                    <br>
                    <br>
                    </td>
                    <th>Biaya DSK</th>
                    <td>
                    Rp. <label id="o_biaya_dsk"></label>
                    <br>
                    </td>
                    </tr>
                    <tr>
                    <th>Biaya Overtime</th>
                    <td>
                    Rp. <label id="o_biaya_overtime"></label>
                    <br>
                    <br>
                    </td>
                    <th>Biaya Catering</th>
                    <td>
                    Rp. <label id="o_biaya_catering"></label>
                    <br>
                    </td>
                    </tr>
                    <tr>
                    <th>Biaya Mandi</th>
                    <td>
                    Rp. <label id="o_biaya_mandi"></label>
                    <br>
                    <br>
                    </td>
                    <th>Biaya Sarapan</th>
                    <td>
                    Rp. <label id="o_biaya_sarapan"></label>
                    <br>
                    </td>
                    </tr>
                    <tr>
                    <th>Total Tunggakan</th>
                    <td>
                    Rp. <label id="o_total_tunggakan"></label>
                    <br>
                    <br>
                    </td>
                    </tr>
  
                </table>

                </div>
            </div>

            <hr>

            </div>

        </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">

        <div class="card">
            <div class="card-body">

            <div class="col-md-12 text-center">
                <b>TAGIHAN KELAS BARU</b>
                <br>
                <b><h2 id="o_total_tagihan"></h2></b>
                </div>

            <div class="row">
                <div class="col-md-12">
                <h6><b>Data Tagihan</b></h6>
                <br>
                <table class="table">
                    <tr>
                    <th>Biaya DPP</th>
                    <td><br>
                    Rp. <input type="number" name="dpp_baru" id="dpp_baru" class="form-control">
                    <br>
                    <br>
                    </td>
                    <th>Biaya DSK</th>
                    <td><br>
                    Rp. <input type="number" name="dsk_baru" id="dsk_baru" class="form-control">
                    <br>
                    </td>
                    <th>Biaya Seragam</th>
                    <td><br>
                    Rp. <input type="number" name="seragam_baru" id="seragam_baru" class="form-control">
                    <br>
                    </td>
                    </tr>
                    
                   
  
                </table>
                <div class="form-group">
                    <b><label for="lname">Jenis Pembayaran</label></b><br>
                    <select required class="form-control" name="jenis_pembayaran" id="jenis_pembayaran">
                        <option value="">--Pilih Jenis Pembayaran--</option>
                        <option value="cash">Cash</option>
                        <option value="transfer">Transfer</option>
                    </select>
                    </div>

                    <div class="form-group" id="divRekening">
                    <b><label for="lname">Rekening</label></b><br>
                    <select disabled class="form-control" name="rekening" id="rekening">
                        <option value="">--Pilih Rekening--</option>
                    </select>
                    </div>

                    <div class="form-group" id="">
                    <b><label for="lname">No Rekening</label></b><br>
                    <input name="no_rek" type="number" readonly class="form-control" id="no_rek">
                    </div>
                    <center><button class="btn btn-primary" id="btn_naik" type="submit">Naik Kelas</button></center>
                    </form>

                </div>
            </div>

            <hr>

            </div>

        </div>
        </div>

    </div>
    
</main>

<!-- end::main content -->

<script type="text/javascript" src="<?=base_url();?>assets/js/autonumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(document).on('change', '#jenis_pembayaran', function() {
    let val = $(this).val()
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    if (val == 'transfer')
    {
        $('#rekening').prop("disabled", false)
        $('#no_rek').prop("disabled", false)
        $.get(baseUrl+"/keuangan/rekening/ajaxRekening", function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            $('#rekening').empty()
            $('<option>').val("").text("--Pilih Rekening--").appendTo('#rekening');
            $.each(element, function(index, el){
                $('<option>').val(el.kode_rek).text(el.nama_bank).appendTo('#rekening');
            })
        })
    } else {
        $('#rekening').prop("disabled", true)
        $('#no_rek').prop("disabled", true)
        $('#rekening').empty()
        $('#no_rek').val("")

    }
});

$(document).on('change', '#rekening', function() {
    let val = $(this).val()
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        $.get(baseUrl+"/keuangan/rekening/ajaxNoRekening/"+val, function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            $('#no_rek').empty()
            $('#no_rek').val(element.no_rek)
        })
});
    $('#btn_naik').prop('disabled', true)

    $(document).on('click', '#btn_cari', function() {
        let val = $('#nis_cari').val()
        const getUrl = window.location;
        const baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
            $.get(baseUrl+"/akademik/transaksi/naikkelas/ajaxSiswa/"+val, function(data, status){
                console.log(data)
                let element = JSON.parse(data)

                //kode                
                $('#o_nis').text(element.siswa.nis)
                $('#id_siswa').val(element.siswa.id_siswa)

                //siswa
                $('#o_nama_lengkap').text(element.siswa.nama_siswa)
                $('#o_panggilan_siswa').text(element.siswa.panggilan_siswa)
                $('#o_ttl').text(element.siswa.tempat_lahir + "/" + element.siswa.tanggal_lahir)
                $('#o_anak_ke').text(element.siswa.anak_ke)
                $('#o_jk').text(element.siswa.jenis_kelamin)
                $('#o_jumlah_saudara').text(element.siswa.jumlah_saudara)
                $('#o_agama').text(element.siswa.agama)
                $('#o_alamat').text(element.siswa.alamat)
                $('#o_status').text(element.siswa.status_ortu)
                //kelas lama
                $('#kode_kelas').text(element.siswa.kode_kelas)
                $('#nama_kelas').text(element.siswa.nama_kelas)
                $('#level_kelas').text(element.siswa.level_kelas)
                $('#status_kelas').text(element.siswa.status_kelas)

                //tagihan
                $('#o_biaya_dpp').text(element.sisadpp)
                $('#dpp_baru').val(element.dppbaru)
                $('#dsk_baru').val(element.dskbaru)
                $('#seragam_baru').val(element.seragambaru)
                $('#o_biaya_dsk').text(element.sisadsk)
                $('#o_biaya_overtime').text(element.sisaovertime)
                $('#o_biaya_catering').text(element.sisacatering)
                $('#o_biaya_mandi').text(element.sisamandi)
                $('#o_biaya_sarapan').text(element.sisasarapan)
                $('#o_total_tunggakan').text(parseInt(element.sisadpp) + parseInt(element.sisadsk) + parseInt(element.sisaovertime) + parseInt(element.sisacatering) + parseInt(element.sisamandi) + parseInt(element.sisasarapan))
                $("#o_biaya_dpp").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });
                $("#o_biaya_dsk").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });
                $("#o_biaya_overtime").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });
                $("#o_biaya_mandi").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });
                $("#o_biaya_sarapan").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });
                $("#o_biaya_catering").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });
                $("#dppbaru").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });
                $("#dskbaru").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });

                if (parseInt($('#o_total_tunggakan').text()) == 0)
                {
                    $('#btn_naik').prop('disabled', false)
                } else {
                    $('#btn_naik').prop('disabled', true)
                }

                var level_kelas = element.siswa.level_kelas
                $.get(baseUrl+"/akademik/transaksi/naikkelas/ajaxKelas/"+level_kelas, function(data, status){
                let element2 = JSON.parse(data)
                
                $('#id_kelas_selanjutnya').empty()
                $('<option>').val("").text("--Pilih Kelas--").appendTo('#id_kelas_selanjutnya');
                $.each(element2, function(index, el){
                    $('<option>').val(el.id_kelas).text(el.kode_kelas).appendTo('#id_kelas_selanjutnya');
                })


            })

            })
    
    })

    $(document).on('change', '#id_kelas_selanjutnya', function() {
        const getUrl = window.location;
        const baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        var id_kelas = $(this).val()
        $.get(baseUrl+"/akademik/transaksi/naikkelas/ajaxKelasDetail/"+id_kelas, function(data, status){
            let element = JSON.parse(data)
            //kelas baru
            $('#kode_kelas_selanjutnya').text(element.kode_kelas)
            $('#nama_kelas_selanjutnya').text(element.nama_kelas)
            $('#level_kelas_selanjutnya').text(element.level_kelas)
            $('#status_kelas_selanjutnya').text(element.status)

         })
    })


})

</script>

