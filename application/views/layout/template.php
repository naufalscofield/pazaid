<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>P GAB</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/media/image/favicon.png"/>

    <!-- Plugin styles -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/bundle.css" type="text/css">

    <!-- Datepicker -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/datepicker/daterangepicker.css">

    <!-- Slick -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/slick/slick.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/slick/slick-theme.css">

    <!-- Vmap -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/vmap/jqvmap.min.css">

    <!-- Datatable -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/app.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/themify-icons/themify-icons.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/clockpicker/bootstrap-clockpicker.min.css" type="text/css">

    <style type="text/css">
       .clockpicker-popover {
z-index: 999999 !important;
}
    </style>
    <script src="<?php echo base_url() ?>assets/vendors/bundle.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
</head>
<body>

<!-- begin::page loader-->
<div class="page-loader">
    <div class="spinner-border"></div>
</div>
<!-- end::page loader -->

<!-- begin::sidebar user profile -->
<div class="sidebar" id="userProfile">
    <div class="text-center p-4">
        <figure class="avatar avatar-state-success avatar-lg mb-4">
            <img src="<?php echo base_url() ?>assets/media/image/avatar.jpg" class="rounded-circle" alt="image">
        </figure>
        <h4 class="text-primary">Admin</h4>
        <p class="text-muted d-flex align-items-center justify-content-center line-height-0 mb-0">
            Team Leader <a href="#" class="ml-2" data-toggle="tooltip" title="Settings"
                           data-sidebar-open="#settings">
            <i class="ti-settings"></i> </a>
        </p>
    </div>
    <hr class="m-0">
    <div class="p-4">
        <div class="mb-4">
            <h6 class="text-uppercase font-size-11 mb-3">
                Profile completion
                <span class="float-right">%25</span>
            </h6>
            <div class="progress m-b-20" style="height: 5px;">
                <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25"
                     aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="mb-4">
            <h6 class="text-uppercase font-size-11 mb-3">
                Storage
                <span class="float-right">%77</span>
            </h6>
            <div class="progress m-b-20" style="height: 5px;">
                <div class="progress-bar bg-danger" role="progressbar" style="width: 77%;" aria-valuenow="77"
                     aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="mb-4">
            <h6 class="text-uppercase font-size-11 mb-3 ">
                Completed Tasks
                <span class="float-right">%40</span>
            </h6>
            <div class="progress m-b-20" style="height: 5px;">
                <div class="progress-bar bg-success" role="progressbar" style="width: 40%;" aria-valuenow="40"
                     aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="mb-4">
            <h6 class="text-uppercase font-size-11 mb-3">About</h6>
            <p class="text-muted">I love reading, traveling and discovering new things. You need to be happy in
                life.</p>
        </div>
        <div class="mb-4">
            <h6 class="text-uppercase font-size-11 mb-3">City</h6>
            <p class="text-muted">Germany / Berlin</p>
        </div>
        <div class="mb-4">
            <h6 class="text-uppercase font-size-11 mb-3">Social Links</h6>
            <ul class="list-inline mb-4">
                <li class="list-inline-item">
                    <a href="#" class="btn btn-sm btn-floating btn-facebook">
                        <i class="fa fa-facebook"></i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a href="#" class="btn btn-sm btn-floating btn-twitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a href="#" class="btn btn-sm btn-floating btn-dribbble">
                        <i class="fa fa-dribbble"></i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a href="#" class="btn btn-sm btn-floating btn-whatsapp">
                        <i class="fa fa-whatsapp"></i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a href="#" class="btn btn-sm btn-floating btn-linkedin">
                        <i class="fa fa-linkedin"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="mb-4">
            <h6 class="text-uppercase font-size-11 mb-3">Settings</h6>
            <div class="form-group">
                <div class="form-item custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="customSwitch11">
                    <label class="custom-control-label" for="customSwitch11">Block</label>
                </div>
            </div>
            <div class="form-group">
                <div class="form-item custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" checked="" id="customSwitch12">
                    <label class="custom-control-label" for="customSwitch12">Mute</label>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end::sidebar user profile -->

<!-- begin::sidebar settings -->
<div class="sidebar" id="settings">
    <header>
        <i class="ti-settings"></i> Settings
    </header>
    <div class="p-4">
        <div class="mb-3">
            <h6 class="text-uppercase font-size-11 mb-3 text-muted">System</h6>
            <ul class="list-group list-group-flush">
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Automatic updates</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked>
                        <label class="custom-control-label" for="customSwitch1"></label>
                    </div>
                </li>
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Current statistics</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch2" checked>
                        <label class="custom-control-label" for="customSwitch2"></label>
                    </div>
                </li>
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>User suggestions</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch3" checked>
                        <label class="custom-control-label" for="customSwitch3"></label>
                    </div>
                </li>
            </ul>
        </div>
        <div class="mb-3">
            <h6 class="text-uppercase font-size-11 mb-3 text-muted">Account</h6>
            <ul class="list-group list-group-flush">
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Senior account security</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch4">
                        <label class="custom-control-label" for="customSwitch4"></label>
                    </div>
                </li>
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Account protection</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch5" checked>
                        <label class="custom-control-label" for="customSwitch5"></label>
                    </div>
                </li>
            </ul>
        </div>

        <div class="mb-3">
            <h6 class="text-uppercase font-size-11 mb-3 text-muted">Notifications</h6>
            <ul class="list-group list-group-flush">
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Browser notifications</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch6">
                        <label class="custom-control-label" for="customSwitch6"></label>
                    </div>
                </li>
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Mobile notifications</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch7">
                        <label class="custom-control-label" for="customSwitch7"></label>
                    </div>
                </li>
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Email subscription</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch8">
                        <label class="custom-control-label" for="customSwitch8"></label>
                    </div>
                </li>
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Sms notifications</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch9" checked>
                        <label class="custom-control-label" for="customSwitch9"></label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- end::sidebar settings -->

<!-- begin::navigation -->
<div class="navigation">
    <div class="navigation-icon-menu">
        <ul>
            <li <?php if($this->session->userdata('menu') == 'akademik'){ echo 'class="active"'; } ?> data-toggle="tooltip" title="BAGIAN AKADEMIK">
                <a href="#navigationAPK">
                    <i class="icon ti-book"></i>
                </a>
            </li>
            <li <?php if($this->session->userdata('menu') == 'hr'){ echo 'class="active"'; } ?>  data-toggle="tooltip" title="PENGGAJIAN">
                <a href="#navigationDashboards" title="PENGGAJIAN">
                    <i class="icon ti-pie-chart"></i>
                    <span class="badge badge-warning">2</span>
                </a>
            </li>
            <li <?php if($this->session->userdata('menu') == 'aset'){ echo 'class="active"'; } ?> data-toggle="tooltip" title="ASET">
                <a href="#navigationApps" title="ASET">
                    <i class="icon ti-package"></i>
                </a>
            </li>
            <li <?php if($this->session->userdata('menu') == 'keuangan'){ echo 'class="active"'; } ?> data-toggle="tooltip" title="BAGIAN KEUANGAN">
                <a href="#navigationPlugins">
                    <i class="icon ti-brush-alt"></i>
                </a>
            </li>
            <li data-toggle="tooltip" title="KONFIRMASI"  <?php if($this->session->userdata('menu') == 'acc'){ echo 'class="active"'; } ?>>
                <a href="#navigationElements">
                    <i class="icon ti-layers"></i>
                </a>
            </li>
        </ul>
        <ul>
            <li data-toggle="tooltip" title="Edit Profile">
                <a href="#" class="go-to-page">
                    <i class="icon ti-settings"></i>
                </a>
            </li>
            <li data-toggle="tooltip" title="Logout">
                <a href="<?php echo site_url('auth/logout') ?>" class="go-to-page">
                    <i class="icon ti-power-off"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="navigation-menu-body">
        <ul id="navigationAPK" <?php if($this->session->userdata('menu') == 'akademik'){ echo 'class="navigation-active"'; } ?>>
            <li class="navigation-divider">MASTER DATA</li>
            <li><a href="<?php echo site_url('akademik/masterdata/tahunajaran') ?>">Tahun Ajaran</a></li>
            <li><a href="<?php echo site_url('akademik/masterdata/kelas') ?>">Kelas</a></li>
            <li>
                <a href="Javascript:void(0)">Siswa</a>
                <ul>
                    <li><a href="<?php echo site_url('akademik/masterdata/siswacalon') ?>">Calon Siswa</a></li>
                    <li><a href="<?php echo site_url('akademik/masterdata/siswatetap') ?>">Siswa Tetap</a></li>
                    <li><a href="<?php echo site_url('akademik/masterdata/siswaundurdiri') ?>">Siswa Undur Diri</a></li>
                </ul>
            </li>


            <li class="navigation-divider">Transaksi</li>
            <li><a href="<?php echo site_url('akademik/transaksi/pendaftaransiswa') ?>">Pendaftaran Siswa</a></li>
            <li><a href="<?php echo site_url('transaksi/generatespp') ?>">Generate SPP Siswa</a></li>

            <li><a href="<?php echo site_url('akademik/masterdata/daycare') ?>">Daycare</a></li>
            <li><a href="<?php echo site_url('akademik/transaksi/naikkelas') ?>">Kenaikan Kelas</a></li>

            <li>
                <a href="Javascript:void(0)">Tagihan Lainnya</a>
                <ul>
                    <li><a href="<?php echo site_url('akademik/transaksi/overtime') ?>">Overtime</a></li>
                    <li><a href="<?php echo site_url('akademik/transaksi/catering') ?>">Catering</a></li>
                    <li><a href="<?php echo site_url('akademik/transaksi/sarapan') ?>">Sarapan</a></li>
                    <li><a href="<?php echo site_url('akademik/transaksi/mandi') ?>">Mandi</a></li>
                </ul>
            </li>

            <li><a href="<?php echo site_url('akademik/transaksi/pengakuan') ?>">Generate Jurnal Pengakuan Pendapatan</a></li>

            <li class="navigation-divider">Laporan</li>
            <li>
                <a href="Javascript:void(0)">Kartu Piutang Siswa</a>
                <ul>
                    <li><a href="<?php echo site_url('akademik/laporan/pendaftaran') ?>">Pendaftaran</a></li>
                    <li><a href="<?php echo site_url('akademik/laporan/spp') ?>">SPP</a></li>
                    <li><a href="<?php echo site_url('akademik/laporan/lainnya') ?>">Lainnya</a></li>
                </ul>
            </li>

        </ul>
        <ul id="navigationDashboards" <?php if($this->session->userdata('menu') == 'hr'){ echo 'class="navigation-active"'; } ?>>
            <li class="navigation-divider">MASTER DATA</li>
            <li>
                <a href="<?php echo site_url('penggajian/jabatan') ?>">Jabatan</a>
            </li>
            <li>
                <a href="<?php echo site_url('penggajian/pegawai') ?>">Pegawai</a>
            </li>
            <li>
                <a href="<?php echo site_url('penggajian/lembur') ?>">Lembur</a>
            </li>

            <li>
                <a href="<?php echo site_url('penggajian/tunjangan') ?>">Tunjangan</a>
            </li>
            <li class="navigation-divider">TRANSAKSI</li>
            <li>
                <a href="Javascript:void(0)">Presensi</a>
                <ul>
                    <li><a href="<?php echo site_url('penggajian/presensi/cuti') ?>">Cuti</a></li>
                    <li><a href="<?php echo site_url('penggajian/presensi/daftar') ?>">Daftar Presensi</a></li>
                </ul>
            </li>

            <li>
                <a href="Javascript:void(0)">Gaji</a>
                <ul>
                    <li><a href="<?php echo site_url('penggajian/gaji/lembur') ?>">Input Lembur</a></li>
                    <li><a href="<?php echo site_url('penggajian/gaji/tunjangan') ?>">Input Tunjangan</a></li>
                    <li><a href="<?php echo site_url('penggajian/gaji/daftar') ?>">Daftar Gaji</a></li>
                </ul>
            </li>

            <li>
                <a href="<?php echo site_url('RFID') ?>">RFID</a>
            </li>

            <li class="navigation-divider">LAPORAN</li>
            <li>
                <a href="<?php echo site_url('penggajian/laporan/gaji') ?>">Gaji</a>
            </li>
             <li>
                <a href="<?php echo site_url('penggajian/laporan/absensi') ?>">Absensi</a>
            </li>
            
        </ul>
        <ul id="navigationApps" <?php if($this->session->userdata('menu') == 'aset'){ echo 'class="navigation-active"'; } ?>>
            <li class="navigation-divider">MASTER DATA</li>
            <li>
                <a href="<?php echo site_url('aset/kategori') ?>">Kategori</a>
            </li>
            <li>
                <a href="<?php echo site_url('aset/daftar') ?>">Aset</a>
            </li>
            <li>
                <a href="<?php echo site_url('aset/ruangan') ?>">Ruangan</a>
            </li>
            <li>
                <a href="<?php echo site_url('aset/vendor') ?>">Vendor</a>
            </li>

            <li class="navigation-divider">TRANSAKSI</li>

            <li>
                <a href="<?php echo site_url('aset/perolehan') ?>">Perolehan</a>
            </li>
            <li>
                <a href="<?php echo site_url('aset/penyusutan') ?>">Penyusutan</a>
            </li>

            <li>
                <a href="Javascript:void(0)">Pengalokasian Aset</a>
                <ul>
                    <li><a href="<?php echo site_url('aset/penerimaan') ?>">Penerimaan</a></li>
                    <li><a href="<?php echo site_url('aset/penempatan') ?>">Penempatan</a></li>
                </ul>
            </li>
            
            <li>
                <a href="Javascript:void(0)">Perawatan</a>
                <ul>
                    <li><a href="<?php echo site_url('aset/transaksi/pemeliharaan') ?>">Pemeliharaan</a></li>
                    <li><a href="<?php echo site_url('aset/transaksi/perbaikan') ?>">Perbaikan</a></li>
                </ul>
            </li>

            <li class="navigation-divider">LAPORAN</li>
            <li>
                <a href="<?php echo site_url('laporan/kartu_aktiva') ?>">Kartu Aktiva</a>
                <a href="<?= site_url('laporan/aset/perolehan') ?>">Perolehan</a>
                <a href="<?= site_url('laporan/aset/pemeliharaan') ?>">Pemeliharaan</a>
                <a href="<?= site_url('laporan/aset/perbaikan') ?>">Perbaikan</a>
            </li>
        </ul>
        <ul id="navigationPlugins" <?php if($this->session->userdata('menu') == 'keuangan'){ echo 'class="navigation-active"'; } ?>>
            <li class="navigation-divider">MASTER DATA</li>
            <li><a href="<?php echo site_url('keuangan/coa') ?>">Chart Of Account</a></li>
            <li><a href="<?php echo site_url('keuangan/pemilik') ?>">Pemilik</a></li>
            <li><a href="<?php echo site_url('keuangan/beban') ?>">Beban</a></li>
            <li><a href="<?php echo site_url('keuangan/rekening') ?>">Rekening</a></li>
            <li><a href="<?php echo site_url('keuangan/komponenbiaya') ?>">Komponen Biaya</a></li>

            <!-- <li><a href="<?php echo site_url('keuangan/bop') ?>">Biaya Operasional PAUD</a></li> -->

            <li class="navigation-divider">KAS</li>
            <li><a href="<?php echo site_url('keuangan/transaksi_beban') ?>">Transaksi Beban</a></li>
            <li>
                <a href="Javascript:void(0)">Modal</a>
                <ul>
                    <li><a href="<?php echo site_url('keuangan/modal/setor') ?>">Setor</a></li>
                    <li><a href="<?php echo site_url('keuangan/modal/penarikan') ?>">Penarikan</a></li>
                </ul>
            </li>
            <li>
                <a href="Javascript:void(0)">Bank</a>
                <ul>
                    <li><a href="<?php echo site_url('keuangan/bank/setor') ?>">Setor</a></li>
                    <li><a href="<?php echo site_url('keuangan/bank/tarik') ?>">Penarikan</a></li>
                </ul>
            </li>

            <li>
                <a href="Javascript:void(0)">BOP</a>
                <ul>
                    <li><a href="<?php echo site_url('keuangan/bop/masuk') ?>">Masuk</a></li>
                    <li><a href="<?php echo site_url('keuangan/bop/keluar') ?>">Pemakaian</a></li>
                </ul>
            </li>

            <li><a href="<?php echo site_url('keuangan/pembayaranpendaftaran') ?>">Pembayaran Pendaftaran Awal</a></li>
            <li><a href="<?php echo site_url('keuangan/pembayarannaikkelas') ?>">Pembayaran Pendaftaran Kenaikan Kelas</a></li>
            <li><a href="<?php echo site_url('keuangan/pelunasanpendaftaran') ?>">Pelunasan Pendaftaran Awal</a></li>
            <li><a href="<?php echo site_url('keuangan/pelunasannaikkelas') ?>">Pelunasan Pendaftaran Naik Kelas</a></li>
            <li><a href="<?php echo site_url('keuangan/pembayaranspp') ?>">Pembayaran SPP</a></li>
            <li><a href="<?php echo site_url('keuangan/pembayaranlainnya') ?>">Pembayaran Tagihan Lainnya</a></li>
            <li><a href="<?php echo site_url('keuangan/pembayarandaycare') ?>">Pembayaran Daycare</a></li>
            <!-- <li><a href="<?php echo site_url('keuangan/modal/setor') ?>">Setoran Modal</a></li>
            <li><a href="<?php echo site_url('keuangan/bank/setor') ?>">Penarikan Bank</a></li> -->

            <li><a href="<?php echo site_url('keuangan/kas_keluar') ?>">Acc Uang Keluar</a></li>
            <li><a href="<?php echo site_url('keuangan/transaksi/jurnal') ?>">Post Jurnal</a></li>

            <li><a href="<?php echo site_url('keuangan/transaksi/bunga') ?>">Pencatatan Bunga</a></li>


            <li class="navigation-divider">LAPORAN</li>
            <li><a href="<?php echo site_url('keuangan/laporan/kas_keluar') ?>">Kas Keluar</a></li>
            <li><a href="<?php echo site_url('keuangan/laporan/jurnal') ?>">Jurnal Umum</a></li>
            <li><a href="<?php echo site_url('keuangan/laporan/ayat_jurnal_penutup') ?>">Ayat Jurnal Penutup</a></li>

            <li><a href="<?php echo site_url('keuangan/laporan/buku_besar') ?>">Buku Besar</a></li>
            <li><a href="<?php echo site_url('keuangan/laporan/modal_pemilik') ?>">Kartu Modal Pemilik</a></li>
            <li><a href="<?php echo site_url('keuangan/laporan/perubahan_modal') ?>">Perubahan Modal</a></li>
            <li><a href="<?php echo site_url('keuangan/laporan/laba_rugi') ?>">Laba Rugi</a></li>

            <li><a href="<?php echo site_url('keuangan/transaksi/koran') ?>">Rekening Koran</a></li>
            <li><a href="<?php echo site_url('keuangan/transaksi/neraca') ?>">Laporan Neraca</a></li>

            <li>
                <a href="Javascript:void(0)">Laporan Penerimaan</a>
                <ul>
                    <li><a href="<?php echo site_url('keuangan/laporan/cash') ?>">Penerimaan Cash</a></li>
                    <li><a href="<?php echo site_url('keuangan/laporan/bank') ?>">Penerimaan Bank</a></li>
                </ul>
            </li>
 
            <li><a href="<?php echo site_url('keuangan/transaksi/saldokas') ?>">Laporan Saldo Kas</a></li>

        </ul>
        <ul id="navigationElements" <?php if($this->session->userdata('menu') == 'acc'){ echo 'class="navigation-active"'; } ?>>
            <li class="navigation-divider">Kepala Sekolah</li>
            <li>
                <a href="#">ACC</a>
                <ul>
                    <li><a href="<?php echo site_url('acc/kas') ?>">Kas Keluar</a></li>
                    <li><a href="<?php echo site_url('acc/cuti') ?>l">Pengajuan Cuti </a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- end::navigation -->

<?php echo $contents ?>

<!-- Plugin scripts -->


<!-- Chartjs -->
<script src="<?php echo base_url() ?>assets/vendors/charts/chartjs/chart.min.js"></script>

<!-- Circle progress -->
<script src="<?php echo base_url() ?>assets/vendors/circle-progress/circle-progress.min.js"></script>

<!-- Peity -->
<script src="<?php echo base_url() ?>assets/vendors/charts/peity/jquery.peity.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/examples/charts/peity.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url() ?>assets/vendors/datepicker/daterangepicker.js"></script>

<!-- Slick -->
<script src="<?php echo base_url() ?>assets/vendors/slick/slick.min.js"></script>

<!-- Vamp -->
<script src="<?php echo base_url() ?>assets/vendors/vmap/jquery.vmap.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/vmap/maps/jquery.vmap.usa.js"></script>
<script src="<?php echo base_url() ?>assets/js/examples/vmap.js"></script>

<script src="<?php echo base_url() ?>assets/vendors/clockpicker/bootstrap-clockpicker.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/examples/clockpicker.js"></script>



<!-- Dashboard scripts -->
<script src="<?php echo base_url() ?>assets/js/examples/dashboard.js"></script>
<div class="colors"> <!-- To use theme colors with Javascript -->
    <div class="bg-primary"></div>
    <div class="bg-primary-bright"></div>
    <div class="bg-secondary"></div>
    <div class="bg-secondary-bright"></div>
    <div class="bg-info"></div>
    <div class="bg-info-bright"></div>
    <div class="bg-success"></div>
    <div class="bg-success-bright"></div>
    <div class="bg-danger"></div>
    <div class="bg-danger-bright"></div>
    <div class="bg-warning"></div>
    <div class="bg-warning-bright"></div>
</div>

<!-- App scripts -->
<script src="<?php echo base_url() ?>assets/js/app.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/format_rp.js"></script>

<script type="text/javascript">
  $('.datepicker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    });
</script>


</body>

</html>