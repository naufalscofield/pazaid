<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>P GAB</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/media/image/favicon.png"/>

    <!-- Plugin styles -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/bundle.css" type="text/css">

    <!-- Datepicker -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/datepicker/daterangepicker.css">

    <!-- Slick -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/slick/slick.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/slick/slick-theme.css">

    <!-- Vmap -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/vmap/jqvmap.min.css">

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/app.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/themify-icons/themify-icons.css" type="text/css">
    <script src="<?php echo base_url() ?>assets/vendors/bundle.js"></script>
</head>
<body>

<!-- begin::page loader-->
<div class="page-loader">
    <div class="spinner-border"></div>
</div>
<!-- end::page loader -->

<!-- begin::sidebar user profile -->
<div class="sidebar" id="userProfile">
    <div class="text-center p-4">
        <figure class="avatar avatar-state-success avatar-lg mb-4">
            <img src="<?php echo base_url() ?>assets/media/image/avatar.jpg" class="rounded-circle" alt="image">
        </figure>
        <h4 class="text-primary">Kenneth Hune</h4>
        <p class="text-muted d-flex align-items-center justify-content-center line-height-0 mb-0">
            Team Leader <a href="#" class="ml-2" data-toggle="tooltip" title="Settings"
                           data-sidebar-open="#settings">
            <i class="ti-settings"></i> </a>
        </p>
    </div>
    <hr class="m-0">
    <div class="p-4">
        <div class="mb-4">
            <h6 class="text-uppercase font-size-11 mb-3">
                Profile completion
                <span class="float-right">%25</span>
            </h6>
            <div class="progress m-b-20" style="height: 5px;">
                <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25"
                     aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="mb-4">
            <h6 class="text-uppercase font-size-11 mb-3">
                Storage
                <span class="float-right">%77</span>
            </h6>
            <div class="progress m-b-20" style="height: 5px;">
                <div class="progress-bar bg-danger" role="progressbar" style="width: 77%;" aria-valuenow="77"
                     aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="mb-4">
            <h6 class="text-uppercase font-size-11 mb-3 ">
                Completed Tasks
                <span class="float-right">%40</span>
            </h6>
            <div class="progress m-b-20" style="height: 5px;">
                <div class="progress-bar bg-success" role="progressbar" style="width: 40%;" aria-valuenow="40"
                     aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="mb-4">
            <h6 class="text-uppercase font-size-11 mb-3">About</h6>
            <p class="text-muted">I love reading, traveling and discovering new things. You need to be happy in
                life.</p>
        </div>
        <div class="mb-4">
            <h6 class="text-uppercase font-size-11 mb-3">City</h6>
            <p class="text-muted">Germany / Berlin</p>
        </div>
        <div class="mb-4">
            <h6 class="text-uppercase font-size-11 mb-3">Social Links</h6>
            <ul class="list-inline mb-4">
                <li class="list-inline-item">
                    <a href="#" class="btn btn-sm btn-floating btn-facebook">
                        <i class="fa fa-facebook"></i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a href="#" class="btn btn-sm btn-floating btn-twitter">
                        <i class="fa fa-twitter"></i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a href="#" class="btn btn-sm btn-floating btn-dribbble">
                        <i class="fa fa-dribbble"></i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a href="#" class="btn btn-sm btn-floating btn-whatsapp">
                        <i class="fa fa-whatsapp"></i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a href="#" class="btn btn-sm btn-floating btn-linkedin">
                        <i class="fa fa-linkedin"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="mb-4">
            <h6 class="text-uppercase font-size-11 mb-3">Settings</h6>
            <div class="form-group">
                <div class="form-item custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" id="customSwitch11">
                    <label class="custom-control-label" for="customSwitch11">Block</label>
                </div>
            </div>
            <div class="form-group">
                <div class="form-item custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" checked="" id="customSwitch12">
                    <label class="custom-control-label" for="customSwitch12">Mute</label>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end::sidebar user profile -->

<!-- begin::sidebar settings -->
<div class="sidebar" id="settings">
    <header>
        <i class="ti-settings"></i> Settings
    </header>
    <div class="p-4">
        <div class="mb-3">
            <h6 class="text-uppercase font-size-11 mb-3 text-muted">System</h6>
            <ul class="list-group list-group-flush">
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Automatic updates</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch1" checked>
                        <label class="custom-control-label" for="customSwitch1"></label>
                    </div>
                </li>
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Current statistics</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch2" checked>
                        <label class="custom-control-label" for="customSwitch2"></label>
                    </div>
                </li>
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>User suggestions</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch3" checked>
                        <label class="custom-control-label" for="customSwitch3"></label>
                    </div>
                </li>
            </ul>
        </div>
        <div class="mb-3">
            <h6 class="text-uppercase font-size-11 mb-3 text-muted">Account</h6>
            <ul class="list-group list-group-flush">
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Senior account security</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch4">
                        <label class="custom-control-label" for="customSwitch4"></label>
                    </div>
                </li>
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Account protection</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch5" checked>
                        <label class="custom-control-label" for="customSwitch5"></label>
                    </div>
                </li>
            </ul>
        </div>
        <div class="mb-3">
            <h6 class="text-uppercase font-size-11 mb-3 text-muted">Notifications</h6>
            <ul class="list-group list-group-flush">
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Browser notifications</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch6">
                        <label class="custom-control-label" for="customSwitch6"></label>
                    </div>
                </li>
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Mobile notifications</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch7">
                        <label class="custom-control-label" for="customSwitch7"></label>
                    </div>
                </li>
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Email subscription</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch8">
                        <label class="custom-control-label" for="customSwitch8"></label>
                    </div>
                </li>
                <li class="list-group-item d-flex justify-content-between p-l-r-0 p-t-b-5">
                    <span>Sms notifications</span>
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="customSwitch9" checked>
                        <label class="custom-control-label" for="customSwitch9"></label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- end::sidebar settings -->

<!-- begin::navigation -->
<div class="navigation">
    <div class="navigation-icon-menu">
        <ul>
            <li class="active" data-toggle="tooltip" title="PENGGAJIAN">
                <a href="#navigationDashboards" title="PENGGAJIAN">
                    <i class="icon ti-pie-chart"></i>
                    <span class="badge badge-warning">2</span>
                </a>
            </li>
        </ul>
        <ul>
            <li data-toggle="tooltip" title="Edit Profile">
                <a href="#" class="go-to-page">
                    <i class="icon ti-settings"></i>
                </a>
            </li>
            <li data-toggle="tooltip" title="Logout">
                <a href="<?php echo site_url('auth/logout') ?>" class="go-to-page">
                    <i class="icon ti-power-off"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="navigation-menu-body">
        <ul id="navigationDashboards" class="navigation-active">
            <li class="navigation-divider">MENU</li>
            <li>
                <a href="<?php echo site_url('karyawan') ?>">Presensi</a>
            </li>
            <li>
                <a href="<?php echo site_url('karyawan/gaji') ?>">Gaji</a>
            </li>
            <li>
                <a href="<?php echo site_url('karyawan/izin') ?>">Perizinan</a>
            </li>

            <li>
                <a href="<?php echo site_url('karyawan/pinjaman') ?>">Pinjaman</a>
            </li>
            
        </ul>
    </div>
</div>
<!-- end::navigation -->

<?php echo $contents ?>

<!-- Plugin scripts -->


<!-- Chartjs -->
<script src="<?php echo base_url() ?>assets/vendors/charts/chartjs/chart.min.js"></script>

<!-- Circle progress -->
<script src="<?php echo base_url() ?>assets/vendors/circle-progress/circle-progress.min.js"></script>

<!-- Peity -->
<script src="<?php echo base_url() ?>assets/vendors/charts/peity/jquery.peity.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/examples/charts/peity.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url() ?>assets/vendors/datepicker/daterangepicker.js"></script>

<!-- Slick -->
<script src="<?php echo base_url() ?>assets/vendors/slick/slick.min.js"></script>

<!-- Vamp -->
<script src="<?php echo base_url() ?>assets/vendors/vmap/jquery.vmap.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/vmap/maps/jquery.vmap.usa.js"></script>
<script src="<?php echo base_url() ?>assets/js/examples/vmap.js"></script>

<script src="<?php echo base_url() ?>assets/js/format_rp.js"></script>

<!-- Dashboard scripts -->
<script src="<?php echo base_url() ?>assets/js/examples/dashboard.js"></script>
<div class="colors"> <!-- To use theme colors with Javascript -->
    <div class="bg-primary"></div>
    <div class="bg-primary-bright"></div>
    <div class="bg-secondary"></div>
    <div class="bg-secondary-bright"></div>
    <div class="bg-info"></div>
    <div class="bg-info-bright"></div>
    <div class="bg-success"></div>
    <div class="bg-success-bright"></div>
    <div class="bg-danger"></div>
    <div class="bg-danger-bright"></div>
    <div class="bg-warning"></div>
    <div class="bg-warning-bright"></div>
</div>

<!-- App scripts -->
<script src="<?php echo base_url() ?>assets/js/app.min.js"></script>

<script type="text/javascript">
  $('.datepicker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    });
</script>

</body>

</html>