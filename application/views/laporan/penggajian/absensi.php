<!-- begin::header -->

<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Laporan Absensi</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Absensi</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Buku Besar</h5>
                </div>

                <div class="card-body">
                    
                    <form method="GET">
                <div class="row">

                  <div class="col-md-2">
                    <label><b>Bulan</b></label>
                    <select class="form-control" name="bulan" required="">
                      <option value="">Pilih</option>
                      <?php for ($i = 1 ; $i <= 12 ; $i++){ ?>
                          <option <?php if($this->input->get('bulan') == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo get_monthname($i) ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="col-md-2">
                    <label><b>Tahun</b></label>
                    <select class="form-control" name="tahun" required="">
                      <option value="">Pilih</option>
                      <?php for ($i = 2019 ; $i <= (date('Y')+ 1) ; $i++){ ?>
                          <option <?php if($this->input->get('tahun') == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="col-md-1">
                    <br>
                    <button style="margin-top: 8px" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>

              <br>

              <?php if($this->input->get('bulan')){ ?>

                  <hr>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="col-md-8">
                        <center>
                          <h4>PT. ABC</h4>
                          <h5>LAPORAN ABSENSI</h5>
                          Periode Bulan <?php echo get_monthname($this->input->get('bulan'))." Tahun ".$this->input->get('tahun') ?>
                        </center>
                      </div>
                      <div class="col-md-2"></div>
                    </div>
                  <hr>

                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th rowspan="2">No</th>
                          <th rowspan="2">Pegawai</th>
                          <th class="text-center" colspan="2">Kehadiran</th>
                          <th class="text-center" colspan="4">Perizinan</th>
                        </tr>

                        <tr>
                          <th>Tepat Waktu</th>
                          <th>Terlambat</th>
                          <th>Cuti</th>
                          <th>Dinas</th>
                          <th>Izin</th>
                          <th>Sakit</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php 
                        $n = 0;
                        foreach ($absensi as $row){ $n++; ?>
                          
                          <tr>
                            <td><?= $n ?></td>
                            <td><?= $row['pegawai']['kode_pegawai']." / ".$row['pegawai']['nama_pegawai'] ?></td>

                            <?php foreach ($row['absensi'] as $abs){ 
                              if($abs == ''){
                                $abs = 0;
                              }
                            ?>
                              <td class="text-center"><?= $abs ?></td>
                            <?php } ?>
                          </tr>

                       <?php } ?>
                      </tbody>
                    </table>
                  </div>

              <?php } ?>
              

            

                </div>
            </div>
        </div>
    </div>
    
</main>