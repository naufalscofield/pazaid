<!-- begin::header -->

<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Laporan Penggajian</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gaji</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Gaji</h5>
                </div>

                <div class="card-body">
                    
                    <form method="GET">
                <div class="row">
                  <div class="col-md-3">
                    <label><b>Gaji</b></label>
                    <select class="form-control" name="gaji_id" required="">
                      <option value="">Pilih</option>
                      <?php foreach ($gaji as $row) { ?>
                          <option <?php if($row['id'] == $this->input->get('gaji_id')){ echo "selected='selected'"; } ?> value="<?php echo $row['id'] ?>"><?php echo get_monthname($row['bulan'])." ".$row['tahun'] ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="col-md-1">
                    <br>
                    <button style="margin-top: 8px" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>

              <br>

              <?php if($this->input->get('gaji_id')){ ?>
                  <hr>
                    <div class="row">
                      <div class="col-md-2"></div>
                      <div class="col-md-8">
                        <center>
                          <h4>PT. ABC</h4>
                          <h5>LAPORAN PEGGAJIAN</h5>
                          Periode Bulan <?php echo get_monthname($detail['bulan'])." Tahun ".$detail['tahun'] ?>
                        </center>
                      </div>
                      <div class="col-md-2"></div>
                    </div>
                  <hr>

                  <table class="table">
                    <tr>
                      <th style="width: 20%; background-color: #eee" class="">WAKTU</th>
                      <td><?php echo get_monthname($detail['bulan'])." ".$detail['tahun'] ?></td>
                    </tr>
                    <tr>
                      <th style="background-color: #eee">JUMLAH PEGAWAI</th>
                      <td><?php echo $detail['total_pegawai'] ?></td>
                    </tr>
                    <tr>
                      <th style="background-color: #eee">TOTAL</th>
                      <td>Rp. <?php echo number_format($detail['total_gaji'],0,'','.') ?></td>
                    </tr>
                  </table>

                  <br><br>

                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th style="width:5%;vertical-align: middle;" class="text-center" rowspan="2">NO</th>
                          <th style="width:23%;vertical-align: middle;" rowspan="2">NAMA</th>
                          <th colspan="2" class="text-center">KEHADIRAN</th>
                          <th colspan="4" class="text-center">KOMPONEN GAJI</th>
                          <th rowspan="2" class="text-center">TOTAL GAJI</th>
                        </tr>
                        <tr>
                          <th class="text-center">H</th>
                          <th class="text-center">T</th>
                          <th align="center"><center>Gaji</center></th>
                          <th class="text-center">Lembur</th>
                          <th class="text-center">Tunjangan</th>
                          <th class="text-center">Pph</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php $n=0; foreach ($list as $row) { $n++;?>
                        <tr>
                          <td><?php echo $n ?></td>
                          <td><?php echo $row['kode_pegawai']." / ".$row['nama_pegawai'] ?></td>
                          <td class="text-center"><?php echo $row['total_hadir'] ?></td>
                          <td class="text-center"><?php echo $row['total_terlambat'] ?></td>
                          <td style="text-align: right">Rp. <?php echo number_format($row['gaji_pokok'],0,'','.') ?></td>
                          <td style="text-align: right">Rp. <?php echo number_format($row['tunjangan_lembur'],0,'','.') ?></td>
                          <td style="text-align: right">
                            Rp. <?php echo number_format($row['tunjangan_lain'],0,'','.') ?>
                          </td>
                          <td style="text-align: right">Rp. <?php echo number_format($row['pph'],0,'','.') ?></td>

                          <td style="text-align: right">Rp. <?php echo number_format($row['total_gaji'],0,'','.') ?></td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
              <?php } ?>
              


                </div>
            </div>
        </div>
    </div>
    
</main>