<!-- begin::header -->

<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Laporan</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Perbaikan</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Laporan Perbaikan</h5>
                </div>

                <div class="card-body">
                    
                    <form method="GET">
                      <div class="row">

                        <div class="col-md-2">
                          <label><b>Bulan</b></label>
                          <select class="form-control" name="bulan" required="">
                            <option value="">Pilih</option>
                            <?php for ($i = 1 ; $i <= 12 ; $i++){ ?>
                                <option <?php if($this->input->get('bulan') == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo get_monthname($i) ?></option>
                            <?php } ?>
                          </select>
                        </div>

                        <div class="col-md-2">
                          <label><b>Tahun</b></label>
                          <select class="form-control" name="tahun" required="">
                            <option value="">Pilih</option>
                            <?php for ($i = 2019 ; $i <= date('Y') ; $i++){ ?>
                                <option <?php if($this->input->get('tahun') == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                            <?php } ?>
                          </select>
                        </div>

                        <div class="col-md-1">
                          <br>
                          <button class="btn btn-primary btn-sm"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </form>

                    <br>

                    <?php if($this->input->get('bulan')){ ?>
                        <div class="text-center">
                          <hr>
                          <h3>PT. ABC</h3>
                          <h4>LAPORAN PERBAIKAN ASET</h4>

                          Periode
                          <h5>
                          <?php 
                            if($this->input->get('bulan') != 'all'){
                              echo " Bulan ".get_monthname($this->input->get('bulan'));
                            }

                            if($this->input->get('tahun') != 'all'){
                              echo " Tahun ".$this->input->get('tahun');
                            }

                           ?>
                          </h5>
                          <hr>
                        </div>

                        <table class="table">
                          <thead style="background-color:#eee">
                            <tr>
                              <th style="width: 5%">No</th>
                              <th>Transaksi</th>
                              <th>Tanggal</th>
                              <th>Pembayaran</th>
                              <th>Total Transaksi</th>
                              <th>Sudah Dibayar</th>
                              <th>Belum Dibayar</th>
                            </tr>
                          </thead>

                          <tbody>
                          <?php 
                            $n = $tt = $tb = $sb = 0;
                            foreach ($list as $row) { $n++; 
                              $tt += $row['total_transaksi'];
                              $tb += $row['total_bayar'];
                              $sb += $row['total_transaksi'] - $row['total_bayar'];
                          ?>
                              <tr>
                                <td><?= $n ?></td>
                                <td><?= $row['kode_transaksi']."<br><br><small style='font-size:12px'>".$row['nama_vendor']."</small>" ?></td>
                                <td><?= $row['tanggal_transaksi'] ?></td>
                                <td><?= $row['pembayaran']." - ".$row['metode'] ?></td>
                                <td class="text-right"><?= format_rp($row['total_transaksi']) ?></td>
                                <td class="text-right"><?= format_rp($row['total_bayar']) ?></td>
                                <td class="text-right"><?= format_rp($row['total_transaksi'] - $row['total_bayar']) ?></td>
                              </tr>
                            <?php } ?>

                            <tr>
                              <th colspan="4"><b>TOTAL</b></th>
                              <th class="text-right"><?= format_rp($tt) ?></th>
                              <th class="text-right"><?= format_rp($tb) ?></th>
                              <th class="text-right"><?= format_rp($sb) ?></th>
                            </tr>
                          </tbody>
                        </table>
                    <?php } ?>


                </div>
            </div>
        </div>
    </div>
    
</main>