<!-- begin::header -->

<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Laporan Kas Keluar</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Kas Keluar</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Kas Keluar</h5>
                </div>

                <div class="card-body">
                    
                    <form method="GET">
                <div class="row">

                  <div class="col-md-2">
                    <label><b>Bulan</b></label>
                    <select class="form-control" name="bulan" required="">
                      <option value="">Pilih</option>
                      <?php for ($i = 1 ; $i <= 12 ; $i++){ ?>
                          <option <?php if($this->input->get('bulan') == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo get_monthname($i) ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="col-md-2">
                    <label><b>Tahun</b></label>
                    <select class="form-control" name="tahun" required="">
                      <option value="">Pilih</option>
                      <?php for ($i = 2019 ; $i <= (date('Y')+ 1) ; $i++){ ?>
                          <option <?php if($this->input->get('tahun') == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="col-md-1">
                    <br>
                    <button style="margin-top: 8px" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>

              <br>

              <?php if($this->input->get('bulan')){ ?>
                <hr>
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                      <center>
                        <h4>PT. ABC</h4>
                        <h5>LAPORAN KAS KELUAR</h5>
                        Periode Bulan <?php echo get_monthname($this->input->get('bulan'))." Tahun ".$this->input->get('tahun') ?>
                      </center>
                    </div>
                    <div class="col-md-2"></div>
                  </div>
                <hr>

                <div class="table-responsive">
                  <table class="display table table-hover datatables" >
                    <thead class="bg-primary">
                      <tr>
                        <th style="width: 5%">No</th>
                        <th>Kode</th>
                        <th>Tanggal Transaksi</th>
                        <th>Total Transaksi</th>

                        <th class="text-center">Pembayaran</th>
                        <th>Total Bayar</th>
                        <th class="text-center">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $n = 0;
                            $total = 0;
                                    foreach ($list as $row) { $n++; $total += $row['total_bayar'] ?>

                                      <tr>
                                        <td><?php echo $n ?></td>
                                        <td><?php echo $row['kode_transaksi'] ?><br><small class="text-muted"><?= $row['tipe'] ?></small></td>
                                            <td><?php echo $row['tanggal_transaksi'] ?></td>
                                            <td class="text-right"><?php echo format_rp($row['total_transaksi']) ?></td>
                                            <td class="text-center">
                                              <?php echo $row['pembayaran'] ?>
                                            </td>

                                            <td class="text-right"><?php echo format_rp($row['total_bayar']) ?></td>
                                            <td class="text-center">

                                              <?php
                                                  if($row['tipe'] == 'perbaikan'){
                                                      if($row['level'] == '3'){
                                                          if($row['level_vendor'] == '1'){
                                                              echo "<span class='badge badge-primary'>Menunggu konfirmasi vendor</span>";
                                                          
                                                          }else if($row['level_vendor'] == '2'){
                                                              echo "<span class='badge badge-warning'>Menunggu konfimasi harga</span>";
                                                          }else if($row['level_vendor'] == '3'){
                                                              echo "<span class='badge badge-info'>Menunggu penyelesaian ".$row['tipe']."</span>";
                                                          }else if($row['level_vendor'] == '4'){
                                                              echo "<span class='badge badge-success'><i class='fa fa-check'></i> Selesai</span>";
                                                          }
                                                      }else{
                                                          echo "<span class='badge badge-primary'>Menunggu Acc Bag. Keuangan</span>";
                                                      }
                                                      
                                                  }else{
                                                      echo show_level($row['level']);
                                                  }
                                               ?></td>

                                      </tr>

                              <?php } ?>

                              <tr>
                                <th colspan="2">TOTAL</th>
                                <th colspan="4" class="text-right"><?= format_rp($total) ?></th>
                                <th></th>
                              </tr>
                    </tbody>
                  </table>
                </div>
              <?php } ?>

                </div>
            </div>
        </div>
    </div>
    
</main>