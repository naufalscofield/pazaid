<!-- begin::header -->

<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Perubahan Modal</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Perubahan Modal</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Laporan Perubahan Modal</h5>
                </div>

                <div class="card-body">
                    
                    <form method="GET">
                      <div class="row">
                         <div class="col-md-2">
                          <label><b>Bulan</b></label>
                          <select class="form-control" name="bulan" required="">
                            <option value="">Pilih</option>
                            <?php for ($i = 1 ; $i <= 12 ; $i++){ ?>
                                <option <?php if($this->input->get('bulan') == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo get_monthname($i) ?></option>
                            <?php } ?>
                          </select>
                        </div>

                        <div class="col-md-2">
                          <label><b>Tahun</b></label>
                          <select class="form-control" name="tahun" required="">
                            <option value="">Pilih</option>
                            <?php for ($i = 2019 ; $i <= (date('Y')+ 1) ; $i++){ ?>
                                <option <?php if($this->input->get('tahun') == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                            <?php } ?>
                          </select>
                        </div>

                        <div class="col-md-1">
                          <br>
                          <button style="margin-top: 8px" class="btn btn-primary"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </form>

                    <br>

                    <?php if($this->input->get('bulan')){ ?>

                    
                    <hr>
                        <div class="row">
                          <div class="col-md-2"></div>
                          <div class="col-md-8">
                            <center>
                              <h4>PT. ABC</h4>
                              <h5>LAPORAN PERUBAHAN MODAL</h5>
                              Periode Bulan <?php echo $this->input->get('bulan')." Tahun ".$this->input->get('tahun') ?>
                            </center>
                          </div>
                          <div class="col-md-2"></div>
                        </div>
                      <hr>

                      <div class="row">
                        <div class="col-md-12">
                          <table class="table">

                            <?php if($laba_rugi < 0){ 
                                $text1 = 'Rugi';
                            }else{
                                $text1 = 'Laba Bersih';
                            } ?>

                            <tr>
                              <td>Modal</td>
                              <td></td>
                              <td class="text-right"><?= format_rp($modal) ?></td>
                            </tr>
                            <tr>
                              <td><?= $text1 ?></td>
                              <td class="text-right"><?= format_rp($laba_rugi) ?></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td>Prive</td>
                              <td class="text-right"><?= format_rp($prive) ?></td>
                              <td></td>
                            </tr>

                            <?php 

                                $penambahan_modal = $laba_rugi - $prive;

                                if($penambahan_modal < 0){
                                  $text2 = 'Pengurangan Modal';
                                }else{
                                  $text2 = 'Penambahan Modal';
                                }

                            ?>
                            <tr>
                              <td><?= $text2 ?></td>
                              <td></td>
                              <td class="text-right"><?= format_rp($penambahan_modal) ?></td>
                            </tr>
                            <tr>
                              <th>Modal Akhir Semester <?= $this->input->get('semester')." Tahun ".$this->input->get('tahun') ?></th>
                              <td></td>
                              <th class="text-right"><?= format_rp($modal + $penambahan_modal) ?></th>
                            </tr>
                          </table>
                        </div>
                      </div>

                  <?php } ?>
                </div>
            </div>
        </div>
    </div>
    
</main>