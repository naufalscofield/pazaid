<!-- begin::header -->

<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Laba Rugi</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Laba Rugi</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Laba Rugi</h5>
                </div>

                <div class="card-body">
                    
                    <form method="GET">
                      <div class="row">

                        <div class="col-md-2">
                          <label><b>Bulan</b></label>
                          <select class="form-control" name="bulan" required="">
                            <option value="">Pilih</option>
                            <?php for ($i = 1 ; $i <= 12 ; $i++){ ?>
                                <option <?php if($this->input->get('bulan') == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo get_monthname($i) ?></option>
                            <?php } ?>
                          </select>
                        </div>

                        <div class="col-md-2">
                          <label><b>Tahun</b></label>
                          <select class="form-control" name="tahun" required="">
                            <option value="">Pilih</option>
                            <?php for ($i = 2019 ; $i <= (date('Y')+ 1) ; $i++){ ?>
                                <option <?php if($this->input->get('tahun') == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                            <?php } ?>
                          </select>
                        </div>

                        <div class="col-md-1">
                          <br>
                          <button style="margin-top: 8px" class="btn btn-primary"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </form>

              <br>

              <hr>
                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-md-8">
                    <center>
                      <h4>PT. ABC</h4>
                      <h5>LAPORAN LABA RUGI</h5>
                      Periode Bulan <?php echo get_monthname($this->input->get('bulan'))." Tahun ".$this->input->get('tahun') ?>
                    </center>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              <hr>

              <?php if($this->input->get('bulan')){ ?>

                    <div class="table-responsive">
                      <table class="table">
                        <tbody>
                          <tr style="background-color: #eee">
                            <th>PENDAPATAN</th>
                            <th></th>
                            <th colspan="2"></th>
                          </tr>
                          <?php 
                            $total_pendapatan = 0;
                            $totalDaftar = 0;
                            foreach ($pendapatan as $row){ $total_pendapatan += $row['total_pendapatan']; 
                            ?>
                              <tr>
                                <td>&emsp;&emsp;<?= $row['tipe'] ?></td>
                                <td class="text-right"><?= format_rp($row['total_pendapatan']) ?></td>
                                <td></td>
                              </tr>
                          <?php } ?>
                          <?php
                          ?>
                              <tr>
                                <td>&emsp;&emsp;<?= $pendaftaran['tipe'] ?></td>
                                <td class="text-right"><?= format_rp($pendaftaran['totalPendaftaran']) ?></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>&emsp;&emsp;<?= $seragam['tipe'] ?></td>
                                <td class="text-right"><?= format_rp($seragam['totalSeragam']) ?></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>&emsp;&emsp;<?= $dpp['tipe'] ?></td>
                                <td class="text-right"><?= format_rp($dpp['totalDpp']) ?></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>&emsp;&emsp;<?= $dsk['tipe'] ?></td>
                                <td class="text-right"><?= format_rp($dsk['totalDsk']) ?></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>&emsp;&emsp;<?= $lainnya['tipe'] ?></td>
                                <td class="text-right"><?= format_rp($lainnya['totalTagihan']) ?></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>&emsp;&emsp;<?= $daycare['tipe'] ?></td>
                                <td class="text-right"><?= format_rp($daycare['tagihDaycare']) ?></td>
                                <td></td>
                              </tr>
                              <?php $totalAkhir = 0; ?>
                              <?php $totalAkhir = $pendaftaran['totalPendaftaran'] + $seragam['totalSeragam'] + $dpp['totalDpp'] + $dsk['totalDsk'] + $lainnya['totalTagihan'] + $daycare['tagihDaycare'] ?>
                              <?php $hasil = $totalAkhir + $total_pendapatan?>
                          <tr>
                            <th>TOTAL</th>
                            <th></th>
                            <th class="text-right"><?= format_rp($hasil) ?></th>
                          </tr>

                          <tr style="background-color: #eee">
                            <th>PENGELUARAN</th>
                            <th colspan="2"></th>
                          </tr>
                          <?php 
                          $total_pengeluaran = 0;
                          foreach ($pengeluaran as $row){ $total_pengeluaran += $row['total_pengeluaran']; ?>
                                  <tr>
                                    <td>&emsp;&emsp;<?= $row['tipe'] ?></td>
                                    <td class="text-right"><?= format_rp($row['total_pengeluaran']) ?></td>
                                    <td></td>
                                  </tr>
                          <?php } ?>
                          <tr>
                            <th>TOTAL</th>
                            <th></th>
                            <th class="text-right"><?= format_rp($total_pengeluaran) ?></th>
                          </tr>

                          <?php 

                            $grandTotal = $hasil - $total_pengeluaran;
                            if($grandTotal >= 0){
                              $title = 'LABA';
                              $class = 'bg-success';
                            }else{
                              $title = 'RUGI';
                              $class = 'bg-danger';
                              $grandTotal = str_replace('-', '', $grandTotal);
                            }
                          ?>

                          <tr class="<?= $class ?>">
                            <th><h5><?= $title ?></h5></th>
                            <th></th>
                            <th class="text-right"><h5><?= format_rp($grandTotal) ?></h5></th>
                          </tr>

                        </tbody>
                      </table>
                    </div>

              <?php } ?>

        </div>
    </div>
    
</main>