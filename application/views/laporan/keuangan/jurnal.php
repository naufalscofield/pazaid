<!-- begin::header -->

<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Jurnal Umum</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Jurnal umum</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Jurnal umum</h5>
                </div>

                <div class="card-body">
                    
                    <form method="GET">
                      <div class="row">
                        <div class="col-md-3">
                          <label>Tanggal Mulai</label>
                          <input autocomplete="off" value="<?php echo $this->input->get('start') ?>" type="text" name="start" class="form-control datepicker" required="" placeholder="Tanggal Mulai...">
                        </div>

                        <div class="col-md-3">
                          <label>Tanggal Selesai</label>
                          <input autocomplete="off" value="<?php echo $this->input->get('end') ?>" type="text" name="end" class="form-control datepicker" required="" placeholder="Tanggal Selesai...">
                        </div>

                        <div class="col-md-2">
                          <br>
                          <button style="margin-top: 8px" class="btn btn-primary"><i class="fa fa-search"></i></button>
                        </div>

                        <div class="col-md-4 text-right">
                          <a href="<?php echo site_url('laporan/jurnal') ?>" class="btn btn-success btn-flat">Lihat Semua</a>
                        </div>
                      </div>
                    </form>

                    <br>

                    <hr>
                        <div class="row">
                          <div class="col-md-2"></div>
                          <div class="col-md-8">
                            <center>
                              <h4>PT. ABC</h4>
                              <h5>JURNAL UMUM</h5>
                              <?php if($this->input->get('start')){
                                echo "Periode ".$this->input->get('start')." s/d ".$this->input->get('end');
                              } ?>
                            </center>
                          </div>
                          <div class="col-md-2"></div>
                        </div>
                      <hr>

                    <div class="table-responsive">
                      <table class="table datatable">
                        <thead>
                          <tr>
                            <th>Tanggal</th>
                            <th style="width: 25%">Akun</th>
                            <th style="width: 5%">Ref</th>
                            <th class="text-center">Debit</th>
                            <th class="text-center">Kredit</th>
                          </tr>
                        </thead>

                        <tbody id="tableItem">
                          <?php $n = $total_debit = $total_kredit = 0; 
                                foreach ($jurnal['list'] as $row){
                                  if($row['alt_name'] != ''){
                                    $row['nama_coa'] = $row['alt_name'];
                                  }

                                  if($row['posisi'] == 'debit'){
                                    $title = $row['nama_coa'];
                                    $total_debit += $row['nominal'];
                                  
                                  }else{
                                    $title = '&emsp;&emsp;&emsp;'.$row['nama_coa'];
                                    $total_kredit += $row['nominal'];
                                  }
                          ?>

                                  <tr>
                                    <td><?php echo date('Y-m-d',strtotime($row['tgl_jurnal'])) ?></td>
                                    <td><?php echo $title ?></td>
                                    <td><?php echo $row['kode_coa'] ?></td>

                                    <?php if($row['posisi'] == 'debit'){ ?>
                                        <td class="text-right"><?php echo format_rp($row['nominal']) ?></td>
                                        <td></td>
                                    <?php }else{ ?>
                                        <td></td>
                                        <td class="text-right"><?php echo format_rp($row['nominal']) ?></td>
                                    <?php } ?>

                                  </tr>

                          <?php } ?>

                        </tbody>

                          <tr>
                            <td colspan="3"><h4><b>TOTAL</b></h4></td>
                            <td class="text-right"><h6><b id="grandTotal"><?php echo format_rp($total_debit) ?></b></h6></td>
                            <td class="text-right"><h6><b id="grandTotal"><?php echo format_rp($total_kredit) ?></b></h6></td>
                          </tr>

                      </table>
                    </div>


                </div>
            </div>
        </div>
    </div>
    
</main>