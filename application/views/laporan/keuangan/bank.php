<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Laporan Penerimaan Bank</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Keuangan</a></li>
                    <li class="breadcrumb-item"><a href="#">Laporan Penerimaan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Penerimaan Bank</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Laporan Peneriman Bank</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
                    <div class="col-md-12">
                      <?php echo $this->session->flashdata('alert_message') ?>
                    </div>
                  </div>

                  <!-- <button data-toggle="modal" data-target="#modalTambahKTG" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah Pencatatan</button> -->
                          <!-- <br><br> -->

                  <div class="table-responsive">
                    <table class="display table table-primary datatables" >
                      <thead style="background-color: #eee">
                        <tr>
                          <th style="width: 5%">No</th>
                          <th>Tanggal Transaksi</th>
                          <th>Total Pembayaran</th>
                          <th>Jenis Pembayaran</th>
                          <th>Status Pembayaran</th>
                          <th class="text-center"><i class="fa fa-cog"></i></th>
                        </tr>
                      </thead>
                      <tbody>
                      
                        <?php $n = 0;
                              foreach ($pendaftaran as $row) { $n++; ?>

                                <tr>
                                  <td style="width: 5%"><?php echo $n ?></td>
                                  <td><?php echo $row['tanggal_transaksi'] ?></td>
                                  <td><?php echo format_rp($row['total_pembayaran']) ?></td>
                                  <td><?php if($row['jenis_pembayaran'] = "transfer"){ ?>
                                    Transfer
                                  <?php }else{ ?>
                                  ***
                                  <?php } ?>
                                  <td><?php if($row['status_bayar'] = "Sudah Bayar"){ ?>
                                      <t class="badge badge-primary"><?php echo $row['status_bayar']?></t>
                                  <?php }else{ ?>
                                      <t class="badge badge-danger"><?php echo $row['status_bayar']?></t>
                                  <?php } ?>
                                  </td>
                                  
                                </tr>

                        <?php } ?>
                        <?php foreach ($spp as $row) { $n++; ?>

                            <tr>
                                <td style="width: 5%"><?php echo $n ?></td>
                                <td><?php echo $row['tanggal_transaksi'] ?></td>
                                <td><?php echo format_rp($row['biaya_spp']) ?></td>
                                <td><?php if($row['jenis_pembayaran'] = "transfer"){ ?>
                                  Transfer
                                  <?php }else{ ?>
                                  ***
                                  <?php } ?>
                                </td>
                                <td><?php if($row['status_spp'] = "Sudah Dibayar"){ ?>
                                      <t class="badge badge-primary"><?php echo $row['status_spp']?></t>
                                  <?php }else{ ?>
                                      <t class="badge badge-danger"><?php echo $row['status_spp']?></t>
                                  <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php foreach ($lainnya as $row) { $n++; ?>

                            <tr>
                                <td style="width: 5%"><?php echo $n ?></td>
                                <td><?php echo $row['tanggal_transaksi'] ?></td>
                                <td><?php echo format_rp($row['total_tagihan']) ?></td>
                                <td><?php if($row['jenis_pembayaran'] = "transfer"){ ?>
                                    Transfer
                                  <?php }else{ ?>
                                    ***
                                  <?php } ?>
                                </td>
                                <td><?php if($row['status_lainnya'] = "Lunas"){ ?>
                                      <t class="badge badge-primary"><?php echo $row['status_lainnya']?></t>
                                  <?php }else{ ?>
                                      <t class="badge badge-danger"><?php echo $row['status_lainnya']?></t>
                                  <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php foreach ($daycare as $row) { $n++; ?>

                            <tr>
                                <td style="width: 5%"><?php echo $n ?></td>
                                <td><?php echo $row['created_at'] ?></td>
                                <td><?php echo format_rp($row['total_tagihan']) ?></td>
                                <td><?php if($row['jenis_pembayaran'] = "transfer"){ ?>
                                  Transfer
                                  <?php }else{ ?>
                                  ***
                                  <?php } ?>
                                </td>
                                <td><?php if($row['status'] = "Sudah Bayar"){ ?>
                                      <t class="badge badge-primary"><?php echo $row['status']?></t>
                                  <?php }else{ ?>
                                      <t class="badge badge-danger"><?php echo $row['status']?></t>
                                  <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>

                      </tbody>
                    </table>
                  </div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->


<div class="modal fade" id="modalKu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Rincian Rekening Koran</h4>
          <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>

        <div class="modal-body">

          <div class="form-group" id="rekBody">
              <label>Nama Bank</label>
              <input type="hidden" readonly name="nama_bank" id="e_id" class="form-control" placeholder="" id="bank"/>
              <input type="text" readonly name="nama_bank" id="e_bank" class="form-control" placeholder="" id="bank"/>
          </div>

          <div class="form-group">
              <label>Atas Nama</label>
              <input type="text" readonly name="atas_nama" id="e_nama" class="form-control" placeholder="" id="nama"/>
          </div>

          <div class="form-group">
              <label>No Rekening</label>
              <input type="text" readonly name="no_rek" id="e_rek" class="form-control" placeholder="" id="norek"/>
          </div>
          
          <div class="table-responsive">
            <table id="example" class="display nowrap" style="width:100%">
              <thead>
                  <tr>
                      <th>Tanggal</th>
                      <th>Saldo Akhir</th>
                      <th>Potongan Bunga</th>
                      <th>Perolehan Bunga</th>
                      <th>Deskripsi</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td id="e_tanggal"></td>
                      <td id="e_saldoAkhir"></td>
                      <td id="e_potonganBunga"></td>
                      <td id="e_perolehanBunga"></td>
                      <td id="e_deskripsi"></td>
                  </tr>
                </tbody>
              </table>
            </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Tutup</button>
        </div>

      </div>
  </div>
</div>

<script>
function edit(id, bank, nama, rek, tanggal, saldoAkhir, potonganBunga, perolehanBunga, deskripsi){

$('#e_id').val(id);
$('#e_bank').val(bank);
$('#e_nama').val(nama);
$('#e_rek').val(rek);
$('#e_tanggal').text(tanggal);
$('#e_saldoAkhir').text(format_rp(saldoAkhir));
$('#e_potonganBunga').text(potonganBunga+' %');
$('#e_perolehanBunga').text(format_rp(perolehanBunga));
$('#e_deskripsi').text(deskripsi);


$('#modalKu').modal('show'); 
}

$(document).ready(function() {
    $('#example').DataTable( {
        "scrollX": true
    } );
} );
</script>
