<!-- begin::header -->

<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Buku Besar</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Buku Besar</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Buku Besar</h5>
                </div>

                <div class="card-body">
                    
                    <form method="GET">
                <div class="row">
                  <div class="col-md-3">
                    <label><b>Akun</b></label>
                    <select class="form-control" name="coa_id" required="">
                      <option value="">Pilih</option>
                      <?php foreach ($akun as $row) { ?>
                          <option <?php if($row['id'] == $this->input->get('coa_id')){ echo "selected='selected'"; } ?> value="<?php echo $row['id'] ?>"><?php echo $row['kode_coa']." - ".$row['nama_coa'] ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="col-md-2">
                    <label><b>Bulan</b></label>
                    <select class="form-control" name="bulan" required="">
                      <option value="">Pilih</option>
                      <?php for ($i = 1 ; $i <= 12 ; $i++){ ?>
                          <option <?php if($this->input->get('bulan') == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo get_monthname($i) ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="col-md-2">
                    <label><b>Tahun</b></label>
                    <select class="form-control" name="tahun" required="">
                      <option value="">Pilih</option>
                      <?php for ($i = 2019 ; $i <= (date('Y')+ 1) ; $i++){ ?>
                          <option <?php if($this->input->get('tahun') == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="col-md-1">
                    <br>
                    <button style="margin-top: 8px" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>

              <br>

              <hr>
                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-md-8">
                    <center>
                      <h4>PT. ABC</h4>
                      <h5>LAPORAN BUKU BESAR</h5>
                      Periode Bulan <?php echo get_monthname($this->input->get('bulan'))." Tahun ".$this->input->get('tahun') ?>
                    </center>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              <hr>

              <div class="table-responsive">
                  <table class="table datatable">
                    <thead class="bg-primary">
                      <tr>
                        <th rowspan="2">Tanggal</th>
                        <th rowspan="2" style="width: 25%">Akun</th>
                        <th rowspan="2" style="width: 5%">Ref</th>
                        <th rowspan="2" class="text-center">Debit</th>
                        <th rowspan="2" class="text-center">Kredit</th>
                        <th colspan="2" class="text-center">Saldo</th>
                      </tr>

                      <tr>
                        <th class="text-center">Debit</th>
                        <th class="text-center">Kredit</th>
                      </tr>
                    </thead>

                    <?php 

                      $saldo = $jurnal['saldo_awal'];

                    ?>

                    <tbody id="tableItem">

                      <tr>
                        <th colspan="5"><h6><b>SALDO AWAL</b></h6></th>

                        <?php if($saldo > 0){ ?>
                            <td class="text-right"><h6><b><?php echo format_rp($saldo) ?></b></h6></td>
                            <td class="text-right"></td>


                        <?php }else{ ?>

                            <td class="text-right"></td>
                            <td class="text-right"><h6><b><?php echo str_replace('-', '', format_rp($saldo))?></b></h6></td>

                        <?php } ?>
                      </tr>


                      <?php 

                      if($this->input->get('coa_id')){ ?>

                          <?php 
                                
                                $n = $total_debit = $total_kredit = 0; 
                                foreach ($jurnal['list'] as $row) { 
                                  if($row['posisi'] == 'debit'){
                                    $saldo += $row['nominal'];
                                    $title = $row['nama_coa'];
                                    $total_debit += $row['nominal'];

                                  }else{
                                    $saldo -= $row['nominal'];
                                    $title = '&emsp;&emsp;&emsp;'.$row['nama_coa'];
                                    $total_kredit += $row['nominal'];
                                  }
                          ?>

                                  <tr>
                                    <td>
                                      <?php echo date('Y-m-d',strtotime($row['tgl_jurnal'])) ?>
                                    </td>
                                    <td><?php echo $title ?></td>
                                    <td><?php echo $row['kode_coa'] ?></td>

                                    <?php if($row['posisi'] == 'debit'){ ?>
                                        <td class="text-right"><?php echo format_rp($row['nominal']) ?></td>
                                        <td></td>
                                    <?php }else{ ?>
                                        <td></td>
                                        <td class="text-right"><?php echo str_replace('-', '', format_rp($row['nominal']))?></td>
                                    <?php } ?>

                                    
                                    <?php if($saldo > 0){ ?>
                                        <td class="text-right"><?php echo format_rp($saldo) ?></td>
                                        <td class="text-right"></td>


                                    <?php }else{ ?>

                                        <td class="text-right"></td>
                                        <td class="text-right"><?php echo str_replace('-', '', format_rp($saldo))?></td>

                                    <?php } ?>

                                  </tr>

                          <?php } ?>

                      <?php } ?>

                    </tbody>

                      <tr>
                        <td colspan="5"><h6><b>SALDO AKHIR</b></h6></td>
                        <?php if($saldo > 0){ ?>
                            <td class="text-right"><h6><b id="grandTotal"><?php echo format_rp($saldo) ?></b></h6></td>
                            <td class="text-right"><h4><b id="grandTotal"></td>


                        <?php }else{ ?>

                            <td class="text-right"><h4><b id="grandTotal"></td>
                            <td class="text-right"><h6><b id="grandTotal"><?php echo str_replace('-', '', format_rp($saldo))?></b></h6></td>

                        <?php } ?>
                        
                      </tr>

                  </table>
                </div>

                </div>
            </div>
        </div>
    </div>
    
</main>