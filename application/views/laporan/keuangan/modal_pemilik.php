<!-- begin::header -->

<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Kartu Modal Pemilik</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Kartu Modal Pemilik</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Kartu Modal Pemilik</h5>
                </div>

                <div class="card-body">
                    
                    <form method="GET">
                      <div class="row">
                        <div class="col-md-3">
                          <label><b>Pemilik</b></label>
                          <select class="form-control" name="pemilik_id" required="">
                            <option value="">Pilih</option>
                            <?php foreach ($pemilik as $row) { ?>
                                <option <?php if($row['id'] == $this->input->get('pemilik_id')){ echo "selected='selected'"; } ?> value="<?php echo $row['id'] ?>"><?php echo $row['nama_pemilik'] ?></option>
                            <?php } ?>
                          </select>
                        </div>

                        <div class="col-md-1">
                          <br>
                          <button style="margin-top: 8px" class="btn btn-primary"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </form>

                    <br>

                    <?php if($this->input->get('pemilik_id')){ ?>

                      <hr>
                        <div class="row">
                          <div class="col-md-2"></div>
                          <div class="col-md-8">
                            <center>
                              <h4>PT. ABC</h4>
                              <h5>KARTU MODAL PEMILIK</h5>
                              <?= $detail['nama_pemilik'] ?>
                            </center>
                          </div>
                          <div class="col-md-2"></div>
                        </div>
                      <hr>

                      <div class="row">
                        <div class="col-md-12">
                          <table class="table">
                            <thead style="background-color: #eee">
                              <tr>
                                <th>Kode Transaksi</th>
                                <th>Tipe</th>
                                <th>Tanggal</th>
                                <th class="text-center">Nominal</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                                $total = 0;
                                foreach ($list as $row) { 
                                  if($row['tipe'] == 'penarikan'){
                                    $total -= $row['total_transaksi'];
                                  }else{
                                    $total += $row['total_transaksi'];
                                  }
                              ?>
                                <tr>
                                  <td><?= $row['kode_transaksi'] ?></td>
                                  <td><?= $row['tipe'] ?></td>
                                  <td><?= $row['tanggal_transaksi'] ?></td>
                                  <td class="text-right"><?= format_rp($row['total_transaksi']) ?></td>
                                </tr>
                              <?php } ?>
                              <tr>
                                <th colspan="3"><h5>SISA DANA</h5></th>
                                <th class="text-right"><h5><?= format_rp($total) ?></h5></th>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>

                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
    
</main>