<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">KONFIRMASI KAS</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                	<li class="breadcrumb-item"><a href="<?php echo site_url('acc/kas') ?>">Kas Keluar</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Kas Keluar</h5>
                </div>

                <div class="card-body">
                    
                   <div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Detail Transaksi</h4>
								</div>
								<div class="card-body">

									<div class="row">
										<div class="col-md-12">
											<?php echo $this->session->flashdata('alert_message') ?>
										</div>
									</div>

									<a href="<?php echo site_url('acc/kas') ?>" class="btn btn-outline-primary"><i class="fa fa-chevron-left"></i> &nbsp;KEMBALI</a>
              						<br><br>

									
              						<div class="row">
						                <div class="col-md-8">
						                  <div class="card">
						                    <div class="card-header"><b>DETAIL</b></div>
						                    <div class="card-body">

						                    	 <table class="table">
								                    <tr>
								                      <th style="width: 20%; background-color: #eee">KODE</th>
								                      <td><?php echo $transaksi['kode_transaksi'] ?></td>

								                      <th style="width: 30%; background-color: #eee">TOTAL TRANSAKSI</th>
								                      <td><?php echo format_rp($transaksi['total_transaksi']) ?></td>
								                    </tr>

								                    <tr>
								                      <th style="width: 20%; background-color: #eee">PEMBAYARAN</th>
								                      <td><?php echo $transaksi['pembayaran'] ?></td>

								                      <th style="width: 20%; background-color: #eee">TOTAL BAYAR</th>
								                      <td><?php echo format_rp($transaksi['total_bayar']) ?></td>
								                    </tr>

								                    <tr>
								                      <th style="width: 20%; background-color: #eee">TIPE</th>
								                      <td><?php echo $transaksi['tipe'] ?></td>
								                      <th style="width: 20%; background-color: #eee">SISA BAYAR</th>
								                      <td><?php echo format_rp($transaksi['sisa_bayar']) ?></td>
								                    </tr>

								                     <tr>
								                      <th style="width: 20%; background-color: #eee">STATUS</th>
								                      <td><?php if($transaksi['status'] == 'Lunas'){
								                          echo "<span class='badge badge-success'>Lunas</span>";
								                        }else{
								                          echo "<span class='badge badge-danger'>Belum Lunas</span>";
								                        } ?></td>
                                        <th style="width: 20%; background-color: #eee">METODE</th>
                                        <td><?php echo $transaksi['metode'] ?></td>
								                    </tr>

								                    <tr>
								                      <th style="width: 20%; background-color: #eee">KETERANGAN</th>
								                      <td colspan="3"><?php echo $transaksi['keterangan'] ?></td>
								                    </tr>
								                  </table>

						                    </div>
						                  </div>

						                </div>

<div class="col-md-4">
	<div class="card">
        <div class="card-header"><b>PERSETUJUAN</b></div>

        <div class="card-body text-center">

    <?php 

      $segment = 'set_trans_level';
      if($transaksi['tipe'] == 'Pemakaian BOP'){
        $segment = 'set_trans_level_bop';
      }

      if($transaksi['level'] == '1'){ ?>
        	<a onclick="return confirm('Apakah anda yakin ?')" class="btn btn-success btn-sm" href="<?php echo site_url($segment.'/approve/'.$transaksi['id']) ?>"><i class="fa fa-check"></i> &nbsp;TERIMA</a>
        	&emsp;
        	<a onclick="return confirm('Apakah anda yakin ?')" class="btn btn-danger btn-sm" href="<?php echo site_url($segment.'/deny/'.$transaksi['id']) ?>"><i class="fa fa-ban"></i> &nbsp;TOLAK</a>
        
    <?php }else{
    		if($transaksi['is_deny'] != '1'){ 
				echo '<span class="badge badge-success"><i class="fa fa-check-circle"></i> Diterima</span>';
	    				
    ?>

    <?php   }else{ ?>
    			<span class="badge badge-danger"><i class="fa fa-ban"></i> Ditolak</span>
    <?php   } 

		} 
	?>
		</div>
    </div>
</div>

<div class="col-md-12">

    <div class="card">
      <div class="card-header"><b>DAFTAR BARANG</b></div>
      <div class="card-body">
      	<div class="table-responsive">
          <table class="table">
            <thead class="bg-primary">
              <tr>
                <th>PRODUK</th>
                <th class="text-center">HARGA SATUAN</th>
                <th class="text-center" style="width: 10%">JUMLAH</th>
                <th class="text-center" style="width: 25%">SUBTOTAL</th>
              </tr>
            </thead>

            <tbody id="tableItem">
              <?php $n = $grandTotal = 0; 
                    foreach ($item as $row) { $n++; $grandTotal += $row['subtotal']?>

                      <tr>
                        <td><?php echo $row['nama_komponen'] ?></td>
                        <td class="text-right"><?php echo format_rp($row['harga']) ?></td>
                        <td class="text-center"><?php echo $row['jumlah'] ?></td>
                        <td class="text-right"><?php echo format_rp($row['subtotal']) ?></td>
                      </tr>

              <?php } ?>

            </tbody>

              <tr>
                <td colspan="3"><input type="hidden" id="inputGrandTotal" name="grandTotal" value="<?php echo $grandTotal ?>">
                	<h4><b>TOTAL</b></h4></td>
                <td class="text-right"><h4><b id="grandTotal">
                	<?php if($grandTotal == 0){
                			if($transaksi['level'] == '0'){
                				echo "<span class='badge badge-danger'>Ditolak Bag. Keuangan</span>";
                			}else if($transaksi['level'] == '2'){
                				echo "<span class='badge badge-primary'>Menunggu Acc Bag. Keuangan</span>";
                			}else if($transaksi['level_vendor'] == '1'){
                				echo "<span class='badge badge-primary'>Menunggu Konfirmasi Vendor</span>";
                			}
                  		
                  	}else{
                  		echo format_rp($grandTotal);
                  	} ?>
                	</b></h4>
                </td>
              </tr>

          </table>
        </div>
      </div>
    </div>

</div>
						              </div>

								</div>
							</div>
						</div>


					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->

