<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">KONFIRMASI KAS</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                	<li class="breadcrumb-item"><a href="<?php echo site_url('acc/kas') ?>">Kas Keluar</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Kas Keluar</h5>
                </div>

                <div class="card-body">
                    
                   <div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Detail Transaksi</h4>
								</div>
								<div class="card-body">

									<div class="row">
										<div class="col-md-12">
											<?php echo $this->session->flashdata('alert_message') ?>
										</div>
									</div>

									<a href="<?php echo site_url('vendor/'.$tipe) ?>" class="btn btn-outline-primary"><i class="fa fa-chevron-left"></i> &nbsp;KEMBALI</a>
              						<br><br>

									
              						<div class="row">
						                <div class="col-md-12">
						                  <div class="card">
						                    <div class="card-header"><b>DETAIL</b></div>
						                    <div class="card-body">

	 <table class="table">
        <tr>
          <th style="width: 20%; background-color: #eee">KODE</th>
          <td><?php echo $transaksi['kode_transaksi'] ?></td>

          <th style="width: 30%; background-color: #eee">TOTAL PEMBELIAN</th>
          <td><?php echo format_rp($transaksi['total_transaksi']) ?></td>
        </tr>

        <tr>
          <th style="width: 20%; background-color: #eee">PEMBAYARAN</th>
          <td><?php echo $transaksi['pembayaran'] ?></td>

          <th style="width: 20%; background-color: #eee">TOTAL BAYAR</th>
          <td><?php echo format_rp($transaksi['total_bayar']) ?></td>
        </tr>

        <tr>
          <th style="width: 20%; background-color: #eee">TIPE</th>
          <td><?php echo $transaksi['tipe'] ?></td>
          <th style="width: 20%; background-color: #eee">SISA BAYAR</th>
          <td><?php echo format_rp($transaksi['sisa_bayar']) ?></td>
        </tr>

         <tr>
          <th style="width: 20%; background-color: #eee">STATUS</th>
          <td><?php if($transaksi['status'] == 'Lunas'){
              echo "<span class='badge badge-success'>Lunas</span>";
            }else{
              echo "<span class='badge badge-danger'>Belum Lunas</span>";
            } ?></td>

          <?php if($transaksi['tipe'] == 'perbaikan'){ ?>
                  <th style="width: 20%; background-color: #eee">KONFIRMASI</th>
                  <td><?php if($transaksi['level_vendor'] == '1'){
                      echo "<span class='badge badge-primary'>Menunggu Penginputan Harga</span>";
                    }else if($transaksi['level_vendor'] == '2'){
                      echo "<span class='badge badge-warning'>Menunggu Acc Bag. Keuangan</span>";
                    }else if($transaksi['level_vendor'] == '3'){
                      echo "<span class='badge badge-primary'>Menunggu penyelesaian ".$transaksi['tipe']."</span>";
                    }else if($transaksi['level_vendor'] == '4'){
                      echo "<span class='badge badge-success'>Selesai</span>";
                    }else{
                      echo "<span class='badge badge-danger'>Ditolak</span>";
                    } ?>
                  </td>

          <?php } ?>
          
        </tr>

      </table>

						                    </div>
						                  </div>

						                </div>



<div class="col-md-12">
    <div class="card">
      <div class="card-header"><b>DAFTAR BARANG</b></div>

      <div class="card-body">

        <?php if($transaksi['tipe'] == 'perbaikan'){ ?>
                <div class="row">
                  <div class="col-md-8">
                    <b>Waktu Penginputan Harga :</b> <br><?= $transaksi['tanggal_konfirmasi_vendor'] ?>
                  </div>
                  <div class="col-md-4">
                    <b>Progress Penyelesaian</b>
                    <div class="progress">
                        <div id="progress" class="progress-bar" role="progressbar" style="" aria-valuenow="25"
                             aria-valuemin="0" aria-valuemax="100">
                        </div>
                    </div>
                  </div>
                </div>

        <?php } ?>
        
      	
      	<bR><br>
      	<div class="table-responsive">
      		<form method="POST" action="<?= site_url('insert_price/'.$tipe.'/'.$transaksi['id']) ?>">
            <table class="table">
              <thead style="background-color:#eee">
                <tr>
                  <th style="width: 25%">ASET</th>
                  <th style="width: 25%">KETERANGAN KEUANGAN</th>
                  <th>KETERANGAN VENDOR</th>
                  <th class="text-center" style="width: 20%">HARGA</th>
                  <th class="text-center"></th>
                </tr>
              </thead>

              <tbody id="tableItem">
                <?php $n = $grandTotal = 0; 
                      $done = 0; $k = 0;
                      foreach ($item as $row) { $k++; $grandTotal += $row['harga'];
                        if($row['is_done'] == '1'){
                          $done++;
                        }
                ?>
                        <tr>
                          <td>
                          	<?php echo $row['nama_komponen'] ?><br><br>
                          	<a class="img-preview" href="javascript:void(0)" data-img="<?= $row['gambar'] ?>"><i class="fa fa-photo"></i> Preview</a>
                          </td>
                          <td><?php echo $row['keterangan'] ?></td>

                          <td>
                          	<?php if($row['harga'] == '' || $row['harga'] == 0){ ?>

                          		<textarea placeholder="Ketik Disini..." name="keterangan[<?= $row['detail_id'] ?>]" class="form-control"></textarea>

                          	<?php }else{
                          		echo $row['keterangan_vendor'];
                          	} ?>
                          </td>

                          <td class="text-right">
                          	<?php if($row['harga'] == '' || $row['harga'] == 0){ ?>

                          		<input required="" autocomplete="off" placeholder="Rp. " value="Rp 0" type="text" class="form-control rupiah price" name="harga[<?= $row['detail_id'] ?>]">

                          	<?php }else{
                          		echo format_rp($row['harga']);
                          	} ?>
                          </td>
                          
                          <td class="text-center">
                            <?php if($transaksi['level_vendor'] >= 3){ 

                                    if($row['is_done'] == '1'){
                            ?>
                                      <i class='fa fa-check-circle text-success'></i>
                            <?php   }else{ ?>
                                      <a onclick='return confirm("Selesaikan <?= $transaksi['tipe'] ?> ?")' href="<?= site_url('fix_done/'.$transaksi['tipe']."/".$transaksi['id'].'/'.$row['detail_id']) ?>" data-toggle="tooltip" title="Sudah Selesai" class="btn btn-success btn-sm"><i class="fa fa-check-circle"></i></a>
                            <?php   } ?>
                                    
                            <?php } ?>
                          </td>
                        </tr>
                <?php } ?>

              </tbody>

                <tr>
                  <td colspan="3"><input type="hidden" id="inputGrandTotal" name="grandTotal" value="<?php echo $grandTotal ?>"><h4><b>TOTAL</b></h4></td>
                  <td colspan="1" class="text-right"><h4>
                  	<b id="grandTotal">
                  		<?php echo format_rp($grandTotal); ?>
                  	</b></h4></td>
                </tr>

                <?php if($transaksi['level_vendor'] == '1' && $transaksi['tipe'] == 'perbaikan'){ ?>

                		<tr>
		                	<td colspan="3"></td>
		                	<td>
		                		<button disabled="disabled" id="btnSave" class="btn btn-success btn-block">SIMPAN</button>
		                	</td>
		                </tr>

                <?php } ?>

            </table>
        	</form>
          </div>
      </div>
    </div>
</div>


						              </div>

								</div>
							</div>
						</div>


					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->




<div class="modal fade" id="modalPreview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
       <div class="modal-content">
         <div class="modal-header bg-primary">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Preview Foto</h4>
         </div>

         <input type="hidden" name="penjualan_detail_id" id="e_id">

         <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <center>
                  <img src="" id="place-img" class="img-fluid">
                </center>
              </div>
            </div>
         </div>

         <div class="modal-footer">
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
         </div>

       </div>
    </div>
 </div>


 <script type="text/javascript">
  var done = <?= $done ?>;
  var all  = <?= $k ?>;

  var percent = (done / all) * 100;
  $('#progress').attr('style', 'width : ' + percent + "%").text(done +" / "+all);

 	$(document).on('click','.img-preview', function(){
      var source = '<?= base_url('assets/aset/perbaikan') ?>'+'/'+$(this).attr('data-img');

      $('#place-img').attr('src', source);
      $('#modalPreview').modal('show');
    });

    $(document).on('keypress, keyup', '.price', function(){
    	var total = 0; var error = 0;
    	$('.price').each(function(index, val){
    		var price = format_angka($(this).val());

    		if(price == 0 || price == '' || isNaN(price)){
    			error++;
    			price = 0;

    		}

    		total += price;
    	});

    	if(error > 0){
    		$('#btnSave').attr('disabled', 'disabled');
    	}else{
    		$('#btnSave').removeAttr('disabled');
    	}

    	$('#grandTotal').text(format_rp(total));
    });        
 </script>