<!doctype html>
<html lang="en">

<!-- Mirrored from nextable.laborasyon.com/default/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Nov 2019 05:09:30 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>P GAB</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/media/image/favicon.png"/>

    <!-- Plugin styles -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/bundle.css" type="text/css">

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/app.min.css" type="text/css">
</head>
<body class="form-membership">

<!-- begin::page loader-->
<div class="page-loader">
    <div class="spinner-border"></div>
</div>
<!-- end::page loader -->

<div class="form-wrapper">

    <!-- logo -->
    <div class="logo">
        <img src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
    </div>
    <!-- ./ logo -->

    <h5>Sign in</h5>

    <!-- form -->
    <form method="POST" action="<?php echo site_url('auth/login') ?>">
        <div class="form-group">
            <input name="username" type="text" class="form-control" placeholder="Username or email" required autofocus>
        </div>
        <div class="form-group">
            <input name="password" type="password" class="form-control" placeholder="Password" required>
        </div>
        <button class="btn btn-primary btn-block">Sign in</button>

    </form>
    <!-- ./ form -->

</div>

<!-- Plugin scripts -->
<script src="<?php echo base_url() ?>assets/vendors/bundle.js"></script>

<!-- App scripts -->
<script src="<?php echo base_url() ?>assets/js/app.min.js"></script>
</body>

<!-- Mirrored from nextable.laborasyon.com/default/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Nov 2019 05:09:30 GMT -->
</html>