<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">PENGGAJIAN</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Gaji</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Input Tunjangan</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Tunjangan Pegawai</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<button data-toggle="modal" data-target="#modalTambahKTG" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah Tunjangan</button>
						<br><br>

					<div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
                                    <th>Tunjangan</th>
									<th>Pegawai</th>
									<th>Jumlah</th>
									<th class="text-center">Nominal</th>
                                    <th>Tanggal</th>
								</tr>
							</thead>
							<tbody>
								<?php $n = 0;
		                          foreach ($list as $row) { $n++; ?>

		                            <tr>
		                              <td><?php echo $n ?></td>
                                      <td><?php echo $row['nama_tunjangan'] ?></td>
		                              <td><?php echo $row['kode_pegawai']." / ".$row['nama_pegawai'] ?></td>
		                              <td><?php echo $row['jumlah'] ?></td>
		                              <td class="text-right"><?php echo format_rp($row['total_nominal']) ?></td>
                                      <td><?php echo $row['waktu_tunjangan'] ?></td>

		                            </tr>

		                    <?php } ?>
							</tbody>
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->


<form action="<?php echo site_url('insert_gaji_tunjangan') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalTambahKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Tunjangan Pegawai</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <div class="modal-body">

                <div class="form-group">
                   <label>Pegawai</label>
                   <select required="" class="form-control" name="pegawai_id">
                   	<option value="">Pilih</option>
                   	<?php foreach ($pegawai as $row) { ?>
                   		<option value="<?php echo $row['pegawai_id'] ?>"><?php echo $row['nama_pegawai'] ?></option>
                   	<?php } ?>
                   </select>
                </div>

                <div class="form-group">
                   <label>Tunjangan</label>
                   <select required="" class="form-control" name="tunjangan_id">
                    <option value="">Pilih</option>
                    <?php foreach ($tunjangan as $row) { ?>
                        <option value="<?php echo $row['id'] ?>"><?php echo $row['nama_tunjangan'] ?></option>
                    <?php } ?>
                   </select>
                </div>

                <div class="form-group">
                   <label>Jumlah</label>
                   <input autocomplete="off" type="text" name="jumlah" id="nama" class="form-control" placeholder="Jumlah..." required/>
                </div>

                <div class="form-group">
                   <label>Tanggal</label>
                   <input autocomplete="off" type="text" name="waktu_tunjangan" id="nama" class="form-control datepicker" placeholder="Tanggal Tunjangan..." required/>
                </div>

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>
