<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">PENGGAJIAN</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Gaji</a></li>
                    <li class="breadcrumb-item"><a href="#">Daftar Penggajian</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Detail Penggajian</h5>
                </div>

                <div class="card-body">
                    
                    <a class="btn btn-outline-primary" href="<?php echo site_url('penggajian/gaji/daftar') ?>"><i class="fa fa-chevron-left"></i> KEMBALI</a>

					<br><br>

                    <table class="table">
						<tr>
							<th style="width: 20%; background-color: #eee" class="">WAKTU</th>
							<td><?php echo get_monthname($gaji['bulan'])." ".$gaji['tahun'] ?></td>
						</tr>
						<tr>
							<th style="background-color: #eee">JUMLAH PEGAWAI</th>
							<td><?php echo $gaji['total_pegawai'] ?></td>
						</tr>
						<tr>
							<th style="background-color: #eee">TOTAL</th>
							<td>Rp. <?php echo number_format($gaji['total_gaji'],0,'','.') ?></td>
						</tr>
					</table>

					<br><br>

					<table class="table">
						<thead>
							<tr>
								<th style="width:5%;vertical-align: middle;" class="text-center" rowspan="2">NO</th>
								<th style="width:23%;vertical-align: middle;" rowspan="2">NAMA</th>
								<th colspan="2" class="text-center">KEHADIRAN</th>
								<th colspan="4" class="text-center">KOMPONEN GAJI</th>
								<th rowspan="2" class="text-center">TOTAL GAJI</th>
								<th rowspan="2" class="text-center"><i class="fa fa-cog"></i></th>
							</tr>
							<tr>
								<th class="text-center">H</th>
								<th class="text-center">T</th>
								<th align="center"><center>Gaji</center></th>
								<th class="text-center">Lembur</th>
								<th class="text-center">Tunjangan</th>
								<th class="text-center">Pph</th>
							</tr>
						</thead>
						<tbody>
						<?php $n=0; foreach ($list as $row) { $n++;?>
							<tr>
								<td><?php echo $n ?></td>
								<td><?php echo $row['kode_pegawai']." / ".$row['nama_pegawai'] ?></td>
								<td class="text-center"><?php echo $row['total_hadir'] ?></td>
								<td class="text-center"><?php echo $row['total_terlambat'] ?></td>
								<td style="text-align: right">Rp. <?php echo number_format($row['gaji_pokok'],0,'','.') ?></td>
								<td style="text-align: right">Rp. <?php echo number_format($row['tunjangan_lembur'],0,'','.') ?></td>
								<td style="text-align: right">
									Rp. <?php echo number_format($row['tunjangan_lain'],0,'','.') ?>
									<a  onclick="t_komponen(
									    '<?php echo $row["kode_pegawai"]." / ".$row["nama_pegawai"] ?>',
										'<?php echo htmlentities(json_encode($row["tunjangan_komponen"])) ?>'
								    )" 
									href="javascript:void(0)" class="text-primary"><i class="fa fa-search"></i></a>
								</td>
								<td style="text-align: right">Rp. <?php echo number_format($row['pph'],0,'','.') ?></td>

								<td style="text-align: right">Rp. <?php echo number_format($row['total_gaji'],0,'','.') ?></td>
								<td class="text-center">
									<a class='text-info' target="_blank" href="<?php echo site_url('penggajian/gaji/daftar/detail/'.$gaji['gaji_id'].'/cetak/'.$row['pegawai_id']) ?>" data-toggle="tooltip" title="Cetak Slip aji"><i class='fa fa-print'></i></a>
								</td>
							</tr>
						<?php } ?>
						</tbody>
					</table>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->


<input type="hidden" name="source" value="lain">
   <input type="hidden" name="tipe" value="lembur">
     <div class="modal fade" id="modalTunjangan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Detail Tunjangan</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <div class="modal-body">

                <div class="form-group">
                   <label>Karyawan</label>
                   <h5 id="d_karyawan"></h5>
                </div>

                <table class="table">
                	<thead>
                		<tr>
                			<th>Tunjangan</th>
                			<th class="text-center">Nominal</th>
                		</tr>
                	</thead>
                	<tbody id="tableTunjangan">
                		
                	</tbody>
                	<tfoot>
                		<tr style="background-color: #eee">
                			<th>TOTAL</th>
                			<th id="d_total" class="text-right"></th>
                		</tr>
                	</tfoot>
                </table>

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
             </div>

           </div>
        </div>
     </div>

<script type="text/javascript">
	function t_komponen(karyawan, komponen){
		var len = komponen.length;
		var js = komponen.substring(1, len-1);
		
		obj = $.parseJSON(js);

		var txt = ""; var subtotal = 0;
		$.each(obj, function(index, val){
			subtotal += parseInt(val.nominal);
			txt += "<tr>";
			txt += "<td>"+ val.name +"</td>";
			txt += "<td class='text-right'>"+ format_rp(val.nominal) +"</td>";
			txt += "</tr>";
		});

		$('#tableTunjangan').html(txt);
		$('#d_total').text(format_rp(subtotal));

		$('#d_karyawan').text(karyawan);
		$('#modalTunjangan').modal('show');
	}
</script>