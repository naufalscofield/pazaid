
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">PENGGAJIAN</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Gaji</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Daftar</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Gaji</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12" id="status">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<button data-toggle="modal" data-target="#modalTambahKTG" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> &nbsp;Tambah Penggajian</button>
                    <br><br>

					<div class="row">
						<div class="col-md-4">
							<form method="GET" id="formGaji">
								<div class="form-group">
									<label class="control-label"><b>Tahun</b></label>
									<select id="tahun" class="form-control" name="tahun">
										<?php for ($i=2019; $i <= date('Y'); $i++) {  ?>
													<option <?php if($tahun == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo $i ?></option>
										<?php } ?>
									</select>
								</div>
								
							</form>
						</div>
					</div>

					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr style="background-color: #eee">
									<th style="width:15%">BULAN</th>
									<th style="width:10%">TAHUN</th>
									<th class="text-center" style="width:20%">JUMLAH PEGAWAI</th>
									<th style="text-align: center">TOTAL</th>
									<th style="width: 20%" class="text-center"><i class="fa fa-cog"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php $n = 0; $total_gaji = 0;
									  foreach($gaji as $row){ $n++; 

									  	if($row['total_gaji'] == 0){
								  			$total_gaji 	= $row['next']['total_gaji'];
								  			$total_pegawai = $row['next']['total_pegawai'];

									  	}else{
									  		$total_gaji 	= $row['total_gaji'];
									  		$total_pegawai = $row['total_pegawai'];
									  	}

									  	if($row['id_gaji'] != ''){ 
								?>
										<tr>
											<td><?php echo get_monthname($row['bulan']) ?></td>
											<td><?php echo $row['tahun'] ?></td>
											<td class="text-center"><?php echo $total_pegawai ?></td>
											<td style="text-align: right"><?php echo format_rp($total_gaji) ?></td>
											<td class="text-center">
												<a class='btn btn-primary btn-sm shadow' href="<?php echo site_url('penggajian/gaji/daftar/detail/'.$row['id_gaji']) ?>" data-toggle="tooltip" title="Lihat Detail"><i class='fa fa-search'></i></a>
											</td>
										</tr>
								<?php 
										}

										$total_pegawai = $row['total_pegawai'];
										$total_gaji 	= $row['total_gaji'];
									} 
								?>
							</tbody>
						</table>
					</div>
                </div>
            </div>
        </div>
    </div>
    
</main>

     <div class="modal fade" id="modalTambahKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Gaji</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <div class="modal-body">



                <div class="form-group">
                   <label><b>Bulan</b></label>
                   <select required="" class="form-control" name="bulan" id="bulan">
                   	 <option value="">Pilih</option>
                   	 <?php foreach ($gaji as $row) {
	             		if($row['id_gaji'] == ''){
	             			$d = cal_days_in_month(CAL_GREGORIAN,$row['bulan'],$row['tahun']);
	             			//$d = '20';
	             			
	             			$min = strtotime($d."-".$row['bulan']."-".$row['tahun']);

	             			if(strtotime(date('d')."-".date('m')."-".date('Y')) >= $min){
	             				echo "<option value='".$row['bulan']."'>".get_monthname($row['bulan'])."</option>";
	             			}
	             		}
	             		
	             	} ?>
                   </select>
                   <small>* Hanya bulan yang tersedia yang dapat ditampilkan</small>
                </div>

                <div class="form-group">
                   <label><b>Tahun</b></label><br>
                   <?php echo $tahun ?>
                </div>

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat generateGaji"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>


<script>

	$(document).on('change','#tahun', function(){
		$('#formGaji').submit();
	});

	$(document).on('click','.generateGaji',function(){

		var bulan = $('#bulan').val();
		var tahun = <?php echo date('Y') ?>;

		var r = confirm("Apakah anda yakin ingin melakukan penggajian bulan "+filter_month(bulan)+" "+tahun+" ?");
		if (r == true) {
		  $.ajax({
				url : "<?php echo site_url('generate_gaji') ?>",
				method : "POST",
				dataType : "json",
				data : {
					bulan : bulan,
					tahun : tahun
				},
				beforeSend : function(){
					$(this).html('<i class="fa fa-spinner fa-spin"></i> Proses...').attr('disabled','disabled');
				},
				success : function(res){
					if(res.status == 'success'){
						$(this).remove();
						alert('Gaji berhasil digenerate');
						location.reload();
					}else{
						$('#status').html(res.message);
					}
					
				},
				complete : function(){
					$(this).html('GENERATE GAJI').removeAttr('disabled');
				}
			})
		}
	});

	function filter_month(month){
		if(month == 1){
			month = "Januari";
		}else if(month == 2){
			month = "Februari";
		}else if(month == 3){
			month = "Maret";
		}else if(month == 4){
			month = "April";
		}else if(month == 5){
			month = "Mei";
		}else if(month == 6){
			month = "Juni";
		}else if(month == 7){
			month = "Juli";
		}else if(month == 8){
			month = "Agustus";
		}else if(month == 9){
			month = "September";
		}else if(month == 10){
			month = "Oktober";
		}else if(month == 11){
			month = "November";
		}else if(month == 12){
			month = "Desember";
		}

		return month;
	}
</script>