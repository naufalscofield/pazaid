<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">PENGGAJIAN</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Gaji</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Input Lembur</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Lembur Pegawai</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<button data-toggle="modal" data-target="#modalTambahKTG" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah Lembur</button>
						<br><br>

					<div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>Karyawan</th>
									<th>Jumlah Lembur (Jam)</th>
									<th>Tanggal</th>
									<th class="text-center">Nominal</th>
								</tr>
							</thead>
							<tbody>
								<?php $n = 0;
		                          foreach ($list as $row) { $n++; ?>

		                            <tr>
		                              <td><?php echo $n ?></td>
		                              <td><?php echo $row['kode_pegawai']." / ".$row['nama_pegawai'] ?></td>
		                              <td><?php echo $row['total_jam'] ?></td>
		                              <td><?php echo $row['tanggal'] ?></td>
		                              <td class="text-right"><?php echo format_rp($row['nominal_lembur']) ?></td>

		                            </tr>

		                    <?php } ?>
							</tbody>
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->


<form action="<?php echo site_url('insert_lembur') ?>" method="post" enctype="multipart/form-data">
	 <input type="hidden" name="tipe" value="lembur">
     <div class="modal fade" id="modalTambahKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Lembur</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <div class="modal-body">

                <div class="form-group">
                   <label>Pegawai</label>
                   <select required="" class="form-control" name="pegawai_id">
                   	<option value="">Pilih</option>
                   	<?php foreach ($pegawai as $row) { ?>
                   		<option value="<?php echo $row['pegawai_id'] ?>"><?php echo $row['nama_pegawai'] ?></option>
                   	<?php } ?>
                   </select>
                </div>

                <div class="form-group">
                   <label>Total Jam Lembur</label>
                   <input autocomplete="off" type="text" name="jml_lembur" id="nama" class="form-control" placeholder="Total Jam Lembur..." required/>
                </div>

                <div class="form-group">
                   <label>Tanggal</label>
                   <input autocomplete="off" type="text" name="tgl" id="nama" class="form-control datepicker" placeholder="Tanggal Lembur..." required/>
                </div>

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>
