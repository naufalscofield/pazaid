<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from demo.themekita.com/atlantis/livepreview/examples/demo1/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 30 Oct 2019 18:06:06 GMT -->
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>P GAB</title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">


  <link rel="shortcut icon" href="<?php echo base_url() ?>assets/media/image/favicon.png"/>

    <!-- Plugin styles -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/bundle.css" type="text/css">

    <!-- Datepicker -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/datepicker/daterangepicker.css">

    <!-- Slick -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/slick/slick.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/slick/slick-theme.css">

    <!-- Vmap -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendors/vmap/jqvmap.min.css">

    <!-- App styles -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/app.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/themify-icons/themify-icons.css" type="text/css">

  <style type="text/css">
    .timeline_active{
      border:2px solid #31CE36 ; padding: 10px;border-radius: 10px;
    }
  </style>
</head>
<body onload="window.print()">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-primary">
                        <h5 class="card-title">GAJI</h5>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th style="width:30%">Bulan</th>
                                <td><?php echo get_monthname($gaji['bulan']) ?></td>
                            </tr>
                            <tr>
                                <th style="width:30%">Tahun</th>
                                <td><?php echo $gaji['tahun'] ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                
                <br>

                <div class="card">
                    <div class="card-header bg-primary">
                        <h5 class="card-title">PEGAWAI</h5>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <tr>
                                <th style="width:30%">Kode Pegawai</th>
                                <td><?php echo $pegawai['kode_pegawai'] ?></td>
                            </tr>
                            <tr>
                                <th style="width:30%">Nama</th>
                                <td><?php echo $pegawai['nama_pegawai'] ?></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header bg-primary">
                        <h5 class="card-title">GAJI</h5>
                    </div>
                    <div class="card-body">
                        <?php $base = ($pegawai['gaji'] / $list['maksimal_kehadiran']) ?>

                        <table class="table">
                            <tr>
                                <th style="width:30%">Kehadiran</th>
                                <td><?php echo $list['total_masuk']." / ".$list['maksimal_kehadiran'] ?></td>
                            </tr>
                            <tr>
                                <th>&emsp;&emsp;Hadir Penuh</th>
                                <td><?php echo $list['total_hadir'] ?> x <?php echo format_rp($base) ?> &emsp; = &emsp;Rp. <?php echo number_format($list['gaji_kehadiran'],0,'','.') ?></td>
                            </tr>
                            <tr>
                                <th>&emsp;&emsp;Hadir Terlambat</th>
                                <td><?php echo $list['total_terlambat'] ?> x <?php echo format_rp((50/100) * $base) ?> &emsp; = &emsp;Rp. <?php echo number_format($list['gaji_keterlambatan'],0,'','.') ?></td>
                            </tr>
                            <tr>
                                <th style="width:30%">Tunjangan Lembur</th>
                                <td><?php echo format_rp($list['tunjangan_lembur']) ?></td>
                            </tr>

                            <tr>
                                <th style="width:30%">Tunjangan Lain - Lain</th>
                                <td></td>
                            </tr>

                            <?php $arr = json_decode($list['tunjangan_komponen'], true); ?>

                            <?php foreach ($arr as $row) { ?>
                                    <tr>
                                        <th>&emsp;&emsp; <?php echo $row['name'] ?></th>
                                        <td><?php echo format_rp($row['nominal']) ?></td>
                                    </tr>
                            <?php } ?>

                            <tr>
                                <th style="width:30%">Piutang</th>
                                <td><?php echo format_rp($list['hutang']) ?></td>
                            </tr>
                            <tr>
                                <th style="width:30%">Pph</th>
                                <td><?php echo format_rp($list['pph']) ?></td>
                            </tr>


                            <tr>
                                <th style="width:30%">Gaji Bersih</th>
                                <td><b><?php echo format_rp($list['total_gaji']) ?></b></td>
                            </tr>
                        </table>
                    </div>
                </div>
                

            </div>
        </div>
    </div>

</body>
</html>