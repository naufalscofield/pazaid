<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">PENGGAJIAN</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Presensi</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Cuti</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Izin</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo $this->session->flashdata('alert_message') ?>
                        </div>
                    </div>

                    <div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
                                    <tr>
                                      <th style="width:5%">No</th>
                                      <th style="width:20%">Karyawan</th>
                                      <th style="width:10%">Tipe</th>
                                      <th class="text-center">Rentang Waktu</th>
                                      <th style="width:27%">Keterangan</th>
                                      <th style="width:15%" class="text-center"><i class="fa fa-cog"></i></th>
                                    </tr>
							</thead>
							<tbody>
                                <?php 
                                    $n = 0;
                                    foreach ($izin as $row) { $n++; 

                                      $date1 = new DateTime($row['tanggal_mulai']);
                                      $date2 = new DateTime($row['tanggal_selesai']);
                                      $interval = $date1->diff($date2);

                                ?>
                                      <tr>
                                        <td><?php echo $n; ?></td>
                                         <td><?php echo $row['kode_pegawai']." / ".$row['nama_pegawai'] ?></td>
                                        <td><?php echo $row['tipe'] ?></td>
                                        <td class='text-center'><?php echo $row['tanggal_mulai']." s/d ".$row['tanggal_selesai'] ?> <br> ( <?php echo $interval->days + 1 ?> Hari )</td>
                                        <td><?php echo $row['keterangan'] ?></td>
                                        <td class="text-center">

                                          <?php if($row['status'] == 'pending'){ ?>

		                                          <a onclick="return confirm('Apakah anda yakin menolak izin ini ?')" class='btn btn-danger btn-sm' href="<?php echo site_url('set_izin/deny/'.$row['id']) ?>" data-toggle="tooltip" title="Tolak"><span class='fa fa-ban'></span></a> &nbsp;
		                                          <a onclick="return confirm('Apakah anda yakin menyetujui izin ini ?')" class='btn btn-success btn-sm' href="<?php echo site_url('set_izin/approve/'.$row['id']) ?>" data-toggle="tooltip" title="Setujui"><span class='fa fa-check'></span></a>

                                          <?php }else if($row['status'] == 'approve'){ ?>

                                          			<span class="badge badge-success bg-success"><i class="fa fa-check"></i> Diterima</span>
	                                               &nbsp;
	                                               <a class='btn btn-danger btn-sm' href="<?php echo site_url('set_izin/undo/'.$row['id']) ?>" data-toggle="tooltip" title="Batalkan"><span class='fa fa-ban'></span></a>

                                          <?php }else{ ?>

                                          			<span class="badge badge-danger bg-danger"><i class="fa fa-ban"></i> Ditolak</span>

                                          <?php } ?>

                                          
                                        </td>
                                      </tr>
                                <?php } ?>
                              </tbody>
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main 