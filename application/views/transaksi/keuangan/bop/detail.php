<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">KEUANGAN</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                	<li class="breadcrumb-item"><a href="#">Dana BOP</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Pemakaian</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Detail BOP</h5>
                </div>

                <div class="card-body">
                    
                   	<div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<a href="<?php echo site_url('keuangan/bop/keluar') ?>" class="btn btn-outline-primary btn-flat"><i class="fa fa-chevron-left"></i> &nbsp;KEMBALI</a>
						<br><br>

					
						<div class="row">
		                <div class="col-md-12">
		                  <div class="card">
		                    <div class="card-header"><b>DETAIL</b></div>

		                    <div class="card-body">
		                    	<table class="table">
				                    <tr>
				                      <th style="width: 20%; background-color: #eee">KODE</th>
				                      <td><?php echo $transaksi['kode_transaksi'] ?></td>
				                      <th style="width: 20%; background-color: #eee">TOTAL DANA</th>
				                      <td><?php echo format_rp($transaksi['total_transaksi']) ?></td>
				                    </tr>

				                    <tr>
				                      <th style="width: 20%; background-color: #eee">TIPE</th>
				                      <td><?php echo $transaksi['pembayaran'] ?></td>
				                      <th style="width: 20%; background-color: #eee">TOTAL PEMAKAIAN</th>
				                      <td><?php echo format_rp($transaksi['total_bayar']) ?></td>
				                    </tr>

				                    <tr>
				                      <th style="width: 20%; background-color: #eee">TANGGAL TERIMA</th>
				                      <td><?php echo $transaksi['tanggal_transaksi'] ?></td>
				                      <th style="width: 20%; background-color: #eee">SISA DANA</th>
				                      <td><?php echo format_rp($transaksi['sisa_bayar']) ?></td>
				                    </tr>

				                    <tr>
				                    	<th style="width: 20%; background-color: #eee">JANGKA WAKTU</th>
				                      <td colspan="3"><?php echo $transaksi['tanggal_selesai'] ?></td>
				                    </tr>

				                  </table>
		                    </div>
		                  </div>

		                </div>

		                <div class="col-md-12">
		                    <div class="card">
		                      <div class="card-header"><b>DAFTAR PEMAKAIAN</b></div>

		                      <div class="card-body">
		                      	<button data-toggle="modal" data-target="#modalTambahBOP" class="btn btn-primary btn-flat mt-2 mb-2"><i class="fa fa-plus"></i> &nbsp; Tambah</button>
							    <br><br>

		                      	<div class="table-responsive">
			                      <table class="table">
			                        <thead class="bg-primary">
			                          <tr>
			                            <th>KOMPONEN PENGELUARAN</th>
			                            <th>TANGGAL</th>
			                            <th class="text-center">PEMBAYARAN</th>
			                            <th class="text-center">STATUS</th>
			                            <th style="width: 25%">SUBTOTAL</th>
			                          </tr>
			                        </thead>

			                        <tbody id="tableItem">
			                          <?php $n = $grandTotal = 0; 
			                                foreach ($item as $row) { $n++; 
			                                	if($row['is_acc'] == '1'){
			                                		$grandTotal += $row['subtotal'];
			                                	}
			                                ?>

			                                <?php if($row['subtotal'] != ''){ ?>

			                                  <tr>
			                                    <td><?php echo $row['nama_komponen']."<br><small class='text-muted'>".$row['keterangan']."</small>" ?></td>
			                                    <td><?= $row['tanggal_pengeluaran'] ?></td>
			                                    <td class="text-center">
			                                    	<?= $row['pembayaran'] ?>
			                                    	<?php 
			                                    		if($row['pembayaran'] == 'Transfer'){
			                                    			echo "<br><small>".$row['rekening']."</small>";
			                                    		}
			                                    	?>
			                                    </td>
			                                    <td class="text-center">
			                                    	<?php 
			                                    		if($row['is_acc'] == ''){ 
			                                    			echo "<span class='badge badge-primary'>Pending</span>";
			                                    		}else if($row['is_acc'] == '1'){
			                                    			echo "<span class='badge badge-succuess'>Diterima</span>";
			                                    		}else{
			                                    			echo "<span class='badge badge-succuess'>Ditolak</span>";
			                                    		}
			                                    	?>
			                                    </td>
			                                    <td class="text-right"><?php echo format_rp($row['subtotal']) ?></td>
			                                  </tr>

			                                <?php } ?>
			                          <?php } ?>

			                        </tbody>

			                          <tr>
			                            <td colspan="4"><input type="hidden" id="inputGrandTotal" name="grandTotal" value="<?php echo $grandTotal ?>"><h4><b>TOTAL</b></h4></td>
			                            <td class="text-right"><h4><b id="grandTotal"><?php echo format_rp($grandTotal) ?></b></h4></td>
			                          </tr>

			                      </table>
			                    </div>
		                      </div>
		                    </div>

		                </div>
		              </div>

                </div>
            </div>
        </div>
    </div>
    
</main>


<form action="<?php echo site_url('insert_pengeluaran_bop/'.$transaksi['id']) ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalTambahBOP" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
           	<div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> PENGGUNAAN BOP</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

             <div class="modal-body">
                <div class="form-group">
                   <label>Kode Transaksi</label>
                   <input type="text" name="kode_transaksi" id="kode" class="form-control" placeholder="Kode Jabatan" value="<?php echo $transaksi['kode_transaksi'];?>" readonly="" autocomplete="off" required />
                </div>

                <div class="form-group">
                	<div class="row">
                		<div class="col-md-6">
                			<label>Dana BOP</label><br>
                   			<h4><?php echo format_rp($transaksi['total_transaksi']) ?></h4>
                		</div>
                		<div class="col-md-6">
                			<label>Penggunaan</label><br>
                   			<h4><?php echo format_rp($total_penggunaan) ?></h4>
                		</div>
                	</div>
                </div>

                <div class="form-group">
                	<div class="row">
                		<div class="col-md-8">
                			<label>Nama Pengeluaran</label><br>
                   			<input type="text" name="nama_komponen" id="kode" class="form-control" placeholder="Nama Pengeluaran" autocomplete="off" required />
                		</div>
                		<div class="col-md-4">
                			<label>Nominal</label><br>
                   			<input type="text" name="subtotal" id="nominal" class="form-control rupiah" placeholder="Nominal" autocomplete="off" required />
                		</div>
                	</div>
                </div>

                <div class="form-group">
                	<label>Pencairan</label><br>
                	<div class="selectgroup selectgroup-secondary selectgroup-pills">
						<label class="selectgroup-item">
							<input type="radio" name="metode_pengeluaran" value="Cash" class="selectgroup-input m" checked="">
							<span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-money-bill-wave"></i> Cash</span>
						</label>
						<label class="selectgroup-item">
							<input type="radio" name="metode_pengeluaran" value="Transfer" class="selectgroup-input m">
							<span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-laptop"></i> Transfer</span>
						</label>
					</div>
                </div>

                <div class="form-group" id="rekBody">
                   <label>Pilih Rekening</label>
                   <select class="form-control" name="rek_id">
                   	<option value="">Pilih</option>
                   	<?php foreach ($rek as $row) { ?>
                   			<option value="<?php echo $row['id'] ?>"><?php echo $row['nama_bank']." - ".$row['no_rek']." a/n ".$row['atas_nama'] ?></option>
                    <?php } ?>
                   </select>
                </div>

                 <div class="form-group" id="rekBody">
                   <label>Keterangan</label>
                   <textarea class="form-control" name="keterangan_pengeluaran"></textarea>
                </div>


             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>

   <script type="text/javascript">
   	$('#rekBody').hide();
   	var total_bop = <?php echo $transaksi['total_transaksi'] ?>;
   	var total_penggunaan = <?php echo $total_penggunaan ?>;


   	$(document).on('change', '.m', function(){
   		var val = $(this).val();

   		if(val == 'Transfer'){
   			$('#rekBody').show();
   		}else{
   			$('#rekBody').hide();
   		}
   	})

   	$(document).on('keyup', '#nominal', function(){
   		var nominal = format_angka($('#nominal').val());

   		var penggunaan = total_penggunaan + nominal;
   		if(penggunaan > total_bop){
   			alert('Tidak dapat melebihi Total BOP');
   			$('#nominal').val('').focus();
   		}

   	})
   </script>