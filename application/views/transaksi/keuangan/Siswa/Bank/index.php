<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Periode Tagihan Siswa Melalui Bank</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Periode Tagihan Siswa Bank</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Periode Tagihan Siswa Bank</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<button data-toggle="modal" data-target="#modalTambahKTG" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah Periode</button>
						<br><br>

					<div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>Kode Periode</th>
									<th>Jumlah Tagihan</th>
									<th class="text-center"><i class="fa fa-cog"></i></th>
								</tr>
							</thead>
							<tbody>
                     <?php $n = 1; 
                     foreach ($periode as $row) {
                        if($row['status'] == 'bank')
                        {
                     ?>
                        <tr>
                           <td> <?php echo $n++; ?> </td>
                           <td> <?php echo $row['kode_periode'] ?></td>
                           <td> RP. <?php echo number_format($row['jumlah_tagihan']) ?></td>
                           <td class = "text-center"> 
                              <a href="javascript:void(0)" data-toggle="tooltip" title="Ubah" class="btn btn-warning btn-sm"
                                    onclick="
                                       edit(
                                          '<?php echo $row['kode_periode'] ?>',
                                          '<?php echo $row['jumlah_tagihan'] ?>',
                                       )">
                                 <i class="fa fa-edit"></i>
                              </a>
                              &nbsp;
                              <a onclick="return confirm('Hapus data ini ?')" href="<?php echo site_url('keuangan/tagihan/siswa/bank/delete/'.$row['id_periode'])  ?>" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm">
                              <i class="fa fa-trash"></i>
                              </a>
                           </td>
                        </tr>
                        <?php } ?>
                     <?php }?> 
							</tbody>
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->

<form action="<?php echo site_url('keuangan/tagihan/siswa/bank/tambah') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalTambahKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Periode Tagihan Bank</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <div class="modal-body">
                <div class="form-group">
                   <label>Kode Periode</label>
                   <input type="text" name="kode_periode" id="kode" class="form-control" placeholder="Kode Periode" value="<?php echo $get_code; ?>" readonly="" autocomplete="off" required />
                   <input type="hidden" name="status" id="status" class="form-control" placeholder="Kode Periode" value="bank" readonly="" autocomplete="off" required />
                </div>
                <div class="form-group">
                   <label>Jumlah Tagihan</label>
                   <input autocomplete="off" type="number" name="jumlah_tagihan" id="jumlah" class="form-control" placeholder="Jumlah Tagihan..." required/>
                </div>
             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>

   <form action="<?php echo site_url('keuangan/tagihan/siswa/bank/update') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalEditKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Ubah Periode</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>
             <div class="modal-body">
                   <input type="text" name="kode_periode" id="e_id" class="form-control" placeholder="Kode Periode..." value="<?php echo $row['id_periode']; ?>" readonly="" autocomplete="off" required />
                <div class="form-group">
                   <label>Kode Periode</label>
                   <input type="text" name="kode_periode" id="e_kode" class="form-control" placeholder="Kode Periode..." value="<?php echo $get_code; ?>" readonly="" autocomplete="off" required />
                </div>
                <div class="form-group">
                   <label>Jumlah Tagihan</label>
                   <input autocomplete="off" type="text" name="jumlah_tagihan" id="e_jumlah" class="form-control" placeholder="Jumlah Tagihan..." value="<?php echo $row['jumlah_tagihan'] ?>" required/>
                </div>
             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i> Ubah</button>
             </div>

           </div>
        </div>
     </div>
   </form>

<script type="text/javascript">
 function edit(kode, jumlah){

  $('#e_kode').val(kode);
  $('#e_jumlah').val(jumlah);


  $('#modalEditKTG').modal('show'); 
}
</script>