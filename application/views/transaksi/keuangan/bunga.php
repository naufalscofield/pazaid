<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Pencatatan Bunga</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Keuangan</a></li>
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Pencatatan Bunga</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Pencatatan Bunga</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
                    <div class="col-md-12">
                      <?php echo $this->session->flashdata('alert_message') ?>
                    </div>
                  </div>

                  <button data-toggle="modal" data-target="#modalTambahKTG" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah Pencatatan</button>
                          <br><br>

                  <div class="table-responsive">
                      <table class="display table table-hover datatables" >
                        <thead style="background-color: #eee">
                          <tr>
                            <th style="width: 5%">No</th>
                            <th>Tanggal</th>
                            <th>Saldo Akhir</th>
                            <th class="text-center">Potongan Bunga</th>
                            <th>Rekening</th>
                            <th>Nama Rekening</th>
                            <th>Perolehan Bunga</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $n = 0;
                                foreach ($data as $row) { $n++; ?>

                                  <tr>
                                    <td><?php echo $n ?></td>
                                    <td><?php echo $row['tanggal'] ?></td>
                                    <td><?php echo format_rp($row['saldo_akhir']) ?></td>
                                    <td class="text-center"><?php echo $row['potongan_bunga'] ?> %</td>
                                    <td><?php echo $row['no_rek'] ?></td>
                                    <td><?php echo $row['atas_nama'] ?></td>
                                    <td><?php echo format_rp($row['perolehan_bunga']) ?></td>
                                  </tr>

                          <?php } ?>
                        </tbody>
                      </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->


<form action="<?php echo site_url('insert_bunga') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalTambahKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Pencatatan Bunga</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <div class="modal-body">

                <div class="form-group" id="rekBody">
                   <label>Pilih Rekening</label>
                   <select class="form-control" name="rek_id">
                    <option value="">Pilih</option>
                    <?php foreach ($rek as $row) { ?>
                        <option value="<?php echo $row['id'] ?>"><?php echo $row['nama_bank']." - ".$row['no_rek']." a/n ".$row['atas_nama'] ?></option>
                    <?php } ?>
                   </select>
                </div>

                <div class="form-group">
                   <label>Potongan Bunga</label>
                   <div class="col">
                      <div class="row">
                        <input type="number" name="potongan_bunga" id="" class="form-control" style="width:30%"placeholder="" required/>
                        <input autocomplete="off" type="text" disabled class="form-control rupiah" style="width:10%"placeholder="" value="%" required/>
                      </div>
                    </div>
                </div>

                <div class="form-group">
                   <label>Deskripsi</label>
                   <textarea name="deskripsi" id="" cols="30" rows="10" class="form-control"></textarea>
                </div>


             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>
