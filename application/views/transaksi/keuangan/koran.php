<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Rekening Koran</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Keuangan</a></li>
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Rekening Koran</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Rekening Koran</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
                    <div class="col-md-12">
                      <?php echo $this->session->flashdata('alert_message') ?>
                    </div>
                  </div>

                  <!-- <button data-toggle="modal" data-target="#modalTambahKTG" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah Pencatatan</button> -->
                          <!-- <br><br> -->

                  <div class="table-responsive">
                    <table class="display table table-primary datatables" >
                      <thead style="background-color: #eee">
                        <tr>
                          <th style="width: 5%">No</th>
                          <th>Nama Bank</th>
                          <th>Atas Nama</th>
                          <th>No Rekening</th>
                          <th class="text-center"><i class="fa fa-cog"></i></th>
                        </tr>
                      </thead>
                      <tbody>
                      
                        <?php $n = 0;
                        $saldoAkhir = 0;
                        $perolehanBunga = 0;
                              foreach ($data as $row) { $n++; ?>

                                <tr>
                                  <td style="width: 5%"><?php echo $n ?></td>
                                  <td><?php echo $row['nama_bank'] ?></td>
                                  <td><?php echo $row['atas_nama'] ?></td>
                                  <td><?php echo $row['no_rek'] ?></td>
                                  <td>
                                  <!-- <button data-id="<?php echo $row['id'] ?>" id="btnRincianOrtu" class="btn btn-success fa fa-circle-o" data-target="#modalKu" data-toggle="modal"></button> -->
                                  <a href="javascript:void(0)" data-toggle="tooltip" title="Rincian" class="btn btn-primary btn-sm"
                                                onclick="
                                                edit(		
                                                  '<?php echo $row['id'] ?>',                                             
                                                  '<?php echo $row['nama_bank'] ?>',                                             
                                                  '<?php echo $row['atas_nama'] ?>',                                             
                                                  '<?php echo $row['no_rek'] ?>',                                             
                                                  '<?php echo $row['tanggal'] ?>',                                             
                                                  '<?php echo $row['saldo_akhir'] ?>',                                             
                                                  '<?php echo $row['potongan_bunga'] ?>',                                             
                                                  '<?php echo $row['perolehan_bunga'] ?>',                                             
                                                  '<?php echo $row['deskripsi'] ?>',                                             
                                                )">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <?php if ($row['nama_bank'] == 'BRI' && $row['atas_nama'] == 'Almalia') { ?>
                                  <button data-id="<?=$row['id'];?>" id="btn_setor_bank" class="btn btn-warning btn-sm">Setor Bank</button>
                                        <?php } else if ($row['nama_bank'] == 'BRI' && $row['atas_nama'] == 'Bunda Fitri') { ?>
                                  <button data-id="<?=$row['id'];?>" id="btn_setor_bank" class="btn btn-warning btn-sm">Setor Bank</button>
                                        <?php } else if ($row['nama_bank'] == 'BRI' && $row['atas_nama'] == 'Bunda Ocha') { ?>
                                  <button data-id="<?=$row['id'];?>" id="btn_setor_bank" class="btn btn-warning btn-sm">Setor Bank</button>
                                        <?php } else if ($row['nama_bank'] == 'BJB') { ?>
                                  <button data-id="<?=$row['id'];?>" id="btn_bop" class="btn btn-danger btn-sm">Pemasukan Pengeluaran BOP</button>
                                        <?php } else { ?>
                                  <button data-id="<?=$row['id'];?>" id="btn_setor_bank" class="btn btn-warning btn-sm">Setor Bank</button>
                                  <button data-id="<?=$row['no_rek'];?>" id="btn_penerimaan_bank" class="btn btn-info btn-sm">Penerimaan Bank</button>
                                  <button data-id="<?=$row['id'];?>" id="btn_penarikan_bank" class="btn btn-success btn-sm">Penarikan Bank</button>
                                        <?php } ?>
                                  </td>
                                </tr>
                                <?php
                                // $perolehanBunga += $row['perolehan_bunga'];
                                ?>

                        <?php } ?>
                      </tbody>
                    </table>
                  </div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->


<div class="modal fade" id="modalKu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Rincian Rekening Koran</h4>
          <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>

        <div class="modal-body">

          <div class="form-group" id="rekBody">
              <label>Nama Bank</label>
              <input type="hidden" readonly name="nama_bank" id="e_id" class="form-control" placeholder="" id="bank"/>
              <input type="text" readonly name="nama_bank" id="e_bank" class="form-control" placeholder="" id="bank"/>
          </div>

          <div class="form-group">
              <label>Atas Nama</label>
              <input type="text" readonly name="atas_nama" id="e_nama" class="form-control" placeholder="" id="nama"/>
          </div>

          <div class="form-group">
              <label>No Rekening</label>
              <input type="text" readonly name="no_rek" id="e_rek" class="form-control" placeholder="" id="norek"/>
          </div>
          
          <div class="table-responsive">
            <table id="example" class="display nowrap" style="width:100%">
              <thead>
                  <tr>
                      <th>Tanggal</th>
                      <th>Saldo Akhir</th>
                      <th>Potongan Bunga</th>
                      <th>Perolehan Bunga</th>
                      <th>Deskripsi</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td id="e_tanggal"></td>
                      <td id="e_saldoAkhir"></td>
                      <td id="e_potonganBunga"></td>
                      <td id="e_perolehanBunga"></td>
                      <td id="e_deskripsi"></td>
                  </tr>
                </tbody>
              </table>
            </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Tutup</button>
        </div>

      </div>
  </div>
</div>

<div class="modal fade" id="modalPenerimaanBank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Penerimaan Bank</h4>
          <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>

        <div class="modal-body">

          <div class="table-responsive">
            <table id="DtPenerimaanBank" class="display nowrap" style="width:100%">
              <thead>
                  <tr>
                      <th>Tanggal</th>
                      <th>Nominal</th>
                      <th>Transaksi</th>
                  </tr>
              </thead>
              <tbody>
                </tbody>
              </table>
            </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Tutup</button>
        </div>

      </div>
  </div>
</div>

<div class="modal fade" id="modalSetorBank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Setor Bank</h4>
          <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>

        <div class="modal-body">

          <div class="table-responsive">
            <table id="DtSetorBank" class="display nowrap" style="width:100%">
              <thead>
                  <tr>
                      <th>Tanggal</th>
                      <th>Nominal</th>
                      <th>Transaksi</th>
                  </tr>
              </thead>
              <tbody>
                </tbody>
              </table>
            </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Tutup</button>
        </div>

      </div>
  </div>
</div>

<div class="modal fade" id="modalPenarikanBank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Penarikan Bank</h4>
          <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>

        <div class="modal-body">

          <div class="table-responsive">
            <table id="DtPenarikanBank" class="display nowrap" style="width:100%">
              <thead>
                  <tr>
                      <th>Tanggal</th>
                      <th>Nominal</th>
                      <th>Transaksi</th>
                  </tr>
              </thead>
              <tbody>
                </tbody>
              </table>
            </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Tutup</button>
        </div>

      </div>
  </div>
</div>

<div class="modal fade" id="modalBop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Transaksi BOP</h4>
          <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>

        <div class="modal-body">

          <div class="table-responsive">
            <table id="DtBop" class="display nowrap" style="width:100%">
              <thead>
                  <tr>
                      <th>Tanggal</th>
                      <th>Nominal</th>
                      <th>Nama Komponen</th>
                      <th>Keterangan Pengeluaran</th>
                  </tr>
              </thead>
              <tbody>
                </tbody>
              </table>
            </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Tutup</button>
        </div>

      </div>
  </div>
</div>

<script>
$(document).ready(function() {
  var t = $('#DtPenerimaanBank').DataTable({
      'columnDefs': [
            {
                "targets": "_all", // your case first column
                "className": "text-center",
            }
          ],
        'bFilter' : false,
        "lengthChange": false,
        "paging"  : false,
        "info"  : false
    });
    var t2 = $('#DtSetorBank').DataTable({
      'columnDefs': [
            {
                "targets": "_all", // your case first column
                "className": "text-center",
            }
          ],
        'bFilter' : false,
        "lengthChange": false,
        "paging"  : false,
        "info"  : false
    });
    var t3 = $('#DtPenarikanBank').DataTable({
      'columnDefs': [
            {
                "targets": "_all", // your case first column
                "className": "text-center",
            }
          ],
        'bFilter' : false,
        "lengthChange": false,
        "paging"  : false,
        "info"  : false
    });
    
      $('#example').DataTable( {
          "scrollX": true
      } );
  var t4 = $('#DtBop').DataTable({
      'columnDefs': [
            {
                "targets": "_all", // your case first column
                "className": "text-center",
            }
          ],
        'bFilter' : false,
        "lengthChange": false,
        "paging"  : false,
        "info"  : false,
        "scrollX" : true
    });
  function edit(id, bank, nama, rek, tanggal, saldoAkhir, potonganBunga, perolehanBunga, deskripsi){

$('#e_id').val(id);
$('#e_bank').val(bank);
$('#e_nama').val(nama);
$('#e_rek').val(rek);
$('#e_tanggal').text(tanggal);
$('#e_saldoAkhir').text(format_rp(saldoAkhir));
$('#e_potonganBunga').text(potonganBunga+' %');
$('#e_perolehanBunga').text(format_rp(perolehanBunga));
$('#e_deskripsi').text(deskripsi);


$('#modalKu').modal('show'); 
}

  $(document).on('click', '#btn_penerimaan_bank', function() {
    var no_rek = $(this).data('id');
    console.log(no_rek)
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    $.get(baseUrl+"/keuangan/transaksi/koran/ajaxPenerimaanBank/"+no_rek, function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            t.clear()
            $.each(element, function(index, el){
              t.row.add( [
                        el['tanggal_transaksi'],
                        el['nominal'],
                        el['transaksi'],
                    ]).draw( false )
            })

            $('#modalPenerimaanBank').modal('show')
        })
})
  $(document).on('click', '#btn_setor_bank', function() {
    var id = $(this).data('id');
    console.log(id)
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    $.get(baseUrl+"/keuangan/transaksi/koran/ajaxSetorBank/"+id, function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            t2.clear()
            $.each(element, function(index, el){
              t2.row.add( [
                        el['tanggal_transaksi'],
                        el['nominal'],
                        el['transaksi'],
                    ]).draw( false )
            })

            $('#modalSetorBank').modal('show')
        })
})
  $(document).on('click', '#btn_penarikan_bank', function() {
    var id = $(this).data('id');
    console.log(id)
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    $.get(baseUrl+"/keuangan/transaksi/koran/ajaxPenarikanBank/"+id, function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            t3.clear()
            $.each(element, function(index, el){
              t3.row.add( [
                        el['tanggal_transaksi'],
                        el['nominal'],
                        el['transaksi'],
                    ]).draw( false )
            })

            $('#modalPenarikanBank').modal('show')
        })
})
  $(document).on('click', '#btn_bop', function() {
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    $.get(baseUrl+"/keuangan/transaksi/koran/ajaxBop", function(data, status){
            console.log(data)
            let element = JSON.parse(data)
              t4.clear()
            $.each(element, function(index, el){
              t4.row.add( [
                        el['tanggal'],
                        el['nominal'],
                        el['nama_komponen'],
                        el['keterangan_pengeluaran'],
                    ]).draw( false )
            })

            $('#modalBop').modal('show')
        })
})

} )
</script>
