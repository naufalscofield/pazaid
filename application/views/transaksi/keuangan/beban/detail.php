<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">TRANSAKSI BEBAN</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Kas</a></li>
                    <li class="breadcrumb-item"><a href="#">Daftar Transaksi Beban</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Tambah Transaksi Beban</h5>
                </div>

              
              <div class="row">
                  <div class="col-md-12">
                    <?php echo $this->session->userdata('alert_message') ?>

                    <a href="<?php echo site_url('keuangan/transaksi_beban') ?>" class="btn btn-outline-secondary btn-flat mb-2 mt-2">
                    <i class="fa fa-chevron-left"></i> KEMBALI</a>
                  <br>

                  </div>
                </div>

             


              <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                    <h6 class="mb-4">DETAIL</h6>
                      
                    <table class="table">
                        <tr>
                          <th style="width: 20%; background-color: #eee">KODE</th>
                          <td><?php echo $pembelian['kode_transaksi'] ?></td>
                          <th style="width: 20%; background-color: #eee">TOTAL PEROLEHAN</th>
                          <td><?php echo format_rp($pembelian['total_transaksi']) ?></td>
                        </tr>

                        <tr>
                          <th style="width: 20%; background-color: #eee">TIPE</th>
                          <td><?php echo $pembelian['pembayaran'] ?></td>
                          <th style="width: 20%; background-color: #eee">TOTAL BAYAR</th>
                          <td><?php echo format_rp($pembelian['total_bayar']) ?></td>
                        </tr>

                        <tr>
                          <th style="width: 20%; background-color: #eee">STATUS</th>
                          <td><?php if($pembelian['status'] == 'Lunas'){
                              echo "<span class='badge badge-success'>Lunas</span>";
                            }else{
                              echo "<span class='badge badge-danger'>Belum Lunas</span>";
                            } ?></td>
                          <th style="width: 20%; background-color: #eee">SISA BAYAR</th>
                          <td><?php echo format_rp($pembelian['sisa_bayar']) ?></td>
                        </tr>
                      </table>

                    </div>
                  </div>

                </div>

                <div class="col-md-12">
                    <div class="card">
                      <div class="card-body">
                        <h6 class="mb-4">DAFTAR BEBAN</h6>

                        <div class="table-responsive">
                          <table class="table">
                            <thead style="background-color:#eee">
                              <tr>
                                <th>BEBAN</th>
                                <th class="text-center">HARGA SATUAN</th>
                                <th class="text-center" style="width: 10%">JUMLAH</th>
                                <th class="text-center" style="width: 25%">SUBTOTAL</th>
                              </tr>
                            </thead>

                            <tbody id="tableItem">
                              <?php $n = $grandTotal = 0; 
                                    foreach ($item as $row) { $n++; $grandTotal += $row['qty'] * $row['harga']?>

                                      <tr>
                                        <td><?php echo $row['kode_beban']." / ".$row['nama_beban'] ?></td>
                                        <td class="text-right"><?php echo format_rp($row['harga']) ?></td>
                                        <td class="text-center"><?php echo $row['qty'] ?></td>
                                        <td class="text-right"><?php echo format_rp($row['qty'] * $row['harga']) ?></td>
                                      </tr>

                              <?php } ?>

                            </tbody>

                              <tr>
                                <td><input type="hidden" id="inputGrandTotal" name="grandTotal" value="<?php echo $grandTotal ?>"></td>
                                <td colspan="2"><h4><b>TOTAL</b></h4></td>
                                <td class="text-right"><h4><b id="grandTotal"><?php echo format_rp($grandTotal) ?></b></h4></td>
                              </tr>

                          </table>
                        </div>

                      </div>
                    </div>

                    
                </div>
              </div>

    
</main>
<!-- end::main content -->
