<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">MODAL</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Keuangan</a></li>
                    <li class="breadcrumb-item"><a href="#">Kas</a></li>
                    <li class="breadcrumb-item"><a href="#">Modal</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Setor</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Setoran Modal</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
                    <div class="col-md-12">
                      <?php echo $this->session->flashdata('alert_message') ?>
                    </div>
                  </div>

                  <button data-toggle="modal" data-target="#modalTambahKTG" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah Setoran</button>
                          <br><br>

                  <div class="table-responsive">
                      <table class="display table table-hover datatables" >
                        <thead style="background-color: #eee">
                          <tr>
                            <th style="width: 5%">No</th>
                            <th>Tanggal Setoran</th>
                            <th>Nama Penyetor</th>
                            <th>No Rek</th>
                            <th>Nominal</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $n = 0;
                                foreach ($data as $row) { $n++; ?>

                                  <tr>
                                    <td><?php echo $n ?></td>
                                    <td><?php echo $row['tanggal_setoran'] ?></td>
                                    <td><?php echo $row['nama_pemilik'] ?></td>
                                    <td><?php echo $row['no_rek'] ?></td>
                                    <td><?php echo format_rp($row['nominal']) ?></td>
                                  </tr>

                          <?php } ?>
                        </tbody>
                      </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->


<form action="<?php echo site_url('insert_setoran_modal') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalTambahKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Setoran</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <div class="modal-body">
                <div class="form-group">
                   <label>Kode Setoran</label>
                   <input type="text" value="<?php echo $last_code ?>" readonly name="kode_transaksi" id="" class="form-control" placeholder="Kode Setoran" autocomplete="off" required />
                </div>

                <div class="form-group" id="rekBody">
                   <label>Pilih Penyetor</label>
                   <select class="form-control" name="id_pemilik" id="id_pemilik">
                    <option value="">Pilih</option>
                    <?php foreach ($pemilik as $row) { ?>
                        <option value="<?php echo $row['id'] ?>"><?php echo $row['nama_pemilik']." - ".$row['p_no_rek']." a/n ".$row['p_an'] ?></option>
                    <?php } ?>
                   </select>
                </div>

                <div class="form-group" id="rekBody">
                   <label>Pilih Rekening</label>
                   <select class="form-control" name="rek_id">
                    <option value="">Pilih</option>
                    <?php foreach ($rek as $row) { ?>
                        <option value="<?php echo $row['id'] ?>"><?php echo $row['nama_bank']." - ".$row['no_rek']." a/n ".$row['atas_nama'] ?></option>
                    <?php } ?>
                   </select>
                </div>

                <div class="form-group">
                   <label>Nominal</label>
                   <input autocomplete="off" type="number" name="nominal" id="" class="form-control" placeholder="Nominal (Rp)..." required/>
                </div>

                <!-- <div class="form-group">
                   <label>Keterangan</label>
                   <input autocomplete="off" type="text" name="keterangan" id="" class="form-control" placeholder=" Keterangan..." required/>
                </div> -->

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>

   <script>
    $(document).ready(function(){
      let val = $(this).val()
      var getUrl = window.location;
      var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
          $.get(baseUrl+"/keuangan/modal/ajaxPemilik", function(data, status){
              console.log(data)
              let element = JSON.parse(data)
              $('#no_rek').empty()
              $('#no_rek').val(element.no_rek)
        })
    })
   </script>
