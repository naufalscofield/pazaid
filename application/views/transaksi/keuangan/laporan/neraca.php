<!-- begin::header -->

<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Laporan Neraca</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Neraca</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
              <h5 class="card-title">Daftar Neraca</h5>
          </div>
          <div class="card-body">
            <form method="GET">
              <div class="row">

                <div class="col-md-2">
                  <label><b>Bulan</b></label>
                  <select class="form-control" name="bulan" required="">
                    <option value="">Pilih</option>
                    <?php for ($i = 1 ; $i <= 12 ; $i++){ ?>
                        <option <?php if($this->input->get('bulan') == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo get_monthname($i) ?></option>
                    <?php } ?>
                  </select>
                </div>

                <div class="col-md-2">
                  <label><b>Tahun</b></label>
                  <select class="form-control" name="tahun" required="">
                    <option value="">Pilih</option>
                    <?php for ($i = 2019 ; $i <= (date('Y')+ 1) ; $i++){ ?>
                        <option <?php if($this->input->get('tahun') == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                    <?php } ?>
                  </select>
                </div>

                <div class="col-md-1">
                  <br>
                  <button style="margin-top: 8px" class="btn btn-primary"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
            <br>
              <hr>
                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-md-8">
                    <center>
                      <h4>PT. ABC</h4>
                      <h5>LAPORAN NERACA</h5>
                      Periode Bulan <?php echo get_monthname($this->input->get('bulan'))." Tahun ".$this->input->get('tahun') ?>
                    </center>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              <hr>
            <?php if($this->input->get('bulan')){ ?>
              <div class="container">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="card">
                    <div class="card-header" style="background-color: #eee">
                      AKTIVA
                    </div>
                      <div class="card-body">
                        <div class="table-responsive">
                          <table class="table">
                              <thead style="background-color: #eee">
                              <tr>
                                <td><b>Aktiva Lancar</b></td>
                                <td></td>
                                <td></td>
                              <tr>
                              </thead>
                            <tbody>
                              <?php $totalAktivaLancar=0; foreach ($aktivaLancar as $lancar) { ?>
                                <tr>
                                  <td><?=$lancar['coa_id'];?></td>
                                  <td><?=$lancar['nama_coa'];?></td>
                                  <td><?=format_rp($lancar['nominal']);?></td>
                                </tr>
                              <?php $totalAktivaLancar += $lancar['nominal']; }?>
                              <tr>
                              <th>TOTAL</th>
                              <th></th>
                              <th class="text-right"><?= format_rp($totalAktivaLancar);?></th>
                            </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="card">
                    <div class="card-header" style="background-color: #eee">
                      PASIVA
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                          <table class="table">
                              <thead style="background-color: #eee">
                              <tr>
                                <td><b>Utang Jangka Pendek</b></td>
                                <td></td>
                                <td colspan="2"></td>
                              <tr>
                              </thead>
                            <tbody>
                            <?php $totalPassivaJangkaPendek=0; foreach ($passivaJangkaPendek as $pendek) { ?>
                                <tr>
                                  <td><?=$pendek['coa_id'];?></td>
                                  <td><?=$pendek['nama_coa'];?></td>
                                  <td><?=format_rp($pendek['nominal']);?></td>
                                </tr>
                              <?php $totalPassivaJangkaPendek += $pendek['nominal']; }?>
                              <tr>
                              <th>TOTAL</th>
                              <th></th>
                              <th class="text-right"><?= format_rp($totalPassivaJangkaPendek);?></th>
                            </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="card">
                      <div class="card-body">
                        <div class="table-responsive">
                          <table class="table">
                              <thead style="background-color: #eee">
                              <tr>
                                <td><b>Aktiva Tetap</b></td>
                                <td></td>
                                <td></td>
                              <tr>
                              </thead>
                            <tbody>
                              <?php $totalAktivaTetap=0; foreach ($aktivaTetap as $tetap) { ?>
                                <tr>
                                  <td><?=$tetap['coa_id'];?></td>
                                  <td><?=$tetap['nama_coa'];?></td>
                                  <td><?=format_rp($tetap['nominal']);?></td>
                                </tr>
                              <?php $totalAktivaTetap += $tetap['nominal']; }?>
                              <tr>
                              <th>TOTAL</th>
                              <th></th>
                              <th class="text-right"><?= format_rp($totalAktivaTetap);?></th>
                            </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                          <table class="table">
                              <thead style="background-color: #eee">
                              <tr>
                                <td><b>Utang Jangka Panjang</b></td>
                                <td></td>
                                <td colspan="2"></td>
                              <tr>
                              </thead>
                            <tbody>
                            <?php $totalPassivaJangkaPanjang=0; foreach ($passivaJangkaPanjang as $Panjang) { ?>
                                <tr>
                                  <td></td>
                                  <td>Utang Jangka Panjang</td>
                                  <td>RP 0</td>
                                  <!-- <td><?=$Panjang['coa_id'];?></td>
                                  <td><?=$Panjang['nama_coa'];?></td>
                                  <td><?=format_rp($Panjang['nominal']);?></td> -->
                                </tr>
                              <?php $totalPassivaJangkaPanjang += $Panjang['nominal']; }?>
                              <tr>
                              <th>TOTAL</th>
                              <th></th>
                              <th class="text-right"><?= format_rp($totalPassivaJangkaPanjang);?></th>
                            </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="card">
                      <div class="card-body">
                        
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                          <table class="table">
                              <thead style="background-color: #eee">
                              <tr>
                                <td><b>Modal</b></td>
                                <td></td>
                                <td colspan="2"></td>
                              <tr>
                              </thead>
                            <tbody>
                            <?php $totalModal=0; foreach ($modal as $modal) { ?>
                                <tr>
                                  <td></td>
                                  <td>Utang Jangka modal</td>
                                  <td>RP 0</td>
                                  <!-- <td><?=$modal['coa_id'];?></td>
                                  <td><?=$modal['nama_coa'];?></td>
                                  <td><?=format_rp($modal['nominal']);?></td> -->
                                </tr>
                              <?php $totalModal += $modal['nominal']; }?>
                              <tr>
                              <th>TOTAL</th>
                              <th></th>
                              <th class="text-right"><?= format_rp($totalModal);?></th>
                            </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
        </div>
      </div>
    </div>
    
</main>