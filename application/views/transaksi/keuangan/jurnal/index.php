<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Jurnal</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Jurnal</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Jurnal</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
                      <div class="col-md-12">
                        <?php echo $this->session->flashdata('alert_message') ?>
                      </div>
                    </div>

                    <a class="btn btn-primary" href="<?php echo site_url('keuangan/transaksi/jurnal/tambah') ?>"><i class="fa fa-plus"></i> Tambah Jurnal</a>
                    <br><br>

                    <div class="table-responsive">
                      <table class="display table table-hover datatables" >
                        <thead class="bg-primary">
                          <tr>
                            <th style="width: 5%">No</th>
                            <th>Kode</th>
                            <th>Waktu Jurnal</th>
                            <th>Keterangan</th>
                            <th>Nominal</th>
                            <th>Posting</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $n = 0;
                              foreach ($list as $row) { $n++; ?>

                                <tr>
                                  <td><?php echo $n ?></td>
                                  <td><?php echo $row['kode_jurnal'] ?> <br> 
                                  <td><?php echo $row['tanggal_input'] ?></td>
                                  <td><?= $row['keterangan'] ?></td>

                                   <td class="text-right"><?= format_rp($row['total_nominal']) ?></td>
                                  <td>
                                    <?php if($row['is_post'] == '1'){
                                      echo "<span class='text-success'><i class='fa fa-check-circle'></i> Sudah</span>";
                                    }else{
                                      echo "<span class='text-danger'><i class='fa fa-minus-circle'></i> Belum</span>";
                                    } ?>
                                  </td>

                                  <td class="text-center">
                                    <a class="btn btn-primary btn-sm shadow" href="<?php echo site_url('keuangan/transaksi/jurnal/detail/'.$row['id']) ?>"><i class="fa fa-search"></i></a>
                                  </td>
                                </tr>

                        <?php } ?>
                        </tbody>
                      </table>
                    </div>


                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->


<form action="<?php echo site_url('insert_setoran_bank') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalTambahKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Setoran</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <div class="modal-body">
                <div class="form-group">
                   <label>Kode Setoran</label>
                   <input type="text" value="<?php echo $last_code ?>" readonly name="kode_transaksi" id="" class="form-control" placeholder="Kode Setoran" autocomplete="off" required />
                </div>


                <div class="form-group" id="rekBody">
                   <label>Pilih Rekening</label>
                   <select class="form-control" name="rek_id">
                    <option value="">Pilih</option>
                    <?php foreach ($rek as $row) { ?>
                        <option value="<?php echo $row['id'] ?>"><?php echo $row['nama_bank']." - ".$row['no_rek']." a/n ".$row['atas_nama'] ?></option>
                    <?php } ?>
                   </select>
                </div>

                <div class="form-group">
                   <label>Nominal</label>
                   <input autocomplete="off" type="text" name="total_transaksi" id="" class="form-control rupiah" placeholder="Nominal (Rp)..." required/>
                </div>

                <div class="form-group">
                   <label>Keterangan</label>
                   <input autocomplete="off" type="text" name="keterangan" id="" class="form-control" placeholder=" Keterangan..." required/>
                </div>

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>
