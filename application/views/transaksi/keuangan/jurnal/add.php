<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Jurnal</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
                    <li class="breadcrumb-item"><a href="#">Jurnal</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tambah</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Tambah Jurnal</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
                      <div class="col-md-12">
                        <?php echo $this->session->flashdata('alert_message') ?>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <a href="<?php echo site_url('keuangan/transaksi/jurnal') ?>" class="btn btn-outline-primary btn-flat pull-left"><i class="fa fa-chevron-left"></i> KEMBALI</a>

                        &emsp;

                        <button id="btnAdd" class="btn btn-primary btnAdd"><i class="fa fa-plus"></i> Tambahkan Kolom Baru</button>
                      </div>
                    </div>

                    <br><br>

                    <div class="row">
                      <div class="col-md-12">
                        <form method="POST" action="<?= site_url('insert_jurnal') ?>">
                          <table class="table">
                            <tr>
                                <th>Akun</th>
                                <th>Posisi Akun</th>
                                <th>Nominal</th>
                                <th></th>
                            </tr>
                            <tbody class="input-wrap">
                                <tr>
                                    <td>
                                        <select class="form-control" name="akun[1]" required="">
                                            <option value="">Pilih</option>
                                            <?php foreach ($coa as $row){ ?>
                                                <option value="<?= $row['id'] ?>"><?= $row['kode_coa']." - ".$row['nama_coa'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control" name="posisi_akun[1]" required="">
                                            <option value="">Pilih</option>
                                            <option value="debit">Debit</option>
                                            <option value="kredit">Kredit</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control rupiah" name="nominal[1]" required="" autocomplete="off" placeholder="Rp. 0">
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>

                            <tr>
                                <th colspan="3">
                                    <textarea class="form-control" placeholder="Keterangan..." name="keterangan"></textarea>
                                </th>
                                <th>
                                    <button class="btn btn-success"><i class='fa fa-check'></i> Simpan</button>
                                </th>
                                
                            </tr>
                          </table>
                      </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->

<script type="text/javascript">
    $(document).ready(function() {
        var max_fields      = 100000000; //maximum input boxes allowed
        var wrapper         = '.input-wrap'; //Fields wrapper
        var add_button      = '.btnAdd'; //Add button ID
        
        var x = 2; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            x++; //text box increment
            var txt = '';
            var select  = '<select class="form-control" name="akun['+x+']" required="">';
                select +=   '<option value="">Pilih</option>';

            <?php foreach ($coa as $row) { ?>
                select += '<option value="<?= $row['id'] ?>"><?= $row['kode_coa']." - ".$row['nama_coa'] ?></option>';
            <?php } ?>

                select += '</select>';

            if(x < max_fields){ //max input box allowed
                
                txt += '<tr class="row'+x+'">';
                txt +=    '<td>';
                txt +=        select;
                txt +=    '</td>'
                txt +=    '<td>';
                txt +=     '<select class="form-control" name="posisi_akun['+x+']" required="">';
                txt +=          '<option value="">Pilih</option>';
                txt +=          '<option value="debit">Debit</option>';
                txt +=          '<option value="kredit">Kredit</option>';
                txt +=     '</select>';
                txt +=    '</td>';
                txt +=    '<td>';
                txt +=      '<input type="text" class="form-control rupiah" name="nominal['+x+']" required="" autocomplete="off" placeholder="Rp. 0">';
                txt +=    '</td>';
                txt +=    '<td>';
                txt +=      '<button data-id="'+ x +'" class="remove_field btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                txt +=    '</td>';
                txt += '</tr>';
                $(wrapper).append(txt); //add input box

                $(".rupiah").keyup(function(){
                    value = this.value;
                    angka = format_angka(value);
                    if(isNaN(angka)){
                        this.value = '';
                    }else{
                        rupiah = format_rp(angka);
                        this.value = rupiah;
                    }
                });
            }
        });
        
        $(document).on("click",".remove_field", function(e){ 
            var id = $(this).attr('data-id');
            e.preventDefault(); 
            $('.row'+id).remove();
        })
    });
</script>

