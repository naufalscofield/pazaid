<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Jurnal</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Transaksi</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Jurnal</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Detail Jurnal</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
                      <div class="col-md-12">
                        <?php echo $this->session->flashdata('alert_message') ?>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <a href="<?php echo site_url('keuangan/transaksi/jurnal') ?>" class="btn btn-outline-primary btn-flat pull-left"><i class="fa fa-chevron-left"></i> KEMBALI</a>
                      </div>
                    </div>

                    <br><br>
            
                    <div class="row">
                      <div class="col-md-12">
                        <div class="card">
                          <div class="card-header bg-primary"><b>DETAIL</b></div>
                        </div>

                        <table class="table">
                          <tr>
                            <th style="width: 15%;background-color: #eee">KODE</th>
                            <td style="width: 35%"><?php echo $jurnal['kode_jurnal'] ?></td>
                            <th style="width: 20%;background-color: #eee">TANGGAL</th>
                            <td>
                              <?php echo $jurnal['tanggal_input'] ?>
                            </td>
                          </tr>

                          <tr>
                            <th style="width: 15%;background-color: #eee">SUMBER</th>
                            <td><?php echo $jurnal['source'] ?></td>
                            <th style="width: 15%;background-color: #eee">WAKTU POSTING</th>
                            <td>
                              <?php if($jurnal['is_post'] == '0'){
                                echo "<span class='text-danger'><i class='fa fa-minus-circle'></i> Belum diposting</span>";
                              }else{
                                echo "<span class='text-success'><i class='fa fa-check-circle'></i> ".$jurnal['waktu_post']." </span>";
                              } ?>
                            </td>
                          </tr>

                          <tr>
                            <th style="width: 15%;background-color: #eee">KETERANGAN</th>
                            <td colspan="3"><?php echo $jurnal['keterangan'] ?></td>
                          </tr>

                        </table>

                      </div>

                      <div class="col-md-12">
                          <div class="card">
                            <div class="card-header bg-primary"><b>JURNAL</b></div>
                          </div>

                           <div class="table-responsive">
                            <table class="table">
                              <thead style="background-color:#eee">
                                <tr>
                                  <th style="width: 45%">AKUN</th>
                                  <th>REF</th>
                                  <th>DEBIT</th>
                                  <th>KREDIT</th>
                                </tr>
                              </thead>

                              <form method="POST" action="<?php echo site_url('post_jurnal/'.$jurnal['id']) ?>">
                              <tbody id="tableItem">
                                <?php $n = $grandTotal = $t_debit = $t_kredit = 0; 
                                      foreach ($jurnal_list as $row) { 
                                        if($row['posisi'] == 'debit'){
                                          $t_debit += $row['nominal'];
                                          $title = $row['nama_coa'];
                                        }else{
                                          $t_kredit += $row['nominal'];
                                          $title = '&emsp;&emsp;&emsp;'.$row['nama_coa'];
                                        }
                                ?>

                                        <tr>

                                          <?php if($row['coa_id'] == '1'){ ?>

                                            <td colspan="2">
                                              <select class="form-control" name="coa_id[<?= $row['jurnal_id'] ?>]" required="">
                                                <option value="">Pilih Akun Kas</option>
                                                <option value="25">Kas Kecil</option>
                                                <option value="26">Kas Institusi</option>
                                              </select>
                                            </td>

                                          <?php }else{ ?>

                                            <td><?php echo $title ?></td>
                                            <td><?php echo $row['kode_coa'] ?></td>

                                          <?php } ?>

                                          <?php if($row['posisi'] == 'debit'){ ?>
                                              <td class="text-right"><?php echo format_rp($row['nominal']) ?></td>
                                              <td></td>
                                          <?php }else{ ?>
                                              <td></td>
                                              <td class="text-right"><?php echo format_rp($row['nominal']) ?></td>
                                          <?php } ?>

                                        </tr>

                                <?php } ?>

                              </tbody>

                                <tr>
                                  <td colspan="2"><h5><b>TOTAL</b></h5></td>
                                  <td class="text-right"><h5><b id="grandTotal"><?php echo format_rp($t_debit) ?></b></h5></td>
                                  <td class="text-right"><h5><b id="grandTotal"><?php echo format_rp($t_kredit) ?></b></h5></td>
                                </tr>

                                <?php if($jurnal['is_post'] == '0'){ ?>
                                  <tr>
                                    <th colspan="3"></th>
                                    <th class="text-right">
                                      <button onclick="return confirm('Apakah anda yakin ?')" class="btn btn-success"><i class="fa fa-check"></i>&nbsp; POST SEKARANG</button>
                                    </th>
                                  </tr>
                                <?php } ?>
                              </form>
                            </table>
                          </div>
                      </div>

                      <!--
                      <div class="col-md-4">
                          
                          <div class="card">
                            <div class="card-header bg-primary"><b>POSTING JURNAL</b>
                            </div>

                            <div class="card-body text-center">
                              <?php if($jurnal['status_jurnal'] == '0'){ ?>
                                    <a onclick="return confirm('Apakah anda yakin ?')" class="btn btn-success" href="<?php echo site_url('post_jurnal/'.$transaksi['id']) ?>"><i class="fa fa-check"></i> TERIMA</a>
                                  </div>

                              <?php }else{ ?>

                                    <span class="badge badge-success"><i class="fa fa-check-circle"></i> Sudah diposting</span>

                              <?php } ?>
                            </div>

                      </div> -->
                    </div>


                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->

