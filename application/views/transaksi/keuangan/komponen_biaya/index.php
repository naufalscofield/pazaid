<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Komponen Biaya</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Komponen Biaya</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Komponen Biaya</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
                    <div class="col-md-12">
                      <?php echo $this->session->flashdata('alert_message') ?>
                    </div>
                  </div>

                  <button data-toggle="modal" data-target="#modalTambahKTG" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> &nbsp; Tambah Komponen Biaya</button>
                          <br><br>

                  <div class="table-responsive">
                      <table class="display table table-hover datatables" >
                        <thead style="bg-primary">
                          <tr>
                            <th style="width: 5%">No</th>
                            <th>Nama Komponen</th>
                            <th>Tipe Komponen</th>
                            <th>Harga Komponen</th>
                            <th>Status</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>

                          </tr>
                        </thead>
                        <tbody>
                          <?php $n = 0;
                                foreach ($data as $row) { $n++; ?>

                                  <tr>
                                    <td><?php echo $n ?></td>
                                    <td><?php echo $row['nama_komponen'] ?></td>
                                    <td><?php echo $row['tipe_komponen'] ?></td>
                                    <td><?php echo format_rp($row['harga_komponen']) ?></td>
                                    <td><?php if($row['status_cicil'] == "Tidak Bisa Dicicil") { ?>
                                        <t class ="badge badge-warning"><?php echo $row['status_cicil']?></t>
                                    <?php }else{ ?>
                                        <t class ="badge badge-success"><?php echo $row['status_cicil']?></t>
                                    <?php } ?>
                                    </td>
                                    <td>
                                    <a href="javascript:void(0)" data-toggle="tooltip" title="Ubah" class="btn btn-warning btn-sm"
                                                onclick="
                                                edit(		                   
                                                  '<?php echo $row['nama_komponen'] ?>',                           
                                                  '<?php echo $row['id_komponen'] ?>',                           
                                                  '<?php echo $row['tipe_komponen'] ?>',                           
                                                  '<?php echo $row['harga_komponen'] ?>',                           
                                                  '<?php echo $row['status_cicil'] ?>',                           
                                                )">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        &nbsp;
                                    <a onclick="return confirm('Hapus data ini ?')" href="<?php echo site_url('keuangan/komponenbiaya/delete/'.$row['id_komponen']) ?>" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm">
		                                    <i class="fa fa-trash"></i>
                                        </a>
                                    <td>
                                  </tr>

                          <?php } ?>
                        </tbody>
                      </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->


<form action="<?php echo site_url('keuangan/komponenbiaya/add') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalTambahKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Setoran</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <div class="modal-body">
                <div class="form-group">
                   <label>Nama Komponen</label>
                   <input type="text" name="nama_komponen" id="" class="form-control" placeholder="Nama Komponen" autocomplete="off" required />
                </div>
 
                <div class="form-group">
                   <label>Tipe Komponen</label>
                   <input type="text" name="tipe_komponen" id="" class="form-control" placeholder="Tipe Komponen" autocomplete="off" required />
                </div>

                <div class="form-group">
                   <label>Harga Komponen</label>
                   <input autocomplete="off" class="form-control" type="text" name="harga_komponen" id="" placeholder="Nominal (Rp)..." required/>
                </div>

                <div class="form-group">
                   <label>Status</label>
                   <select class="form-control" name="status_cicil">
                    <option value="">Pilih</option>
                        <option value="Tidak Bisa Dicicil">Tidak Bisa Dicicil</option>
                        <option value="Bisa Dicicil">Bisa Dicicil</option>
                   </select>
                </div>

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
</form>

   <form action="<?php echo site_url('keuangan/komponenbiaya/update') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalEditKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Ubah Komponen</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   <input type="hidden" name="id_komponen" id="e_id_komponen" class="form-control" placeholder="Nama Komponen" autocomplete="off" required />
             </div>


             <div class="modal-body">
 
                <div class="form-group">
                   <label>Nama Komponen</label>
                   <input type="text" name="nama_komponen" id="e_nama" class="form-control" placeholder="Nama Komponen" readonly/>
                   <input type="hidden" name="id_komponen" id="e_id" class="form-control" placeholder="Id Komponen" readonly autocomplete="off" required />
                </div>
                <div class="form-group">
                   <label>Tipe Komponen</label>
                   <input type="text" name="tipe_komponen" id="e_tipe_komponen" class="form-control" placeholder="Tipe Komponen" autocomplete="off" required />
                </div>

                <div class="form-group">
                   <label>Harga Komponen</label>
                   <input autocomplete="off" class="form-control" type="text" name="harga_komponen" id="e_harga_komponen" placeholder="Nominal (Rp)..." required/>
                </div>

                <div class="form-group">
                   <label>Status</label>
                   <select class="form-control" name="status_cicil" id="e_status_cicil">
                    <option value="">Pilih</option>
                        <option value="Tidak Bisa Dicicil">Tidak Bisa Dicicil</option>
                        <option value="Bisa Dicicil">Bisa Dicicil</option>
                   </select>
                </div>

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i> Ubah</button>
             </div>

           </div>
        </div>
     </div>
   </form>

<script type="text/javascript">
function edit(nama_komponen, id_komponen, tipe_komponen, harga_komponen, status_cicil){

  $('e_nama').val(nama_komponen);
  $('#e_id').val(id_komponen);
  $('#e_tipe_komponen').val(tipe_komponen);
  $('#e_harga_komponen').val(harga_komponen);
  $('#e_status_cicilan').val(status_cicil);


  $('#modalEditKTG').modal('show'); 
}
</script>

