<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Pembayaran Lainnya</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Keuangan</a></li>
                    <li class="breadcrumb-item"><a href="#">Penerimaan Kas</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Pembayaran Lainnya</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Tagihan Lainnya</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
                        <div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>Tanggal</th>
									<th>NIS</th>
									<th>Jumlah</th>
									<th>Total Tagihan</th>
									<th>Jenis Tambahan</th>
									<th>Jenis Pembayaran</th>
									<th>Status Pembayaran</th>
								</tr>
							</thead>
							<tbody>
                                <?php $n = 1; 
                                    foreach($data1 as $row){ ?>
                                <tr>
                                    <td><?php echo $n++?>.</td>
                                    <td><?php echo $row['tanggal']?></td>
                                    <td><?php echo $row['nis']?></td>
                                    <td><?php echo $row['jumlah_jam']?></td>
                                    <td><?php echo format_rp($row['total_tagihan'])?></td>
                                    <td><?php echo $row['jenis_tambahan']?></td>
                                    <td><?php echo $row['jenis_pembayaran']?></td>
                                    <td><?php if($row['status_lainnya'] == "Belum Lunas"){?>
                                        <t class="badge badge-danger"><?php echo $row['status_lainnya'] ?></t>
                                    <?php }else{ ?>
                                        <t class="badge badge-success"><?php echo $row['status_lainnya']  ?></t>
                                    <?php } ?>
                                    </td>
                                </tr>    
                                    <?php }?>                  
                                <?php  ?>
							</tbody>
						</table>
					</div>
						</div>
					</div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">NIS</h5>
                    <input type="text" class="form-control" id="nis_cari"placeholder="NIS"><br>
                    <button id="btn_cari" type="button" class="btn btn-info">Cari</button>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

        <div class="card">
            <div class="card-body">


                <div class="row">
        <div class="col-md-12">

        <div class="card">
            <div class="card-body">

            <div class="row">
                <div class="col-md-12">
                <br>
                <table class="table">
            <form action="<?= base_url();?>keuangan/pembayaranlainnya/StorePembayaranLainnya" method="POST">

                        <tr>
                        <th>NIS</th>
                        <td id="">
                        <input readonly type="text" class="form-control" id="nis" name="nis">
                        </td>
                        
                        <th>Nama Siswa</th>
                        <td id="">
                        <input readonly type="text" class="form-control" id="nama_siswa" name="nama_siswa">
                        </td>
                        </tr>

                        <tr>
                        <th>Total Tagihan Overtime</th>
                        <td id="">
                        <input readonly type="number" class="form-control" id="total_overtime" name="total_overtime">
                        </td>
                        
                        <th>Total Tagihan Catering</th>
                        <td id="">
                        <input readonly type="number" class="form-control" name="total_catering" id="total_catering">
                        </td>
                        </tr>
                        
                        <tr>
                        <th>Total Sarapan</th>
                        <td id="">
                        <input readonly type="number" class="form-control" name="total_sarapan" id="total_sarapan">
                        </td>
                        
                        <th>Total Mandi</th>
                        <td id="">
                        <input readonly type="number" class="form-control" id="total_mandi" name="total_mandi">
                        </td>
                        </tr>

                        <tr>
                        
                        <th>Total Tagihan</th>
                        <td id="">
                        <input readonly type="text" class="form-control" id="total_tagihan" name="total_tagihan">
                        <input readonly type="hidden" class="form-control" id="total_tagihan_input" name="total_tagihan_input">
                        </td>
                        </tr>

                    <!-- <div class="form-group"> -->

                </table>
                    
                </div>
            </div>
        </div>

    </div>
            <div class="form-group">
            

            <hr>
            <input readonly type="hidden" class="form-control" id="nis_input" name="nis_input">

            <b><label for="lname">Jenis Pembayaran</label></b><br>
            <select required class="form-control" name="jenis_pembayaran" id="jenis_pembayaran">
                <option value="">--Pilih Jenis Pembayaran--</option>
                <option value="cash">Cash</option>
                <option value="transfer">Transfer</option>
            </select>
            </div>

            <div class="form-group" id="divRekening">
            <b><label for="lname">Rekening</label></b><br>
            <select  class="form-control" name="rekening" id="rekening">
                <option value="">--Pilih Rekening--</option>
            </select>
            </div>

            <div class="form-group" id="">
            <b><label for="lname">No Rekening</label></b><br>
            <input name="no_rek" type="number"  class="form-control" id="no_rek">
            </div>
            <center>
            
            <button type="submit" class="btn btn-primary">Bayar</button>
            </center>

            </form>



            </div>

            </div>

        </div>
        </div>

    </div>

<!-- end::main content -->

<script type="text/javascript" src="<?=base_url();?>assets/js/autonumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(document).on('click', '#btn_cari', function() {
        let val = $('#nis_cari').val()
        const getUrl = window.location;
        const baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
            $.get(baseUrl+"/keuangan/pembayaranlainnya/ajaxTagihanLainnya/"+val, function(data, status){
                let element = JSON.parse(data)
                console.log(element)

                // //anak
                $('#nis').val(element.nis)
                $('#nis_input').val(element.nis)
                $('#nama_siswa').val(element.nama_siswa)
                $('#total_overtime').val(element.overtime)
                $('#total_catering').val(element.catering)
                $('#total_sarapan').val(element.sarapan)
                $('#total_mandi').val(element.mandi)
                $('#total_tagihan').val(element.total)
                // $("#total_tagihan").autoNumeric('init', {
                //     aSep: '.', 
                //     aDec: ',',
                //     aForm: true,
                //     vMax: '999999999',
                //     vMin: '-999999999'
                // });

            })
    
    })

    $(document).on('change', '#jenis_pembayaran', function() {
    let val = $(this).val()
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    if (val == 'transfer')
    {
        $('#rekening').prop("readonly", false)
        $('#no_rek').prop("readonly", false)
        $.get(baseUrl+"/keuangan/rekening/ajaxRekening", function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            $('#rekening').empty()
            $('<option>').val("").text("--Pilih Rekening--").appendTo('#rekening');
            $.each(element, function(index, el){
                if (el.nama_bank == 'MANDIRI')
                {
                    $('<option>').val(el.kode_rek).text(el.nama_bank).appendTo('#rekening');
                }
            })
        })
    } else {
        $('#rekening').prop("readonly", true)
        $('#no_rek').prop("readonly", true)
        $('#rekening').empty()
        $('#no_rek').val("")

    }
});

$(document).on('change', '#rekening', function() {
    let val = $(this).val()
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        $.get(baseUrl+"/keuangan/rekening/ajaxNoRekening/"+val, function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            $('#no_rek').empty()
            $('#no_rek').val(element.no_rek)
        })
});

})

</script>

