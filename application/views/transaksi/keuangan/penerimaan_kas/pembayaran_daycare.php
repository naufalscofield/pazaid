<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Pembayaran Daycare</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Keuangan</a></li>
                    <li class="breadcrumb-item"><a href="#">Penerimaan Kas</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Pembayaran Daycare</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Daycare</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
                        <div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>Kode Transaksi</th>
									<th>Nama Lengkap</th>
									<th>Umur</th>
									<th>Tempat Lahir</th>
									<th>Tanggal Lahir</th>
									<th>Nama Orang Tua</th>
									<th>No Telp </th>
									<th>Jenis Daycare</th>
									<th>Total Tagihan</th>
									<th>Status Pembayaran</th>
								</tr>
							</thead>
							<tbody>
                                <?php $n = 1; 
                                    foreach($data as $row){ ?>
                                        
                                <tr>
                                    <td><?php echo $n++?>.</td>
                                    <td><t class="badge badge-warning"><?php echo $row['kode_transaksi']?></t></td>
                                    <td><?php echo $row['nama_lengkap']?></td>
                                    <td><?php echo $row['umur']?></td>
                                    <td><?php echo $row['tempat_lahir']?></td>
                                    <td><?php echo $row['tanggal_lahir']?></td>
                                    <td><?php echo $row['nama_orang_tua']?></td>
                                    <td><?php echo $row['no_telp']?></td>
                                    <td><?php echo $row['jenis_daycare']?></td>
                                    <td><?php echo $row['total_tagihan']?></td>
                                    <td>
                                        <?php if($row['status'] == "Belum Bayar") { ?>
                                        <t class="badge badge-danger"><?php echo $row['status']; ?></t>
                                        <?php } else { ?>
                                        <t class="badge badge-success"><?php echo $row['status'];  ?></t>
                                        <?php } ?>
                                    </td>
                                </tr>      
                                    <?php } ?>                          
							</tbody>
						</table>
					</div>
						</div>
					</div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Kode Transaksi</h5>
                    <input type="text" class="form-control" id="kode_transaksi"placeholder="Kode Transaksi"><br>
                    <button id="btn_cari" type="button" class="btn btn-info">Cari</button>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

        <div class="card">
            <div class="card-body">

            <div class="col-md-12 text-center">
                <b>Kode Transaksi</b>
                <br>
                <b><h2 id="kode_transaksi_label"></h2></b>
                </div>

                <div class="row">
        <div class="col-md-12">

        <div class="card">
            <div class="card-body">

            <div class="row">
                <div class="col-md-12">
                <br>
                <table class="table">
                    <form action="<?=base_url();?>akademik/masterdata/daycare/store" method="POST">
                        <tr>
                        <th>Nama Lengkap</th>
                        <td id="">
                        <input disabled type="text" class="form-control" id="nama_lengkap" name="nama_lengkap">
                        </td>
                        
                        <th>Nama Orang Tua / Wali</th>
                        <td id="">
                        <input disabled type="text" class="form-control" id="nama_orang_tua" name="nama_orang_tua">
                        </td>
                        </tr>

                        <tr>
                        <th>Tempat Lahir</th>
                        <td id="">
                        <input disabled type="text" class="form-control" id="tempat_lahir" name="tempat_lahir">
                        </td>
                        
                        <th>Tanggal Lahir</th>
                        <td id="">
                        <input disabled type="text" class="form-control" name="tanggal_lahir" id="tanggal_lahir">
                        </td>
                        </tr>
                        
                        <tr>
                        <th>No Telp Orang Tua / Wali</th>
                        <td id="">
                        <input disabled type="number" class="form-control" name="no_telp" id="no_telp">
                        </td>
                        
                        <th>Jenis Daycare</th>
                        <td id="">
                        <input disabled type="text" class="form-control" id="jenis_daycare" name="jenis_daycare">
                        </td>
                        </tr>

                        <tr>
                        <th>Umur</th>
                        <td id="">
                        <input disabled type="number" class="form-control" name="umur" id="umur">
                        </td>
                        
                        <th>Total Tagihan</th>
                        <td id="">
                        <input disabled type="text" class="form-control" id="total_tagihan" name="total_tagihan">
                        <input disabled type="hidden" class="form-control" id="total_tagihan_input" name="total_tagihan_input">
                        </td>
                        </tr>

                    <!-- <div class="form-group"> -->

                </table>
                    </form>
                    
                </div>
            </div>
        </div>

    </div>
            <div class="form-group">
            

            <hr>
            <form action="<?= base_url();?>keuangan/pembayarandaycare/StorePembayaranDaycare" method="POST">
            <input type="hidden" id="id_daycare" name="id_daycare">
            <b><label for="lname">Jenis Pembayaran</label></b><br>
            <select required class="form-control" name="jenis_pembayaran" id="jenis_pembayaran">
                <option value="">--Pilih Jenis Pembayaran--</option>
                <option value="cash">Cash</option>
                <option value="transfer">Transfer</option>
            </select>
            </div>

            <div class="form-group" id="divRekening">
            <b><label for="lname">Rekening</label></b><br>
            <select  class="form-control" name="rekening" id="rekening">
                <option value="">--Pilih Rekening--</option>
            </select>
            </div>

            <div class="form-group" id="">
            <b><label for="lname">No Rekening</label></b><br>
            <input name="no_rek" type="number"  class="form-control" id="no_rek">
            </div>
            <center>
            
            <button type="submit" class="btn btn-primary">Bayar</button>
            </center>

            </form>



            </div>

            </div>

        </div>
        </div>

    </div>

<!-- end::main content -->

<script type="text/javascript" src="<?=base_url();?>assets/js/autonumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    $.get(baseUrl+"/akademik/masterdata/ajaxTahunAjaran", function(data, status){
        console.log(data)
        var element = JSON.parse(data)
        $('#tahun_ajaran').val(element.kode_tahun)
        $('#id_tahun_ajaran').val(element.id_tahun)
    });
    $(document).on('click', '#btn_cari', function() {
        let val = $('#kode_transaksi').val()
        const getUrl = window.location;
        const baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
            $.get(baseUrl+"/keuangan/pembayarandaycare/ajaxDaycare/"+val, function(data, status){
                let element = JSON.parse(data)
                console.log(element)
                //kode                
                $('#kode_transaksi_label').text(element.kode_transaksi)

                //anak
                $('#id_daycare').val(element.id)
                $('#nama_lengkap').val(element.nama_lengkap)
                $('#nama_orang_tua').val(element.nama_orang_tua)
                $('#tempat_lahir').val(element.tempat_lahir)
                $('#tanggal_lahir').val(element.tanggal_lahir)
                $('#no_telp').val(element.no_telp)
                $('#jenis_daycare').val(element.jenis_daycare)
                $('#umur').val(element.umur)
                $('#total_tagihan').val('Rp. '+element.total_tagihan)
                $('#total_tagihan_input').val(element.total_tagihan)
                $("#total_tagihan").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });

            })
    
    })

    $(document).on('change', '#jenis_pembayaran', function() {
    let val = $(this).val()
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    if (val == 'transfer')
    {
        $('#rekening').prop("disabled", false)
        $('#no_rek').prop("disabled", false)
        $.get(baseUrl+"/keuangan/rekening/ajaxRekening", function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            $('#rekening').empty()
            $('<option>').val("").text("--Pilih Rekening--").appendTo('#rekening');
            $.each(element, function(index, el){
                if (el.nama_bank == 'MANDIRI')
                {
                    $('<option>').val(el.kode_rek).text(el.nama_bank).appendTo('#rekening');
                }
            })
        })
    } else {
        $('#rekening').prop("disabled", true)
        $('#no_rek').prop("disabled", true)
        $('#rekening').empty()
        $('#no_rek').val("")

    }
});

$(document).on('change', '#rekening', function() {
    let val = $(this).val()
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        $.get(baseUrl+"/keuangan/rekening/ajaxNoRekening/"+val, function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            $('#no_rek').empty()
            $('#no_rek').val(element.no_rek)
        })
});

})

</script>

