<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Pembayaran Tagihan SPP Siswa</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Keuangan</a></li>
                    <li class="breadcrumb-item"><a href="#">Penerimaan Kas</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Pembayaran SPP Siswa</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Siswa</h5>
                    <button data-target="#modalBulan" class="btn btn-primary" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                        Pilih Bulan Ke
                    </button>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>NIS Siswa</th>
									<th>Nama Siswa</th>
									<th>Panggilan Siswa</th>
									<th>Tempat Lahir</th>
									<th>Tanggal Lahir</th>
									<th>Usia</th>
									<th>Jenis Kelamin</th>
									<th>Agama</th>
									<th>Jumlah Saudara</th>
									<th>Anak Ke-</th>
									<th>Alamat</th>
									<th>Status Siswa</th>
									<th>Status Bayar</th>
									<th>Nama Kelas</th>
									<th>Level Kelas</th>
									<th>Tahun Ajaran</th>
									<th class="text-center"><i class="fa fa-cog"></i></th>
								</tr>
							</thead>
                            <?php if (isset($data)) { ?>

							<tbody>
                                <?php $n = 1; 
                                    foreach($data as $row){ 
                                        if($row['nis'] != 0 && $row['status_siswa'] != "Undur Diri" && $row['status_bayar'] != "Belum Bayar"){?>
                                <tr>
                                    <td><?php echo $n++?>.</td>
                                    <td><?php echo $row['nis']?></td>
                                    <td><?php echo $row['nama_siswa']?></td>
                                    <td><?php echo $row['panggilan_siswa']?></td>
                                    <td><?php echo $row['tempat_lahir']?></td>
                                    <td><?php echo date("d-M-Y", strtotime($row['tanggal_lahir']))?></td>
                                    <?php 
                                    $lahir  = new DateTime($row['tanggal_lahir']);
                                    $today  = new DateTime();
                                    $umur   = $today->diff($lahir)->y;
                                    ?>
                                    <td><?php echo $umur?> Tahun</td>
                                    <td><?php echo $row['jenis_kelamin']?></td>
                                    <td><?php echo $row['agama']?></td>
                                    <td><?php echo $row['jumlah_saudara']?> Bersaudara</td>
                                    <td><?php echo $row['anak_ke']?></td>
                                    <td><?php echo $row['alamat']?></td>
                                    <td><?php echo $row['status_ortu']?></td>
                                    <td><?php if($row['status_bayar_spp'] == "Belum Dibayar"){?>
                                        <t class="badge badge-danger"><?php echo $row['status_bayar_spp'] ?></t>
                                    <?php }else{ ?>
                                        <t class="badge badge-success"><?php echo $row['status_bayar_spp']  ?></t>
                                    <?php } ?>
                                    </td>
                                    <td><?php echo $row['nama_kelas']?></td>
                                    <td><?php echo $row['level_kelas']?></td>
                                    <td><?php echo date("d-M-Y", strtotime($row['created_at']))?></td>
                                </tr>    
                                    <?php }?>                  
                                <?php } ?>
							</tbody>
                                <?php } ?>
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">NIS</h5>
                    <input type="text" class="form-control" id="nis"placeholder="NIS"><br>
                    <button id="btn_cari" type="button" class="btn btn-info">Cari</button>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

        <div class="card">
            <div class="card-body">

            <div class="col-md-12 text-center">
                <b>NIS</b>
                <br>
                <b><h2 id="o_nis"></h2></b>
                </div>

            <div class="row">
                <div class="col-md-12">
                <h6><b>Data Siswa</b></h6>
                <br>
                <table class="table">
                    <tr>
                    <th>Nama Lengkap</th>
                    <td id="o_nama_lengkap"></td>
                    
                    <th>Kelas</th>
                    <td id="o_kelas"></td>
                    </tr>

                    <tr>
                    <th>Kode Tahun Ajaran</th>
                    <td id="o_tahun_ajaran"></td>
                    <th>SPP Bulan Ke</th>
                    <td id="o_bulan_ke"></td>
                    </tr>

                    <tr>
                    <th>Biaya SPP</th>
                    <td id="o_biaya_spp"></td>
                    </tr>

                    <!-- <div class="form-group"> -->

                </table>
                    
                </div>
            </div>
            <div class="form-group">
            

            <hr>
            <form action="<?= base_url();?>keuangan/pembayaranspp/add" method="POST">
            <b><label for="lname">Jenis Pembayaran</label></b><br>
            <select required class="form-control" name="jenis_pembayaran" id="jenis_pembayaran">
                <option value="">--Pilih Jenis Pembayaran--</option>
                <option value="cash">Cash</option>
                <option value="transfer">Transfer</option>
            </select>
            </div>

            <div class="form-group" id="divRekening">
            <b><label for="lname">Rekening</label></b><br>
            <select  class="form-control" name="rekening" id="rekening">
                <option value="">--Pilih Rekening--</option>
            </select>
            </div>

            <div class="form-group" id="">
            <b><label for="lname">No Rekening</label></b><br>
            <input name="no_rek" type="number"  class="form-control" id="no_rek">
            </div>
            <input type="hidden" id="id_spp"  name="id_spp">
            <center>
            
            <button type="submit" class="btn btn-primary">Bayar</button>
            </center>

            </form>



            </div>

            </div>

        </div>
        </div>

    </div>

<div class="modal" id="modalBulan" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Pilih Bulan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?= base_url();?>keuangan/pembayaranspp" method="POST">
            <div class="form-group">
            <b><label for="lname">Tahun Ajaran</label></b><br>
            <input readonly type="text" id="tahun_ajaran" name="tahun_ajaran" class="form-control">
            <input readonly type="hidden" id="id_tahun_ajaran" name="id_tahun_ajaran" class="form-control">
            </div>

            <div class="form-group">
            <b><label for="lname">Bulan Ke</label></b><br>
            <select name="bulan_ke" id="" class="form-control">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
            </select>
            </div>        

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">OK</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
      </form>

    </div>
  </div>
</div>

    
</main>

<div class="modal fade" id="modalBayar" tabindex="-1" role="dialog" aria-lableledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Bayar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="form">
                                    <form action="<?= base_url();?>/akademik/transaksi/siswacalon/bayar" method="POST">
                                        <input type="hidden" id="id_siswa" name="id_siswa">

                                        <div class="form-group">
                                        <b><label for="name">Biaya Pendaftaran</label></b><br>
                                        Rp. <label id="biaya_pendaftaran"></label>
                                        <input type="hidden" readonly name="biaya_pendaftaran" id="biaya_pendaftaran_input"></input>
                                        </div>

                                        <div class="form-group">
                                        <b><label for="lname">Biaya Seragam</label>&nbsp<label id="bayarJK"></label></b><br>
                                        Rp. <label id="biaya_seragam"></label>
                                        <input id="biaya_seragam_input" type="hidden" readonly name="biaya_seragam"></input>
                                        </div>
                                        
                                        <div class="form-group">
                                        <b><label for="name">Biaya DPP (Pembangunan)</label></b><br>
                                        Rp. <label id="biaya_dpp"></label>
                                        <!-- <input type="hidden" readonly name="biaya_dpp" id="biaya_dpp_input"></input>&nbsp &nbsp<input name="cb_dpp" value="true" id="cbDpp"type="checkbox">Cicil -->
                                        <input type="hidden" readonly name="biaya_dpp" id="biaya_dpp_input"></input>
                                        <input placeholder="Nominal cicilan awal" type="number"class="form-control" name="biaya_dpp_cicil" id="biaya_dpp_cicil">
                                        </div>
                                        
                                        <div class="form-group">
                                        <b><label for="name">Biaya DSK (Semester)</label></b><br>
                                        Rp. <label id="biaya_dsk"></label>
                                        <!-- <input type="hidden" readonly name="biaya_dsk" id="biaya_dsk_input"></input>&nbsp &nbsp<input value="true" id="cbDsk"type="checkbox">Cicil -->
                                        <input type="hidden" readonly name="biaya_dsk" id="biaya_dsk_input"></input>
                                        <input placeholder="Nominal cicilan awal" type="number" class="form-control" name="biaya_dsk_cicil" id="biaya_dsk_cicil">
                                        </div>
                                        
                                        <div class="form-group">
                                        <b><label for="lname">Tahun Ajaran</label></b><br>
                                        <input readonly type="text" id="tahun_ajaran" name="tahun_ajaran" class="form-control">
                                        <input readonly type="hidden" id="id_tahun_ajaran" name="id_tahun_ajaran" class="form-control">
                                        </div>

                                        <div class="form-group">
                                        <b><label for="lname">Jenis Pembayaran</label></b><br>
                                        <select required class="form-control" name="jenis_pembayaran" id="jenis_pembayaran">
                                            <option value="">--Pilih Jenis Pembayaran--</option>
                                            <option value="cash">Cash</option>
                                            <option value="transfer">Transfer</option>
                                        </select>
                                        </div>

                                        <div class="form-group" id="divRekening">
                                        <b><label for="lname">Rekening</label></b><br>
                                        <select disabled class="form-control" name="rekening" id="rekening">
                                            <option value="">--Pilih Rekening--</option>
                                        </select>
                                        </div>

                                        <div class="form-group" id="">
                                        <b><label for="lname">No Rekening</label></b><br>
                                        <input name="no_rek" type="number" readonly class="form-control" id="no_rek">
                                        </div>
                                        
                                        <hr>

                                        <div class="form-group" id="">
                                        <b><h4 for="lname" style="color:red">Total Tagihan:  Rp. <label id="total_tagihan"></label></h4></b><br>
                                        <input id="total_tagihan_input" type="hidden" readonly name="total_tagihan"></input>

                                        <b><h4 for="lname" style="color:blue">Total Pembayaran:  Rp. <label id="total_pembayaran"></label></h4></b><br>
                                        <input id="total_pembayaran_input" type="hidden" readonly name="total_pembayaran"></input>
                                        </div>
                                        
                                        <hr>
                                        <div class="form-group" id="">
                                        <b><h4 for="lname" style="color:green">Sisa Tagihan:  Rp. <label id="sisa_tagihan"></label></h4></b><br>
                                        <input id="sisa_tagihan_input" type="hidden" readonly name="sisa_tagihan"></input>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">OK</button>
      </div>
      </form>

    </div>
  </div>
</div>
<!-- end::main content -->

<script type="text/javascript" src="<?=base_url();?>assets/js/autonumeric/autoNumeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    $.get(baseUrl+"/akademik/masterdata/ajaxTahunAjaran", function(data, status){
        console.log(data)
        var element = JSON.parse(data)
        $('#tahun_ajaran').val(element.kode_tahun)
        $('#id_tahun_ajaran').val(element.id_tahun)
    });
    $(document).on('click', '#btn_cari', function() {
        let val = $('#nis').val()
        const getUrl = window.location;
        const baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
            $.get(baseUrl+"/keuangan/pembayaranspp/ajaxTagihanSpp/"+val, function(data, status){
                let element = JSON.parse(data)
                console.log(element.siswa)

                //kode                
                $('#o_nis').text(element.siswa.nis)

                //siswa
                $('#o_nama_lengkap').text(element.siswa.nama_siswa)
                $('#o_kelas').text(element.siswa.kode_kelas)
                $('#o_tahun_ajaran').text(element.siswa.kode_tahun)
                $('#o_bulan_ke').text(element.tagihan.bulan_ke)
                $('#o_biaya_spp').text(element.tagihan.biaya_spp)
                $('#id_spp').val(element.tagihan.id)
                $("#o_biaya_spp").autoNumeric('init', {
                    aSep: '.', 
                    aDec: ',',
                    aForm: true,
                    vMax: '999999999',
                    vMin: '-999999999'
                });

            })
    
    })

    $(document).on('change', '#jenis_pembayaran', function() {
    let val = $(this).val()
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
    if (val == 'transfer')
    {
        $('#rekening').prop("disabled", false)
        $('#no_rek').prop("disabled", false)
        $.get(baseUrl+"/keuangan/rekening/ajaxRekening", function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            $('#rekening').empty()
            $('<option>').val("").text("--Pilih Rekening--").appendTo('#rekening');
            $.each(element, function(index, el){
                if (el.nama_bank == 'MANDIRI')
                {
                    $('<option>').val(el.kode_rek).text(el.nama_bank).appendTo('#rekening');
                }
            })
        })
    } else {
        $('#rekening').prop("disabled", true)
        $('#no_rek').prop("disabled", true)
        $('#rekening').empty()
        $('#no_rek').val("")

    }
});

$(document).on('change', '#rekening', function() {
    let val = $(this).val()
    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        $.get(baseUrl+"/keuangan/rekening/ajaxNoRekening/"+val, function(data, status){
            console.log(data)
            let element = JSON.parse(data)
            $('#no_rek').empty()
            $('#no_rek').val(element.no_rek)
        })
});

})

</script>

