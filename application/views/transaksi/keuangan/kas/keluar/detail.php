
<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">KONFIRMASI KAS</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo site_url('keuangan/kas_keluar') ?>">Kas Keluar</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Kas Keluar</h5>
                </div>

                <div class="card-body">
                    
                   	<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Detail Kas Keluar</h4>
								</div>
								<div class="card-body">

									<div class="row">
										<div class="col-md-12">
											<?php echo $this->session->flashdata('alert_message') ?>
										</div>
									</div>

									<a href="<?php echo site_url('keuangan/kas_keluar') ?>" class="btn btn-outline-primary"><i class="fa fa-chevron-left"></i> &nbsp;KEMBALI</a>
              						<br><br>

									
              						<div class="row">
						                <div class="col-md-8">
						                  <div class="card">
						                    <div class="card-header"><b>DETAIL</b></div>
						                    <div class="card-body">

						                    	 <table class="table">
								                    <tr>
								                      <th style="width: 20%; background-color: #eee">KODE</th>
								                      <td><?php echo $transaksi['kode_transaksi'] ?></td>

								                      <th style="width: 30%; background-color: #eee">TOTAL PEMBELIAN</th>
								                      <td><?php echo format_rp($transaksi['total_transaksi']) ?></td>
								                    </tr>

								                    <tr>
								                      <th style="width: 20%; background-color: #eee">TIPE</th>
								                      <td><?php echo $transaksi['pembayaran'] ?></td>

								                      <th style="width: 20%; background-color: #eee">TOTAL BAYAR</th>
								                      <td><?php echo format_rp($transaksi['total_bayar']) ?></td>
								                    </tr>

								                    <tr>
								                      <th style="width: 20%; background-color: #eee">STATUS</th>
								                      <td><?php if($transaksi['status'] == 'Lunas'){
								                          echo "<span class='badge badge-success'>Lunas</span>";
								                        }else{
								                          echo "<span class='badge badge-danger'>Belum Lunas</span>";
								                        } ?></td>
								                      <th style="width: 20%; background-color: #eee">SISA BAYAR</th>
								                      <td><?php echo format_rp($transaksi['sisa_bayar']) ?></td>
								                    </tr>

								                    <tr>
                                                      <th style="width: 20%; background-color: #eee">JENIS</th>
                                                      <td><?php echo $transaksi['tipe'] ?></td>

								                      <th style="width: 20%; background-color: #eee">KETERANGAN</th>
								                      <td><?php echo $transaksi['keterangan'] ?></td>
								                    </tr>
								                  </table>

						                    </div>
						                  </div>

						                </div>


<div class="col-md-4">
	<div class="card">
        <div class="card-header"> &nbsp;<b>PERSETUJUAN</b>
        </div>

        <div class="card-body text-center">
    <?php if($transaksi['level'] == '2'){ 
    		if($transaksi['tipe'] == 'perbaikan'){
   	?>
   				<a onclick="return confirm('Apakah anda yakin ?')" class="btn btn-success btn-sm" href="<?= site_url('set_approval_to_vendor/'.$transaksi['id']) ?>"><i class="fa fa-check"></i> &nbsp;TERIMA</a>
	        	&emsp;
	        	<a class="btn btn-danger btn-sm" href="javascript:void(0)" data-toggle="modal" data-target="#modalDeny"><i class="fa fa-ban"></i> &nbsp;TOLAK</a>
   	<?php
    		}else{
    ?>
    			<a data-toggle="modal" data-target="#modalApprove" class="btn btn-success btn-sm" href="javascript:void(0)"><i class="fa fa-check"></i> &nbsp;TERIMA</a>
	        	&emsp;
	        
          <!--<a class="btn btn-danger btn-sm" href="javascript:void(0)" data-toggle="modal" data-target="#modalDeny"><i class="fa fa-ban"></i> &nbsp;TOLAK</a>-->
    <?php
    		}
    ?>
        	
        </div>
    <?php }else{

    		if($transaksi['level'] == '3'){ 
    			if($transaksi['tipe'] == 'perbaikan'){ 
    				if($transaksi['level_vendor'] == '1'){
    					echo "<span class='badge badge-primary'>Menunggu Konfirmasi Harga Vendor</span>";

    				}else if($transaksi['level_vendor'] == '2'){
    ?>
    					<a data-toggle="modal" data-target="#modalApprove" class="btn btn-success btn-sm" href="javascript:void(0)"><i class="fa fa-check"></i> &nbsp;TERIMA</a>
			        	&emsp;
			        	<a class="btn btn-danger btn-sm" href="javascript:void(0)" data-toggle="modal" data-target="#modalDeny"><i class="fa fa-ban"></i> &nbsp;TOLAK</a>

	<?php 			}else if($transaksi['level_vendor'] == '3'){
						echo "<span class='badge badge-primary'>Menunggu penyelesaian ".$transaksi['tipe']."</span>";
    				
    				}else if($transaksi['level_vendor'] == '4'){
    					echo "<span class='badge badge-success'>Selesai</span>";
    				}else if($transaksi['level_vendor'] == '0'){
    					echo "<span class='badge badge-danger'>Ditolak ( konfirmasi harga )</span><br>";
    					echo $transaksi['keterangan_deny'];
    				}
    ?>

    <?php 		}else{ ?>
    				<span class="badge badge-success"><i class="fa fa-check-circle"></i> Diterima</span>
	    			<hr>
	    			<p class="text-muted">Keterangan</p>
	    			<?php echo $transaksi['keterangan_acc'] ?>

            <hr>

            <center>
              <button id="btnBukti" class="btn btn-primary btn-sm" data-photo="<?= base_url('assets/bukti_terima/'.$transaksi['bukti_terima']) ?>" data-toggle="modal" data-target="#modalBukti">Lihat Bukti Terima</button>
            </center>

   	<?php 		}
    ?>

    <?php   }else{ ?>

    			<span class="badge badge-danger"><i class="fa fa-ban"></i> Ditolak</span>
    			<hr>
    			<p class="text-muted">Keterangan</p>
    			<?php echo $transaksi['keterangan_deny'] ?>

    <?php   } 

		}
	?>

    </div>
</div>

						              </div>

								</div>

								<div class="row">
					            	<div class="col-md-12">

					                    <div class="card">
					                      <div class="card-header"><b>DAFTAR BARANG</b></div>
					                      <div class="card-body">
					                      	<div class="table-responsive">
<table class="table">
    <thead class="bg-primary">
      <tr>
        <th>PRODUK</th>
        <?php if($transaksi['tipe'] == 'perbaikan'){
        	$colspan = '4';
         ?>
         		<th>KETERANGAN ASET</th>
         		<th>KETERANGAN VENDOR</th>

        <?php }else{ $colspan = '3'; ?>
        		<th class="text-center">HARGA SATUAN</th>
    	<?php } ?>
        
        <th class="text-center" style="width: 10%">JUMLAH</th>
        <th class="text-center" style="width: 25%">SUBTOTAL</th>
      </tr>
    </thead>

    <tbody id="tableItem">
      <?php $n = $grandTotal = 0; 
            foreach ($item as $row) { $n++; $grandTotal += $row['subtotal']?>

              <tr>
                <td><?php echo $row['nama_komponen'] ?><br><br>
                <?php if($transaksi['tipe'] == 'perbaikan'){ ?>
                		<a class="img-preview" href="javascript:void(0)" data-img="<?= $row['gambar'] ?>"><i class="fa fa-photo"></i> Preview</a>
		        <?php } ?>
                	
                </td>

                <?php if($transaksi['tipe'] == 'perbaikan'){ ?>
                		<td><?php echo $row['keterangan'] ?></td>
                		<td><?php echo $row['keterangan_vendor'] ?></td>
		        <?php }else{ ?>
		        		<td class="text-right"><?php echo format_rp($row['harga']) ?></td>
		    	<?php } ?>

                <td class="text-center"><?php echo $row['jumlah'] ?></td>
                <td class="text-right"><?php echo format_rp($row['subtotal']) ?></td>
              </tr>

      <?php } ?>

    </tbody>

      <tr>
        <td colspan="<?= $colspan ?>">
        	<input type="hidden" id="inputGrandTotal" name="grandTotal" value="<?php echo $grandTotal ?>">
        	<h4><b>TOTAL</b></h4></td>
        <td class="text-right"><h4><b id="grandTotal">
        	<?php if($grandTotal == 0){
          		echo "<span class='badge badge-danger'>Menunggu Konfirmasi Vendor</span>";
          	}else{
          		echo format_rp($grandTotal);
          	} ?>
        	</b></h4>
        </td>
      </tr>

</table>
						                    </div>
					                      </div>
					                    </div>

					                </div>
						        </div>


								<div class="row">

					                <div class="col-md-12">
					                  <div class="card">
					                    <div class="card-header"><b>DAFTAR PEMBAYARAN</b></div>

					                    <div class="card-body">
					                    	<div class="table-responsive">
						                      <table class="table">
						                        <thead class="bg-primary">
						                          <tr>
						                            <th style="width:5%">NO</th>
						                            <th>WAKTU</th>
						                            <th>JUMLAH BAYAR</th>
						                          </tr>
						                        </thead>

						                        <tbody id="tableItem">
						                          <?php $n = $grandTotal = 0; 
						                                foreach ($pembayaran as $row) { $n++; $grandTotal += $row['jumlah_bayar']?>

						                                  <tr>
						                                    <td><?php echo $n ?></td>
						                                    <td><?php echo $row['tanggal_bayar'] ?></td>
						                                    <td class="text-right"><?php echo format_rp($row['jumlah_bayar']) ?></td>
						                                  </tr>

						                          <?php } ?>

						                        </tbody>

						                          <tr>
						                            <td><input type="hidden" id="inputGrandTotal" name="grandTotal" value="<?php echo $grandTotal ?>"></td>
						                            <td><h4><b>TOTAL</b></h4></td>
						                            <td class="text-right"><h4><b id="grandTotal"><?php echo format_rp($grandTotal) ?></b></h4></td>
						                          </tr>

						                      </table>
						                    </div>
					                    </div>
					                  </div>

					                </div>

							</div>
						</div>


					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->

<form action="<?php echo site_url('insert_approve_keuangan/approve/'.$transaksi['id']) ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalApprove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           	<div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> PERSETUJUAN</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

             <div class="modal-body">
                <div class="form-group">
                   <label>Kode Transaksi</label>
                   <input type="text" name="kode_transaksi" id="kode" class="form-control" placeholder="Kode Jabatan" value="<?php echo $transaksi['kode_transaksi'];?>" readonly="" autocomplete="off" required />
                </div>

                <?php if($transaksi['tipe'] == 'perbaikan'){ ?>

                	<div class="form-group">
	                   <label>Pembayaran</label><br>
	                   <select id="pembayaran" required="" class="form-control" name="pembayaran">
	                   	<option value="">Pilih</option>
	                   	<option value="Tunai">Tunai</option>
	                   	<option value="Kredit">Kredit</option>
	                   </select>
	                </div>

	                <div class="form-group">
	                   <label>Nominal</label><br>
	                   <input type="text" id="nominal" class="form-control rupiah" required="" autocomplete="off" readonly="readonly" value="Harap Pilih Pembayaran !" name="total_bayar" placeholder="Rp 0">
	                </div>

                <?php }else{ ?>
                		<div class="form-group">
		                   <label>Nominal</label><br>
		                   <h4>
		                   	<?php if($transaksi['tipe'] != 'pinjaman'){ echo format_rp($transaksi['total_bayar']); }else{  echo format_rp($transaksi['total_transaksi']); } ?>
		                   </h4>
		                </div>
                <?php } ?>
                

                <div class="form-group">
                	<label>Pencairan</label><br>
                	<div class="selectgroup selectgroup-secondary selectgroup-pills">
						<label class="selectgroup-item">
							<input type="radio" name="metode" value="Cash" class="selectgroup-input m" checked="">
							<span class="selectgroup-button selectgroup-button-icon"><i class="fas fa-money-bill-wave"></i> Cash</span>
						</label>

						<?php if($transaksi['tipe'] != 'setoran_bank'){ ?>

								<label class="selectgroup-item">
									<input type="radio" name="metode" value="Transfer" class="selectgroup-input m">
									<span class="selectgroup-button selectgroup-button-icon"> Transfer</span>
								</label>

						<?php } ?>

					</div>
                </div>

                <div class="form-group" id="rekBody">
                   <label>Rekening</label>
                   <select class="form-control" name="rek_id" id="rek">
                   	<option value="">Pilih</option>
                   	<?php foreach ($rek as $row) { ?>
                   		<option data-price="<?= $row['saldo'] ?>" value="<?= $row['id'] ?>"><?= $row['nama_bank']." - ".$row['no_rek']." a/n ".$row['atas_nama'] ?></option>
                   	<?php } ?>
                   </select>

                   <span id="ket_rekening"></span>
                </div>

                <div class="form-group">
                  <label>Bukti Terima</label> &emsp;
                  <input type="file" name="file" required="" class="btn btn-light">
                </div>
             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button id="btnSubmit" type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>

   <form action="<?php echo site_url('insert_approve_keuangan/deny/'.$transaksi['id']) ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalDeny" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           	<div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> PENOLAKAN</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

             <div class="modal-body">

                 <div class="form-group">
                   <label>Keterangan Penolakan</label>
                   <textarea class="form-control" name="keterangan"></textarea>
                </div>


             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>


   <script type="text/javascript">
   	$('#rekBody').hide();

   	$(document).on('change', '.m', function(){
   		var val = $(this).val();

   		if(val == 'Transfer'){
   			$('#rekBody').show();
   		}else{
   			$('#rekBody').hide();
   		}
   	})
   </script>

<div class="modal fade" id="modalPreview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
       <div class="modal-content">
         <div class="modal-header bg-primary">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Preview Foto</h4>
         </div>

         <input type="hidden" name="penjualan_detail_id" id="e_id">

         <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <center>
                  <img src="" id="place-img" class="img-fluid">
                </center>
              </div>
            </div>
         </div>

         <div class="modal-footer">
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
         </div>

       </div>
    </div>
 </div>


<div class="modal fade" id="modalBukti" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-image"></i> BUKTI TERIMA</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

             <div class="modal-body">

                 <div class="form-group">
                  <center>
                   <img src="" id="img_terima" class="img-fluid">
                  </center>
                </div>


             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
             </div>

           </div>
        </div>
     </div>

 <script type="text/javascript">
 	$(document).on('click','.img-preview', function(){
      var source = '<?= base_url('assets/aset/perbaikan') ?>'+'/'+$(this).attr('data-img');

      $('#place-img').attr('src', source);
      $('#modalPreview').modal('show');
    });

    $(document).on('keyup', '#nominal', function(){
    	var price = format_angka($(this).val());

    	if(price >= <?= $transaksi['total_transaksi'] ?>){
    		alert('Pembayaran Kredit harus dibawah nilai transaksi ('+ format_rp(<?= $transaksi['total_transaksi'] ?>) +')');
    		$('#nominal').val('');
    	}

    	if($('#rek').val() != ''){
    		var sisa_saldo = parseInt($('#rek option:selected').attr('data-price'));
    		var txt = '<br><b>Sisa Saldo </b> : '+ format_rp(sisa_saldo);
    
    		if(sisa_saldo < price){
    			txt += '<br><span class="text-danger"><i class="fa fa-ban"></i> Saldo Tidak Mencukupi</span>';
    		}
    		$('#ket_rekening').html(txt);
    	}
    });

    $(document).on('change', '#pembayaran', function(){
    	if($(this).val() == 'Tunai'){
    		$('#nominal').val(format_rp(<?= $transaksi['total_transaksi'] ?>)).attr('readonly','readonly');

    		if($('#rek').val() != ''){
    			var sisa_saldo = $('#rek option:selected').attr('data-price');
	    		var txt = '<br><b>Sisa Saldo </b> : '+ format_rp(sisa_saldo);

	    		if(sisa_saldo < format_angka($('#nominal').val())){
	    			txt += '<br><span class="text-danger"><i class="fa fa-ban"></i> Saldo Tidak Mencukupi</span>';
	    		}

	    		$('#ket_rekening').html(txt);
    		}

    	}else if($(this).val() == 'Kredit'){
    		$('#nominal').val('').removeAttr('readonly','readonly');
    		
    		var sisa_saldo = $('#rek option:selected').attr('data-price');
	    	var txt = '<br><b>Sisa Saldo </b> : '+ format_rp(sisa_saldo);
	    	$('#ket_rekening').html(txt);

    	}else{
    		$('#nominal').val('Harap Pilih Pembayaran').attr('readonly', 'readonly');
    		if($('#rek').val() != ''){
    			var sisa_saldo = $('#rek option:selected').attr('data-price');
	    		var txt = '<br><b>Sisa Saldo </b> : '+ format_rp(sisa_saldo);
	    		$('#ket_rekening').html(txt);
    		}
    	}
    })  

    $(document).on('change', '#rek', function(){
    	if($(this).val() != ''){
    		var sisa_saldo = $('#rek option:selected').attr('data-price');
    		var txt = '<br><b>Sisa Saldo </b> : '+ format_rp(sisa_saldo);
        var s = false;

        if('<?php $transaksi['tipe'] ?>' == 'perbaikan'){
          if(sisa_saldo < format_angka($('#nominal').val())){
            s = true;
          }

        }else{
          if(sisa_saldo < <?= $transaksi['total_bayar'] ?>){
            s = true;
          }
        }

    		if(s){
    			txt += '<br><span class="text-danger"><i class="fa fa-ban"></i> Saldo Tidak Mencukupi</span>';
    		}

    		$('#ket_rekening').html(txt);

    	}else{
    		$('#ket_rekening').text('');
    	}
    	
    });

    $(document).on('click', '#btnBukti', function(){
      $('#img_terima').attr('src', $(this).attr('data-photo'));
    })          
 </script>