<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">KONFIRMASI KAS</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Kas Keluar</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Kas Keluar</h5>
                </div>

                <div class="card-body">
                    
                   	<div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>Kode</th>
		                            <th>Tanggal Transaksi</th>
		                            <th>Total</th>
		                            <th class="text-center">Pembayaran</th>
		                            <th class="text-center">Status</th>
									<th class="text-center"><i class="fa fa-cog"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php $n = 0;
		                          foreach ($list as $row) { $n++; ?>

		                            <tr>
		                              <td><?php echo $n ?></td>
		                              <td><?php echo $row['kode_transaksi'] ?></td>
                                      <td><?php echo $row['tanggal_transaksi'] ?></td>
                                      <td><?php echo format_rp($row['total_transaksi']) ?></td>
                                      <td class="text-center">
                                      	<?php echo $row['pembayaran'] ?>
                                      </td>

                                      <td class="text-center">

                                        <?php
                                            if($row['tipe'] == 'perbaikan'){
                                                if($row['level'] == '3'){
                                                    if($row['level_vendor'] == '1'){
                                                        echo "<span class='badge badge-primary'>Menunggu konfirmasi vendor</span>";
                                                    
                                                    }else if($row['level_vendor'] == '2'){
                                                        echo "<span class='badge badge-warning'>Menunggu konfimasi harga</span>";
                                                    }else if($row['level_vendor'] == '3'){
                                                        echo "<span class='badge badge-info'>Menunggu penyelesaian ".$row['tipe']."</span>";
                                                    }else if($row['level_vendor'] == '4'){
                                                        echo "<span class='badge badge-success'><i class='fa fa-check'></i> Selesai</span>";
                                                    }
                                                }else{
                                                    echo "<span class='badge badge-primary'>Menunggu Acc Bag. Keuangan</span>";
                                                }
                                                
                                            }else{
                                                echo show_level($row['level']);
                                            }
                                         ?></td>

		                              <td class="text-center">
		                                  
		                                  <a href="<?php echo site_url('keuangan/kas_keluar/detail/'.$row['id']) ?>" data-toggle="tooltip" title="Lihat Detail" class="btn btn-primary btn-sm">
		                                    <i class="fa fa-search"></i>
		                                  </a>
		                              </td>
		                            </tr>

		                    <?php } ?>
							</tbody>
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->
