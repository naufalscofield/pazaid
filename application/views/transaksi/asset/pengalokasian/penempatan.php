<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">Aset</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Penempatan</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Penempatan Aset</h5>
                </div>

                <div class="card-body">

                	<div class="row">
                		<div class="col-md-12">
                			<?php echo $this->session->flashdata('alert_message') ?>
                		</div>
                	</div>
                    
                   	<form method="GET">
										<div class="row">
											<div class="col-md-4">
												<label>Aktiva</label>
												<select required="" class="form-control" name="aset_id">
													<option value="">Pilih</option>
													<?php foreach ($aset as $row) { ?>
															<option <?php if($row['id_aset'] == $this->input->get('aset_id')){ echo "selected='selected'"; } ?> value="<?php echo $row['id_aset'] ?>"><?php echo $row['kode_aset']." / ".$row['nama_aset'] ?></option>
													<?php } ?>
												</select>
											</div>

											<div class="col-md-1">
												<br>
												<button style="margin-top: 8px" class="btn btn-primary"><i class="fa fa-search"></i></button>
											</div>

										</div>
									</form>

									<?php if($this->input->get('aset_id')){ ?>

									<br>

									<form method="POST" action="<?php echo site_url('insert_asset_location/'.$this->input->get('aset_id')) ?>">

										<div class="table-responsive">
											<table class="display table table-hover datatables" >
												<thead class="bg-primary">
													<tr>
														<th style="width: 5%">No</th>
														<th>Aktiva</th>
														<th class="text-center" style="width: 5%">Pilih</th>
													</tr>
												</thead>
												<tbody>
													<?php 

													  $n = 0;
							                          foreach ($list as $row) { $n++; ?>

							                          	<tr>
							                          		<td><?php echo $n ?></td>
							                          		<td><?php echo $row['kode_detail_aset']." / ".$row['nama_aset'] ?></td>
							                          		<td class="text-center">
							                          			<div class="custom-control custom-checkbox">
																	<input name="detail_aset[]" type="checkbox" class="custom-control-input" value="<?php echo $row['aset_detail_id'] ?>" id="customCheck<?php echo $n ?>">
																	<label class="custom-control-label" for="customCheck<?php echo $n ?>"></label>
																</div>
							                          		</td>
							                          	</tr>

							                    	<?php } ?>
												</tbody>
											</table>
										</div>

										<br>

										<div class="row">
							                <div class="col-md-12">
							                	<div class="form-group">
								                   <label>Lokasi</label>
								                   <select class="form-control" required="" name="ruangan_id">
								                   		<option value="">Pilih</option>
								                   		<?php foreach ($ruangan as $row){ ?>
								                   					<option value="<?php echo $row['id'] ?>"><?php echo $row['kode_ruangan']." / ".$row['nama_ruangan'] ?></option>
								                   		<?php } ?>
								                   </select>
								                </div>
							                </div>

							                <div class="col-md-12">
							                	<button class="btn btn-success shadow mt-3"><i class="fa fa-plus"></i> <i class="fas fa-truck-loading"></i> Simpan</button>
							                </div>
										</div>
									</form>

									<br>

									<?php } ?>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->

