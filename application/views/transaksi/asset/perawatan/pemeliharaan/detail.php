<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">ASET</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                	<li class="breadcrumb-item"><a href="#">Pemeliharaan</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Pemeliharaan</h5>
                </div>

                <div class="card-body">
                    
                   	<div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<a href="<?php echo site_url('aset/transaksi/pemeliharaan') ?>" class="btn btn-outline-primary btn-flat"><i class="fa fa-chevron-left"></i> &nbsp;KEMBALI</a>
						<br><br>

					
						<div class="row">
		                <div class="col-md-12">
		                  <div class="card">
		                    <div class="card-header"><b>DETAIL</b></div>

		                    <div class="card-body">
		                    	<table class="table">
				                    <tr>
				                      <th style="width: 20%; background-color: #eee">KODE</th>
				                      <td><?php echo $transaksi['kode_transaksi'] ?></td>
				                      <th style="width: 25%; background-color: #eee">TOTAL PEMELIHARAAN</th>
				                      <td><?php echo format_rp($transaksi['total_transaksi']) ?></td>
				                    </tr>

				                    <tr>
				                      <th style="width: 20%; background-color: #eee">TIPE</th>
				                      <td><?php echo $transaksi['pembayaran'] ?></td>
				                      <th style="width: 20%; background-color: #eee">TOTAL BAYAR</th>
				                      <td><?php echo format_rp($transaksi['total_bayar']) ?></td>
				                    </tr>

				                    <tr>
				                      <th style="width: 20%; background-color: #eee">STATUS</th>
				                      <td><?php if($transaksi['status'] == 'Lunas'){
				                          echo "<span class='badge badge-success'>Lunas</span>";
				                        }else{
				                          echo "<span class='badge badge-danger'>Belum Lunas</span>";
				                        } ?></td>
				                      <th style="width: 20%; background-color: #eee">SISA BAYAR</th>
				                      <td><?php echo format_rp($transaksi['sisa_bayar']) ?></td>
				                    </tr>

				                    <tr>
				                      <th style="width: 20%; background-color: #eee">VENDOR</th>
				                      <td><?php echo $transaksi['kode_vendor']." / ".$transaksi['nama_vendor'] ?></td>
				                      <th style="width: 20%; background-color: #eee">KETERANGAN</th>
				                      <td><?php echo $transaksi['keterangan'] ?></td>
				                    </tr>
				                  </table>
		                    </div>
		                  </div>

		                </div>

		                <div class="col-md-12">
		                    <div class="card">
		                      <div class="card-header"><b>DAFTAR BARANG</b></div>

		                      <div class="card-body">
		                      	<div class="table-responsive">
		                            <table class="table">
		                              <thead style="background-color:#eee">
		                                <tr>
		                                  <th style="width: 30%">ASET</th>
		                                  <th style="width: 40%">KETERANGAN</th>
		                                  <th style="width: 20%">HARGA</th>
		                                </tr>
		                              </thead>

		                              <tbody id="tableItem">
		                                <?php $n = $grandTotal = 0; 
		                                      foreach ($item as $row) { $n++; $grandTotal += $row['harga'] ?>

		                                        <tr>
		                                          <td><?php echo $row['nama_komponen'] ?></td>
		                                          <td><?php echo $row['keterangan'] ?></td>
		                                          <td class="text-right"><?php echo format_rp($row['harga']) ?></td>
		                                        </tr>

		                                <?php } ?>

		                              </tbody>

		                                <tr>
		                                  <td colspan="2"><input type="hidden" id="inputGrandTotal" name="grandTotal" value="<?php echo $grandTotal ?>"><h4><b>TOTAL</b></h4></td>
		                                  <td colspan="1" class="text-right"><h4><b id="grandTotal"><?php echo format_rp($grandTotal) ?></b></h4></td>
		                                </tr>

		                            </table>
		                          </div>
		                      </div>
		                    </div>

		                </div>
		              </div>

		              <div class="row">

		                <div class="col-md-12">
		                  <div class="card">
		                    <div class="card-header"><b>DAFTAR PEMBAYARAN</b></div>

		                    <div class="card-body">
		                    	<div class="table-responsive">
			                      <table class="table">
			                        <thead class="bg-primary">
			                          <tr>
			                            <th style="width:5%">NO</th>
			                            <th>WAKTU</th>
			                            <th>JUMLAH BAYAR</th>
			                          </tr>
			                        </thead>

			                        <tbody id="tableItem">
			                          <?php $n = $grandTotal = 0; 
			                                foreach ($pembayaran as $row) { $n++; $grandTotal += $row['jumlah_bayar']?>

			                                  <tr>
			                                    <td><?php echo $n ?></td>
			                                    <td><?php echo $row['tanggal_bayar'] ?></td>
			                                    <td class="text-right"><?php echo format_rp($row['jumlah_bayar']) ?></td>
			                                  </tr>

			                          <?php } ?>

			                        </tbody>

			                          <tr>
			                            <td><input type="hidden" id="inputGrandTotal" name="grandTotal" value="<?php echo $grandTotal ?>"></td>
			                            <td><h4><b>TOTAL</b></h4></td>
			                            <td class="text-right"><h4><b id="grandTotal"><?php echo format_rp($grandTotal) ?></b></h4></td>
			                          </tr>

			                      </table>
			                    </div>
		                    </div>
		                  </div>

		                </div>
		              </div>

                </div>
            </div>
        </div>
    </div>
    
</main>
