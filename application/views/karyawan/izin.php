<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">PENGGAJIAN</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Presensi</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Cuti</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Izin</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo $this->session->flashdata('alert_message') ?>
                        </div>
                    </div>

                    <button class="btn btn-primary" data-toggle="modal" data-target="#modalIzin"><i class="fa fa-plus"></i> Ajukan Perizinan</button>

									<br><br>

                    <div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
                                    <tr>
                                      <th style="width:5%">No</th>
                                      <th style="width:20%">Karyawan</th>
                                      <th style="width:10%">Tipe</th>
                                      <th class="text-center">Rentang Waktu</th>
                                      <th style="width:27%">Keterangan</th>
                                      <th style="width:15%" class="text-center"><i class="fa fa-cog"></i></th>
                                    </tr>
							</thead>
							<tbody>
                                <?php 
                                    $n = 0;
                                    foreach ($izin as $row) { $n++; 

                                      $date1 = new DateTime($row['tanggal_mulai']);
                                      $date2 = new DateTime($row['tanggal_selesai']);
                                      $interval = $date1->diff($date2);

                                ?>
                                      <tr>
                                        <td><?php echo $n; ?></td>
                                         <td><?php echo $row['kode_pegawai']." / ".$row['nama_pegawai'] ?></td>
                                        <td><?php echo $row['tipe'] ?></td>
                                        <td class='text-center'><?php echo $row['tanggal_mulai']." s/d ".$row['tanggal_selesai'] ?> <br> ( <?php echo $interval->days + 1 ?> Hari )</td>
                                        <td><?php echo $row['keterangan'] ?></td>
                                        <td class="text-center">

                                          <?php if($row['status'] == 'pending'){ ?>

		                                          <span class="badge badge-success bg-primary"><i class="fa fa-spinner fa-spin"></i> Pending</span>

                                          <?php }else if($row['status'] == 'approve'){ ?>

                                          			<span class="badge badge-success bg-success"><i class="fa fa-check"></i> Diterima</span>

                                          <?php }else{ ?>

                                          			<span class="badge badge-danger bg-danger"><i class="fa fa-ban"></i> Ditolak</span>

                                          <?php } ?>

                                          
                                        </td>
                                      </tr>
                                <?php } ?>
                              </tbody>
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>

<form action="<?php echo site_url('insert_izin') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalIzin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Perizinan</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
             </div>

             <div class="modal-body">
                
                <div class="form-group row">
				    <label for="inputEmail3" class="col-sm-3 col-form-label">Tipe</label>
				    <div class="col-sm-3">
				      <select class="form-control" name="tipe" required="required">
				      	<option value="">Pilih</option>
				      	<option value="Izin">Cuti</option>
				      	<option value="Sakit">Sakit</option>
				      	<option value="Dinas">Dinas</option>
				      </select>
				    </div>
				</div>

				<div class="form-group row">
				    <label for="inputEmail3" class="col-sm-3 col-form-label">Tanggal Mulai</label>
				    <div class="col-md-9">
				      <input autocomplete="off" id="start" type="text" required="required" class="form-control date-picker" id="inputEmail3"  name="start" placeholder="Tanggal Mulai...">
				    </div>
				</div>

				<div class="form-group row">
				    <label for="inputEmail3" class="col-sm-3 col-form-label">Tanggal Selesai</label>
				    <div class="col-md-9">
				       <input autocomplete="off" id="end" type="text" required="required" class="form-control date-picker" id="inputEmail3"  name="end" placeholder="Tanggal Selesai...">
				    </div>
				</div>

				<div class="form-group row">
				    <label for="inputEmail3" class="col-sm-3 col-form-label">Keterangan</label>
				    <div class="col-sm-9">
				      <textarea autocomplete="off" type="text" required="required" class="form-control" id="inputEmail3"  name="keterangan" placeholder="Keterangan..."></textarea>
				    </div>
				</div>

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>


 <script type="text/javascript">
    $(function() {
        $('.date-picker').datepicker({
          dateFormat : "yy-mm-dd"
        });
    });
</script>


