<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">PENGGAJIAN</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Gaji</a></li>
                    <li class="breadcrumb-item"><a href="#">Daftar Penggajian</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Detail Penggajian</h5>
                </div>

                <div class="card-body">
                    
                    <a class="btn btn-outline-primary" href="<?php echo site_url('karyawan/gaji') ?>"><i class="fa fa-chevron-left"></i> KEMBALI</a>

					<br><br>

                    <table class="table">
						<tr>
							<th class="bg-primary" style="width: 20%;" class="">WAKTU</th>
							<td><?php echo get_monthname($gaji['bulan'])." ".$gaji['tahun'] ?></td>
						</tr>
						<tr>
							<th class="bg-primary">TOTAL</th>
							<td>Rp. <?php echo number_format($gaji['total_gaji'],0,'','.') ?></td>
						</tr>
					</table>

					<br><br>

					<table class="table">
						<thead>
							<tr>
								<th colspan="2" class="text-center">KEHADIRAN</th>
								<th colspan="2" class="text-center">KOMPONEN GAJI</th>
								<th rowspan="2" class="text-center">TOTAL GAJI</th>
								<th rowspan="2" class="text-center"><i class="fa fa-cog"></i></th>
							</tr>
							<tr>
								<th class="text-center">H</th>
								<th class="text-center">T</th>
								<th align="center"><center>Gaji</center></th>
								<th>Lembur</th>
							</tr>
						</thead>
						<tbody>
						<?php $n=0; foreach ($list as $row) { $n++;?>
							<tr>
								<td class="text-center"><?php echo $row['total_hadir'] ?></td>
								<td class="text-center"><?php echo $row['total_terlambat'] ?></td>
								<td style="text-align: right">Rp. <?php echo number_format($row['gaji_pokok'],0,'','.') ?></td>
								<td style="text-align: right">Rp. <?php echo number_format($row['tunjangan_lembur'],0,'','.') ?></td>
								<td style="text-align: right">Rp. <?php echo number_format($row['total_gaji'],0,'','.') ?></td>
								<td class="text-center">
									<a class='text-info' target="_blank" href="<?php echo site_url('penggajian/gaji/daftar/detail/'.$gaji['gaji_id'].'/cetak/'.$row['pegawai_id']) ?>" data-toggle="tooltip" title="Cetak Slip aji"><i class='fa fa-print'></i></a>
								</td>
							</tr>
						<?php } ?>
						</tbody>
					</table>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->
