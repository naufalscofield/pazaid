
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">PENGGAJIAN</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Gaji</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Daftar</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Gaji</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12" id="status">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							<form method="GET" id="formGaji">
								<div class="form-group">
									<label class="control-label"><b>Tahun</b></label>
									<select id="tahun" class="form-control" name="tahun">
										<?php for ($i=2019; $i <= date('Y'); $i++) {  ?>
													<option <?php if($tahun == $i){ echo "selected='selected'"; } ?> value="<?php echo $i ?>"><?php echo $i ?></option>
										<?php } ?>
									</select>
								</div>
								
							</form>
						</div>
					</div>

					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr style="background-color: #eee">
									<th style="width:15%">BULAN</th>
									<th style="width:10%">TAHUN</th>
									<th style="text-align: center">TOTAL</th>
									<th style="width: 20%" class="text-center"><i class="fa fa-cog"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php $n = 0; $total_gaji = 0;
									  foreach($gaji as $row){ $n++; 

									  	if($row['total_gaji'] == 0){
								  			$total_gaji 	= $row['next']['total_gaji'];
								  			$total_pegawai = $row['next']['total_pegawai'];

									  	}else{
									  		$total_gaji 	= $row['total_gaji'];
									  		$total_pegawai = $row['total_pegawai'];
									  	}

									  	if($row['id_gaji'] != ''){ 
								?>
										<tr>
											<td><?php echo get_monthname($row['bulan']) ?></td>
											<td><?php echo $row['tahun'] ?></td>
											<td style="text-align: right"><?php echo format_rp($total_gaji) ?></td>
											<td class="text-center">
												<a class='btn btn-primary btn-sm shadow' href="<?php echo site_url('karyawan/gaji/detail/'.$row['id_gaji']) ?>" data-toggle="tooltip" title="Lihat Detail"><i class='fa fa-search'></i></a>
											</td>
										</tr>
								<?php 
										}

										$total_pegawai = $row['total_pegawai'];
										$total_gaji 	= $row['total_gaji'];
									} 
								?>
							</tbody>
						</table>
					</div>
                </div>
            </div>
        </div>
    </div>
    
</main>


<script>

	$(document).on('change','#tahun', function(){
		$('#formGaji').submit();
	})

	$(document).on('click','.generateGaji',function(){

		var bulan = $('#bulan').val();
		var tahun = <?php echo date('Y') ?>;

		var r = confirm("Apakah anda yakin ingin melakukan penggajian bulan "+filter_month(bulan)+" "+tahun+" ?");
		if (r == true) {
		  $.ajax({
				url : "<?php echo site_url('generate_gaji') ?>",
				method : "POST",
				dataType : "json",
				data : {
					bulan : bulan,
					tahun : tahun
				},
				beforeSend : function(){
					$(this).html('<i class="fa fa-spinner fa-spin"></i> Proses...').attr('disabled','disabled');
				},
				success : function(res){
					if(res.status == 'success'){
						$(this).remove();
						alert('Gaji berhasil digenerate');
						location.reload();
					}else{
						$('#status').html(res.message);
					}
					
				},
				complete : function(){
					$(this).html('GENERATE GAJI').removeAttr('disabled');
				}
			})
		}
	});

	function filter_month(month){
		if(month == 1){
			month = "Januari";
		}else if(month == 2){
			month = "Februari";
		}else if(month == 3){
			month = "Maret";
		}else if(month == 4){
			month = "April";
		}else if(month == 5){
			month = "Mei";
		}else if(month == 6){
			month = "Juni";
		}else if(month == 7){
			month = "Juli";
		}else if(month == 8){
			month = "Agustus";
		}else if(month == 9){
			month = "September";
		}else if(month == 10){
			month = "Oktober";
		}else if(month == 11){
			month = "November";
		}else if(month == 12){
			month = "Desember";
		}

		return month;
	}
</script>