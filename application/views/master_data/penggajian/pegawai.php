<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">PENGGAJIAN</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Pegawai</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Pegawai</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
                      <div class="col-md-12">
                        <?php echo $this->session->flashdata('alert_message') ?>
                      </div>
                    </div>

                    <button data-toggle="modal" data-target="#modalTambahKTG" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah Pegawai</button>
                            <br><br>

                    <div class="table-responsive">
                      <table class="display table table-hover datatables" >
                        <thead class="bg-primary">
                          <tr>
                            <th style="width: 5%">No</th>
                            <th>Kode</th>
                            <th>RFID</th>
                            <th>Nama Pegawai</th>
                            <th>Jenis Kelamin</th>
                            <th>Jabatan</th>
                            <th>Alamat / No Telp</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $n = 0;
                                        foreach ($list as $row) { $n++; ?>

                                          <tr>
                                            <td><?php echo $n ?></td>
                                            <td><?php echo $row['kode_pegawai'] ?></td>
                                            <td><?php echo $row['rfid'] ?></td>
                                            <td><?php echo $row['nama_pegawai'] ?></td>
                                            <td><?php echo $row['jenis_kelamin'] ?></td>
                                            <td><?php echo $row['kode_jabatan']." / ".$row['nama_jabatan'] ?></td>
                                            <td><?php echo $row['alamat'] ?> / <?php echo $row['no_telp'] ?></td>

                                            <td class="text-center">
                                                <a href="javascript:void(0)" data-toggle="tooltip" title="Ubah" class="btn btn-warning btn-sm"
                                                        onclick="
                                                          edit(
                                                            '<?php echo $row['pegawai_id'] ?>',
                                                            '<?php echo $row['jabatan_id'] ?>',
                                                            '<?php echo $row['rfid'] ?>',
                                                            '<?php echo $row['kode_pegawai'] ?>',
                                                            '<?php echo $row['nama_pegawai'] ?>',
                                                            '<?php echo $row['alamat'] ?>',
                                                            '<?php echo $row['jenis_kelamin'] ?>',
                                                            '<?php echo $row['no_telp'] ?>',
                                                            '<?php echo $row['username'] ?>',
                                                            '<?php echo $row['password'] ?>',
                                                            '<?php echo $row['tempat_lahir'] ?>',
                                                            '<?php echo $row['tanggal_lahir'] ?>',
                                                            '<?php echo $row['pendidikan_terakhir'] ?>',
                                                            '<?php echo $row['jenis'] ?>',
                                                            '<?php echo $row['tanggal_mulai_kerja'] ?>',
                                                            '<?php echo $row['is_nikah'] ?>',
                                                            '<?php echo $row['jml_anak'] ?>'
                                                          )">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                &nbsp;
                                                <a onclick="return confirm('Hapus data ini ?')" href="<?php echo site_url('delete_pegawai/'.$row['pegawai_id']) ?>" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm">
                                                  <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                          </tr>

                                  <?php } ?>
                        </tbody>
                      </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->

<form action="<?php echo site_url('insert_pegawai') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalTambahKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
           <div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Pegawai</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <div class="modal-body">
                <div class="row">
                  <div class="form-group col-md-3">
                     <label>Kode Pegawai</label>
                     <input type="text" name="kode_pegawai" id="kode" class="form-control" placeholder="Kode Aktiva..." value="<?php echo $last_code;?>" readonly="" autocomplete="off" required />
                  </div>

                  <div class="form-group col-md-6">
                     <label>Nama Pegawai</label>
                     <input autocomplete="off" type="text" name="nama_pegawai" id="nama" class="form-control" placeholder="Nama Pegawai..." required/>
                  </div>

                  <div class="form-group col-md-3">
                     <label>Jenis Kelamin</label>
                     <select class="form-control" name="jenis_kelamin" required="">
                      <option value="">Pilih</option>
                      <option value="Laki - Laki">Laki - Laki</option>
                      <option value="Perempuan">Perempuan</option>
                     </select>
                  </div>

                  <div class="form-group col-md-3">
                     <label>Jabatan</label>
                     <select class="form-control" name="jabatan_id" required="">
                      <option value="">Pilih</option>
                      <?php foreach ($jabatan as $row) { ?>
                          <option value="<?php echo $row['id'] ?>"><?php echo $row['nama_jabatan'] ?></option>
                      <?php } ?>
                     </select>
                  </div>

                   <div class="form-group col-md-6">
                     <label>Alamat</label>
                     <input autocomplete="off" type="text" name="alamat" id="nama" class="form-control" placeholder="Alamat..." required/>
                  </div>

                  <div class="form-group col-md-3">
                     <label>No Telepon</label>
                     <input autocomplete="off" type="text" name="no_telp" id="nama" class="form-control" placeholder="No Telepon..." required/>
                  </div>

                  <div class="form-group col-md-3">
                     <label>Tempat Lahir</label>
                     <input autocomplete="off" type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" placeholder="Tempat Lahir..." required/>
                  </div>

                  <div class="form-group col-md-3">
                     <label>Tanggal Lahir</label>
                     <input autocomplete="off" type="text" name="tanggal_lahir" id="tgl_lahir" class="form-control datepicker" placeholder="Username..." required/>
                  </div>

                  <div class="form-group col-md-3">
                     <label>Pendidikan Terakhir</label>
                     <input autocomplete="off" type="text" name="pendidikan_terakhir" id="tempat_lahir" class="form-control" placeholder="Pendidikan Terakhir..." required/>
                  </div>

                  <div class="form-group col-md-3">
                     <label>Status</label>
                     <select class="form-control" name="jenis" required="">
                      <option value="">Pilih</option>
                      <option value="Tetap">Tetap</option>
                      <option value="Kontrak">Kontrak</option>
                     </select>
                  </div>

                  <div class="form-group col-md-3">
                   <label>Tanggal Mulai Kerja</label>
                   <input autocomplete="off" type="text" name="tanggal_mulai_kerja" class="form-control datepicker" placeholder="Tanggal Mulai Kerja..." required/>
                  </div>

                  <div class="form-group col-md-3">
                     <label>Rfid</label>
                     <input autocomplete="off" type="text" name="rfid" id="nama" class="form-control" placeholder="Nomor Rfid..." required/>
                  </div>

                  <div class="form-group col-md-3">
                     <label>Username</label>
                     <input autocomplete="off" type="text" name="username" id="nama" class="form-control" placeholder="Username..." required/>
                  </div>

                  <div class="form-group col-md-3">
                     <label>Password</label>
                     <input autocomplete="off" type="text" name="password" id="nama" class="form-control" placeholder="Password..." required/>
                  </div>

                  <div class="form-group col-md-3">
                    <label>Sudah Menikah ?</label>
                    <select class="form-control" required="" id="is_nikah" name="is_nikah">
                      <option value="">Pilih</option>
                      <option value="1">Ya</option>
                      <option value="0">Tidak</option>
                    </select>
                  </div>

                  <div class="form-group col-md-3" id="input_anakBody">
                    <label>Jumlah Anak</label>
                    <input autocomplete="off" type="text" name="jml_anak" id="jml_anak" class="form-control" placeholder="Jumlah Anak...">
                  </div>
                </div>
             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>

   <form action="<?php echo site_url('update_pegawai') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalEditKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
           <div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Ubah Karyawan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <input type="hidden" name="id_pegawai" id="e_id">

             <div class="modal-body">
              <div class="row">
                <div class="form-group col-md-3">
                   <label>Kode Karyawan</label>
                   <input type="text" name="kode_pegawai" id="e_kode" class="form-control" placeholder="Kode Aktiva..." value="<?php echo $last_code;?>" readonly="" autocomplete="off" required />
                </div>

                <div class="form-group col-md-6">
                   <label>Nama Karyawan</label>
                   <input autocomplete="off" type="text" name="nama_pegawai" id="e_nama" class="form-control" placeholder="Nama Karyawan..." required/>
                </div>

                <div class="form-group col-md-3">
                   <label>Jenis Kelamin</label>
                   <select class="form-control" name="jenis_kelamin" required="" id="e_jk">
                    <option value="">Pilih</option>
                    <option value="Laki - Laki">Laki - Laki</option>
                    <option value="Perempuan">Perempuan</option>
                   </select>
                </div>

                <div class="form-group col-md-3">
                   <label>Jabatan</label>
                   <select class="form-control" name="jabatan_id" required="" id="e_jabatan">
                    <option value="">Pilih</option>
                    <?php foreach ($jabatan as $row) { ?>
                        <option value="<?php echo $row['id'] ?>"><?php echo $row['nama_jabatan'] ?></option>
                    <?php } ?>
                   </select>
                </div>

                 <div class="form-group col-md-6">
                   <label>Alamat</label>
                   <input autocomplete="off" type="text" name="alamat" id="e_alamat" class="form-control" placeholder="Alamat..." required/>
                </div>

                <div class="form-group col-md-3">
                   <label>No Telepon</label>
                   <input autocomplete="off" type="text" name="no_telp" id="e_telp" class="form-control" placeholder="No Telepon..." required/>
                </div>

                <div class="form-group col-md-3">
                   <label>Tempat Lahir</label>
                   <input autocomplete="off" type="text" name="tempat_lahir" id="e_tempat_lahir" class="form-control" placeholder="Tempat Lahir..." required/>
                </div>

                <div class="form-group col-md-3">
                   <label>Tanggal Lahir</label>
                   <input autocomplete="off" type="text" name="tanggal_lahir" id="e_tgl_lahir" class="form-control datepicker" placeholder="Tanggal Lahir..." required/>
                </div>

                <div class="form-group col-md-3">
                   <label>Pendidikan Terakhir</label>
                   <input autocomplete="off" type="text" name="pendidikan_terakhir" id="e_pendidikan_terakhir" class="form-control" placeholder="Pendidikan Terakhir..." required/>
                </div>

                <div class="form-group col-md-3">
                   <label>Status</label>
                   <select class="form-control" name="jenis" required="" id="e_jenis">
                    <option value="">Pilih</option>
                    <option value="Tetap">Tetap</option>
                    <option value="Kontrak">Kontrak</option>
                   </select>
                </div>

                <div class="form-group col-md-3">
                   <label>Tanggal Mulai Kerja</label>
                   <input autocomplete="off" type="text" name="tanggal_mulai_kerja" id="e_tgl_kerja" class="form-control datepicker" placeholder="Tanggal Mulai Kerja..." required/>
                </div>


                <div class="form-group col-md-3">
                   <label>Rfid</label>
                   <input autocomplete="off" type="text" name="rfid" id="e_rfid" class="form-control" placeholder="Nomor Rfid..." required/>
                </div>

                <div class="form-group col-md-3">
                   <label>Username</label>
                   <input autocomplete="off" type="text" name="username" id="e_username" class="form-control" placeholder="Username..." required/>
                </div>

                <div class="form-group col-md-3">
                   <label>Password</label>
                   <input autocomplete="off" type="text" name="password" id="e_password" class="form-control" placeholder="Password..." required/>
                </div>

                <div class="form-group col-md-3">
                    <label>Sudah Menikah ?</label>
                    <select class="form-control" required="" id="e_is_nikah" name="is_nikah">
                      <option value="">Pilih</option>
                      <option value="1">Ya</option>
                      <option value="0">Tidak</option>
                    </select>
                  </div>

                  <div class="form-group col-md-3" id="update_anakBody">
                    <label>Jumlah Anak</label>
                    <input autocomplete="off" type="text" name="jml_anak" id="e_jml_anak" class="form-control" placeholder="Jumlah Anak...">
                  </div>

              </div>
             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i> Ubah</button>
             </div>

           </div>
        </div>
     </div>
   </form>

<script type="text/javascript">

  $('#input_anakBody').hide();

 function edit(id, jabatan_id, rfid, kode, nama, alamat, jenis_kelamin, no_telp, username, password, t_lahir, tgl_lahir, pendidikan_terakhir, jenis, tgl_kerja, is_nikah, jml_anak){
  $('#e_id').val(id);
  $('#e_kode').val(kode);
  $('#e_nama').val(nama);
  $('#e_alamat').val(alamat);
  $('#e_rfid').val(rfid);
  $('#e_telp').val(no_telp);
  $('#e_username').val(username);
  $('#e_password').val(password);

  $('#e_tempat_lahir').val(t_lahir);
  $('#e_tgl_lahir').val(tgl_lahir);
  $('#e_pendidikan_terakhir').val(pendidikan_terakhir);
  $('#e_tgl_kerja').val(tgl_kerja);

  $('#e_jk option').removeAttr('selected')
  $('#e_jk option[value="'+jenis_kelamin+'"]').attr('selected', 'selected');

  $('#e_jabatan option').removeAttr('selected')
  $('#e_jabatan option[value="'+jabatan_id+'"]').attr('selected', 'selected');

  $('#e_jenis option').removeAttr('selected')
  $('#e_jenis option[value="'+jenis+'"]').attr('selected', 'selected');

  $('#e_is_nikah option').removeAttr('selected')
  $('#e_is_nikah option[value="'+is_nikah+'"]').attr('selected', 'selected');

  if(is_nikah == '1'){
    $('#update_anakBody').show();
    $('#e_jml_anak').val(jml_anak);
  }else{
    $('#update_anakBody').hide();
    $('#e_jml_anak').val('');
  }
  
  $('#modalEditKTG').modal('show'); 
}

$(document).on('change', '#is_nikah', function(){
  if($(this).val() == '1'){
    $('#input_anakBody').show();
  }else{
    $('#input_anakBody').hide();
  }
})

$(document).on('change', '#e_is_nikah', function(){
  if($(this).val() == '1'){
    $('#update_anakBody').show();
  }else{
    $('#update_anakBody').hide();
  }
})
</script>