<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">COA</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Coa</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar COA</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
                      <div class="col-md-12">
                        <?php echo $this->session->flashdata('alert_message') ?>
                      </div>
                    </div>

                    <button data-toggle="modal" data-target="#modalTambahAKT" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah COA</button>
                            <br><br>

                    <div class="table-responsive">
                      <table class="display table table-hover datatable">
                        <thead class="bg-primary">
                          <tr>
                            <th style="width: 5%">No</th>
                            <th>Kode</th>
                            <th>Nama COA</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $n = 0;
                                        foreach ($list as $row) { $n++; ?>

                                          <tr>
                                            <td><?php echo $n ?></td>
                                            <td style="width: 15%"><?php echo $row['kode_coa'] ?></td>
                                            <td><?php echo $row['nama_coa'] ?></td>

                                            <td style="width: 15%" class="text-center">
                                                <a href="javascript:void(0)" data-toggle="tooltip" title="Ubah" class="text-warning"
                                                        onclick="
                                                          edit(
                                                            '<?php echo $row['id'] ?>',
                                                            '<?php echo $row['kode_coa'] ?>',
                                                            '<?php echo $row['nama_coa'] ?>'
                                                          )">
                                                    <i class="fa fa-edit"></i>
                                                </a>

                                                &nbsp;

                                                <a onclick="return confirm('Hapus data ini ?')" href="<?php echo site_url('delete_coa/'.$row['id']) ?>" data-toggle="tooltip" title="Hapus" class="text-danger">
                                                  <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                          </tr>

                                  <?php } ?>
                        </tbody>
                      </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->


<form action="<?php echo site_url('insert_coa') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalTambahAKT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah COA</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">x</button>
                
                
             </div>

             <div class="modal-body">
                <div class="form-group">
                   <label>Kode COA</label>
                   <input type="text" name="kode_coa" id="kode" class="form-control" placeholder="Kode COA" autocomplete="off" required />
                </div>

                <div class="form-group">
                   <label>Nama COA</label>
                   <input autocomplete="off" type="text" name="nama_coa" id="nama" class="form-control" placeholder="Nama COA..." required/>
                </div>

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>

   <form action="<?php echo site_url('update_coa') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalEditAKT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header bg-primary">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Ubah COA</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">x</button>
                
             </div>

             <input type="hidden" name="id_coa" id="e_id">

             <div class="modal-body">
                <div class="form-group">
                   <label>Kode COA</label>
                   <input type="text" name="kode_coa" id="e_kode" class="form-control" placeholder="Kode COA" autocomplete="off" required />
                </div>

                <div class="form-group">
                   <label>Nama COA</label>
                   <input autocomplete="off" type="text" name="nama_coa" id="e_nama" class="form-control" placeholder="Nama COA..." required/>
                </div>

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i> Ubah</button>
             </div>

           </div>
        </div>
     </div>
   </form>

<script type="text/javascript">
 function edit(id, kode, nama, id_kategori){
  $('#e_id').val(id);
  $('#e_kode').val(kode);
  $('#e_nama').val(nama);

  $('#modalEditAKT').modal('show'); 
}
</script>