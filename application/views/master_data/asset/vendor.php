<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">ASET</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Vendor</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Vendor</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
                      <div class="col-md-12">
                        <?php echo $this->session->flashdata('alert_message') ?>
                      </div>
                    </div>

                    <button data-toggle="modal" data-target="#modalTambahKTG" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah Vendor</button>
                            <br><br>

                    <div class="table-responsive">
                      <table class="display table table-hover datatables" >
                        <thead class="bg-primary">
                          <tr>
                            <th style="width: 5%">No</th>
                            <th>Kode</th>
                            <th>Username</th>
                            <th style="width: 25%">Nama</th>
                            <th style="width: 25%">Alamat</th>
                            <th>No Telp</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $n = 0;
                                        foreach ($list as $row) { $n++; ?>

                                          <tr>
                                            <td><?php echo $n ?></td>
                                            <td><?php echo $row['kode_vendor'] ?></td>
                                            <td><?php echo $row['username'] ?></td>
                                            <td><?php echo $row['nama_vendor'] ?></td>
                                            <td>
                                              <?php echo $row['alamat'] ?>
                                            </td>
                                            <td><?php echo $row['no_telp'] ?></td>

                                            <td class="text-center">
                                                <a href="javascript:void(0)" data-toggle="tooltip" title="Ubah" class="btn btn-warning btn-sm"
                                                        onclick="
                                                          edit(
                                                            '<?php echo $row['id'] ?>',
                                                            '<?php echo $row['kode_vendor'] ?>',
                                                            '<?php echo $row['nama_vendor'] ?>',
                                                            '<?php echo $row['no_telp'] ?>',
                                                            '<?php echo $row['alamat'] ?>',
                                                            '<?php echo $row['username'] ?>',
                                                            '<?php echo $row['password'] ?>'
                                                          )">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                &nbsp;
                                                <a onclick="return confirm('Hapus data ini ?')" href="<?php echo site_url('delete_vendor/'.$row['id']) ?>" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm">
                                                  <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                          </tr>

                                  <?php } ?>
                        </tbody>
                      </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->

<form action="<?php echo site_url('insert_vendor') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalTambahKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Vendor</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <div class="modal-body">
                <div class="form-group">
                   <label>Kode Vendor</label>
                   <input type="text" name="kode_vendor" id="kode_vendor" class="form-control" placeholder="Kode Vendor..." value="<?php echo $last_code;?>" readonly="" autocomplete="off" required />
                </div>

                <div class="form-group">
                   <label>Nama Vendor</label>
                   <input autocomplete="off" type="text" name="nama_vendor" id="nama_vendor" class="form-control" placeholder="Nama Vendor..." required/>
                </div>

                <div class="form-group">
                   <label>Alamat</label>
                   <input autocomplete="off" type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat..." required/>
                </div>

                <div class="form-group">
                   <label>No. Telp</label>
                   <input autocomplete="off" type="text" name="no_telp" id="no_telp" class="form-control" placeholder="No Telepon..." required/>
                </div>

                <div class="form-group">
                   <label>Username</label>
                   <input autocomplete="off" type="text" name="username" id="username" class="form-control" placeholder="Username..." required/>
                </div>

                <div class="form-group">
                   <label>Password</label>
                   <input autocomplete="off" type="password" name="password" id="password" class="form-control" placeholder="Password..." required/>
                </div>

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>

   <form action="<?php echo site_url('update_vendor') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalEditKTG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Ubah Vendor</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <input type="hidden" name="id_vendor" id="e_id">

             <div class="modal-body">
                <div class="form-group">
                   <label>Kode Supplier</label>
                   <input type="text" name="kode_vendor" id="e_kode" class="form-control" placeholder="Kode Supplier" value="<?php echo $last_code;?>" readonly="" autocomplete="off" required />
                </div>

                <div class="form-group">
                   <label>Nama Supplier</label>
                   <input autocomplete="off" type="text" name="nama_vendor" id="e_nama" class="form-control" placeholder="Nama Vendor..." required/>
                </div>

                <div class="form-group">
                   <label>Alamat</label>
                   <input autocomplete="off" type="text" name="alamat" id="e_alamat" class="form-control" placeholder="Alamat..." required/>
                </div>

                <div class="form-group">
                   <label>No. Telp</label>
                   <input autocomplete="off" type="text" name="no_telp" id="e_telp" class="form-control" placeholder="No Telepon..." required/>
                </div>

                <div class="form-group">
                   <label>Username</label>
                   <input autocomplete="off" type="text" name="username" id="e_username" class="form-control" placeholder="Username..." required/>
                </div>

                <div class="form-group">
                   <label>Password</label>
                   <input autocomplete="off" type="text" name="password" id="e_password" class="form-control" placeholder="Password..." required/>
                </div>

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i> Ubah</button>
             </div>

           </div>
        </div>
     </div>
   </form>

<script type="text/javascript">
 function edit(id, kode, nama, telp, alamat, username, password){
  $('#e_id').val(id);
  $('#e_kode').val(kode);
  $('#e_nama').val(nama);

  $('#e_telp').val(telp);
  $('#e_alamat').val(alamat);
  $('#e_username').val(username);
  $('#e_password').val(password);

  $('#modalEditKTG').modal('show'); 
}
</script>