<!-- begin::header -->
<div class="header">

    <!-- begin::header logo -->
    <div class="header-logo">
        <a href="index-2.html">
            <img class="large-logo" src="<?php echo base_url() ?>assets/media/image/logo.png" alt="image">
            <img class="small-logo" src="<?php echo base_url() ?>assets/media/image/logo-sm.png" alt="image">
            <img class="dark-logo" src="<?php echo base_url() ?>assets/media/image/logo-dark.png" alt="image">
        </a>
    </div>
    <!-- end::header logo -->

    <!-- begin::header body -->
    <div class="header-body">

        <div class="header-body-left">

            <h3 class="page-title">ASET</h3>

            <!-- begin::breadcrumb -->
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Master Data</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Aset</li>
                </ol>
            </nav>
            <!-- end::breadcrumb -->

        </div>

    </div>
    <!-- end::header body -->
</div>
<!-- end::header -->

<!-- begin::main content -->
<main class="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Daftar Aset</h5>
                </div>

                <div class="card-body">
                    
                    <div class="row">
						<div class="col-md-12">
							<?php echo $this->session->flashdata('alert_message') ?>
						</div>
					</div>

					<button data-toggle="modal" data-target="#modalTambahAKT" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah Aset</button>
						<br><br>

					<div class="table-responsive">
						<table class="display table table-hover datatables" >
							<thead class="bg-primary">
								<tr>
									<th style="width: 5%">No</th>
									<th>Kode</th>
									<th>Nama Aset</th>
									<th>Kategori</th>
									<th class="text-center"><i class="fa fa-cog"></i></th>
								</tr>
							</thead>
							<tbody>
								<?php $n = 0;
		                          foreach ($list as $row) { $n++; ?>

		                            <tr>
		                              <td><?php echo $n ?></td>
		                              <td><?php echo $row['kode_aset'] ?></td>
		                              <td><?php echo $row['nama_aset'] ?></td>
		                              <td><?php echo $row['kode_kategori']." / ".$row['nama_kategori'] ?></td>

		                              <td class="text-center">
		                                  <a href="javascript:void(0)" data-toggle="tooltip" title="Ubah" class="btn btn-warning btn-sm"
		                                          onclick="
		                                            edit(
		                                              '<?php echo $row['id_aset'] ?>',
		                                              '<?php echo $row['kode_aset'] ?>',
		                                              '<?php echo $row['nama_aset'] ?>',
		                                              '<?php echo $row['id_kategori'] ?>'
		                                            )">
		                                      <i class="fa fa-edit"></i>
		                                  </a>

		                                  &nbsp;

		                                  <a href="<?php echo site_url('aset/daftar/detail/'.$row['id_aset']) ?>" data-toggle="tooltip" title="Lihat Detail" class="btn btn-info btn-sm">
		                                    <i class="fa fa-search"></i>
		                                  </a>

		                                  &nbsp;

		                                  <a onclick="return confirm('Hapus data ini ?')" href="<?php echo site_url('delete_aset/'.$row['id_aset']) ?>" data-toggle="tooltip" title="Hapus" class="btn btn-danger btn-sm">
		                                    <i class="fa fa-trash"></i>
		                                  </a>
		                              </td>
		                            </tr>

		                    <?php } ?>
							</tbody>
						</table>
					</div>

                </div>
            </div>
        </div>
    </div>
    
</main>
<!-- end::main content -->

<form action="<?php echo site_url('insert_aset') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalTambahAKT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Aset</h4>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <div class="modal-body">
                <div class="form-group">
                   <label>Kode Aset</label>
                   <input type="text" name="kode_aset" id="kode" class="form-control" placeholder="Kode Jabatan" value="<?php echo $last_code;?>" readonly="" autocomplete="off" required />
                </div>

                <div class="form-group">
                   <label>Nama Aset</label>
                   <input autocomplete="off" type="text" name="nama_aset" id="nama" class="form-control" placeholder="Nama Aset..." required/>
                </div>

                <div class="form-group">
                   <label>Kategori</label>
                   <select class="form-control" required="" name="kategori_id">
                   		<option value="">Pilih</option>
                   		<?php foreach ($kategori as $row){ ?>
                   					<option value="<?php echo $row['id'] ?>"><?php echo $row['nama_kategori'] ?></option>
                   		<?php } ?>
                   </select>
                </div>

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i> Simpan</button>
             </div>

           </div>
        </div>
     </div>
   </form>

   <form action="<?php echo site_url('update_aset') ?>" method="post" enctype="multipart/form-data">
     <div class="modal fade" id="modalEditAKT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i> Ubah Aset</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>

             <input type="hidden" name="id_aset" id="e_id">

             <div class="modal-body">
                <div class="form-group">
                   <label>Kode Aset</label>
                   <input type="text" name="kode_aset" id="e_kode" class="form-control" placeholder="Kode Aset" value="<?php echo $last_code;?>" readonly="" autocomplete="off" required />
                </div>

                <div class="form-group">
                   <label>Nama Aset</label>
                   <input autocomplete="off" type="text" name="nama_aset" id="e_nama" class="form-control" placeholder="Nama Aset..." required/>
                </div>

                <div class="form-group">
                   <label>Kategori</label>
                   <select class="form-control" required="" name="kategori_id" id="kategori">
                   		<option value="">Pilih</option>
                   		<?php foreach ($kategori as $row){ ?>
                   					<option value="<?php echo $row['id'] ?>"><?php echo $row['nama_kategori'] ?></option>
                   		<?php } ?>
                   </select>
                </div>

             </div>

             <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i> Ubah</button>
             </div>

           </div>
        </div>
     </div>
   </form>

<script type="text/javascript">
 function edit(id, kode, nama, id_kategori){
  $('#e_id').val(id);
  $('#e_kode').val(kode);
  $('#e_nama').val(nama);

  $('#kategori option').removeAttr('selected');
  $('#kategori option[value="'+id_kategori+'"]').attr('selected','selected');

  $('#modalEditAKT').modal('show'); 
}
</script>
